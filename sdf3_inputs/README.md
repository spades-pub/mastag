# Input examples in SDF3 format

Input examples are all SDF application graphs.
SDF stands for *Synchronous Data Flow*, see
E.A. Lee and D.G. Messerschmitt, 1987.

SDF3 format comes from the university of Eindhoven:
- [main page](https://www.es.ele.tue.nl/sdf3/)
- [format doc](https://www.es.ele.tue.nl/sdf3/manuals/xml/)
We redistribute in this folder a few examples from
their group. Here is a citation for their main tool:
```
@INPROCEEDINGS{1640245,
  author={Stuijk, S. and Geilen, M. and Basten, T.},
  booktitle={Sixth International Conference on Application of Concurrency to System Design (ACSD'06)}, 
  title={SDF^3: SDF For Free}, 
  year={2006},
  volume={},
  number={},
  pages={276-278},
  doi={10.1109/ACSD.2006.23}}
```

Other files come from the PhD of Adnan Boukaz,
and can be retrieved in the repository of the ADFG tool:
https://gitlab.inria.fr/ADFG/adfg

The applications abstracted by dataflow graphs in the examples
come from various sources listed in publications of
the SDF3 authors and of ADFG authors.
Multiple applications come from the StreamIt benchmark.


