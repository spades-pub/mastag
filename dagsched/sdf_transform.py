#! /usr/bin/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from .dag_analysis import *
from .dag_transform import *
from .dag_mem_min_unicore_algo import *

import networkx
# from itertools import pairwise
from sympy import divisors
from sortedcontainers import SortedDict

import bracelogger
logger = bracelogger.get_logger("dagsched_log")


def copy_graph_sdf(dag):
    """Deep copy of the graph with only SDF minimal info.

    :param dag: Graph to copy.
    :returns: A copy of the input dag with node attributes
    'name' and edge attributes 'rateSrc' and 'rateDst'
    (and 'delay' only if already present).
    """
    dag_copy = networkx.DiGraph()
    for src,dst,dicoEdge in dag.edges(data=True):
        dicoNewEdge = dict()
        dicoNewEdge['rateSrc'] = dicoEdge['rateSrc']
        dicoNewEdge['rateDst'] = dicoEdge['rateDst']
        if dicoEdge.get('delay', None):
            dicoNewEdge['delay'] = dicoEdge['delay']
        dag_copy.add_edge(src, dst, **dicoNewEdge)
    for node, dicoNode in dag.nodes(data=True):
        dicoNewNode = dag_copy.nodes[node]
        dicoNewNode['name'] = dicoNode['name']
    return dag_copy


def set_actor_cost(dagSDF):
    """Update the SDF DAG having rates with 
    relative actor memory increase (may be negative).
    Edge attributes 'rateSrc' and 'rateDst'
    must be set before, and
    node attributes 'actorMemCost' will be set

    :param dagSDF: SDF DAG to update (in place).
    """
    for node, dicoNode in dagSDF.nodes(data=True):
        inMemWeight = 0
        for _, dicoEdge in dagSDF.pred[node].items():
            inMemWeight += dicoEdge['rateDst']
        outMemWeight = 0
        for _, dicoEdge in dagSDF.succ[node].items():
            outMemWeight += dicoEdge['rateSrc']
        dicoNode['actorMemCost'] = outMemWeight - inMemWeight


def set_sdf_tmpMemInc(dag):
    """Set 'tmpMemInc' node attribute to emulate PBC model in CBP model
    on an SDF graph: only output actor rates are considered.

    :param dag: DAG with attribute 'rateSrc' set on edges.
    """
    for node, dicoNode in dag.nodes(data=True):
        pbcMemInc = 0
        for succ, dicoEdge in dag.succ[node].items():
            pbcMemInc += dicoEdge['rateSrc']
        dicoNode['tmpMemInc'] = pbcMemInc

        
def set_peg_tmpMemInc(dagPEG, dagSDF):
    """Set 'tmpMemInc' node attribute in PEG model:
    PEG node peak is set as its SDF original node peak.

    :param dagPEG: DAG where to set node attribute "tmpMemInc",
    node attrubutes "sdfNode" must be already set.
    :param dagSDF: DAG where to copy the node attribute "tmpMemInc"
    from, node attributes "nodeMemCost", "divisors" and "divisorIdx"
    must be also set.
    """
    for node, dicoNodePEG in dagPEG.nodes(data=True):
        dicoNodeSDF = dagSDF.nodes[dicoNodePEG['sdfNode']]
        tmpMemInc = dicoNodeSDF['tmpMemInc']
        actorMemCost = dicoNodeSDF['actorMemCost']
        if actorMemCost > 0:
            divisor = dicoNodeSDF['divisors'][dicoNodeSDF['divisorIdx']]
            tmpMemInc += (divisor-1) * actorMemCost
        dicoNodePEG['tmpMemInc'] = max(0, tmpMemInc,
                                       dicoNodePEG['nodeMemCost'])


def setSDFrepetitionVector(dag):
    """Compute and set repetition vector of an SDF graph.

    :raise ValueError: If the graph is not consistent.
    :param dag: SDF graph to analyse (in place).
    Will set the node attribute 'nbRepeat'.
    :returns: Total number of firings.
    """
    actorFractions = {x: Fraction() for x in dag.nodes()}    
    if len(actorFractions) == 0:
        return
    basis = next(iter(dag.nodes()))
    actorFractions[basis] = Fraction(1,1)
    actorsToVisit = [basis]
    for node in actorsToVisit:
        fractionNode = actorFractions[node]
        for pred in dag.pred[node]:
            dicoEdge = dag[pred][node]
            fractionEdge = Fraction(dicoEdge['rateDst'], dicoEdge['rateSrc'])
            fractionNew = fractionEdge * fractionNode 
            if actorFractions[pred].numerator == 0:
                actorFractions[pred] = fractionNew
                actorsToVisit.append(pred)
            elif actorFractions[pred] != fractionNew:
                raise ValueError("Inconsistent graph")
        for succ in dag.succ[node]:
            dicoEdge = dag[node][succ]
            fractionEdge = Fraction(dicoEdge['rateSrc'], dicoEdge['rateDst'])
            fractionNew = fractionEdge * fractionNode 
            if actorFractions[succ].numerator == 0:
                actorFractions[succ] = fractionNew
                actorsToVisit.append(succ)
            elif actorFractions[succ] != fractionNew:
                raise ValueError("Inconsistent graph")
    lcmDenom = 1
    for frac in actorFractions.values():
        lcmDenom = lcm(lcmDenom, frac.denominator)
    totalFirings = 0
    for node, dicoNode in dag.nodes(data=True):
        frac = actorFractions[node]
        nbRepeat = (lcmDenom // frac.denominator) * frac.numerator
        dicoNode['nbRepeat'] = nbRepeat
        totalFirings += nbRepeat
        logger.debug("Node {} is repeated {} times.",
                     dicoNode['name'], nbRepeat)
    logger.info("Total firings: {}", totalFirings)
    return totalFirings

        
def convert_sdf_to_srsdf(dagSDF, autoconcurrency=False):
    """Expand an SDF graph to a task graph.

    :raise ValueError: If the graph is not consistent.
    :param dagSDF: SDF graph to expand, with node attribute
    'name' and 'nbRepeat' already set,
    and edge attributes 'rateSrc', 'rateDst' and
    'delay' already set.
    :param autoconcurrency: If True, adds depdencies
    limiting autoconcurrency. /!\ Not tested with self-loops.
    :returns: Expanded graph with edge attribute
    'edgeMem' set. May have two extra nodes
    """
    if len(dagSDF.nodes()) == 0:
        return networkx.DiGraph()
    # generate offsets for node indexes in new dag
    iteratorNodesR = iter(dagSDF.nodes(data='nbRepeat'))
    dicoOffset = dict()
    firstNode, firstR = next(iteratorNodesR)
    dicoOffset[firstNode] = 0
    prevOffset = firstR
    while True:
        try:
            currNode, currR = next(iteratorNodesR)
            dicoOffset[currNode] = prevOffset
            prevOffset += currR
        except StopIteration:
            break
    indexSourceDelays = prevOffset
    indexSinkDelays = prevOffset+1
    exp_dag = networkx.DiGraph()
    # fill the graph
    for src, dst, dicoEdge in dagSDF.edges(data=True):
        srcR = dagSDF.nodes[src]['nbRepeat']
        dstR = dagSDF.nodes[dst]['nbRepeat']
        rateSrc = dicoEdge['rateSrc']
        rateDst = dicoEdge['rateDst']
        delay = dicoEdge.get('delay', 0)
        tokenLeft = min(delay, rateSrc*srcR)
        offsetSrc = dicoOffset[src]
        offsetDst = dicoOffset[dst]
        # non auto-concurrency delay edges
        if not autoconcurrency:
            for i in range(0, srcR-1):
                exp_dag.add_edge(offsetSrc+i, offsetSrc+i+1, edgeMem=0)
            for i in range(0, dstR-1):
                exp_dag.add_edge(offsetDst+i, offsetDst+i+1, edgeMem=0)
        # delay source to dst
        delayedFiringsDst = tokenLeft // rateDst
        dstR -= delayedFiringsDst
        for i in range(0, delayedFiringsDst):
            exp_dag.add_edge(indexSourceDelays, offsetDst+i, edgeMem=rateDst)
        tokenLeft = tokenLeft % rateDst
        offsetDst += delayedFiringsDst
        previousLastDep = indexSourceDelays if tokenLeft else None
        # regular dependencies
        while dstR > 0:
            # number of firings of src to ensure at least one firing of dst
            nextSrcR = (rateDst - tokenLeft + rateSrc - 1) // rateSrc
            if tokenLeft:
                exp_dag.add_edge(previousLastDep, offsetDst, edgeMem=tokenLeft)
            # dependencies from all src firings to the first dst firings
            for i in range(0, nextSrcR-1):
                exp_dag.add_edge(offsetSrc+i, offsetDst, edgeMem=rateSrc)
            tokenLeft += rateSrc * (nextSrcR - 1)
            lastDepMem = rateDst - tokenLeft
            exp_dag.add_edge(offsetSrc+nextSrcR-1, offsetDst, edgeMem=lastDepMem)
            tokenLeft +=  rateSrc - rateDst
            offsetSrc += nextSrcR
            offsetDst += 1
            srcR -= nextSrcR
            dstR -= 1
            previousLastDep = offsetSrc-1
            # dependencies from the last src firing to all dst firings
            nextDstR = tokenLeft // rateDst
            for i in range(0, nextDstR):
                exp_dag.add_edge(previousLastDep, offsetDst+i, edgeMem=rateDst)
            tokenLeft = tokenLeft % rateDst                
            offsetDst += nextDstR
            dstR -= nextDstR
        # src to delay sink
        if tokenLeft:
            exp_dag.add_edge(previousLastDep, indexSinkDelays, edgeMem=tokenLeft)
        for i in range(0, srcR):
            exp_dag.add_edge(offsetSrc+i, indexSinkDelays, edgeMem=rateSrc)
        # delay source to delay sink, if more than one delayed iteration
        extraDelays = delay - dagSDF.nodes[src]['nbRepeat'] * rateSrc
        if extraDelays > 0:
            if exp_dag.has_edge(indexSourceDelays, indexSinkDelays):
                exp_dag[indexSourceDelays][indexSinkDelays]['edgeMem'] += extraDelays
            else:
                exp_dag.add_edge(indexSourceDelays, indexSinkDelays, edgeMem=extraDelays)                

    # check if not a DAG
    if not networkx.is_directed_acyclic_graph(exp_dag):
        raise ValueError("SR-SDF graph is not live (not a dag).")
    # finally copy nodes info:
    for node, offset in dicoOffset.items():
        dicoNode = dagSDF.nodes[node]
        nbRepeat = dicoNode['nbRepeat']
        name = dicoNode['name']
        for i in range(0, nbRepeat):
            expDicoNode = exp_dag.nodes[offset+i]
            expDicoNode.update(dicoNode)
            expDicoNode['name'] = "{}_f{}".format(name, i+1)
            expDicoNode['sdfNode'] = node
    if exp_dag.has_node(indexSourceDelays):
        exp_dag.nodes[indexSourceDelays]['name'] = "_delaySource"
        exp_dag.nodes[indexSinkDelays]['name'] = "_delaySink"
        # connect them to all sources and sinks because
        # they must be alive all along
        firstNodes = [node for node, in_degree in exp_dag.in_degree if in_degree == 0]
        firstNodes.remove(indexSourceDelays)
        for fst in firstNodes:
            exp_dag.add_edge(indexSourceDelays, fst, edgeMem=0)
        lastNodes = [node for node, out_degree in exp_dag.out_degree if out_degree == 0]
        lastNodes.remove(indexSinkDelays)
        for lst in lastNodes:
            exp_dag.add_edge(lst, indexSinkDelays, edgeMem=0)
             
    # return new expanded dag
    return exp_dag

def set_sdf_singleTask(dagSDF):
    """Update an SDF graph to a flat SAS task graph.

    :raise ValueError: If the graph is not consistent.
    :param dagSDF: SDF graph to update (in place), with
    edge attributes 'rateSrc' and 'rateDst' and
    'nbRepeat' already set.
    Edge attribute 'delay' is ignored.
    Will set the node attribute 'nbRepeat' and the edge
    attribute 'edgeMem'.  
    """
    for src, dst, dicoEdge in dagSDF.edges(data=True):
        weightSrc = dicoEdge['rateSrc'] * dagSDF.nodes[src]['nbRepeat']
        weightDst = dicoEdge['rateDst'] * dagSDF.nodes[dst]['nbRepeat']
        if weightSrc != weightDst:
            raise ValueError("Inconsistent graph")
        dicoEdge['edgeMem'] = weightSrc
        # logger.debug("Edge weight from {} to {} has been set to {}.",
        #              dagSDF.nodes[src]['name'], dagSDF.nodes[dst]['name'], weightSrc)


def update_actor_divisor(dagSDF, node, newDivIdx):
    """ Update properties related to firings divisor.

    :param dagSDF: SDF DAG containing the node.
    :param node: Node to update.
    :param newDivIdx: New divisor (assumed in 'divisors' list).
    :param updateActorCost: If True, updates also
    the 'actorMemCost' node attribute.
    """
    dicoNode = dagSDF.nodes[node]
    divs_factor = dicoNode['divisors']
    div_cur = divs_factor[dicoNode['divisorIdx']]
    div_new = divs_factor[newDivIdx]
    dicoNode['divisorIdx'] = newDivIdx
    dicoNode['nbRepeat'] = (dicoNode['nbRepeat'] * div_cur) // div_new
    for _, dicoEdge in dagSDF.succ[node].items():
        dicoEdge['rateSrc'] = (dicoEdge['rateSrc'] // div_cur) * div_new
    for _, dicoEdge in dagSDF.pred[node].items():
        dicoEdge['rateDst'] = (dicoEdge['rateDst'] // div_cur) * div_new
    
        
def set_sdf_optimalFactor(dagSDF, nodeSubset=None):
    """Update an SDF graph with the repetition vector
    of each task being the maximal reachable value without
    modifying the global solution.

    :param dagSDF: SDF graph to update (in place), with
    edge attributes 'rateSrc' and 'rateDst' and node attributes
    'actorMemCost', 'nbRepeat', 'divisorIdx' and 'divisors' 
    already preset. Edge attribute 'delay' is ignored.
    :param nodeSubset: Nodes to test. None = All.
    :param updateActorCost: Whether or not 'actorMemCost' node
    attribute must be updated.
    :return: Updated nodes.
    """
    neighborsOfPartiallyDominated = set()
    dicoNodesToUpdate = dict()
    if nodeSubset is None:
        nodesToCheck = [node for node in dagSDF.nodes()]
    else:
        nodesToCheck = nodeSubset
    for node in nodesToCheck:
        dicoNode = dagSDF.nodes[node]
        divs_factor = dicoNode['divisors']
        div_cur = divs_factor[dicoNode['divisorIdx']]
        maxFactor = divs_factor[-1]
        if maxFactor == div_cur:
            continue
        maxFactorSucc = maxFactor
        for _, dicoEdge in dagSDF.succ[node].items():
            maxFactorNeighbor = dicoEdge['rateDst'] // (dicoEdge['rateSrc'] // div_cur)                
            maxFactorSucc = min(maxFactorSucc, maxFactorNeighbor)
        maxFactorPred = maxFactor
        for _, dicoEdge in dagSDF.pred[node].items():
            maxFactorNeighbor = dicoEdge['rateSrc'] // (dicoEdge['rateDst'] // div_cur)
            maxFactorPred = min(maxFactorPred, maxFactorNeighbor)
        if maxFactorPred < 1 or maxFactorSucc < 1 or \
           (maxFactorPred * maxFactorSucc == 1 and \
            dagSDF.pred[node] and dagSDF.succ[node]):
            continue
        # so it's possible to decrease nbRepeat of the node
        # according to neighbors ...
        if maxFactorSucc > 1 and maxFactorPred > 1:
            # in case of strict dominance on both sides
            maxFactor = min(maxFactorPred, maxFactorSucc)
        else:
            # in case of dominance on one side only and
            # equality on the other, at the extra condition
            # of a memory gain
            actorMemCost = dicoNode['actorMemCost']
            if maxFactorSucc == 1 and actorMemCost <= 0:
                maxFactor = maxFactorPred
            elif maxFactorPred == 1 and actorMemCost >= 0:
                maxFactor = maxFactorSucc
            else:
                continue
        # now see if it's possible to decrease nbRepeat
        # according to its divisors (increasing order)
        for idx, div in enumerate(divs_factor[::-1]):
            if div <= maxFactor:
                newDivIdx = len(divs_factor) - idx - 1
                break
        if div > 1 and div > div_cur:
            dicoNodesToUpdate[node] = newDivIdx
            if maxFactorSucc == 1 and actorMemCost <= 0:
                neighborsOfPartiallyDominated.update(dagSDF.succ[node])
            elif maxFactorPred == 1 and actorMemCost >= 0:
                neighborsOfPartiallyDominated.update(dagSDF.pred[node])
    # update all suboptimal nodes
    for node, newDivIdx in dicoNodesToUpdate.items():
        update_actor_divisor(dagSDF, node, newDivIdx)
    updatedNodes = set(dicoNodesToUpdate.keys())
    # recursive call on all neighbors of updated nodes
    # (useful only for non strict dominance)
    if neighborsOfPartiallyDominated:
        if nodeSubset:
            # then we check only the intersection
            neighborsToCheck = neighborsOfPartiallyDominated.intersection(nodeSubset)
        else:
            # then we check again every node
            neighborsToCheck = None
        recUpdatedNodes = set_sdf_optimalFactor(dagSDF, neighborsToCheck)
        return recUpdatedNodes.union(updatedNodes)
    return updatedNodes


def computeNodeFiringIncreaseMemScore(dagSDF, node):
    """Compute the memory score of a node if decreasing
    its repetition vector by next divisor.

    :param dagSDF: SDF DAG with node attributes
    'divisors' and 'divisorIdx' and with edge
    attributes 'rateSrc' and 'rateDst' already set.
    :param node: Node to evaluate.
    :returns: Score, the lower, the better.
    """
    dicoNode = dagSDF.nodes[node]
    div_idx = dicoNode['divisorIdx']
    divs_factor = dicoNode['divisors']
    div_cur = divs_factor[div_idx]
    if div_cur == divs_factor[-1]:
        return None
    div_next = divs_factor[div_idx + 1]
    scoreSuccNeighbors = 0
    # process succ
    for _, dicoEdge in dagSDF.succ[node].items():
        otherRate = dicoEdge['rateDst']
        currentRate = dicoEdge['rateSrc']
        currentFactorN = (otherRate + currentRate - 1) // currentRate
        newRate = ((currentRate // div_cur) * div_next) 
        newFactorN = (otherRate + newRate - 1) // newRate
        scoreSuccNeighbors += newFactorN * newRate - currentFactorN * currentRate        
    # precess pred
    scorePredNeighbors = 0
    for _, dicoEdge in dagSDF.pred[node].items():
        otherRate = dicoEdge['rateSrc']
        currentRate = dicoEdge['rateDst']
        currentFactorN = (otherRate + currentRate - 1) // otherRate
        newRate = ((currentRate // div_cur) * div_next) 
        newFactorN = (otherRate + newRate - 1) // otherRate
        scorePredNeighbors += (newFactorN - currentFactorN) * otherRate        
    # return score
    # does not work well with max or especially min
    return scorePredNeighbors+scoreSuccNeighbors


def getNodeBestScoreOnly(dagSDF):
    """Select node having best firing
    reduction score if any available.
    No tie break: first selected kept.
    Original function used for Table3 of LCTES'23.

    :param dagSDF: DAG to consider with
    :returns: Node having smallest score,
    or None if no more reduction available.
    """
    minScore = 0
    nodeMinScore = None
    for node, dicoNode in dagSDF.nodes(data=True):
        score = dicoNode['scoreFiringIncMem']
        if score is None:
            continue
        if nodeMinScore is None or minScore > score:
            minScore = score
            nodeMinScore = node
            continue

    return nodeMinScore


def getNodeBestScoreTopoTieBreak(dagSDF):
    """Select node having best firing
    reduction score if any available.
    Breaks tie with minimum toplogical rank.

    :param dagSDF: DAG to consider with
    :returns: Node having smallest score,
    or None if no more reduction available.
    """
    minScore = 0
    nodeMinScore = None
    for node, dicoNode in dagSDF.nodes(data=True):
        score = dicoNode['scoreFiringIncMem']
        if score is None:
            continue
        if nodeMinScore is None or minScore > score:
            minScore = score
            nodeMinScore = node
            continue
        elif minScore == score:
            ## breaking tie based on clustering ratio does not work well
            # ratioCur = dicoNode['divisorIdx'] / len(dicoNode['divisors'])
            # dicoBest = dagSDF.nodes[nodeMinScore]
            # ratioBest = dicoBest['divisorIdx'] / len(dicoBest['divisors'])
            # if ratioCur < ratioBest:
            #     minScore = score
            #     nodeMinScore = node
            ## the best for qmf seems to break tie with topo rank
            nodeRank = dagSDF.nodes[nodeMinScore]['minRank']
            if nodeRank > dicoNode['minRank']:
                minScore = score
                nodeMinScore = node
                ## other idea: favour nodes that are not participating
                ## to the peak (based on scheduling with heuristic)?
                
    return nodeMinScore


def reduceSDFfirings(dagSDF, nbFiringsObj, recordAllDivs=False, optimalSteps=True,
                     nodeBestScoreFunc=getNodeBestScoreOnly):
    """Reduce the number of firings per actor in an SDF graphs,
    with a heuristic to limit memory peak.

    :param dagSDF: SDF dag to convert with node attribute
    'name' and 'nbRepeat' already set, and edge attributes
    'rateSrc', 'rateDst' and 'delay' already set.
    Will set node attributes 'divisors', 'divisorIdx' and
    'scoreFiringIncMem'.
    :param nbFiringsObj: Objective for the number of firings in the
    partial extension, computed by  a heuristic. If less than the
    number of actors, then equivalent to computing flat SAS with
    set_sdf_singleTask(dagSDF) (but slower).
    :param recordAllDivs: (default False) Whether or not to record
    all intermediate partial expansions until objective.
    :param optimalSteps: If True, calls set_sdf_optimalFactor whenever
    a node is increased.
    :param nodeBestScoreFunc: Function returning the node to reduce
    if any (or None). Takes dagSDF as an argument.
    :returns: If recordAllDivs option enable: one dictionary of all
    intermediate 'divisorIdx' node attributes per total firing number
    (increasing sort). Otherwise an empty dictionary.
    """
    nbActors = len(dagSDF.nodes())
    if nbFiringsObj < nbActors:
        nbFiringsObj = nbActors
    ## in other case we try to reach the objective

    # start by computing all scores and divisors
    dicoFiringsToDiv = SortedDict()
    for node, dicoNode in dagSDF.nodes(data=True):
        dicoNode['scoreFiringIncMem'] = computeNodeFiringIncreaseMemScore(dagSDF, node)

    # compute extra info if some node selection function used
    if nodeBestScoreFunc is getNodeBestScoreTopoTieBreak:
        # set the topo ranks to break tie between scores
        maxTopoRank = topoSortASAPandALAP(dagSDF)
        for node, dicoNode in dagSDF.nodes(data=True):
            minRank = min(dicoNode['asapTopoRank'], maxTopoRank - dicoNode['alapTopoRank'])
            dicoNode['minRank'] = minRank

    
    def recDicoDivIdx(dagSDF, dicoFiringsToDiv, nbFiringsTot):
        logger.debug("-- New record.")
        dicoNodeToDivIdxs = dict()
        for node, divisorIdx in dagSDF.nodes(data='divisorIdx'):
            dicoNodeToDivIdxs[node] = divisorIdx
        dicoFiringsToDiv[nbFiringsTot] = dicoNodeToDivIdxs
    
    ## main loop
    nbIterations = 0
    nodesOpt = frozenset()
    if optimalSteps:
        nodesOpt = set_sdf_optimalFactor(dagSDF, nodeSubset=None)
        logger.debug("++ nbNodes with decreased repetition: {}", len(nodesOpt))
        logger.debug([dagSDF.nodes[node]['name'] for node in nodesOpt])
    neighborsIncreased = set(nodesOpt)
    nbFiringsTot = 0
    for node, dicoNode in dagSDF.nodes(data=True):
        nbFiringsTot += dicoNode['nbRepeat']
    if recordAllDivs:
        recDicoDivIdx(dagSDF, dicoFiringsToDiv, nbFiringsTot)
    while nbFiringsTot > nbFiringsObj:
        nbIterations += 1
        # update score for modified nodes
        for node in neighborsIncreased:
            dagSDF.nodes[node]['scoreFiringIncMem'] = \
                computeNodeFiringIncreaseMemScore(dagSDF, node)
        neighborsIncreased.clear()
        # diagonal suboptimal move, get min score and update it
        nodeMinScore = nodeBestScoreFunc(dagSDF)
        if not (nodeMinScore is None):
            dicoNode = dagSDF.nodes[nodeMinScore]
            newDivIdx = dicoNode['divisorIdx'] + 1
            # newDivIdx = len(dicoNode['divisors']) - 1
            update_actor_divisor(dagSDF, nodeMinScore, newDivIdx)
            logger.debug("++ suboptimal decrease of {} to {} firings",
                         dicoNode['name'], dicoNode['nbRepeat'])
            neighborsIncreased.add(nodeMinScore)
            for succ in dagSDF.succ[nodeMinScore]:
                neighborsIncreased.add(succ)
            for pred in dagSDF.pred[nodeMinScore]:
                neighborsIncreased.add(pred)

            # if there is a symmetric node, then we also increase it
            symNode = dicoNode.get('sdfSymNode', None)
            if symNode:
                dicoNode = dagSDF.nodes[symNode]
                divs_factor = dicoNode['divisors']
                div_idx = dicoNode['divisorIdx']
                if divs_factor[div_idx] != divs_factor[-1]:
                    newDivIdx = dicoNode['divisorIdx'] + 1
                    # newDivIdx = len(dicoNode['divisors']) - 1
                    update_actor_divisor(dagSDF, symNode, newDivIdx)
                    logger.debug("++ suboptimal decrease of {} to {} firings",
                                 dicoNode['name'], dicoNode['nbRepeat'])
                    neighborsIncreased.add(symNode)
                    for succ in dagSDF.succ[symNode]:
                        neighborsIncreased.add(succ)
                    for pred in dagSDF.pred[symNode]:
                        neighborsIncreased.add(pred)
            
        # horizontal optimal mode
        if optimalSteps:
            nodesOpt = set_sdf_optimalFactor(dagSDF, nodeSubset=None) # nodeSubset=neighborsIncreased
            neighborsIncreased.update(nodesOpt)
            logger.debug("++ nbNodes with decreased repetition: {}", len(nodesOpt))
            logger.debug([dagSDF.nodes[node]['name'] for node in nodesOpt])
        # recompute nbFiringsTot for next step
        nbFiringsTot = 0
        for node, nbRepeat in dagSDF.nodes(data='nbRepeat'):
            nbFiringsTot += nbRepeat
        if recordAllDivs:
            recDicoDivIdx(dagSDF, dicoFiringsToDiv, nbFiringsTot)
        
    logger.info("Reached nbFirings objective in {} iterations.", nbIterations)
    return dicoFiringsToDiv


def sdfToDAG(dagSDF, nbFiringsObj=0, optimalRepetition=False, autoconcurrency=False):
    """Higher function to get a SRSDF DAG from an SDF graph.

    :param dagSDF: SDF dag to convert with node attribute
    'name' already set, and edge attributes 'rateSrc',
    'rateDst' and 'delay' already set.
    :param nbFiringsObj: Objective for the number of firings in the
    extension. If 0 a new DAG is created being
    single-rate. If between 0 and number of nodes in dagSDF,
    'edgeMem' attributes are set in place (equivalent flat SAS).
    If higher, then a heuristic is used to compute partial expansion.
    :param optimalRepetition: If the extended graph
    repetition must be optimized (only if extendToSR).
    :param autoconcurrency: If True, adds depdencies
    limiting autoconcurrency. /!\ Not tested with self-loops.
    :returns: A new DAG with 'edgeMem' and 'sdfNode' attributes set.
    """
    nbFiringsTot = setSDFrepetitionVector(dagSDF)
    set_actor_cost(dagSDF) # used for some optimal moves
    for node, dicoNode in dagSDF.nodes(data=True):
        dicoNode['divisors'] = divisors(dicoNode['nbRepeat'])
        dicoNode['divisorIdx'] = 0
    if nbFiringsObj <= 0 or nbFiringsTot <= nbFiringsObj:
        if optimalRepetition:
            nodesOpt = set_sdf_optimalFactor(dagSDF)
            logger.debug("++ nbNodes with decreased repetition: {}", len(nodesOpt))
            logger.debug([dagSDF.nodes[node]['name'] for node in nodesOpt])
        return convert_sdf_to_srsdf(dagSDF, autoconcurrency)
    elif nbFiringsObj <= len(dagSDF.nodes()):
        for node, dicoNode in dagSDF.nodes(data=True):
            dicoNode['divisorIdx'] = len(dicoNode['divisors']) - 1
        dagSDF_copy = copy_graph_sdf(dagSDF)
        for node, dicoNode in dagSDF_copy.nodes(data=True):
            dicoNode['sdfNode'] = node
            dicoOriNode = dagSDF.nodes[node]
            dicoNode['nbRepeat'] = dicoOriNode['nbRepeat']
        set_sdf_singleTask(dagSDF_copy)
        return dagSDF_copy
    reduceSDFfirings(dagSDF, nbFiringsObj, recordAllDivs=False, optimalSteps=optimalRepetition)
    for node, dicoNode in dagSDF.nodes(data=True):
        logger.debug("Node {} is repeated {} times.",
                     dicoNode['name'], dicoNode['nbRepeat'])
    
    return convert_sdf_to_srsdf(dagSDF, autoconcurrency)
