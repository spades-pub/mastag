#! /usr/bin/python3

from .utils import *

import networkx
from fractions import Fraction
from collections import deque
from sortedcontainers import SortedDict

import bracelogger
logger = bracelogger.get_logger("dagsched_log")


def addSuccBFS(dag, node, queue):
    """Subroutine for BFS-like giving ASAP topological sort.

    :param dag: DAG to consider, set node attribute 'nbVisits'.
    :param node: Successor node.
    :param queue: Deque to eventually add successor node (if completely visited).
    :returns: True if node was completely visited and added to the queue.
    """
    dicoNode = dag.nodes[node]
    nbVisits = dicoNode['nbVisits'] + 1
    dicoNode['nbVisits'] = nbVisits
    if nbVisits == dag.in_degree[node]:
        queue.append(node)
        dicoNode['nbVisits'] = 0
        return True
    return False


def addPredBFS(dag, node, queue):
    """Subroutine for BFS-like giving ALAP topological sort.

    :param dag: DAG to consider, set node attribute 'nbVisits'.
    :param node: Predecessor node.
    :param queue: Deque to eventually add predecessor node (if completely visited).
    :returns: True if node was completely visited and added to the queue.
    """
    dicoNode = dag.nodes[node]
    nbVisits = dicoNode['nbVisits'] + 1
    dicoNode['nbVisits'] = nbVisits
    if nbVisits == dag.out_degree[node]:
        queue.append(node)
        dicoNode['nbVisits'] = 0
        return True
    return False


def topoSortASAPandALAP(dag):
    """Compute ASAP partial ordering (subsets per rank)

    :param dag: DAG to consider.
    Set node attributes 'asapTopoRank' and 'alapTopoRank',
    reset 'nbVisits' to 0.
    :returns: The maximum node rank (starting from 0).
    """
    ## BFS-like to compute and fill ASAP and ALAP ranks
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    lastNodes = [node for node, out_degree in dag.out_degree if out_degree == 0]
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['asapTopoRank'] = 0
        dicoNode['alapTopoRank'] = 0
        dicoNode['nbVisits'] = 0
    # ASAP
    queue = deque(firstNodes)
    while queue:
        node = queue.popleft()
        rank = dag.nodes[node]['asapTopoRank']
        for succ in dag.succ[node]:
            dicoSucc = dag.nodes[succ]
            dicoSucc['asapTopoRank'] = max(1 + rank, dicoSucc['asapTopoRank'])           
            addSuccBFS(dag, succ, queue)
    # ALAP
    queue = deque(lastNodes)
    while queue:
        node = queue.popleft()
        rank = dag.nodes[node]['alapTopoRank']
        for pred in dag.pred[node]:
            dicoPred = dag.nodes[pred]
            dicoPred['alapTopoRank'] = max(1 + rank, dicoPred['alapTopoRank'])           
            addPredBFS(dag, pred, queue)
    # reverse ALAP to have one 0 rank among first nodes
    maxRank = 0
    if len(firstNodes) < len(lastNodes):
        for node in firstNodes:
            alapRank = dag.nodes[node]['alapTopoRank']
            maxRank = max(maxRank, alapRank)
    else:
        for node in lastNodes:
            asapRank = dag.nodes[node]['asapTopoRank']
            maxRank = max(maxRank, asapRank)
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['alapTopoRank']  = maxRank - dicoNode['alapTopoRank']
    return maxRank


def get_ordered_subsets(dag, key):
    """Get the subsets of nodes having same value of the keys
    (sorted in ascending order of the value).

    :param dag: DAG with node attributes 'key' set.
    :param key: Node attribute to consider.
    :returns: A sorted dictionnary with keys being the different values
    related to the given key, and with values being the corresponding nodes.
    """
    dicoSubset = SortedDict()
    for node, dicoNode in dag.nodes(data=True):
        dicoSubset.setdefault(dicoNode[key], set()).add(node)
    return dicoSubset


def get_ordered_keylist(dag, key):
    """Get the subsets of nodes having same value of the keys
    (sorted in ascending order of the value) and store them
    in order in a common list.

    :param dag: DAG with node attributes 'key' set.
    :param key: Node attribute to consider.
    :returns: Tuple. [0] A list sorted by key but
    containing values being the corresponding nodes.
    [1] A list of start indexes for subsets in the list.
    """
    # build subsets first
    dicoSubset = SortedDict()
    for node, dicoNode in dag.nodes(data=True):
        dicoSubset.setdefault(dicoNode[key], set()).add(node)
    # put subsets in order in the list
    listNodes = []
    listIndexes = [0]
    for subset in dicoSubset.values():
        listNodes.extend(subset)
        listIndexes.append(listIndexes[-1] + len(subset))
    # return with last extra index
    return (listNodes, listIndexes)


def get_dico_readyNodes(dag, closure=None):
    """Compute for each node, the set of nodes
    which can be ready at the same time (readyNodes).
    /!\ Cubic time since it requires transitive closure.
    /!\ Quadratic space since it stores readyNodes for each node.

    :param dag: Dag to consider.
    :param closure: (default None) Transitive closure of the DAG.
    If none, will be recomputed.
    :returns: Dict of nodes as keys, and readyNodes as values.
    """
    allNodes = frozenset(dag.nodes())
    if closure is None:
        closure = networkx.transitive_closure_dag(dag)
    dicoNodes = dict()
    for node in dag.nodes():
        forbiddenNodes = {node}
        forbiddenNodes.update(closure.pred[node])
        forbiddenNodes.update(closure.succ[node])
        readyNodes = allNodes.difference(forbiddenNodes)
        dicoNodes[node] = readyNodes
    return dicoNodes


def has_same_nodeProps(dag1, dag2):
    """Compares two graphs by their node

    :param dag1: Dag to consider.
    :param dag2: Dag to consider.
    :returns: True if equivalent for all
    'nodeMemCost' and 'tmpMemInc' node attributes.

    """
    set1 = set(dag1.nodes())
    set2 = set(dag2.nodes())
    if set1 != set2:
        return False
    for node1, dicoNode1 in dag1.nodes(data=True):
        dicoNode2 = dag2.nodes[node1]
        if dicoNode1['tmpMemInc'] != dicoNode2['tmpMemInc'] or \
           dicoNode1['nodeMemCost'] != dicoNode2['nodeMemCost']:
            return False
    return True


class GraphEqNodeProp:
    def __init__(self, graph):
        """Constructor of a graph wrapper
        enabling comparison by node properties,
        as defined per 'has_same_nodeProps()'.

        :param graph: Graph to embbed.
        :returns: Wrapper.
        """
        self.graph = graph

    def __eq__(self, other):
        return has_same_nodeProps(self.graph, other.graph)

    def __hash__(self):
        sumTmpMemInc = 0
        for _, tmpMemInc in self.graph.nodes(data='tmpMemInc'):
            sumTmpMemInc += tmpMemInc
        return sumTmpMemInc

        
class Chain:
    def __init__(self, startNode, startOutEdgeMem):
        """Constructor of a chain storing nodes and their out edge attributes 'edgeMem' (both in order).
        Members are list: nodes and outEdgeCosts.

        :param startNode: First node(s) of the chain (positive integer, or list of positive integer). 
        :param startOutEdgeMem: First out edge(s) memory (positive integer, or list of positive integer).
        :returns: A chain storing nodes and their out edge attributes 'edgeMem' (both in order).
        """
        if isinstance(startNode, int) and isinstance(startOutEdgeMem, int):
            self.nodes = [startNode]
            self.outEdgeCosts = [startOutEdgeMem]
        elif isinstance(startNode, list) and isinstance(startOutEdgeMem, list):
            self.nodes = startNode
            self.outEdgeCosts = startOutEdgeMem
        else:
            raise ValueError("Arguments should be either both int or both list of int.")

    def popFirst(self):
        """Pop first node of the chain, raise standard list exception if empty.

        :returns: First node if any.
        """
        self.outEdgeCosts.pop(0)
        return self.nodes.pop(0)

    def popLast(self):
        """Pop last node of the chain, raise standard list exception if empty.

        :returns: Last node if any.
        """
        self.outEdgeCosts.pop()
        return self.nodes.pop()

    def __str__(self):
        bufString = ["Chain: "]
        for (x,y) in zip(self.nodes, self.outEdgeCosts):
            bufString.append("({}) --- t{} --> ".format(x,y))
        return ''.join(bufString)

    def strWNames(self, dag):
        """Chain formatter, with both node names and edge memory cost.

        :param dag: DAG containing the chain.
        :returns: String representing the chain (with real names of nodes).
        """
        bufString = ["Chain: "]
        for (x,y) in zip(self.nodes, self.outEdgeCosts):
            bufString.append("({}) --- t{} --> ".format(dag.nodes[x]['name'], y))
        return ''.join(bufString)

    def strSubchainWNames(self, dag, slicer):
        """Chain formatter, with node names of nodes in slicer.

        :param dag: DAG containing the chain.
        :param slicer: Slice object to represent the subchain.
        :returns: String representing the nodes in subchain (with real names of nodes).
        """
        bufString = []
        if isinstance(slicer, int):
            slicer = slice(slicer, slicer+1)
        for x in self.nodes[slicer]:
            bufString.append(dag.nodes[x]['name'])
        return ', '.join(bufString)


def chains_generator(dag):
    """Generate all chains of the DAG, in topological order of
    their first node. 

    :param dag: DAG with 'edgeMem' attribute set on edges.
    :yields: Next chain.
    """
    current_chain = None # (start node, min, max) or (start node, max, min)
    stack_nodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    # Depth First Search for chains transformation
    # but otherwise BFS-like:
    # pop right current node, and push right
    # only the ready children
    while stack_nodes:
        current_node = stack_nodes.pop()
        if not dag.has_node(current_node):
            # we should not go here
            raise ValueError("Branching node modified during generation.")
        for child_node in dag.succ[current_node]:
            addSuccBFS(dag, child_node, stack_nodes)
            #stack_nodes.append(child_node)
        nbSucc = len(dag.succ[current_node])
        nbPred = len(dag.pred[current_node])
        if current_chain:
            if not dag.has_edge(current_chain.nodes[-1], current_node):
                # we finished a chain not including current_node
                yield current_chain
                current_chain = None
            elif nbSucc != 1 or nbPred != 1:
                # we finished the chain including current_node
                if nbSucc == 0 and nbPred == 1:
                    # current_node is a leaf, we add a 0 edge first
                    current_chain.nodes.append(current_node)
                    current_chain.outEdgeCosts.append(0)
                yield current_chain
                current_chain = None
            else:
                ## then we continue the chain
                succ = next(iter(dag.succ[current_node]))
                outEdgeMem = dag[current_node][succ]['edgeMem']
                current_chain.nodes.append(current_node)
                current_chain.outEdgeCosts.append(outEdgeMem)
        if current_chain is None and \
           nbSucc <= 1 and nbPred <= 1:
            # we start a new chain
            outEdgeMem = 0
            if nbSucc == 1:
                succ = next(iter(dag.succ[current_node]))
                outEdgeMem = dag[current_node][succ]['edgeMem']
            current_chain = Chain(current_node, outEdgeMem)


def getSPdagFJtuples(dag):
    """Compute all fork-join node pairs
    present in an SP-DAG. Note that a
    fork can be paired to multiple joins,
    and vice versa.

    :param dag: SP DAG to be considered.
    :returns: All fork-join node pairs
    (as a set of tuples) present in an
    SP-DAG (raise Exception if not an SP-DAG).
    """
    tupleSet = set()
    dag_copy = networkx.DiGraph()
    for src,dst in dag.edges():
        dag_copy.add_edge(src, dst)
    for node, kind in dag.nodes(data='kind'):
        if kind == "GC":
            dag_copy.remove_node(node)

    update = True
    while update:
        update = False
        stack_nodes = [node for node, in_degree in
                       dag_copy.in_degree if in_degree == 0]
        for _, dicoNode in dag_copy.nodes(data=True):
            dicoNode['nbVisits'] = 0
        while stack_nodes:
            node = stack_nodes.pop()
            for child_node in dag_copy.succ[node]:
                addSuccBFS(dag_copy, child_node, stack_nodes)
            nbPreds = len(dag_copy.pred[node])
            nbSuccs = len(dag_copy.succ[node])
            if nbPreds == 1 and nbSuccs == 1:
                pred = next(iter(dag_copy.pred[node]))
                succ = next(iter(dag_copy.succ[node]))
                dag_copy.remove_node(node)
                update = True
                if dag_copy.has_edge(pred, succ):
                    # then we have a fork-join couple
                    tupleSet.add((pred,succ))
                else:
                    dag_copy.add_edge(pred, succ)
            elif nbPreds <= 1 and nbSuccs <= 1:
                dag_copy.remove_node(node)
                
    if len(dag_copy.nodes()) > 0:
        raise Exception("Input was not an SP DAG.")

    return tupleSet
                
            
def strictSTsubDAG_check_deprecated(dag, start_node, reverse=False):
    """Check if subgraph starting from given node
    is strict ST-DAG and returns it if so.
    /!\ Here 'strict' means that start_node is considered
    as the only source, all its successors being in the
    eventual ST-DAG starting from it.
    /!\ Not tested thoroughly.

    :param dag: DAG to analyze.
    :param start_node: Starting node of the ST subgraphs.
    :param reverse: Iterate over predecessors
    instead of successors.
    :returns: None if not an ST subgraph,
    the subgraph copy otherwise.
    """
    subgraph = networkx.DiGraph()
    reached_nodes = set()
    reached_leaves = set()
    stack_nodes = []
    addBFS = addSuccBFS
    if reverse:
        addBFS = addPredBFS
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    # init sched (do-while)
    current_node = start_node
    if reverse:
        children_nodes = dag.pred[current_node]
    else:
        children_nodes = dag.succ[current_node]
    for child_node in children_nodes:
        reached_nodes.add(child_node)
        subgraph.add_edge(current_node, child_node)
        addBFS(dag, child_node, stack_nodes)
    # start random sched from source
    while stack_nodes:
        current_node = stack_nodes.pop()
        reached_nodes.remove(current_node)
        if reverse and dag.in_degree(current_node) == 0 or \
           not reverse and dag.out_degree(current_node) == 0:
            reached_leaves.add(current_node)
        if not stack_nodes and not reached_nodes and \
           not reached_leaves:
            # reached end of an ST subgraph
            break
        if reverse:
            children_nodes = dag.pred[current_node]
        else:
            children_nodes = dag.succ[current_node]
        for child_node in children_nodes:
            reached_nodes.add(child_node)
            subgraph.add_edge(current_node, child_node)
            addBFS(dag, child_node, stack_nodes)
    # check if remaining reach_nodes
    if reached_nodes:
        # but last node can be not completely reached?
        return None
    # check if only leaves (useless?)
    if reached_leaves and not (current_node in reached_leaves):
        return None
    # otherwise copy missing data in the subgraph
    if reverse:
        # in such case we must reverse all edges first
        subgraph = subgraph.reverse(copy=True)
    for node, dicoNode in subgraph.nodes(data=True):
        dicoNode.update(dag.nodes[node])
    for src, dst, dicoEdge in subgraph.edges(data=True):
        dicoEdge.update(dag[src][dst])
    return subgraph

            
def strictSTsubDAG_generator_deprecated(dag, onlyBiggest=True):
    """Generate all strict ST subgraphs in the DAG,
    excluding chains of any length and DAG itself.
    /!\ Here 'strict' means that all children of
    every start_node are considered.
    /!\ Do not check for graphs in reverse.
    /!\ Not tested thoroughly.

    :param dag: DAG to analyze.
    :param onlyBiggest: (default True) If true, do not
    generate ST subgraph included in a bigger one.
    :yields: Next ST subgraph with attributes
    copied from original.
    """
    if onlyBiggest:
        (nodesAsap, indexesAsap) = get_ordered_keylist(dag, 'asapTopoRank')
        nodes = nodesAsap
        visitedNodes = set()
    else:
        nodes = list(dag.nodes())
    nbNodesOri = len(nodes)
    # sink nodes are obviously not source of an STsubDAG
    sink_nodes = [node for node, out_degree in dag.out_degree if out_degree == 0]
    for node in sink_nodes:
        nodes.remove(node)
    # main loop on all other nodes
    for node in nodes:
        if onlyBiggest and node in visitedNodes:
            continue
        subgraph = strictSTsubDAG_check_deprecated(dag, node)
        if not subgraph is None:
            nbNodesSub = len(subgraph.nodes())
            if nbNodesSub > 2 and nbNodesSub < nbNodesOri:
                if onlyBiggest:
                    # at this point, all new nodes cannot
                    # not already be in the set, check it?
                    visitedNodes.update(subgraph.nodes())
                    # but keep the sinks which may start other subgraphs
                    sub_sinks = [node for node, out_degree in
                                 subgraph.out_degree if out_degree == 0]
                    for node in sub_sinks:
                        visitedNodes.remove(node)
                yield subgraph
        # second loop on reverse side
        # identify each subgraph by its source/sinks to avoid
        # redundant graphs?


def genSTsubDAG_check(dag, start_nodes, preds):
    """Check if subgraph starting from given nodes
    (assuming given fully connected predecessors),
    is generic ST-DAG and returns it if so.
    /!\ Here 'generic' means that there might be multiple
    start nodes of an STsubDAG, and all their children
    need not to be in the same STsubDAG.
    /!\ Do not work if transitive edge in the
    STsubDAG (and so in the input dag). Do
    not accept mere chains.
    /!\ If called by genSTsubDAG_generator(dag),
    only the smallest STsubDAGs will be found, thanks to
    the extra node attribute 'pred_STsubDAG' it sets.
    If the attribute is True, the corresponding
    children nodes are not visited,
    which also accelerates the check.
    /!\ Reset node attribute 'nbVisits'.

    :param dag: DAG to analyze.
    :param start_nodes: Set of nodes
    starting the ST subgraph.
    :returns: List of eventual ST subgraphs
    (copies with data), along with their end nodes
    and their succesors in a tuple.
    """
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    dicoSubgraphsReached = dict()
    for node in start_nodes:
        subgraph = networkx.DiGraph()
        subgraph.add_node(node)
        dicoSubgraphsReached[node] = subgraph
    resList = []
    leafSTsubDAGs = []
    stack_nodes = list(start_nodes)
    # start random sched from sources
    while stack_nodes:
        reachedClosingNode = False
        scheduledLeaf = False
        current_node = stack_nodes.pop()
        subgraph = dicoSubgraphsReached.pop(current_node)
        # update subgraph
        for parent_node in dag.pred[current_node]:
            subgraph.add_edge(parent_node, current_node)
        # visit children
        if not dag.nodes[current_node]['pred_STsubDAG']:
            children_nodes = dag.succ[current_node]
            for child_node in children_nodes:
                wasQueued = addSuccBFS(dag, child_node, stack_nodes)
                if wasQueued and len(dag.pred[child_node]) > 1:
                    # we reached a closing node,
                    # so we will test if STsubDAG
                    reachedClosingNode = True
                subgraph_child = dicoSubgraphsReached.setdefault(
                    child_node, subgraph)
                if subgraph_child != subgraph:
                    # merge subgraphs                    
                    subgraph.add_nodes_from(subgraph_child.nodes())
                    subgraph.add_edges_from(subgraph_child.edges())
                    # update all reached nodes
                    for k, v in dicoSubgraphsReached.items():
                        if v == subgraph_child:
                            dicoSubgraphsReached[k] = subgraph
            if not children_nodes:
                # scheduled node was a leaf,
                # so we will test if STsubDAG
                # and we will store it as leaf
                scheduledLeaf = True
                
        if not scheduledLeaf and not reachedClosingNode:
            continue
        # main test
        # - all sinks must have same succs
        # - and same succs == reached nodes
        # - and same succs == stack nodes
        # (last test until BFS ends only)
        lastNodes = {node for node, out_degree in
                     subgraph.out_degree if out_degree == 0}
        succsLastNodes = set()
        for sink in lastNodes:
            succsLastNodes.add(frozenset(dag.succ[sink]))
        if len(succsLastNodes) > 1:
            continue
        succs = next(iter(succsLastNodes))
        stack_nodes_subgraph = {n for n in stack_nodes if
                                dicoSubgraphsReached[n] == subgraph}
        reached_nodes_subgraph = {n for n,sub in
                                  dicoSubgraphsReached.items()
                                  if sub == subgraph}
        if succs == stack_nodes_subgraph and \
           succs == reached_nodes_subgraph:
            # we got it, clean subgraph and copy data
            subgraph.remove_nodes_from(preds)
            for node, dicoNode in subgraph.nodes(data=True):
                dicoNode.update(dag.nodes[node])
            for src, dst, dicoEdge in subgraph.edges(data=True):
                dicoEdge.update(dag[src][dst])
            if scheduledLeaf:
                leafSTsubDAGs.append(tuple((subgraph, lastNodes, succs)))
            else:
                resList.append(tuple((subgraph, lastNodes, succs)))
            # remove corresponding nodes from stack
            # to avoid continuing on this path
            for node in stack_nodes_subgraph:
                stack_nodes.remove(node)
                dicoSubgraphsReached.pop(node)
    # BFS ends so we test again remaining subgraphs
    # with weaker conditions (no stack check)
    remainingSubgraphs = set(dicoSubgraphsReached.values())
    for remSubgraph in remainingSubgraphs:
        # final test (no leafSTsubDAG here)
        # - all sinks must have same succs
        # - and same succs == reached nodes
        # - NO stack nodes check
        lastNodes = {node for node, out_degree in
                     remSubgraph.out_degree if out_degree == 0}
        succsLastNodes = set()
        for sink in lastNodes:
            succsLastNodes.add(frozenset(dag.succ[sink]))
        if len(succsLastNodes) > 1:
            continue
        succs = next(iter(succsLastNodes))
        reached_nodes_subgraph = {n for n,sub in
                                  dicoSubgraphsReached.items()
                                  if sub == remSubgraph}
        if succs == reached_nodes_subgraph:
            # we got it, clean subgraph and copy data
            remSubgraph.remove_nodes_from(preds)
            # maybe after cleaning, there is only a chain left
            # (since unclosed reached), so we discard the whole subgraph
            if len(remSubgraph.nodes()) - 1 == len(remSubgraph.edges()):
                continue
            for node, dicoNode in remSubgraph.nodes(data=True):
                dicoNode.update(dag.nodes[node])
            for src, dst, dicoEdge in remSubgraph.edges(data=True):
                dicoEdge.update(dag[src][dst])
            resList.append(tuple((remSubgraph, lastNodes, succs)))
    # finally merge all leafSTsubDAGs if any
    if leafSTsubDAGs:
        leafSubgraph = networkx.DiGraph()
        leafLasts = set()
        leafSuccs = set()
        for leafSTsubDAG, lastNodes, succs in leafSTsubDAGs:
            leafLasts.update(lastNodes)
            leafSuccs.update(succs)
            leafSubgraph.add_nodes_from(leafSTsubDAG.nodes(data=True))
            leafSubgraph.add_edges_from(leafSTsubDAG.edges(data=True))
        if leafSuccs:
            raise Exception("Found a leaf STsubDAG with not only leaves.")
        if len(leafLasts) > 1:
            # this is useful only if we have multiple leaves
            resList.append(tuple((leafSubgraph, leafLasts, leafSuccs)))
    return resList
        

def genSTsubDAG_generator(dag):
    """Generate all generic ST subgraphs in the DAG,
    excluding chains of any length and DAG itself.
    /!\ Here 'generic' means that there might be multiple
    start nodes of an STsubDAG, and all their children
    need not to be in the same STsubbDAG.
    /!\ Do not generate chain STsubDAG.
    Do not generate STsubDAG containing
    any other STsubDAG. Reset node attribute
    'pred_STsubDAG' to do so.
    /!\ Reset node attribute 'nbVisits', and
    requires 'alapTopoRank' already set.

    :param dag: DAG to analyze.
    :yields: Next (in reverse ALAP order) STsubDAG
    as a tuple (start_nodes, STsubDAG, end_nodes)
    with attributes copied from original.
    """
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['pred_STsubDAG'] = False
    dicoAsap = get_ordered_subsets(dag, 'asapTopoRank')
    nbNodesTot = len(dag.nodes())
    # iterates over nodes by reverse ASAP
    for nodes in dicoAsap.values()[::-1]:
        # regroup nodes if they have same set of predecessors
        dicoSamePred = dict()
        for node in nodes:
            preds = frozenset(dag.pred[node])
            sameStartNodes = dicoSamePred.setdefault(preds, set())
            sameStartNodes.add(node)
        # now we can test STsubDAG for each set in dicoSamePred
        for preds, sameStartNodes in dicoSamePred.items():
            if len(sameStartNodes) == 1:
                # if node is alone (as in a chain), ignore it
                continue
            resList = genSTsubDAG_check(dag, sameStartNodes, preds)
            for res in resList:
                STsubDAG, end_nodes, succs = res
                nbNodesSub = len(STsubDAG.nodes())
                if nbNodesSub + len(preds) + len(succs) == nbNodesTot:
                    # in this case, we just found the input DAG itself
                    continue
                # if it is an STsubDAG, mark preds to stop next searches
                for pred in preds:
                    dag.nodes[pred]['pred_STsubDAG'] = True
                start_nodes = sameStartNodes.intersection(set(STsubDAG.nodes()))
                yield (start_nodes, STsubDAG, end_nodes)




def ffst_min_cut(dag, closure=None, compulsoryNodes=None):
    """Computes an ST-min-cut based on node impact.
    Such cut respect feed-forward constraint of a
    regular task graph cut. This as to be used instead
    of the solution presented in [Jin+23] 
    (proof Theorem 3.17). "New Tools for Peak Memory Scheduling"

    :param dag: DAG to consider, with 'nodeMemCost' attribute set.
    :param closure: Transitive closure of the DAG,
    will be computed if None.
    :param compulsoryNodes: A set of nodes to be in the left
    (reachable) partition. Will be empty if None.
    :returns: A left (reachable) node partition ensuring minimum cut.
    """
    if compulsoryNodes is None:
        compulsoryNodes = set()
    if closure is None:
        closure = networkx.transitive_closure_dag(dag)
    dicoReadyNodes = get_dico_readyNodes(dag, closure)
    
    # main loop: try cut containing each negative impact node
    bestCutValue = 0
    if compulsoryNodes is None:
        trCompulsoryNodes = set()
        bestCutLset = set()
    else:
        trCompulsoryNodes = set(compulsoryNodes)
        for node in compulsoryNodes:
            trCompulsoryNodes.update(closure.pred[node])
        for node in trCompulsoryNodes:
            bestCutValue += dag.nodes[node]['nodeMemCost']
        bestCutLset = set(trCompulsoryNodes)
    for node, nodeMemCost in dag.nodes('nodeMemCost'):
        if node in trCompulsoryNodes:
            # such node will always be included
            continue
        if nodeMemCost >= 0:
            # since we start at 0, a min cut must contain
            # a negative node
            continue
        currentCutLset = set(closure.pred[node])
        currentCutLset.add(node)
        currentCutLset.update(trCompulsoryNodes)
        currentCutValue = 0
        for anc in currentCutLset:
            currentCutValue += dag.nodes[anc]['nodeMemCost']
        currentReadyNodes = set()
        # filter ready nodes
        for coNode in  dicoReadyNodes[node]:
            if coNode in currentCutLset or \
               dag.nodes[coNode]['nodeMemCost'] >= 0:
                continue
            currentReadyNodes.add(coNode)
        # now try to complete the cut
        while currentReadyNodes:
            bestExtCutValue = 0
            bestExtCutLset = set()
            # get min impact of all extending node
            for coNode in currentReadyNodes:
                extraNodes = set(closure.pred[coNode]).difference(currentCutLset)
                currentExtCutValue = dag.nodes[coNode]['nodeMemCost']
                for extNode in extraNodes:
                    currentExtCutValue += dag.nodes[extNode]['nodeMemCost']
                if currentExtCutValue < bestExtCutValue:
                    bestExtCutValue = currentExtCutValue
                    bestExtCutLset = set(extraNodes)
                    bestExtCutLset.add(coNode)
            # update current cut if necessary or stop
            if not bestExtCutLset:
                break
            currentCutValue += bestExtCutValue
            currentCutLset.update(bestExtCutLset)
            nextReadyNodes = currentReadyNodes.difference(bestExtCutLset)
            currentReadyNodes = nextReadyNodes
        # finally update best cut if necessary
        # either better cut or first non empty one
        if currentCutValue < bestCutValue or \
           ((not bestCutLset) and currentCutValue == 0):
            bestCutValue = currentCutValue
            bestCutLset = currentCutLset
                
    return bestCutLset

