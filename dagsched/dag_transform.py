#! /usr/bin/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from .alphanumericBaseConversion import *
from .utils import *
from .dag_analysis import *

import networkx
# from itertools import pairwise
from collections import deque
from sortedcontainers import SortedList, SortedDict

import bracelogger
logger = bracelogger.get_logger("dagsched_log")


def getSubDAG(dag, nodes=None, offset=0, subdag=None):
    """Create subdag having only given nodes
    and internal edges.
    /!\ Adapted from DiGraph.subgraph(nodes)
    documentation because networkx subgraph is
    not always what we want ...

    :param dag: DAG to consider.
    :param nodes: Nodes to keep in subdag.
    If None, copy all nodes.
    :param offset: Offset all node
    indexes in the subdag.
    :param subdag: If None, return a new
    digraph, otherwise fill this graph.
    :returns: Subdag.
    """
    checkNodes = False
    if subdag is None:
        subdag = networkx.DiGraph()
    else:
        checkNodes = True
    if nodes is None:
        nodes = [n for n in dag.nodes()]
        if checkNodes:
            # sanity check
            for n in nodes:
                if subdag.has_node(n+offset):
                    raise ValueError("Wrong subgraph offset: node already exists.")
    # finally adds nodes and edges
    subdag.add_nodes_from((n+offset, dag.nodes[n]) for n in nodes)
    subdag.add_edges_from((n+offset, nbr+offset, d) \
        for n, nbrs in dag.adj.items() if n in nodes \
        for nbr, d in nbrs.items() if nbr in nodes)
    return subdag


def copy_graph_input(dag):
    """Deep copy of the graph with only minimal info.

    :param dag: Graph to copy.
    :returns: A copy of the input dag with node attributes
    'name' and edge attribute 'edgeMem'.
    """
    dag_copy = networkx.DiGraph()
    dag_copy.add_nodes_from(dag.nodes())
    for src,dst,edgeMem in dag.edges(data='edgeMem'):
        dag_copy.add_edge(src, dst, edgeMem=edgeMem)
    for node, dicoNode in dag.nodes(data=True):
        dicoNode_copy = dag_copy.nodes[node]
        dicoNode_copy['name'] = dicoNode['name']
        if dicoNode.get('kind') != None:
            dicoNode_copy['kind'] = dicoNode['kind']
        if dicoNode.get('oriNode') != None:
            dicoNode_copy['oriNode'] = dicoNode['oriNode']
    return dag_copy
        

def copy_graph_solver(dag):
    """Deep copy of the graph with only minimal info.

    :param dag: Graph to copy.
    :returns: A copy of the input dag with node attributes
    'name', 'nodeMemCost' and 'tmpMemInc' and edge
    attribute 'edgeMem'.
    """
    dag_copy = copy_graph_input(dag)
    for node, dicoNode in dag.nodes(data=True):
        dicoNode_copy = dag_copy.nodes[node]
        dicoNode_copy['nodeMemCost'] = dicoNode['nodeMemCost']
        dicoNode_copy['tmpMemInc'] = dicoNode['tmpMemInc']
    return dag_copy


def set_alphabetical_node_names(graph):
    """Set the node names as alphabetic letters (from 'A' to 'Z').

    :param graph: Graph where to set node attribute 'name'.
    """
    for node, dicoNode in graph.nodes(data=True):
        dicoNode['name'] = base_10_to_alphabet(node+1)


def reverse_sched_graph(dag):
    """Create the reverse schedule graph.
    All edges are inverted, 'nodeMemCost'
    node attribute is negated, 'tmpMemInc'
    node attribute is the former drop value.

    :param dag: Graph to reverse.
    :returns: A reverse copy of the input dag with 
    only node attributes 'nodeMemCost' and 'tmpMemInc'.
    Copy 'edgeMem' edge attribute only if present (!= None).
    """
    dag_copy = networkx.DiGraph()
    dag_copy.add_nodes_from(dag.nodes())
    for src, dst, edgeMem in dag.edges(data='edgeMem'):
        if edgeMem is None:
            dag_copy.add_edge(dst, src)
        else:
            dag_copy.add_edge(dst, src, edgeMem=edgeMem)
    for node, dicoNode in dag.nodes(data=True):
        dicoNode_copy = dag_copy.nodes[node]
        nodeMemCost = dicoNode['nodeMemCost']
        dicoNode_copy['nodeMemCost'] = - nodeMemCost
        drop = dicoNode['tmpMemInc'] - nodeMemCost
        dicoNode_copy['tmpMemInc'] = drop
    return dag_copy


def build_node_min_cut_form(dag, maxCapacity=None):
    """Creates the auxiliary graph used to compute
    a cut reaching minimum live memory in the dag.
    Always an extra source and sink.
    Code understood from [Jin+23] (proof Theorem 3.17).
    "New Tools for Peak Memory Scheduling"
    /!\ Does not seem to work.

    :param dag: DAG to consider, with node
    attribute 'nodeMemCost' set.
    :param maxCapacity: If None, computed as 1 plus
    the sum of all squared 'nodeMemImpact'.
    :returns: Tuple (DAG, sourceFlowId, sinkFlowId).
    DAG with extra sources and sinks (one flow, one topo),
    and 'capacity' edge attribute set for every
    node either connected to source or sink.
    No capacity set on an edge means maximum.
    sourceTopoId and sinkTopoId respectively are
    sourceFlowId-2 and sinkFlowId-2.
    """
    if maxCapacity is None:
        maxCapacity = 1+sum([i*i for _, i in dag.nodes(data='nodeMemCost')])
    dag_copy = networkx.DiGraph()
    dag_copy.add_nodes_from(dag.nodes())
    maxNodes = max(dag.nodes()) + 1
    for src, dst in dag.edges():
        dag_copy.add_edge(src, dst, capacity=0)
    # connect new topo source to first nodes if multiple ones
    firstNodes = [n for n, in_degree in dag.in_degree if in_degree == 0]
    sourceTopoId = maxNodes
    maxNodes += 1
    dag_copy.add_node(sourceTopoId, nodeMemCost=0)
    for node in firstNodes:
        dag_copy.add_edge(sourceTopoId, node, capacity=0)
    # connect last nodes to new topo sink if multiple ones
    lastNodes = [n for n, out_degree in dag.out_degree if out_degree == 0]
    sinkTopoId = maxNodes
    maxNodes += 1
    dag_copy.add_node(sinkTopoId, nodeMemCost=0)
    for node in lastNodes:
        dag_copy.add_edge(node, sinkTopoId, capacity=0)
    # all orignal nodes are connected to the sink
    # so that every cut (i.e. reachable partition)
    # has to sum the impact of reached nodes
    # for node, nodeMemCost in dag.nodes(data='nodeMemCost'):
    #     dag_copy.add_edge(node, sinkId, capacity=nodeMemCost)
    sourceFlowId = maxNodes
    sinkFlowId = maxNodes + 1
    dag_copy.add_edge(sourceFlowId, sourceTopoId, capacity=0)
    dag_copy.add_edge(sinkTopoId, sinkFlowId, capacity=0)
    for node, nodeMemCost in dag.nodes(data='nodeMemCost'):
        if nodeMemCost < 0:
            dag_copy.add_edge(sourceFlowId, node, capacity=-nodeMemCost)
            dag_copy.add_edge(node, sinkFlowId, capacity=0)
        else:
            dag_copy.add_edge(node, sinkFlowId, capacity=nodeMemCost)
            dag_copy.add_edge(sourceFlowId, node, capacity=0)
    return dag_copy, sourceFlowId, sinkFlowId


def set_default_attributes(dag, resetTmpMemInc=True):
    """Set default schedule graph (SG) node attributes:
    'tmpMemInc' (for temporary memory increase in CBP model),
    'predNodes' (nodes merged before),
    'succNodes' (nodes merged after).

    :param dag: DAG to set (in place).
    :param resetTmpMemInc: (default True) If True,
    reset 'tmpMemInc' node attribute to 0.
    """
    for node, dicoNode in dag.nodes(data=True):
        if resetTmpMemInc:
            dicoNode['tmpMemInc'] = 0
        else:
            dicoNode.setdefault('tmpMemInc', 0)
        dicoNode['predNodes'] = []
        dicoNode['succNodes'] = []


def set_node_cost(dag):
    """Update the DAG having absolute edge memory sizes to 
    relative node memory increase (may be negative).
    Edge attributes 'edgeMem' must be set before, and
    node attributes 'nodeMemCost' will be set

    :param dag: DAG to convert (in place).
    """
    for node, dicoNode in dag.nodes(data=True):
        inMemWeight = 0
        for _, dicoEdge in dag.pred[node].items():
            inMemWeight += dicoEdge['edgeMem']
        outMemWeight = 0
        for _, dicoEdge in dag.succ[node].items():
            outMemWeight += dicoEdge['edgeMem']
        dicoNode['nodeMemCost'] = outMemWeight - inMemWeight
        
        
def convert_to_pbc(dag):
    """Convert dag with memory on edges from the consumed before produced (CBP) model
    to the produced before consumed (PBC) model by duplicating all nodes.

    :param dag: DAG with attributes 'edgeMem' set on edges.
    :returns: A DAG with all nodes duplicated so that it stays in the CBP model while
    modelling PBC. Edges between duplicated nodes have their attribute 'addedByPeakDup'
    set to True.
    """
    pbc_dag = networkx.DiGraph()
    maxNodes = max(dag.nodes()) + 1
    for node, nodeName in dag.nodes(data='name'):
        totMemNode = 0
        for succ, dicoEdge in dag.succ[node].items():
            pbc_dag.add_edge(node+maxNodes, succ, **dicoEdge)
            totMemNode += dicoEdge['edgeMem']
        for pred, dicoEdge in dag.pred[node].items():
            totMemNode += dicoEdge['edgeMem']
        pbc_dag.add_edge(node, node+maxNodes, edgeMem = totMemNode,
                         addedByPeakDup = True)
        pbc_dag.nodes[node]['name'] = nodeName + "_r"
        pbc_dag.nodes[node+maxNodes]['name'] = nodeName + "_w"
    return pbc_dag


def compact_pbc_edges(dag):
    """Compact edges added by convert_to_pbc(dag),
    while setting node attribute 'tmpMemInc' (updated if already present).

    :param dag: A DAG with 'addedByPeakDup' attributes set on some edges.
    :returns: Number of removed nodes.
    """
    edgesToRemove = [(src, dst) for src, dst, addedByPeakDup in
                     dag.edges(data='addedByPeakDup', default=False) if addedByPeakDup]
    for src, dst in edgesToRemove:
        for succ, dicoEdge in dag.succ[dst].items():
            dag.add_edge(src, succ, **dicoEdge)
        dicoSrc = dag.nodes[src]
        dicoDst = dag.nodes[dst]
        logger.debug("Merging PBC node {}.",dicoSrc['name'])
        # updating properties
        # (there should be no predNodes in dst PBC node)
        # (there should be no succNodes in src PBC node)
        dicoSrc['succNodes'].append(dst)
        dicoSrc['succNodes'].extend(dicoDst['succNodes'])
        srcNodeMemCost = dicoSrc['nodeMemCost']
        dstNodeMemCost = dicoDst['nodeMemCost']
        dicoSrc['tmpMemInc'] = max(srcNodeMemCost,
                                   dicoSrc.get('tmpMemInc', 0),
                                   srcNodeMemCost + dicoDst.get('tmpMemInc', 0))
        dicoSrc['nodeMemCost'] += dstNodeMemCost
        dag.remove_node(dst)
    return len(edgesToRemove)
        
    
def set_pbc_tmpMemInc(dag):
    """Set 'tmpMemInc' node attribute to emulate PBC model in CBP model.
    It is an alternative to convert_to_pbc(dag) + compact_pbc_edges(dag).
    To be called on an original graph to evaluate in PBC model.
    If 'tmpMemInc' is already set, max value is kept.

    :param dag: DAG with attribute 'edgeMem' set on edges.
    """
    for node, dicoNode in dag.nodes(data=True):
        pbcMemInc = 0
        for succ, dicoEdge in dag.succ[node].items():
            pbcMemInc += dicoEdge['edgeMem']
        dicoNode['tmpMemInc'] = max(pbcMemInc, dicoNode.get('tmpMemInc', 0))

        
def set_cbp_tmpMemInc(dag):
    """Set 'tmpMemInc' node attribute in CBP model.
    This is harmless to CBP memory peak evaluation but
    offers an initial value as needed by serie and parallel optims.
    (Even though potentially useless?)
    To be called after convert_to_pbc(dag) or directly
    if considering CBP model.
    If 'tmpMemInc' is already set, max value is kept.

    :param dag: DAG with attribute 'nodeMemCost' set on nodes.
    After the call, node attribute 'tmpMemInc' will be set.
    """
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['tmpMemInc'] = max(0, dicoNode['nodeMemCost'],
                                    dicoNode.get('tmpMemInc', 0))    


def set_cbp_common_attr(dag):
    """Set multiple attributes, common to solvers
    considering the cbp model.

    :param dag: DAG to set.
    """
    set_alphabetical_node_names(dag)
    set_default_attributes(dag, resetTmpMemInc=False)
    set_node_cost(dag)
    set_cbp_tmpMemInc(dag)

    
def set_node_peakDrop(dag, opposite=False):
    """Set 'memPeakDrop' node attribute to 'tmpMemInc'-'nodeMemCost'.

    :param dag: DAG with attributes 'nodeMemCost'and 'tmpMemInc'
    set on nodes.
    :param opposite: If True, negate the value.
    After the call, node attribute 'memPeakDrop' will be set.
    """
    factor = -1 if opposite else 1
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['memPeakDrop'] = factor * \
            (dicoNode['tmpMemInc'] - dicoNode['nodeMemCost'])

        
def tmpMemInc_to_cbp(dag, storeSuccInRead=True, offsetNewNodes=0):
    """Transform dag with peak on nodes to the pure consumed before produced (CBP)
    model by duplicating all nodes having their peak greater than their impact.
    /!\ Not correct if 'edgeMem' attributes are not sync with 'nodeMemCost'.

    :param dag: DAG with default node attributes and 'edgeMem' edge attribute.
    A boolean attribute 'addedByPeakDup' is set on new edges.
    :param storeSuccInRead: Wether or not the 'succNodes' node attribute
    must still be stored in the original (read) node instead of the
    new (write) node.
    :param offsetNewNodes: Extra write nodes are added with identifier
    greater than the maximum original node ids and this offset.
    """
    maxNodes = max(offsetNewNodes, max(dag.nodes()))
    oriNodesAndDico = [tup for tup in dag.nodes(data=True)]
    for node, dicoNode in oriNodesAndDico:
        nodeMemCost = dicoNode['nodeMemCost']
        tmpMemInc = dicoNode['tmpMemInc']
        if tmpMemInc > nodeMemCost:
            maxNodes += 1
            # then we duplicate the node
            backupSuccs = [(succ, dicoEdge) for succ, dicoEdge in dag.succ[node].items()]
            for succ, dicoEdge in backupSuccs:
                dag.add_edge(maxNodes, succ, **dicoEdge)
                dag.remove_edge(node, succ)
            predMemNode = 0
            for pred, dicoEdge in dag.pred[node].items():
                predMemNode += dicoEdge['edgeMem']
            dag.add_edge(node, maxNodes, edgeMem = predMemNode + tmpMemInc,
                         addedByPeakDup = True)
            nodeName = dicoNode['name']
            dicoNode['name'] = nodeName + "_r"
            dicoNode['nodeMemCost'] = tmpMemInc
            dicoDup = dag.nodes[maxNodes]
            dicoDup['name'] = nodeName + "_w"
            dicoDup['nodeMemCost'] = nodeMemCost - tmpMemInc
            dicoDup['tmpMemInc'] = 0
            dicoDup['predNodes'] = []
            if storeSuccInRead:
                dicoDup['succNodes'] = []
            else:
                dicoDup['succNodes'] = dicoNode['succNodes']
                dicoNode['succNodes'] = []


def store_succ(dag, srcToKeep, dstListToRemove):
    """Store successors node in the node to keep.

    :param dag: DAG containing the nodes with attributes
    'name', 'predNodes' and 'succNodes' set.
    :param srcToKeep: Where to keep the given successors.
    :param dstListToRemove: Successors to be stored.
    """
    for dstToRemove in dstListToRemove:
        dag.nodes[srcToKeep]['succNodes'].extend(dag.nodes[dstToRemove]['predNodes'])
        dag.nodes[srcToKeep]['succNodes'].append(dstToRemove)
        dag.nodes[srcToKeep]['succNodes'].extend(dag.nodes[dstToRemove]['succNodes'])
    # logger.debug("Compacted following nodes in src {}: - {} -",
    #              dag.nodes[srcToKeep]['name'], strNodesWNames(dag, dstListToRemove))    


def store_pred(dag, srcListToRemove, dstToKeep):
    """Store predecessors node in the node to keep.
    /!\ A new 'predNodes' list is created instead of being updated.

    :param dag: DAG containing the nodes with attributes
    'name', 'predNodes' and 'succNodes' set.
    :param srcListToRemove: Predecessors to be stored.
    :param dstToKeep: Where to keep the given predecessors.
    """
    newPredListOfDst = []
    # we need to create an extra list
    # otherwise we should iterate everything in reverse order
    for srcToRemove in srcListToRemove:
        newPredListOfDst.extend(dag.nodes[srcToRemove]['predNodes'])
        newPredListOfDst.append(srcToRemove)
        newPredListOfDst.extend(dag.nodes[srcToRemove]['succNodes'])
    newPredListOfDst.extend(dag.nodes[dstToKeep]['predNodes'])
    dag.nodes[dstToKeep]['predNodes'] = newPredListOfDst
    # logger.debug("Compacted following nodes in dst {}: - {} -",
    #     dag.nodes[dstToKeep]['name'], strNodesWNames(dag, srcListToRemove))
    
            
def compact_two_nodes(dag, chain):
    """Compact the nodes of chains of length 2, if possible.

    :param dag: DAG containing the chain.
    :param chain: Chain to compact.
    :returns: Number of removed nodes (0 or 1).
    """
    # if there are only two nodes of same sign left, we add them
    if len(chain.nodes) == 2:
        logger.debug("Two nodes case.")
        if not dag.pred[chain.nodes[0]]:
            # then this is a source node so we just keep max of two
            # in the other case of leaf node, an extra edge is added with 0 mem
            if chain.outEdgeCosts[1] >= chain.outEdgeCosts[0]:
                store_pred(dag, [chain.nodes[0]], chain.nodes[1])
                dag.remove_node(chain.nodes[0])
                return 1
            else:
                store_succ(dag, chain.nodes[0], [chain.nodes[1]])
                dag.remove_node(chain.nodes[1])
                return 1
        else:
            pred = next(iter(dag.pred[chain.nodes[0]]))
            edgeMemFromPred = dag[pred][chain.nodes[0]]['edgeMem']
            if chain.outEdgeCosts[0] >= edgeMemFromPred and chain.outEdgeCosts[1] >= chain.outEdgeCosts[0]:
                # ascending case
                succ = next(iter(dag.succ[chain.nodes[1]]))
                logger.debug("Compacted in edge: {} --- t{} --> {}",
                    dag.nodes[chain.nodes[0]]['name'], chain.outEdgeCosts[1], dag.nodes[succ]['name'])
                # there is always a succ here because otherwise outEdgeCosts[1] would be 0
                dag.add_edge(chain.nodes[0], succ, edgeMem = chain.outEdgeCosts[1])
                store_pred(dag, [chain.nodes[1]], succ)
                dag.remove_node(chain.nodes[1])
                return 1
            elif chain.outEdgeCosts[0] <= edgeMemFromPred and chain.outEdgeCosts[1] <= chain.outEdgeCosts[0]:
                # descending case
                logger.debug("Compacted in edge: {} --- t{} --> {}",
                    dag.nodes[pred]['name'], edgeMemFromPred, dag.nodes[chain.nodes[1]]['name'])
                dag.add_edge(pred, chain.nodes[1], edgeMem = edgeMemFromPred)
                store_succ(dag, pred, [chain.nodes[0]])
                dag.remove_node(chain.nodes[0])
                return 1
    # otherwise we cannot merge the nodes
    return 0
        
        
def compact_monotonic_chain_rec(dag, chain, maxFirst, reverse, extraPred=False):
    """Recursive auxiliary function to compact a chain.
    Similar to Liu's method for trees.
    /!\ Should not be called directly.

    :param dag: DAG containing the chain.
    :param chain: Chain to compact.
    :param maxFirst: If first node is a global maximum (considered minimum if False).
    :param reverse: If the chain should be read from the right side first.
    :param extraPred: Whether or not the chain contains the first fork node (only if reverse).
    :returns: Number of removed nodes.
    """
    if not reverse and extraPred:
        raise ValueError("extraPred should not be True if not reverse.")
    
    if len(chain.nodes) < 2+extraPred:
        return 0
    
    if len(chain.nodes) == 2+extraPred:
        schain = chain
        if extraPred:
            # if there was a predecessor we remove it, and read it next time if necessary
            schain = Chain(chain.nodes[1:], chain.outEdgeCosts[1:])
        return compact_two_nodes(dag, schain)

    logger.debug("Recursion call to compact monotonic chain.")
    
    idx = None
    nbNodeRemoved = 0
    if not reverse:
        # case of forward part of the chain, after global max (included)
        if maxFirst:
            idx = argmin(chain.outEdgeCosts, False)
        else:
            idx = argmax(chain.outEdgeCosts, False)
        if idx > 1:
            nbNodeRemoved = idx - 1
            dag.add_edge(chain.nodes[0], chain.nodes[idx], edgeMem = chain.outEdgeCosts[0])
            if maxFirst:
                store_succ(dag, chain.nodes[0], chain.nodes[1:idx])
            else:
                store_pred(dag, chain.nodes[1:idx], chain.nodes[idx])
            dag.remove_nodes_from(chain.nodes[1:idx])
            logger.debug("Compacted in edge: {} --- t{} --> {}",
                dag.nodes[chain.nodes[0]]['name'], chain.outEdgeCosts[0], dag.nodes[chain.nodes[idx]]['name'])
        else:
            idx = 1
        subchain = Chain(chain.nodes[idx:], chain.outEdgeCosts[idx:])
    elif reverse:
        # case of backward part of the chain, before global max (included)
        if maxFirst:
            idx = argmin(chain.outEdgeCosts, True)
        else:
            idx = argmax(chain.outEdgeCosts, True)
        if idx < len(chain.nodes) - 2:
            nbNodeRemoved = len(chain.nodes) - idx - 2
            dag.add_edge(chain.nodes[idx], chain.nodes[-1], edgeMem = chain.outEdgeCosts[idx])
            if maxFirst:
                store_pred(dag, chain.nodes[idx+1:-1], chain.nodes[-1])
            else:
                store_succ(dag, chain.nodes[idx], chain.nodes[idx+1:-1])                
            dag.remove_nodes_from(chain.nodes[idx+1:-1])
            logger.debug("Compacted in edge: {} --- t{} --> {}",
                dag.nodes[chain.nodes[idx]]['name'], chain.outEdgeCosts[idx], dag.nodes[chain.nodes[-1]]['name'])
        else:
            idx = -2
        subchain = Chain(chain.nodes[:idx+1], chain.outEdgeCosts[:idx+1])
    return nbNodeRemoved + compact_monotonic_chain_rec(dag, subchain, not maxFirst, reverse, extraPred)


def compact_asc_dsc(dag, chain):
    """Main function to compact a chain.
    Similar to Liu's method, but in both directions.
    /!\ Will remove nodes directly from the DAG, their index will
    be stored in remaining node attributes 'predNodes' and 'succNodes'.

    :param dag: DAG containing the chain with edge attributes 'edgeMem' set.
    :param chain: Chain to consider (with trailing edge cost).
    :returns: Number of removed nodes.
    """
    if len(chain.nodes) < 2:
        return 0
    if len(chain.nodes) == 2:
        return compact_two_nodes(dag, chain)

    nbNodeRemoved = 0
    # we split into two chains, the one after max (inclusive), and the one before max (inclusive)
    idxMax = argmax(chain.outEdgeCosts, True)
    logger.debug("Start recursive chain compaction.\nMax has index {}.",idxMax)    
    # ascending phase
    if idxMax > 0:
        logger.debug("Ascending phase")
        chain_asc = Chain(chain.nodes[:idxMax+1], chain.outEdgeCosts[:idxMax+1])
        extraPred = False
        startNode = chain.nodes[0]
        if len(dag.pred[startNode]) > 0:
            # this is not a root node, so we temporarily add a predecessor
            # this is not symmetric to the descending case because in such case
            # the last edge is always added (with 0 cost if leaf)
            extraPred = True
            pred = next(iter(dag.pred[startNode]))
            inEdgeMem = dag[pred][startNode]['edgeMem']
            chain_asc.nodes.insert(0, pred)
            chain_asc.outEdgeCosts.insert(0, inEdgeMem)
        nbNodeRemoved += compact_monotonic_chain_rec(dag, chain_asc, True, True, extraPred)
    # descending phase
    if idxMax < len(chain.nodes) - 1:
        logger.debug("Descending phase")
        chain_dsc = Chain(chain.nodes[idxMax:], chain.outEdgeCosts[idxMax:])
        nbNodeRemoved += compact_monotonic_chain_rec(dag, chain_dsc, True, False)
    return nbNodeRemoved
        

def compact_hill_valley(dag, chain):
    """Compact the alternating hill-valley segments of a chain 
    after having called the compact_chain operation.
    /!\ Will remove nodes directly from the DAG, their index will
    be stored in remaining node attributes 'predNodes' and 'succNodes'.

    :param dag: DAG containing the chain.
    :param chain: Chain to compact (valleys will be merged in hills).
    :returns: Number of removed nodes (about a half).
    """
    if len(chain.nodes) < 2:
        return 0
    nbNodeRemoved = 0
    stack_nodes = deque(chain.nodes)
    if dag.nodes[chain.nodes[0]]['nodeMemCost'] < 0:
        # we remove first node if a valley
        # should not happen if related optim applied first
        logger.debug("First chain node NEG")
        stack_nodes.popleft()
    elif dag.nodes[chain.nodes[1]]['nodeMemCost'] > 0:
        # we merge second node into first
        # should not happen at the beginning of the chain after compaction
        # because very first edge from fork is now considered
        # except if not starting from a fork (e.g. chain from source node)
        logger.debug("Second chain node POS")
        hill1 = stack_nodes.popleft()
        hill2 = stack_nodes.popleft()
        if len(dag.succ[hill2]) == 1:
            # there should not be more than 1 successor in the chain, but may be 0 if leaf
            succ = next(iter(dag.succ[hill2]))
            dag.add_edge(hill1, succ, edgeMem = dag[hill2][succ]['edgeMem'])
        dag.nodes[hill1]['nodeMemCost'] += dag.nodes[hill2]['nodeMemCost']
        dag.nodes[hill1]['tmpMemInc'] = dag.nodes[hill1]['nodeMemCost']
        store_succ(dag, hill1, [hill2])
        dag.remove_node(hill2)
        nbNodeRemoved += 1
        # and we put back first node into queue, because next one should be a valley
        stack_nodes.appendleft(hill1)
    while len(stack_nodes) >= 2:
        # now consider all remaining hill-valley segments
        hill = stack_nodes.popleft()
        valley = stack_nodes.popleft()
        hillMemCost = dag.nodes[hill]['nodeMemCost']
        valleyMemCost = dag.nodes[valley]['nodeMemCost']
        if not hillMemCost > 0 or not valleyMemCost < 0:
            raise ValueError("Not an hill-valley chain, call compact_chain first.")
        # keep hill and remove valley
        if len(dag.succ[valley]) == 1:
            # there should not be more than 1 successor in the chain, but may be 0 if leaf
            succ = next(iter(dag.succ[valley]))
            dag.add_edge(hill, succ, edgeMem = dag[valley][succ]['edgeMem'])
        dag.nodes[hill]['nodeMemCost'] += valleyMemCost
        dag.nodes[hill]['tmpMemInc'] = hillMemCost
        store_succ(dag, hill, [valley])
        dag.remove_node(valley)
        nbNodeRemoved += 1

    return nbNodeRemoved
        
        
def compact_chains(dag, hook=compact_asc_dsc):
    """Compact nodes in chains.
    /!\ Will remove nodes directly from the DAG, their index will
    be stored in remaining node attributes 'predNodes' and 'succNodes'.

    :param dag: DAG with default node attributes, 
    'edgeMem' set on edges for default transformation,
    and eventually 'nodeMemCost' set on nodes for others.
    :hook: Comapction algorithm to use, called on each chain,
    must return number of removed nodes.
    :returns: Number of removed nodes.
    """
    nbNodeRemoved = 0
    for chain in chains_generator(dag):
        if len(chain.nodes) > 1:
            logger.debug("Finished chain (length = {}).", len(chain.nodes))
            logger.debug(chain.strWNames(dag))
        nbNodeRemoved += hook(dag, chain)
    return nbNodeRemoved


def get_sync_components(dag):
    """Compute the node components of the DAG which are
    seraparated by synchronization frontiers
    (frontiers are complete bipartite subgraphs with all edges
    in the same direction).
    While this version uses closure and interval graphs,
    it is possible to use only transitive reduction and
    ASAP or ALAP sorting. In the same ASAP (resp. ALAP)
    rank, a synchronization frontier is characterized by
    all nodes having the same set of precessors (resp. succ.), 
    and this predecessors (resp. succ.) having only the ASAP 
    (resp. ALAP) rank set as successors (resp. predecessors).
    This does not change the overall cubic time complexity.

    :param dag: The DAG to analyze.
    :returns: A SortedDict of node componets as values,
    sorted by their maximum absolute node rank as keys.
    """
    
    nbNodes = len(dag.nodes())
    dag_closure = networkx.transitive_closure_dag(dag)
    dicoIntervals = dict()
    for node in dag_closure.nodes():
        asapRank = len(dag_closure.pred[node])
        alapRank = nbNodes - len(dag_closure.succ[node]) - 1
        interval = (asapRank, alapRank)
        dicoIntervals.setdefault(interval, set()).add(node)
    interval_graph = networkx.Graph()
    for interval, nodes in dicoIntervals.items():
        interval_graph.add_node(interval, nodes=nodes)
    # we cannot use networkx.interval_graph here because we need to store the related nodes
    # bu have adapted their implementation to build the interval graph from intervals
    tupled_intervals = [interval for interval in interval_graph.nodes()]
    while tupled_intervals:
        min1, max1 = interval1 = tupled_intervals.pop()
        for interval2 in tupled_intervals:
            min2, max2 = interval2
            if max1 >= min2 and max2 >= min1:
                interval_graph.add_edge(interval1, interval2)
    intCCs = networkx.connected_components(interval_graph)
    # now we sort the CCs by interval to get them in the right order
    orderedIntCCs = SortedDict()
    for cc in intCCs:
        maxRank = 0
        nodes = []
        for minNode, maxNode in cc:
            maxRank = max(maxRank, maxNode)
            nodes.extend(interval_graph.nodes[(minNode, maxNode)]['nodes'])
        orderedIntCCs[maxRank] = nodes
    return orderedIntCCs


def separate_sync_frontiers(dag, onlyNodeCost=False, offsetNewNodes=0):
    """This function separate a DAG according to synchronization frontiers
    found thanks to the interval graph of node topological ranks derived of the transitive closure.
    Each of newly created DAG can be scheduled independently.

    :param dag: DAG to separate (out of place), with edge attribute 'edgeMem' set.
    :param onlyNodeCost: If True, then do not add transitive edges from extra
    sources or to sinks with 'edgeMem' attribute and rely only on 'nodeMemCost'
    node attribute on sources and sinks.
    :param offsetNewNodes: Extra source and sink nodes are added with identifier
    greater than the maximum original node ids and this offset.
    :returns: List of disconnected DAG (in schedule order), each with an extra
    source node (even in first DAG) and sink node (even in last DAG)
    which can be removed while reconstructing schedule tanks to their name ('^' and '$').
    If no frontier and already an ST DAG, returns only the input graph.
    Node identifiers are copied from original graph, along with original
    node and edge attributes (shallow copy).
    """    
    orderedIntCCs = get_sync_components(dag)
    # if disconnected graphs are exactly the first and last node of
    # original graph, then we return it
    if len(orderedIntCCs) == 3 and \
       len(orderedIntCCs.peekitem(index=0)[1]) == 1 and \
       len(orderedIntCCs.peekitem(index=-1)[1]) == 1:
        return [dag]

    maxNodes = max(offsetNewNodes, max(dag.nodes())) + 1
    # now iterating over the ordered CCs to create unconnected graphs    
    disconnectedGraphs = list()
    nodesToCCdag = dict()
    for idxCC, nodes in enumerate(orderedIntCCs.values()):
        newCCdag = networkx.DiGraph()
        # copy nodes
        newCCdag.add_nodes_from(nodes)
        for node, dicoNode in newCCdag.nodes(data=True):
            nodesToCCdag[node] = idxCC
            dicoNode.update(dag.nodes[node])
        newSourceIdx = maxNodes + idxCC*2
        newSinkIdx = newSourceIdx + 1
        newCCdag.add_node(newSourceIdx, name='^',
                          nodeMemCost=0, tmpMemInc=0,
                          predNodes=[], succNodes=[])
        newCCdag.add_node(newSinkIdx, name='$',
                          nodeMemCost=0, tmpMemInc=0,
                          predNodes=[], succNodes=[])
        if not onlyNodeCost:
            newCCdag.add_edge(newSourceIdx, newSinkIdx, edgeMem=0)
        disconnectedGraphs.append(newCCdag)

    # copy edges
    for src, dst, dicoEdge in dag.edges(data=True):
        srcIdxCC = nodesToCCdag[src]
        dstIdxCC = nodesToCCdag[dst]
        edgeMem = dicoEdge['edgeMem']
        if srcIdxCC == dstIdxCC:
            disconnectedGraphs[srcIdxCC].add_edge(src, dst, **dicoEdge)
        elif not onlyNodeCost:
            # add edge towards the sink of src CC
            newSinkIdx = maxNodes + srcIdxCC*2 + 1
            srcDagCC = disconnectedGraphs[srcIdxCC]
            if srcDagCC.has_edge(src, newSinkIdx):
                srcDagCC[src][newSinkIdx]['edgeMem'] += edgeMem
            else:
                srcDagCC.add_edge(src, newSinkIdx, edgeMem = edgeMem)
            srcDagCC.nodes[newSinkIdx]['nodeMemCost'] -= edgeMem
            # add edge from the source of dst CC
            newSourceIdx = maxNodes + dstIdxCC*2
            dstDagCC = disconnectedGraphs[dstIdxCC]
            if dstDagCC.has_edge(newSourceIdx, dst):
                dstDagCC[newSourceIdx][dst]['edgeMem'] += edgeMem
            else:
                dstDagCC.add_edge(newSourceIdx, dst, edgeMem = edgeMem)
            dstDagCC.nodes[newSourceIdx]['nodeMemCost'] += edgeMem
            # add edge from source to sink of all intermediate CC
            for idxCC in range(srcIdxCC+1,dstIdxCC):
                newCCdag = disconnectedGraphs[idxCC]
                newSourceIdx = maxNodes + idxCC*2
                newSinkIdx = newSourceIdx + 1
                newCCdag.nodes[newSinkIdx]['nodeMemCost'] -= edgeMem
                newCCdag.nodes[newSourceIdx]['nodeMemCost'] += edgeMem
                newCCdag[newSourceIdx][newSinkIdx]['edgeMem'] += edgeMem

    if not onlyNodeCost:
        # remove first source and last sink, which are useless?
        # disconnectedGraphs[0].remove_node(maxNodes)
        # disconnectedGraphs[-1].remove_node(maxNodes+2*len(disconnectedGraphs)-1)
        # or at the opposite force link to first source and last sink
        for node, in_degree in dag.in_degree:
            if in_degree == 0:
                disconnectedGraphs[0].add_edge(maxNodes, node, edgeMem=0)
        for node, out_degree in dag.out_degree:
            if out_degree == 0:
                disconnectedGraphs[-1].add_edge(
                    node, maxNodes+2*len(disconnectedGraphs)-1, edgeMem=0)

    else:
        # update all source node cost, and add edges to them
        nextSourceMemCost = 0
        for idxCC, dagCC in enumerate(disconnectedGraphs[:-1]):
            # sum all nodeMemCost of current CC
            for _, nodeMemCost in dagCC.nodes(data='nodeMemCost'):
                nextSourceMemCost += nodeMemCost
            nextSourceIdx = maxNodes + (idxCC+1)*2
            nextSinkIdx = nextSourceIdx + 1
            dicoNextSrc = disconnectedGraphs[idxCC+1].nodes[nextSourceIdx]
            dicoNextSrc['nodeMemCost'] = nextSourceMemCost
            dicoNextSnk = disconnectedGraphs[idxCC+1].nodes[nextSinkIdx]
            dicoNextSnk['nodeMemCost'] = -nextSourceMemCost
        # add edges to source and sinks
        for idxCC, dagCC in enumerate(disconnectedGraphs):
            newSourceIdx = maxNodes + idxCC*2
            for node, in_degree in dagCC.in_degree:
                if in_degree == 0 and node != newSourceIdx:
                    dagCC.add_edge(newSourceIdx, node, edgeMem=0)
            newSinkIdx = newSourceIdx + 1
            for node, out_degree in dagCC.out_degree:
                if out_degree == 0 and node != newSinkIdx:
                    dagCC.add_edge(node, newSinkIdx, edgeMem=0)

    # update tmpMemInc for cbp: greater than nodeMemCost
    for idxCC, dagCC in enumerate(disconnectedGraphs):
        newSourceIdx = maxNodes + idxCC*2
        srcCost = dagCC.nodes[newSourceIdx]['nodeMemCost']
        dagCC.nodes[newSourceIdx]['tmpMemInc'] = max(0, srcCost)
        newSinkIdx = newSourceIdx + 1
        snkCost = dagCC.nodes[newSinkIdx]['nodeMemCost']
        dagCC.nodes[newSinkIdx]['tmpMemInc'] = max(0, snkCost)
                    
    return disconnectedGraphs
    

def remove_transitive_edges_pbc(dag, pbc_dag):
    """Remove redundant edges from a PBC dag by
    computing transitive reduction of its original CBP dag.

    :param dag: DAG with edge attribute 'edgeMem' set.
    :param pbc_dag: PBC version of the given DAG, where to remove edges.
    Must have been computed by convert_to_pbc(dag).
    :returns: The number of removed edges.
    """
    maxNodes = max(dag.nodes()) + 1
    reduced_dag = networkx.transitive_reduction(dag)
    nbRemovedTransitiveEdges = 0
    for src, dst, edgeMem in dag.edges(data='edgeMem'):
        if reduced_dag.has_edge(src, dst):
            continue
        # otherwise, we remove the edge and add its weight to another path
        shortestPath = networkx.astar_path(reduced_dag, src, dst)
        logger.debug("Removing edge: {} --> {}",
                     dag.nodes[src]['name'], dag.nodes[dst]['name'])
        for (srcP, dstP) in pairwise(shortestPath):
            pbc_dag[srcP+maxNodes][dstP]['edgeMem'] += edgeMem
            pbc_dag[dstP][dstP+maxNodes]['edgeMem'] += edgeMem
        # the last one should not be added in PBC
        # Python trick, last local indexes are kept outside the loop
        pbc_dag[dstP][dstP+maxNodes]['edgeMem'] -= edgeMem
        pbc_dag.remove_edge(src+maxNodes, dst)
        nbRemovedTransitiveEdges += 1
    return nbRemovedTransitiveEdges


def remove_transitive_edges_cbp(dag, transferOnPath=False):
    """Remove redundant edges from a CBP dag by
    computing transitive reduction.
    Lossy removal but harmless if reasoning on
    'nodeMemCost' and 'tmpMemInc' node attributes.

    :param dag: DAG with edge attribute 'edgeMem' set.
    :param transferOnPath: If True, then transfer the
    memory of removed edge on another path.
    :returns: The number of removed edges.
    """
    reduced_dag = networkx.transitive_reduction(dag)
    nbRemovedTransitiveEdges = 0
    copyEdges = [e for e in dag.edges()]
    for src, dst in copyEdges:
        if reduced_dag.has_edge(src, dst):
            continue
        # otherwise, we remove the edge
        edgeMem = dag[src][dst]['edgeMem']
        dag.remove_edge(src, dst)
        nbRemovedTransitiveEdges += 1
        # transfer memory
        if transferOnPath:
            shortestPath = networkx.astar_path(reduced_dag, src, dst)
            for (srcP, dstP) in pairwise(shortestPath):
                dag[srcP][dstP]['edgeMem'] += edgeMem
            
    return nbRemovedTransitiveEdges


def storeFirstInPredOfSecond(dag, node1, node2):
    """Store node1 in node2.
    node1 might be a predecessor of node2.
    node2 cannot be a predecessor of node1.

    :param dag: DAG containing the nodes, with edge attribute 'edgeMem' set.
    :param node1: Node to remove.
    :param node2: Node to keep.
    """
    store_pred(dag, [node1], node2)
    for pred1, dicoEdge in dag.pred[node1].items():
        if dag.has_edge(pred1, node2):
            dag[pred1][node2]['edgeMem'] += dicoEdge['edgeMem']
        else:
            dag.add_edge(pred1, node2, **dicoEdge)
    for succ1, dicoEdge in dag.succ[node1].items():
        if succ1 == node2:
            continue
        if dag.has_edge(node2, succ1):
            dag[node2][succ1]['edgeMem'] += dicoEdge['edgeMem']
        else:
            dag.add_edge(node2, succ1, **dicoEdge)
    dag.remove_node(node1)

    
def storeSecondInSuccOfFirst(dag, node1, node2):
    """Store node2 in node1.
    node1 might be a predecessor of node2.
    node2 cannot be a predecessor of node1.

    :param dag: DAG containing the nodes, with edge attribute 'edgeMem' set.
    :param node1: Node to keep.
    :param node2: Node to remove.
    """
    store_succ(dag, node1, [node2])
    for pred2, dicoEdge in dag.pred[node2].items():
        if pred2 == node1:
            continue
        if dag.has_edge(pred2, node1):
            dag[pred2][node1]['edgeMem'] += dicoEdge['edgeMem']
        else:
            dag.add_edge(pred2, node1, **dicoEdge)
    for succ2, dicoEdge in dag.succ[node2].items():
        if dag.has_edge(node1, succ2):
            dag[node1][succ2]['edgeMem'] += dicoEdge['edgeMem']
        else:
            dag.add_edge(node1, succ2, **dicoEdge)
    dag.remove_node(node2)


def remove_local_transitivity_cbp_1Dpred(dag, src, dst):
    """Remove the transitive edges introduced if
    clustering src with dst. Consider only preceeding
    edges of distance 1.

    :param dag: DAG with 'edgeMem' attribute set.
    :param src: Source node to cluster with dst.
    :param dst: Destination node to cluster with src.
    """
    for pred in dag.pred[src]:
        if dag.has_edge(pred, dst):
            edgeMem = dag[pred][dst]['edgeMem']
            dag.remove_edge(pred, dst)
            dag[pred][src]['edgeMem'] += edgeMem

            
def remove_local_transitivity_cbp_1Dsucc(dag, src, dst):
    """Remove the transitive edges introduced if
    clustering src with dst. Consider only succeeding
    edges of distance 1.

    :param dag: DAG with 'edgeMem' attribute set.
    :param src: Source node to cluster with dst.
    :param dst: Destination node to cluster with src.
    """
    for succ in dag.succ[dst]:
        if dag.has_edge(src, succ):
            edgeMem = dag[src][succ]['edgeMem']
            dag.remove_edge(src, succ)
            dag[dst][succ]['edgeMem'] += edgeMem

            
def remove_local_transitivity_cbp_2Dpred(dag, src, dst):
    """Remove the transitive edges introduced if
    clustering src with dst. Consider only preceeding
    edges of distance 2.

    :param dag: DAG with 'edgeMem' attribute set.
    :param src: Source node to cluster with dst.
    :param dst: Destination node to cluster with src.
    """
    lp1 = list(dag.pred[dst])
    lp1.remove(src)
    lp2 = list(dag.pred[src])
    for p in lp1:
        for pp in dag.pred[p]:
            if pp in lp2:
                edgeMem = dag[pp][src]['edgeMem']
                dag.remove_edge(pp, src)
                lp2.remove(pp)
                dag[pp][p]['edgeMem'] += edgeMem
                dag[p][dst]['edgeMem'] += edgeMem

                
def remove_local_transitivity_cbp_2Dsucc(dag, src, dst):
    """Remove the transitive edges introduced if
    clustering src with dst. Consider only succeeding
    edges of distance 2.

    :param dag: DAG with 'edgeMem' attribute set.
    :param src: Source node to cluster with dst.
    :param dst: Destination node to cluster with src.
    """
    ls1 = list(dag.succ[src])
    ls1.remove(dst)
    ls2 = list(dag.succ[dst])
    for s in ls1:
        for ss in dag.succ[s]:
            if ss in ls2:
                edgeMem = dag[dst][ss]['edgeMem']
                dag.remove_edge(dst, ss)
                ls2.remove(ss)
                dag[src][s]['edgeMem'] += edgeMem
                dag[s][ss]['edgeMem'] += edgeMem


def remove_local_transitivity_cbp_1d2d(dag, src, dst):
    """Remove the transitive edges introduced if
    clustering src with dst.

    :param dag: DAG with 'edgeMem' attribute set.
    :param src: Source node to cluster with dst.
    :param dst: Destination node to cluster with src.
    """
    remove_local_transitivity_cbp_1Dpred(dag, src, dst)
    remove_local_transitivity_cbp_1Dsucc(dag, src, dst)
    remove_local_transitivity_cbp_2Dpred(dag, src, dst)
    remove_local_transitivity_cbp_2Dsucc(dag, src, dst)

    
def nodeSerieOptim_aux(dag, nodes, retNodesToRecheck = False):
    """Compact nodes in serie following the three cases of Pascal Fradet.
    /!\ Will remove nodes directly from the DAG, their index will
    be stored in remaining node attributes 'predNodes' and 'succNodes'.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', modified by transformations.
    :param nodes: Node collection to check.
    :param retNodesToRecheck: (default False) Whether or not
    recording neighbors of updated nodes, to recheck.
    :returns: Tuple of removed nodes and neighbors to recheck
    (if option activated, empty otherwise).
    """
    neighborNodes = set()
    removedNodes = set()
    for node in nodes:
        if not dag.has_node(node):
            continue
        succs = dag.succ[node]
        preds = dag.pred[node]
        dicoNode = dag.nodes[node]
        currentNodeMemCost = dicoNode['nodeMemCost']
        currentTmpMemInc = dicoNode['tmpMemInc']
        if len(succs) == 1:
            # will store B in succ of A
            # A == node ; B == succ
            succ = next(iter(succs))
            succNodeMemCost = dag.nodes[succ]['nodeMemCost']
            succTmpMemInc = dag.nodes[succ]['tmpMemInc']
            predsOfSucc = dag.pred[succ]
            nextPeak = currentNodeMemCost + succTmpMemInc
            cond1 = currentNodeMemCost >= 0 and currentTmpMemInc <= nextPeak
            cond2 = succNodeMemCost <= 0 and currentTmpMemInc >= nextPeak
            if len(predsOfSucc) == 1 and (cond1 or cond2):
                # case *-> A --> B *->
                dicoNode['nodeMemCost'] += succNodeMemCost
                dicoNode['tmpMemInc'] = max(currentTmpMemInc, nextPeak)
                storeSecondInSuccOfFirst(dag, node, succ)
                removedNodes.add(succ)
                if retNodesToRecheck:
                    neighborNodes.add(node)
                    neighborNodes.update(dag.pred[node])
                    neighborNodes.update(dag.succ[node])
            if len(predsOfSucc) > 1 and cond1:
                # case *-> A --> B <*-*>
                dicoNode['nodeMemCost'] += succNodeMemCost
                dicoNode['tmpMemInc'] = nextPeak
                # optimization to remove new transitive edges
                if retNodesToRecheck:
                    neighborNodes.update(dag.pred[node])
                remove_local_transitivity_cbp_1d2d(dag, node, succ)
                # finally merge the two nodes
                storeSecondInSuccOfFirst(dag, node, succ)
                removedNodes.add(succ)
                if retNodesToRecheck:
                    neighborNodes.add(node)
                    neighborNodes.update(dag.pred[node])
                    neighborNodes.update(dag.succ[node])
        # might have been updated in previous cases
        currentNodeMemCost = dicoNode['nodeMemCost']
        currentTmpMemInc = dicoNode['tmpMemInc']
        if len(preds) == 1:
            # will store A in pred of B
            # A == pred ; B == node
            pred = next(iter(preds))
            predNodeMemCost = dag.nodes[pred]['nodeMemCost']
            predTmpMemInc = dag.nodes[pred]['tmpMemInc']
            nextPeak = predNodeMemCost + currentTmpMemInc
            cond2 = currentNodeMemCost <= 0 and predTmpMemInc >= nextPeak
            if cond2:
                # case <*-*> A --> B *->
                dicoNode['nodeMemCost'] += predNodeMemCost
                dicoNode['tmpMemInc'] = predTmpMemInc
                # optimization to remove new transitive edges
                if retNodesToRecheck:
                    neighborNodes.update(dag.succ[node])
                remove_local_transitivity_cbp_1d2d(dag, pred, node)
                #finally merge the two nodes
                storeFirstInPredOfSecond(dag, pred, node)
                removedNodes.add(pred)
                if retNodesToRecheck:
                    neighborNodes.add(node)
                    neighborNodes.update(dag.pred[node])
                    neighborNodes.update(dag.succ[node])

    if retNodesToRecheck:
        neighborNodes.difference_update(removedNodes)
    return (removedNodes, neighborNodes)


def nodeSerieOptim(dag, recursion=False):
    """Compact nodes in serie following the three cases of Pascal Fradet.
    /!\ Will remove nodes directly from the DAG, their index will
    be stored in remaining node attributes 'predNodes' and 'succNodes'.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', modified by transformations.
    :param recursion: (default False) If True, recall itself
    on neighbors of compacted nodes.
    :returns: Number of removed nodes.
    """
    # copy node list because we may remove nodes while iterating
    removedNodes, neighborNodes = nodeSerieOptim_aux(
        dag, [x for x in dag.nodes()], recursion)
    nbNodeRemoved = len(removedNodes)
    if not recursion:
        return nbNodeRemoved
    while neighborNodes:
        logger.debug("Start serie optimization on {} nodes, removed {} nodes.",
                    len(neighborNodes), nbNodeRemoved)
        removedNodes, neighborNodes = nodeSerieOptim_aux(
            dag, [x for x in dag.nodes()], recursion)
        nbNodeRemoved += len(removedNodes)
    logger.debug("Finished serie optimization, processed {} nodes.", nbNodeRemoved)
    return nbNodeRemoved
                    

def nodeParallelOptim(dag):
    """Compact nodes in serie following the three cases of Pascal Fradet.
    /!\ Will remove nodes directly from the DAG, their index will
    be stored in remaining node attributes 'predNodes' and 'succNodes'.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    /!\ Needs topological ranks.
    :returns: Number of removed nodes.
    """
    nbNodeRemoved = 0
    ## check for pred
    dicoAsap = get_ordered_subsets(dag, 'asapTopoRank')
    for nodes in dicoAsap.values():
        for node1 in nodes:
            if not dag.has_node(node1):
                continue
            preds1 = set(dag.pred[node1])
            dicoNode1 = dag.nodes[node1]
            for node2 in nodes:
                if not dag.has_node(node2) or node1 <= node2:
                    continue
                preds2 = set(dag.pred[node2])
                if preds1 != preds2:
                    continue
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                dicoNode2 = dag.nodes[node2]
                secondNodeMemCost = dicoNode2['nodeMemCost']
                secondTmpMemInc = dicoNode2['tmpMemInc']
                # in the following cases, we compact only
                # if succ node does not increase the peak
                # and is negative / A is node1
                # kept node has highest peak
                peakAB = max(firstTmpMemInc, secondTmpMemInc + firstNodeMemCost)
                peakBA = max(secondTmpMemInc, firstTmpMemInc + secondNodeMemCost)
                if peakAB < peakBA:
                    # case AB < BA, keep A
                    if secondNodeMemCost <= 0 and firstTmpMemInc >= \
                       secondTmpMemInc + firstNodeMemCost:
                        dicoNode1['nodeMemCost'] += secondNodeMemCost
                        dicoNode1['alapTopoRank'] = min(
                            dicoNode1['alapTopoRank'],
                            dicoNode2['alapTopoRank'])
                        storeSecondInSuccOfFirst(dag, node1, node2)
                        nbNodeRemoved += 1
                elif peakAB > peakBA:
                    # case BA < AB, keep B
                    if firstNodeMemCost <= 0 and secondTmpMemInc >= \
                       firstTmpMemInc + secondNodeMemCost:
                        dicoNode2['nodeMemCost'] += firstNodeMemCost
                        dicoNode2['alapTopoRank'] = min(
                            dicoNode1['alapTopoRank'],
                            dicoNode2['alapTopoRank'])
                        storeSecondInSuccOfFirst(dag, node2, node1)
                        nbNodeRemoved += 1
                        # we break because node1 has been removed
                        break
                else:
                    # case AB = BA, keep A
                    if secondNodeMemCost <= 0 and firstTmpMemInc >= \
                       secondTmpMemInc + firstNodeMemCost and \
                       firstNodeMemCost <= 0 and secondTmpMemInc >= \
                       firstTmpMemInc + secondNodeMemCost:
                        dicoNode1['nodeMemCost'] += secondNodeMemCost
                        dicoNode1['alapTopoRank'] = min(
                            dicoNode1['alapTopoRank'],
                            dicoNode2['alapTopoRank'])
                        storeSecondInSuccOfFirst(dag, node1, node2)
                        nbNodeRemoved += 1
    ## check for succ
    dicoAlap = get_ordered_subsets(dag, 'alapTopoRank')
    for nodes in dicoAlap.values():
        for node1 in nodes:
            if not dag.has_node(node1):
                continue
            succs1 = set(dag.succ[node1])
            dicoNode1 = dag.nodes[node1]
            for node2 in nodes:
                if not dag.has_node(node2) or node1 <= node2:
                    continue
                succs2 = set(dag.succ[node2])
                if succs1 != succs2:
                    continue
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                dicoNode2 = dag.nodes[node2]
                secondNodeMemCost = dicoNode2['nodeMemCost']
                secondTmpMemInc = dicoNode2['tmpMemInc']
                # in the following cases, we compact only
                # if prec node does not increase the peak
                # and is positive / A is node1
                # kept node has highest peak
                peakAB = max(firstTmpMemInc, secondTmpMemInc + firstNodeMemCost)
                peakBA = max(secondTmpMemInc, firstTmpMemInc + secondNodeMemCost)
                if peakAB < peakBA:
                    # case AB < BA, keep B
                    if firstNodeMemCost >= 0 and firstTmpMemInc <= \
                       secondTmpMemInc + firstNodeMemCost:
                        dicoNode2['nodeMemCost'] += firstNodeMemCost
                        dicoNode2['tmpMemInc'] += firstNodeMemCost
                        dicoNode2['asapTopoRank'] = max(
                            dicoNode1['asapTopoRank'],
                            dicoNode2['asapTopoRank'])
                        storeFirstInPredOfSecond(dag, node1, node2)
                        nbNodeRemoved += 1
                        # we break because node1 has been removed
                        break
                elif peakAB > peakBA:
                    # case BA < AB, keep A
                    if secondNodeMemCost >= 0 and secondTmpMemInc <= \
                       firstTmpMemInc + secondNodeMemCost:
                        dicoNode1['nodeMemCost'] += secondNodeMemCost
                        dicoNode1['tmpMemInc'] += secondNodeMemCost
                        dicoNode1['asapTopoRank'] = max(
                            dicoNode1['asapTopoRank'],
                            dicoNode2['asapTopoRank'])
                        storeFirstInPredOfSecond(dag, node2, node1)
                        nbNodeRemoved += 1
                else:
                    # case AB = BA, keep A
                    if firstNodeMemCost >= 0 and firstTmpMemInc <= \
                       secondTmpMemInc + firstNodeMemCost and \
                       secondNodeMemCost >= 0 and secondTmpMemInc <= \
                       firstTmpMemInc + secondNodeMemCost:
                        dicoNode1['nodeMemCost'] += secondNodeMemCost
                        dicoNode1['tmpMemInc'] += secondNodeMemCost
                        dicoNode1['asapTopoRank'] = max(
                            dicoNode1['asapTopoRank'],
                            dicoNode2['asapTopoRank'])
                        storeFirstInPredOfSecond(dag, node2, node1)
                        nbNodeRemoved += 1
    return nbNodeRemoved


def nodeParToSeqOptim(dag):
    """Sequentialize nodes originally in parallel.
    /!\ Will add some edges. Transitive edges are removed.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    /!\ Needs topological ranks.
    :returns: Number of sequentialized nodes.
    """
    nodeSeq = set()
    ## check for pred
    dicoAsap = get_ordered_subsets(dag, 'asapTopoRank')
    for nodes in dicoAsap.values():
        for node1 in nodes:
            if node1 in nodeSeq:
                continue
            preds1 = set(dag.pred[node1])
            dicoNode1 = dag.nodes[node1]
            for node2 in nodes:
                if node2 in nodeSeq or node1 == node2:
                    continue
                preds2 = set(dag.pred[node2])
                if preds1 != preds2:
                    continue
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                dicoNode2 = dag.nodes[node2]
                secondTmpMemInc = dicoNode2['tmpMemInc']
                if firstNodeMemCost <= 0 and \
                   firstTmpMemInc <= secondTmpMemInc:
                    # we can sequentialize node1 --> node2
                    # regarding equations, A is node1
                    # first remove pred to node2 edges
                    totEdgeMemToNode2 = 0
                    for pred in preds2:
                        totEdgeMemToNode2 += dag[pred][node2]['edgeMem']
                        dag.remove_edge(pred, node2)
                    # update mem from last pred (if any) to node1
                    if preds2:
                        dag[pred][node1]['edgeMem'] += totEdgeMemToNode2
                    # add sequentialization edge
                    dag.add_edge(node1, node2, edgeMem=totEdgeMemToNode2)
                    nodeSeq.add(node2)
                    # update topo ranks
                    dicoNode2['asapTopoRank'] += 1
                    dicoNode1['alapTopoRank'] = min(
                            dicoNode1['alapTopoRank'],
                            dicoNode2['alapTopoRank'] - 1)
                
    ## check for succ
    dicoAlap = get_ordered_subsets(dag, 'alapTopoRank')
    for nodes in dicoAlap.values():
        for node1 in nodes:
            if node1 in nodeSeq:
                continue
            succs1 = set(dag.succ[node1])
            dicoNode1 = dag.nodes[node1]
            for node2 in nodes:
                if node2 in nodeSeq or node1 == node2:
                    continue
                succs2 = set(dag.succ[node2])
                if succs1 != succs2:
                    continue
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                dicoNode2 = dag.nodes[node2]
                secondNodeMemCost = dicoNode2['nodeMemCost']
                secondTmpMemInc = dicoNode2['tmpMemInc']
                if firstNodeMemCost >= 0 and \
                   firstNodeMemCost + secondTmpMemInc >= \
                   secondNodeMemCost + firstTmpMemInc:
                    # we can sequentialize node2 --> node1
                    # regarding equations, B is node1
                    # first remove succ of node2 edges
                    totEdgeMemFromNode2 = 0
                    for succ in succs2:
                        totEdgeMemFromNode2 += dag[node2][succ]['edgeMem']
                        dag.remove_edge(node2, succ)
                    # update mem from node1 to last succ (if any)
                    if succs2:
                        dag[node1][succ]['edgeMem'] += totEdgeMemFromNode2
                    # add sequentialization edge
                    dag.add_edge(node2, node1, edgeMem=totEdgeMemFromNode2)
                    nodeSeq.add(node2)
                    # update topo ranks
                    dicoNode2['alapTopoRank'] -= 1
                    dicoNode1['asapTopoRank'] = max(
                            dicoNode1['asapTopoRank'],
                            dicoNode2['asapTopoRank'] + 1)
    return len(nodeSeq)



def nodeParToSeqPred(dag, node1, node2, samePreds):
    """If possible, sequentialize node1 to node2, having same predecessors.

    :param dag: Dag containing nodes.
    :param node2: Node to test
    :param node1: Node to test.
    :param samePreds: Set of common direct predecessors.
    :returns: True if the two nodes were sequentialized.
    """
    dicoNode1 = dag.nodes[node1]
    firstNodeMemCost = dicoNode1['nodeMemCost']
    firstTmpMemInc = dicoNode1['tmpMemInc']
    dicoNode2 = dag.nodes[node2]
    secondTmpMemInc = dicoNode2['tmpMemInc']
    if firstNodeMemCost <= 0 and \
       firstTmpMemInc <= secondTmpMemInc:
        # we can sequentialize node1 --> node2
        # regarding equations, A is node1
        # first remove pred to node2 edges
        totEdgeMemToNode2 = 0
        for pred in samePreds:
            edgeMemToNode2 = dag[pred][node2]['edgeMem']
            totEdgeMemToNode2 += edgeMemToNode2
            dag[pred][node1]['edgeMem'] += edgeMemToNode2
            dag.remove_edge(pred, node2)
        # add sequentialization edge
        # logger.debug("Sequentialize (pred) {} --> {}", dag.nodes[node1]['name'], dag.nodes[node2]['name'])
        dag.add_edge(node1, node2, edgeMem=totEdgeMemToNode2)
        return True
    return False


def nodeParToSeqSucc(dag, node2, node1, sameSuccs):
    """If possible, sequentialize node2 to node1, having same successors.

    :param dag: Dag containing nodes.
    :param node2: Node to test
    :param node1: Node to test.
    :param sameSuccs: Set of common direct successors.
    :returns: True if the two nodes were sequentialized.
    """
    dicoNode1 = dag.nodes[node1]
    firstNodeMemCost = dicoNode1['nodeMemCost']
    firstTmpMemInc = dicoNode1['tmpMemInc']
    dicoNode2 = dag.nodes[node2]
    secondNodeMemCost = dicoNode2['nodeMemCost']
    secondTmpMemInc = dicoNode2['tmpMemInc']
    if firstNodeMemCost >= 0 and \
       firstNodeMemCost + secondTmpMemInc >= \
       secondNodeMemCost + firstTmpMemInc:
        # we can sequentialize node2 --> node1
        # regarding equations, B is node1
        # first remove succ of node2 edges
        totEdgeMemFromNode2 = 0
        for succ in sameSuccs:
            edgeMemFromNode2 = dag[node2][succ]['edgeMem']
            totEdgeMemFromNode2 += edgeMemFromNode2
            dag[node1][succ]['edgeMem'] += edgeMemFromNode2
            dag.remove_edge(node2, succ)
        # add sequentialization edge
        # logger.debug("Sequentialize (succ) {} --> {}", dag.nodes[node2]['name'], dag.nodes[node1]['name'])
        dag.add_edge(node2, node1, edgeMem=totEdgeMemFromNode2)
        return True
    return False


def nodeParToSeqOptimChain(dag, transferOnPath=False):
    """Merge parallel chains thanks to node simple node sequentialization.
    /!\ Will add some edges. Transitive edges are always avoided for SPDAG only.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    :param transferOnPath: If True, then transfer the
    memory of removed edge on another path.
    :returns: Number of sequentialized nodes.
    """
    dicoChains = dict()
    for chain in chains_generator(dag):
        preds = dag.pred[chain.nodes[0]]
        succs = dag.succ[chain.nodes[-1]]
        tupl = (frozenset(preds),frozenset(succs))
        dicoChains.setdefault(tupl,[]).append(chain)

    nbSeq = 0    
    # merge chains two by two
    for ((preds,succs), chains) in dicoChains.items():
        if len(chains) % 2 == 1:
            # we discard the last chain:
            # it cannot be merged with another one
            chains = chains[:-1]
        if not chains:
            continue
        for i in range(0,len(chains),2):
            chain1 = chains[i]
            chain2 = chains[i+1]
            samePreds = preds
            sameSuccs = succs
            while chain1.nodes and chain2.nodes:
                if samePreds:
                    node1 = chain1.nodes[0]
                    node2 = chain2.nodes[0]
                    if nodeParToSeqPred(dag, node1, node2, samePreds):
                        samePreds = [chain1.popFirst()]
                        nbSeq += 1
                    elif nodeParToSeqPred(dag, node2, node1, samePreds):
                        samePreds = [chain2.popFirst()]
                        nbSeq += 1
                if chain1.nodes and chain2.nodes and sameSuccs:
                    node1 = chain1.nodes[-1]
                    node2 = chain2.nodes[-1]
                    if nodeParToSeqSucc(dag, node1, node2, sameSuccs):
                        sameSuccs = [chain2.popLast()]
                        nbSeq += 1
                    elif nodeParToSeqSucc(dag, node2, node1, sameSuccs):
                        sameSuccs = [chain1.popLast()]
                        nbSeq += 1
             
    # remove transitive edges around chains
    for chain in chains_generator(dag):
        preds = dag.pred[chain.nodes[0]]
        succs = dag.succ[chain.nodes[-1]]
        if len(preds) == 1 and len(succs) == 1:
            pred = next(iter(preds))
            succ = next(iter(succs))
            if dag.has_edge(pred, succ):
                if transferOnPath:
                    edgeMem = dag[pred][succ]['edgeMem']
                    dag[pred][chain.nodes[0]]['edgeMem'] += edgeMem
                    dag[chain.nodes[-1]][succ]['edgeMem'] += edgeMem                    
                    for src,dst in pairwise(chain.nodes):
                        dag[src][dst]['edgeMem'] += edgeMem
                dag.remove_edge(pred, succ)
                    
    return nbSeq
        


def nodeParToSeqOptimGen(dag, checkTransitivity=False):
    """Sequentialize nodes originally in parallel.
    /!\ Will add some edges. Transitive edges are not always avoided.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    /!\ Needs topological ranks.
    :param checkTransitivity: (default False) Whether or not transitivity
    is checked before applying transformation (cost of one call to
    networkx.has_path(dag, src, dst) per checked pair of nodes).
    /!\ Advised only if not performing global transitive reduction after.
    :returns: Number of sequentialized nodes.
    """
    nodeSeq = set()
    ## check for pred
    (nodesAsap,indexesAsap) = get_ordered_keylist(dag, 'asapTopoRank')
    for start,end in pairwise(indexesAsap) :
        for node1 in nodesAsap[start:end]:
            if node1 in nodeSeq:
                continue
            preds1 = set(dag.pred[node1])
            dicoNode1 = dag.nodes[node1]
            for node2 in nodesAsap[start:]:
                if node2 in nodeSeq or node1 == node2:
                    continue
                preds2 = set(dag.pred[node2])
                if not preds1 <= preds2 or node1 in preds2:
                    continue
                if checkTransitivity and not preds1 == preds2 and \
                   networkx.has_path(dag, node1, node2):
                    # in such case there is already an ordering constraint
                    # there is no need to sequentialize
                    continue
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                dicoNode2 = dag.nodes[node2]
                secondTmpMemInc = dicoNode2['tmpMemInc']
                if firstNodeMemCost <= 0 and \
                   firstTmpMemInc <= secondTmpMemInc:
                    # we can sequentialize node1 --> node2
                    # regarding equations, A is node1
                    # first remove pred to node2 edges
                    totEdgeMemToNode2 = 0
                    for pred in preds1:
                        edgeMemToNode2 = dag[pred][node2]['edgeMem']
                        totEdgeMemToNode2 += edgeMemToNode2
                        dag[pred][node1]['edgeMem'] += edgeMemToNode2
                        dag.remove_edge(pred, node2)
                    # add sequentialization edge
                    # logger.debug("Sequentialize (pred) {} --> {}", dag.nodes[node1]['name'], dag.nodes[node2]['name'])
                    dag.add_edge(node1, node2, edgeMem=totEdgeMemToNode2)
                    nodeSeq.add(node2)
                    # update topo ranks
                    dicoNode2['asapTopoRank'] += 1
                    dicoNode1['alapTopoRank'] = min(
                            dicoNode1['alapTopoRank'],
                            dicoNode2['alapTopoRank'] - 1)
                
    ## check for succ
    (nodesAlap,indexesAlap) = get_ordered_keylist(dag, 'alapTopoRank')
    for end,start in pairwise(indexesAlap[::-1]) :
        for node1 in nodesAlap[start:end]:
            if node1 in nodeSeq:
                continue
            succs1 = set(dag.succ[node1])
            dicoNode1 = dag.nodes[node1]
            for node2 in nodesAlap[:end]:
                if node2 in nodeSeq or node1 == node2:
                    continue
                succs2 = set(dag.succ[node2])
                if not succs1 <= succs2 or node1 in succs2:
                    continue
                if checkTransitivity and not succs1 == succs2 and \
                   networkx.has_path(dag, node2, node1):
                    # in such case there is already an ordering constraint
                    # there is no need to sequentialize
                    continue
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                dicoNode2 = dag.nodes[node2]
                secondNodeMemCost = dicoNode2['nodeMemCost']
                secondTmpMemInc = dicoNode2['tmpMemInc']
                if firstNodeMemCost >= 0 and \
                   firstNodeMemCost + secondTmpMemInc >= \
                   secondNodeMemCost + firstTmpMemInc:
                    # we can sequentialize node2 --> node1
                    # regarding equations, B is node1
                    # first remove succ of node2 edges
                    totEdgeMemFromNode2 = 0
                    for succ in succs1:
                        edgeMemFromNode2 = dag[node2][succ]['edgeMem']
                        totEdgeMemFromNode2 += edgeMemFromNode2
                        dag[node1][succ]['edgeMem'] += edgeMemFromNode2
                        dag.remove_edge(node2, succ)
                    # add sequentialization edge
                    # logger.debug("Sequentialize (succ) {} --> {}", dag.nodes[node2]['name'], dag.nodes[node1]['name'])
                    dag.add_edge(node2, node1, edgeMem=totEdgeMemFromNode2)
                    nodeSeq.add(node2)
                    # update topo ranks
                    dicoNode2['alapTopoRank'] -= 1
                    dicoNode1['asapTopoRank'] = max(
                            dicoNode1['asapTopoRank'],
                            dicoNode2['asapTopoRank'] + 1)
    return len(nodeSeq)


def nodeParToSeqOptimGenIter(dag, checkTransitivity=False):
    """Sequentialize nodes originally in parallel.
    /!\ Will add some edges. Transitive edges are not always avoided.
    /!\ Iterate until no possible sequentialization.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    /!\ Needs topological ranks.
    :param checkTransitivity: (default False) Whether or not transitivity
    is checked before applying transformation (cost of one call to
    networkx.has_path(dag, src, dst) per checked pair of nodes).
    /!\ Advised only if not performing global transitive reduction after.
    :returns: Number of sequentialized nodes.
    """
    nbSeqTot = 0
    nbSeq = 1
    while nbSeq > 0:
        topoSortASAPandALAP(dag)
        nbSeq = nodeParToSeqOptim(dag)
        #nbSeq = nodeParToSeqOptimGen(dag, checkTransitivity)
        nbSeqTot += nbSeq
    return nbSeqTot


def nodeParToSeqOptimGen3(dag, closure=None):
    """Sequentialize nodes originally in parallel. Cubic time version.
    /!\ Will add some edges. New transitive edges are never avoided.
    /!\ Update the closure if provided.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    /!\ Needs topological ranks.
    :param closure: (default None) Transitive closure of the DAG.
    If none, descendants and ancesorts will be computed online.
    :returns: Number of sequentialized nodes.
    """
    nodeSeq = set()
    ## check for pred
    (nodesAsap,indexesAsap) = get_ordered_keylist(dag, 'asapTopoRank')
    for start,end in pairwise(indexesAsap) :
        for node1 in nodesAsap[start:end]:
            if node1 in nodeSeq:
                continue
            preds1 = set(dag.pred[node1])
            dicoNode1 = dag.nodes[node1]
            firstNodeMemCost = dicoNode1['nodeMemCost']
            firstTmpMemInc = dicoNode1['tmpMemInc']
            for node2 in nodesAsap[start:]:
                if node2 in nodeSeq or node1 == node2:
                    continue
                if closure is None:
                    ances2 = networkx.ancestors(dag, node2)
                else:
                    ances2 = set(closure.pred[node2])
                if not preds1 <= ances2 or node1 in ances2:
                    continue
                dicoNode2 = dag.nodes[node2]
                secondTmpMemInc = dicoNode2['tmpMemInc']
                if firstNodeMemCost <= 0 and \
                   firstTmpMemInc <= secondTmpMemInc:
                    # we can sequentialize node1 --> node2
                    # regarding equations, A is node1
                    # logger.debug("Sequentialize (pred) {} --> {}", dag.nodes[node1]['name'], dag.nodes[node2]['name'])
                    dag.add_edge(node1, node2, edgeMem=0)
                    nodeSeq.add(node2)
                    # update closure
                    if not (closure is None):
                        closure.add_edge(node1, node2)
                        for succ in closure.succ[node2]:
                            closure.add_edge(node1, succ)
                    # update topo ranks
                    dicoNode2['asapTopoRank'] += 1
                    dicoNode1['alapTopoRank'] = min(
                            dicoNode1['alapTopoRank'],
                            dicoNode2['alapTopoRank'] - 1)
                
    ## check for succ
    (nodesAlap,indexesAlap) = get_ordered_keylist(dag, 'alapTopoRank')
    for end,start in pairwise(indexesAlap[::-1]) :
        for node1 in nodesAlap[start:end]:
            if node1 in nodeSeq:
                continue
            succs1 = set(dag.succ[node1])
            dicoNode1 = dag.nodes[node1]
            firstNodeMemCost = dicoNode1['nodeMemCost']
            firstTmpMemInc = dicoNode1['tmpMemInc']
            for node2 in nodesAlap[:end]:
                if node2 in nodeSeq or node1 == node2:
                    continue
                if closure is None:
                    descs2 = networkx.descendants(dag, node2)
                else:
                    descs2 = set(closure.succ[node2])
                if not succs1 <= descs2 or node1 in descs2:
                    continue
                dicoNode2 = dag.nodes[node2]
                secondNodeMemCost = dicoNode2['nodeMemCost']
                secondTmpMemInc = dicoNode2['tmpMemInc']
                if firstNodeMemCost >= 0 and \
                   firstNodeMemCost + secondTmpMemInc >= \
                   secondNodeMemCost + firstTmpMemInc:
                    # we can sequentialize node2 --> node1
                    # regarding equations, B is node1
                    # logger.debug("Sequentialize (succ) {} --> {}", dag.nodes[node2]['name'], dag.nodes[node1]['name'])
                    dag.add_edge(node2, node1, edgeMem=0)
                    nodeSeq.add(node2)
                    # update closure
                    if not (closure is None):
                        closure.add_edge(node2, node1)
                        for pred in closure.pred[node2]:
                            closure.add_edge(pred, node1)
                    # update topo ranks
                    dicoNode2['alapTopoRank'] -= 1
                    dicoNode1['asapTopoRank'] = max(
                            dicoNode1['asapTopoRank'],
                            dicoNode2['asapTopoRank'] + 1)
    return len(nodeSeq)


def nodeParToSeqOptimGen3_opt(dag):
    """Sequentialize nodes originally in parallel. Cubic time version
    with optimized memory (no global transitive closure).
    /!\ Will add some edges. New transitive edges are never avoided.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    /!\ Needs topological ranks (will be updated)
    :returns: Number of sequentialized nodes.
    """
    nodeSeq = set()
    ## check for pred
    dicoPrevPred = dict()
    (nodesAsap,indexesAsap) = get_ordered_keylist(dag, 'asapTopoRank')
    for start,end in pairwise(indexesAsap) :
        for node2 in nodesAsap[start:end]:
            ## online computation of transitive closure
            ances2 = set(dag.pred[node2])
            for pred in dag.pred[node2]:
                prevPred = dicoPrevPred.get(pred, set())
                ances2.update(prevPred)
            dicoPrevPred[node2] = ances2
            ## then regular part
            if node2 in nodeSeq:
                continue
            dicoNode2 = dag.nodes[node2]
            secondTmpMemInc = dicoNode2['tmpMemInc']
            for node1 in nodesAsap[:end]:
                if node1 in nodeSeq or node1 == node2:
                    continue
                preds1 = set(dag.pred[node1])
                if not preds1 <= ances2 or node1 in ances2:
                    continue
                dicoNode1 = dag.nodes[node1]
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                if firstNodeMemCost <= 0 and \
                   firstTmpMemInc <= secondTmpMemInc:
                    # we can sequentialize node1 --> node2
                    # regarding equations, A is node1
                    # logger.debug("Sequentialize (pred) {} --> {}", dag.nodes[node1]['name'], dag.nodes[node2]['name'])
                    dag.add_edge(node1, node2, edgeMem=0)
                    nodeSeq.add(node2)
                    # update closure
                    ances1 = dicoPrevPred.get(node1,
                                              networkx.ancestors(dag, node1))
                    ances2.update(ances1)
                    ances2.add(node1)
                    listSuccToUpdate = list(dag.succ[node2])
                    while listSuccToUpdate:
                        desc = listSuccToUpdate.pop()
                        if desc in dicoPrevPred:
                            dicoPrevPred[desc].update(ances2)
                            listSuccToUpdate.extend(dag.succ[desc])
                    break
        ## clean dicoPrevPred
        nodesToRemove = []
        for nodeD in dicoPrevPred.keys():
            allSuccPresent = True
            for succ in dag.succ[nodeD]:
                if not (succ in dicoPrevPred):
                    allSuccPresent = False
                    break
            if allSuccPresent:
                nodesToRemove.append(nodeD)
        for nodeD in nodesToRemove:
            dicoPrevPred.pop(nodeD)
        
    topoSortASAPandALAP(dag)
    ## check for succ
    dicoPrevSucc = dict()
    (nodesAlap,indexesAlap) = get_ordered_keylist(dag, 'alapTopoRank')
    for end,start in pairwise(indexesAlap[::-1]) :
        for node2 in nodesAlap[start:end]:
            ## online computation of transitive closure
            descs2 = set(dag.succ[node2])
            for succ in dag.succ[node2]:
                prevSucc = dicoPrevSucc.get(succ, set())
                descs2.update(prevSucc)
            dicoPrevSucc[node2] = descs2
            ## then regular part
            if node2 in nodeSeq:
                continue
            dicoNode2 = dag.nodes[node2]
            secondNodeMemCost = dicoNode2['nodeMemCost']
            secondTmpMemInc = dicoNode2['tmpMemInc']
            for node1 in nodesAlap[start:]:
                if node1 in nodeSeq or node1 == node2:
                    continue
                succs1 = set(dag.succ[node1])
                if not succs1 <= descs2 or node1 in descs2:
                    continue
                dicoNode1 = dag.nodes[node1]
                firstNodeMemCost = dicoNode1['nodeMemCost']
                firstTmpMemInc = dicoNode1['tmpMemInc']
                if firstNodeMemCost >= 0 and \
                   firstNodeMemCost + secondTmpMemInc >= \
                   secondNodeMemCost + firstTmpMemInc:
                    # we can sequentialize node2 --> node1
                    # regarding equations, B is node1
                    # logger.debug("Sequentialize (succ) {} --> {}", dag.nodes[node2]['name'], dag.nodes[node1]['name'])
                    dag.add_edge(node2, node1, edgeMem=0)
                    nodeSeq.add(node2)
                    # update closure
                    descs1 = dicoPrevSucc.get(node1,
                                              networkx.descendants(dag, node1))
                    descs2.update(descs1)
                    descs2.add(node1)
                    listPredToUpdate = list(dag.pred[node2])
                    while listPredToUpdate:
                        asc = listPredToUpdate.pop()
                        if asc in dicoPrevSucc:
                            dicoPrevSucc[asc].update(descs2)
                            listPredToUpdate.extend(dag.pred[asc])
                    break
        ## clean dicoPrevSucc
        nodesToRemove = []
        for nodeD in dicoPrevSucc.keys():
            allPredPresent = True
            for pred in dag.pred[nodeD]:
                if not (pred in dicoPrevSucc):
                    allPredPresent = False
                    break
            if allPredPresent:
                nodesToRemove.append(nodeD)
        for nodeD in nodesToRemove:
            dicoPrevSucc.pop(nodeD)

    topoSortASAPandALAP(dag)
    return len(nodeSeq)



def addSortedSuccsBFS(dag, node, slist, nodeTupleFunc):
    """Subroutine for BFS-like giving sorted list of ready nodes.
    /!\ This method is different from the one in .dag_analysis:
    - it operates on sorted list;
    - it contains the (outer) for loop.

    :param dag: DAG to consider, set node attribute 'nbVisits'.
    :param node: Node having successors to visit/add.
    :param slist: SortedList to eventually add successor nodes (if completely visited).
    :param nodeTupleFunc: Function returning node tuple as in list.
    Returned tuple must start with the node itself.
    Function takes as arguments the node and its attribute dictionnary.
    """
    for succ in dag.succ[node]:
        dicoNode = dag.nodes[succ]
        nbVisits = dicoNode['nbVisits'] + 1
        dicoNode['nbVisits'] = nbVisits
        if nbVisits == dag.in_degree[succ]:
            slist.add(nodeTupleFunc(succ, dicoNode))
            dicoNode['nbVisits'] = 0


def scheduleWithSort(dag, getSortNodeTuple, sortKeyNodeTuple):
    """Schedule a dag by peeking last element of the reay list
    each time.

    :param dag: DAG. 
    :param getSortNodeTuple: Function returning node tuple attributes. 
    :param sortKeyNodeTuple: Function to sort the ready list.
    :returns: Ordering.
    """
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    # set the main ready list
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    readyNodesTSLreversed = SortedList(key = sortKeyNodeTuple)
    for node in firstNodes:
        readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
    ordering = []
    # perform BFS like
    while readyNodesTSLreversed:
        tupl = readyNodesTSLreversed.pop()
        node = tupl[0]
        ordering.append(node)
        addSortedSuccsBFS(dag, node, readyNodesTSLreversed, getSortNodeTuple)
    if len(ordering) != len(dag.nodes()):
        raise Exception("Illegal state: odering was not complete.")
    return ordering    


def seqCommonImpact_deprecated(dag, closure=None):
    """Sequentialize nodes originally in parallel. Cubic time version.
    /!\ Will add some edges. Transitive edges are never created.
    /!\ Do not update the closure.
    /!\ Not formally proven yet. Currently rely on a wrong scheduling method:
    scheduleWithSort is not optimal, it should be an exhaustive method
    giving best memory sorted schedule. Either exhaustive_BandB or
    brute_force_dag_min_mem with 'computeBestSrt' option activated.

    :param dag: DAG with default node attributes, especially 
    'nodeMemCost' and 'tmpMemInc', set by transformations.
    :param closure: (default None) Transitive closure of the DAG.
    If none, will be recomputed.
    :returns: Number of sequentialized nodes.
    """
    # first compute transitive closure
    # and define schedule sort keys
    nbSeqNodes = 0
    if closure is None:
        closure = networkx.transitive_closure_dag(dag)
    def getSortNodeTuple(node, dicoNode):
        """Return the tuple needed to sort a node.

        :param node: Node to consider.
        :param dicoNode: Dico of node attributes.
        :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc')
        """
        return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'])
    def sortKeyNodeTuplePos(tupl):
        """Key to sort node tuple by highest
        drop last (peak minus impact), then minimum impact.

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        return ((tupl[2] - tupl[1]), tupl[1])
    def sortKeyNodeTupleNeg(tupl):
        """Key to sort node tuple by lowest
        peak last, then minimum impact.

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        return (-tupl[2], -tupl[1])

    ## case of positive impact first
    ## group all positive nodes with same negative descendants 
    dicoNegSuccs = {}
    for node, nodeMemCost in dag.nodes(data='nodeMemCost'):
        if nodeMemCost >= 0:
            negDsc = list()
            for dsc in closure.succ[node]:
                if dag.nodes[dsc]['nodeMemCost'] < 0:
                    negDsc.append(dsc)
            negDsc = frozenset(negDsc)
            otherPos = dicoNegSuccs.setdefault(negDsc, set())
            otherPos.add(node)
    ## now process all groups with more than a node
    for posSameNeg in dicoNegSuccs.values():
        subdag = getSubDAG(dag, posSameNeg)
        ccs = networkx.weakly_connected_components(subdag)
        for cc in ccs:
            if len(cc) > 1:
                subdag_cc = getSubDAG(subdag, cc)
                nbNodes = len(subdag_cc.nodes())
                nbEdges = len(subdag_cc.edges())
                if nbEdges > nbNodes - 1:
                    # then it is not a chain
                    suborder = scheduleWithSort(subdag_cc, getSortNodeTuple, sortKeyNodeTuplePos)
                    for (u,v) in pairwise(suborder):
                        if not dag.has_edge(u, v):
                            dag.add_edge(u, v, edgeMem=0)
                            nbSeqNodes += 1
    ## case of negative impact first
    ## group all negative nodes with same positive ancestors
    dicoPosPreds = {}
    for node, nodeMemCost in dag.nodes(data='nodeMemCost'):
        if nodeMemCost <= 0:
            negAsc = list()
            for asc in closure.pred[node]:
                if dag.nodes[asc]['nodeMemCost'] > 0:
                    negAsc.append(asc)
            negAsc = frozenset(negAsc)
            otherNeg = dicoPosPreds.setdefault(negAsc, set())
            otherNeg.add(node)
    ## now process all groups with more than a node
    for negSamePos in dicoPosPreds.values():
        subdag = getSubDAG(dag, negSamePos)
        ccs = networkx.weakly_connected_components(subdag)
        for cc in ccs:
            if len(cc) > 1:
                subdag_cc = getSubDAG(subdag, cc)
                nbNodes = len(subdag_cc.nodes())
                nbEdges = len(subdag_cc.edges())
                if nbEdges > nbNodes - 1:
                    # then it is not a chain
                    suborder = scheduleWithSort(subdag_cc, getSortNodeTuple, sortKeyNodeTupleNeg)
                    for (u,v) in pairwise(suborder):
                        if not dag.has_edge(u, v):
                            dag.add_edge(u, v, edgeMem=0)
                            nbSeqNodes += 1
    return nbSeqNodes

         
