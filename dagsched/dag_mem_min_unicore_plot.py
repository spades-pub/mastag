#! /bin/bash/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from .utils import *
from .dag_gen import *
from .dag_analysis import *
from .dag_transform import *
from .dag_mem_min_unicore_algo import *
from .dag_mem_min_unicore_expe import *
from .sdf_transform import *

import random
import networkx
import bracelogger
logger = bracelogger.get_logger("dagsched_log")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from sortedcontainers import SortedList, SortedDict


# other personal folders:
## /home/ahonorat/Documents/dev/insa/sandbox-preesm/sdf3ToPiMM/sdf/AdnanFiles
## /home/ahonorat/Documents/dev/tea/adfg/examples/sdf3/sdf
PATH_SATELLITE = "./sdf3_inputs/satellite_adfg.xml"
PATH_SATELLITE_M = "./sdf3_inputs/satellite_m.xml"


SEED = 17831


def plotSDFparetoPEG(maxFirings=0, minRatioFiringsMore=0.1):
    """Plot graph of memory peaks in function of nbFirings in PEG.
    /!\ May take time.

    :param maxFirings: Maximum number of firings to consider. 
    :param minRatioFiringsMore: Ratio in ]0, 1[ of firings
    more than previous PEG.
    """
    if minRatioFiringsMore >= 1 or minRatioFiringsMore <= 0:
        raise ValueError("minRatioFiringsMore must be in ]0, 1[")
    #dagSDF = parseSdf3(PATH_SATELLITE)
    #dagSDF = badSDFfiringReduction()
    #dagSDF = badSDFfiringReduction2()
    dagSDF = gen_filterbank_fixed(2,1,3,2,3)

    nbFiringsTot = setSDFrepetitionVector(dagSDF)
    set_actor_cost(dagSDF)
    set_sdf_tmpMemInc(dagSDF)
    for node, dicoNode in dagSDF.nodes(data=True):
        dicoNode['divisors'] = divisors(dicoNode['nbRepeat'])
        dicoNode['divisorIdx'] = 0

    allDicoPEG = reduceSDFfirings(dagSDF, 1, recordAllDivs=True, optimalSteps=True)
    if maxFirings <= 0:
        maxFirings = allDicoPEG.keys()[-1]
    dicoHeurPEG = SortedDict()
    dicoMemdagPEG = SortedDict()
    dicoBandbPEG = SortedDict()
    dicoOptPEG = SortedDict()
    prevNbFirings = 0
    
    ## now schedule each PEG
    for nbFirings, dicoPEG in allDicoPEG.items():
        if nbFirings > maxFirings:
            break
        if nbFirings < (1+minRatioFiringsMore) * prevNbFirings:
            continue
        prevNbFirings = nbFirings
        for node, newDivIdx in dicoPEG.items():
            update_actor_divisor(dagSDF, node, newDivIdx)
        dag = convert_sdf_to_srsdf(dagSDF)
        set_default_attributes(dag)
        set_node_cost(dag)
        #set_pbc_tmpMemInc(dag)
        set_peg_tmpMemInc(dag, dagSDF)
        logger.info("==> Scheduling with {} firings ...", nbFirings)

        # topoSortASAPandALAP(dag)
        # plotMemDAG(dag, 'DAG -- PEG({})'.format(nbFirings), False)


        schedRes_heur = heuristic_linear_1dot5steps_impact(dag)
        dicoHeurPEG[nbFirings] = schedRes_heur.dicoRes

        # schedRes_memdag = memdagSPschedule(dag, **{'ensureSPdag': False, 'convertTmpMemInc': True})
        # schedRes_memdag.dicoRes['refinedMemdagPeak'] = mpeak_purecbp_memdag(dag, schedRes_memdag.dicoRes['ordering'])
        # dicoMemdagPEG[nbFirings] = schedRes_memdag.dicoRes

        # edgeMemSum = sum([edgeMem for _,_,edgeMem in dag.edges().data('edgeMem')])
        # schedRes_BandB = exhaustive_BandB_V2(dag, **{
        #     'minMemUpperBound': edgeMemSum + 1,
        #     'timeout': 1, 'step': 4, 'overlap': 2})
        # dicoBandbPEG[nbFirings] = schedRes_BandB.dicoRes
        
        
        KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
                  'local_clustering': True, # local optims serie/parallel
                  'global_sequentialization': True, # global optim parallel
                  'impact_sequentialization': False, # node impact optim
                  'transitivity_red': True, # removes transitive edges
                  'sync_separation_rec': False, # recursive dag separation
                  'sync_separation_end': True, # fallback dag separation
                  'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                  'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                  'fallback': exhaustive_BandB_V2, # after all optims

                  'faster': True, # option for brute_force_dag_min_mem
                  'timeout': 10, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                  'minMemUpperBound': schedRes_heur.dicoRes['memPeak'],
                  'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal)
                  'ensureSPdag': False, # option for memdagSPschedule
                  'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering)
        }
        schedRes_opt = rec_optims_dag_min_mem(dag, **KWargs)
        dicoOptPEG[nbFirings] = schedRes_opt.dicoRes

    ## now plot graph
    firings = [val for val in dicoOptPEG.keys()]
    peaksHeur = [dico['memPeak'] for dico in dicoHeurPEG.values()]
    plt.plot(firings, peaksHeur, label="Memory peaks heur.", marker='x')
    # peaksPureMemdag = [dico['memPeak'] for dico in dicoMemdagPEG .values()]
    # plt.plot(firings, peaksPureMemdag, label="Memory peaks p. memdag", marker='x')
    # peaksRefinedMemdag = [dico['refinedMemdagPeak'] for dico in dicoMemdagPEG.values()]
    # plt.plot(firings, peaksRefinedMemdag, label="Memory peaks r. memdag", marker='x')
    # peaksSteppedBandB = [dico['memPeak'] for dico in dicoBandbPEG.values()]
    # plt.plot(firings, peaksSteppedBandB, label="Memory peaks s. BandB", marker='x')
    peaksOpt = [dico['memPeak'] for dico in dicoOptPEG.values()]
    plt.plot(firings, peaksOpt, label="Memory peaks BandB", marker='.')
    plt.xlabel('#firings')
    plt.ylabel('memory peak')
    plt.grid(True)
    plt.legend()
    plt.show()
    #plt.savefig('paretoSDF.pdf', bbox_inches='tight')
    #plt.clf()

    
def plotPartialBandB(dStep=1,dOverlap=1):
    """Schedule optim

    :param dStep: Step increase between tries.
    :param dOverlap: Overlap increase between tries.
    """
    dagSDF = gen_filterbank(2,3,7,4,7)
    dag = sdfToDAG(dagSDF, nbFiringsObj=0, optimalRepetition=False)
    set_default_attributes(dag)
    set_node_cost(dag)
    set_pbc_tmpMemInc(dag)

    # schedule optimally first
    KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
              'local_clustering': True, # local optims serie/parallel
              'global_sequentialization': True, # global optim parallel
              'impact_sequentialization': False, # node impact optim
              'transitivity_red': True, # removes transitive edges
              'sync_separation_rec': False, # recursive dag separation
              'sync_separation_end': False, # fallback dag separation
              'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
              'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
              'fallback': exhaustive_BandB_V2, # after all optims

              'faster': True, # option for brute_force_dag_min_mem
              'timeout': 200, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
              'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal)
              'ensureSPdag': False, # option for memdagSPschedule
              'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering)
              }
    schedRes = rec_optims_dag_min_mem(dag, **KWargs)
    minPeak = schedRes.dicoRes['memPeak']
    termDAG = schedRes.dicoRes['listSepTermDAG'][0]

    # initializes result 2D array
    nbNodesTot = len(termDAG.nodes())
    # the ugly mgrid version:
    # stepsX,overlapsY = np.mgrid[slice(0, nbNodesTot, dStep),slice(0, nbNodesTot, dOverlap)]
    # peaks = np.full((nbNodesTot//dStep, nbNodesTot//dOverlap), minPeak, dtype=int)
    # runtimes = np.zeros((nbNodesTot//dStep, nbNodesTot//dOverlap), dtype=float)

    stepsX = []
    overlapsY = []
    peaks = []
    runtimes = []
    
    maxX = 0
    maxY = 0
    
    # now iterate over all steps/overlaps
    for x in range(1, nbNodesTot, dStep):
        logger.info("==> Scheduling step/overlap: {}/{}", x, 0)
        KWargs['step'] = x
        KWargs['overlap'] = 0
        schedRes = exhaustive_BandB_V2(termDAG, **KWargs)
        peak = schedRes.dicoRes['memPeak']
        runtime = schedRes.dicoRes['time_search']
        # peaks[x, 0] = peak
        # runtimes[x, 0] = runtime
        stepsX.append(x)
        overlapsY.append(0)
        peaks.append(peak)
        runtimes.append(runtime)
        maxX = max(x, maxX)
        if peak == minPeak:
            break
        
        for y in range(1, x, dOverlap):
            logger.info("==> Scheduling step/overlap: {}/{}", x, y)
            KWargs['overlap'] = y
            schedRes = exhaustive_BandB_V2(termDAG, **KWargs)
            peak = schedRes.dicoRes['memPeak']
            runtime = schedRes.dicoRes['time_search']
            # peaks[x, y] = peak
            # runtimes[x, y] = runtime
            stepsX.append(x)
            overlapsY.append(y)
            peaks.append(peak)
            runtimes.append(runtime)
            maxY = max(y, maxY)
            if peak == minPeak:
                break

    # plot curves
    color_map = plt.cm.viridis ## color maps: PiYG, PRGn, viridis, reversed with _r
    fig, axs = plt.subplots(nrows=1, num="Peaks") ## , figsize=(10,7)
    axs.set_aspect('equal', adjustable='box')
    ## https://stackoverflow.com/questions/30914462/how-to-force-integer-tick-labels
    axs.xaxis.set_major_locator(MaxNLocator(integer=True))
    axs.yaxis.set_major_locator(MaxNLocator(integer=True))

    ## https://stackoverflow.com/questions/14432557/scatter-plot-with-different-text-at-each-data-point
    for i, peak in enumerate(peaks):
        if peak == minPeak:
            axs.annotate("Best", (stepsX[i]+0.1, overlapsY[i]+0.1))
            axs.annotate("{:.1E}".format(runtimes[i]), (stepsX[i]+0.1, overlapsY[i]-0.3))

            
    # the ugly mgrid version:
    # maxPeak = peaks.max()
    # for x in range(1, nbNodesTot, dStep):
    #     for y in range(x+1, nbNodesTot, dOverlap):
    #         peaks[x,y] = minPeak
    # plt.pcolormesh(stepsX, overlapsY, peaks, cmap=color_map, vmax=maxPeak, vmin=minPeak)
    # plt.xlim([1,maxX])
    # plt.ylim([0,maxY])



    # https://stackoverflow.com/questions/14827650/pyplot-scatter-plot-marker-size
    normCM_r = plt.Normalize(vmax=max(runtimes), vmin=min(runtimes))
    plt.scatter(stepsX, overlapsY, s=150, c=runtimes, cmap=color_map, norm = normCM_r, marker='o')

    normCM_p = plt.Normalize(vmax=max(peaks), vmin=minPeak)
    plt.scatter(stepsX, overlapsY, s=20, c=peaks, cmap=color_map, norm = normCM_p, marker='o')

    sm_r = plt.cm.ScalarMappable(cmap=color_map, norm=normCM_r)
    color_bar_r = fig.colorbar(sm_r, location = 'left', label='Runtime (sec) - around', shrink=0.6)
    # https://stackoverflow.com/questions/16826754/change-the-labels-of-a-colorbar-from-increasing-to-decreasing-values
    color_bar_r.ax.invert_xaxis()
    
    sm_p = plt.cm.ScalarMappable(cmap=color_map, norm=normCM_p)
    color_bar_p = fig.colorbar(sm_p, location = 'right', label='Memory peak - center', shrink=0.6, ticks=MaxNLocator(integer=True))
    color_bar_p.ax.invert_xaxis()
    
    plt.xlabel('step')
    plt.ylabel('overlap')
    plt.show()


def plotComplexityDAGsolving():
    """Plot runtime un function of number of nodes for three
    kinds of graphs (tree, SP dag, and branched SP dag).
    """
    
    KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
              'local_clustering': True, # local optims serie/parallel
              'global_sequentialization': False, # global optim parallel
              'impact_sequentialization': False, # node impact optim
              'transitivity_red': False, # removes transitive edges
              'sync_separation_rec': False, # recursive dag separation
              'sync_separation_end': False, # fallback dag separation
              'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
              'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
              'fallback': exhaustive_BandB_V2, # after all optims

              'faster': True, # option for brute_force_dag_min_mem
              'timeout': 1, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
              'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal)
              'ensureSPdag': False, # option for memdagSPschedule
              'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering)
    }

    fig, axs = plt.subplots(nrows=1, num="complexity") ## , figsize=(10,7)
    axs.xaxis.set_major_locator(MaxNLocator(integer=True))

    ## plot trees first
    
    stepBranchs = 1
    stepChainNodes = 4
    nbBranchs = 2
    nbChainNodes = 2
    maxNodes = 5500
    maxDag = 10
    dicoResMemdag = SortedDict()
    dicoResTrees = SortedDict()
    nbNodes = nbBranchs * nbChainNodes + 1
    
    while (nbNodes < maxNodes):
        maxMem = 1 + nbNodes
        for dag, offset in generator_branched_dag(nbBranchs, nbChainNodes,
                                                  generateBranchedTree,
                                                  maxMem, maxMem, maxDag, SEED):

            dag_copy = copy_graph_solver(dag)
            schedRes_memdag = memdagSPschedule(dag_copy, **{'ensureSPdag': True, 'convertTmpMemInc': True})
            dicoResMemdag.setdefault(nbNodes, []).append(schedRes_memdag.dicoRes)

            schedRes_tree = rec_optims_dag_min_mem(dag, **KWargs)
            dicoResTrees.setdefault(nbNodes, []).append(schedRes_tree.dicoRes)
            
        nbBranchs += stepBranchs
        nbChainNodes += stepChainNodes
        nbNodes = nbBranchs * nbChainNodes + 1

    nbNodesX = [val for val in dicoResTrees.keys()]
    
    runtimesMemdag = []
    for liste in dicoResMemdag.values():
        runtimes = [dico['time_search'] for dico in liste]
        avg = np.average(runtimes)
        runtimesMemdag.append(avg)
        # runtimesMemdag.append(max(runtimes))        
    plt.scatter(nbNodesX, runtimesMemdag, label="Runtime trees [memdag]", marker='.', color='orange')

    runtimesY = []
    for liste in dicoResTrees.values():
        runtimes = [dico['time_total'] for dico in liste]
        avg = np.average(runtimes)
        runtimesY.append(avg)
        # runtimesY.append(max(runtimes))
    plt.scatter(nbNodesX, runtimesY, label="Runtime trees [ours]", marker='.', color='blue')

    trend = np.polyfit(nbNodesX, runtimesY, deg=2)
    trendpoly = np.poly1d(trend)
    coefs_trend = ["{:.1E}".format(c) for c in trendpoly.coeffs]
    plt.plot(nbNodesX,trendpoly(nbNodesX), label="Tree poly fit:\n{}".format(coefs_trend), color='blue')

    ## plot split/join second
    
    stepDepth = 1
    depth = 1
    maxNodes = 5500
    maxDag = 10
    dicoResMemdag = SortedDict()
    dicoResSJ = SortedDict()
    nbNodes = 8
    
    while (nbNodes < maxNodes):
        maxMem = 1 + nbNodes
        for dag, offset in generator_splitjoin_dag(depth, maxMem, maxMem, maxDag, SEED+2):
            
            dag_copy = copy_graph_solver(dag)
            schedRes_memdag = memdagSPschedule(dag_copy, **{'ensureSPdag': True, 'convertTmpMemInc': True})
            dicoResMemdag.setdefault(nbNodes, []).append(schedRes_memdag.dicoRes)
            
            schedRes_sj = rec_optims_dag_min_mem(dag, **KWargs)
            dicoResSJ.setdefault(nbNodes, []).append(schedRes_sj.dicoRes)
            
        depth += stepDepth
        nbNodes = 2 * (nbNodes + 2)
            
    nbNodesX = [val for val in dicoResSJ.keys()]

    runtimesMemdag = []
    for liste in dicoResMemdag.values():
        runtimes = [dico['time_search'] for dico in liste]
        avg = np.average(runtimes)
        runtimesMemdag.append(avg)
        # runtimesMemdag.append(max(runtimes))        
    plt.scatter(nbNodesX, runtimesMemdag, label="Runtime SJ [memdag]", marker='x', color='orange')

    runtimesY = []
    for liste in dicoResSJ.values():
        runtimes = [dico['time_total'] for dico in liste]
        avg = np.average(runtimes)
        runtimesY.append(avg)
        # runtimesY.append(max(runtimes))
    plt.scatter(nbNodesX, runtimesY, label="Runtime SJ [ours]", marker='x', color='blue')

    trend = np.polyfit(nbNodesX, runtimesY, deg=3)
    trendpoly = np.poly1d(trend)
    coefs_trend = ["{:.1E}".format(c) for c in trendpoly.coeffs]
    plt.plot(nbNodesX,trendpoly(nbNodesX), label="SJ poly fit:\n{}".format(coefs_trend), color='blue')


    ## plot cxseps third
    
    # stepBranchs = 1
    # stepChainNodes = 1
    # nbBranchs = 2
    # nbChainNodes = 1
    # maxNodes = 50
    # maxDag = 10
    # dicoResCxSeps = SortedDict()
    # nbNodes = nbBranchs * ((nbBranchs-1)*2 + nbChainNodes) + 2
    
    # while (nbNodes < maxNodes):
    #     maxMem = 1 + nbNodes
    #     for dag, offset in generator_branched_dag(nbBranchs, nbChainNodes,
    #                                               generateSeparableDAG,
    #                                               maxMem, maxMem, maxDag, SEED):
    #         schedRes_cxsep = rec_optims_dag_min_mem(dag, **KWargs)
    #         dicoResCxSeps.setdefault(nbNodes, []).append(schedRes_cxsep.dicoRes)
    #     nbBranchs += stepBranchs
    #     nbChainNodes += stepChainNodes
    #     nbNodes = nbBranchs * ((nbBranchs-1)*2 + nbChainNodes) + 2
    
    # nbNodesX = [val for val in dicoResCxSeps.keys()]
    # runtimesY = []
    # for liste in dicoResCxSeps.values():
    #     runtimes = [dico['time_total'] for dico in liste]
    #     avg = np.average(runtimes)
    #     runtimesY.append(avg)
    #     # runtimesY.append(max(runtimes))
    # plt.scatter(nbNodesX, runtimesY, label="Runtime CxSeps", marker='*')

    # trend = np.polyfit(nbNodesX, runtimesY, deg=4)
    # trendpoly = np.poly1d(trend)
    # coefs_trend = ["{:.1E}".format(c) for c in trendpoly.coeffs]
    # plt.plot(nbNodesX,trendpoly(nbNodesX), label="CxSep poly fit:\n{}".format(coefs_trend))

    
    ### plot

    plt.xlabel('#nodes')
    plt.ylabel('runtime (sec)')
    plt.legend()
    plt.grid(True)
    plt.show()


def plotRatioOpt():
    """Plot ratio of optimal results for BandB only and all optims + BandB.
    """
    KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
              'local_clustering': True, # local optims serie/parallel
              'global_sequentialization': True, # global optim parallel
              'impact_sequentialization': False, # node impact optim
              'transitivity_red': True, # removes transitive edges
              'sync_separation_rec': False, # recursive dag separation
              'sync_separation_end': True, # fallback dag separation
              'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
              'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
              'fallback': exhaustive_BandB_V2, # after all optims

              'faster': True, # option for brute_force_dag_min_mem
              'timeout': 10, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
              'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal)
              'ensureSPdag': False, # option for memdagSPschedule
              'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering)
    }

    fig, axs = plt.subplots(nrows=1, num="complexity") ## , figsize=(10,7)
    axs.xaxis.set_major_locator(MaxNLocator(integer=True))

    ## plot BandB only first
    
    stepNodes = 10
    nbNodes = 10
    maxNodes = 100
    maxDag = 20
    edgeProbability = 0.5
    timeout = 1
    dicoResBandB = SortedDict()
    
    while (nbNodes <= maxNodes):
        maxMem = 1 + nbNodes
        for dag, offset in generator_random_dag(nbNodes, edgeProbability,
                                                maxMem, maxMem, maxDag, SEED):
            edgeMemSum = sum([edgeMem for _,_,edgeMem in dag.edges().data('edgeMem')])
            schedRes_BandB = exhaustive_BandB_V2(dag, **{
                'minMemUpperBound': maxMem + edgeMemSum + 1,
                'timeout': timeout})
            dicoResBandB.setdefault(nbNodes, []).append(schedRes_BandB.dicoRes)
        nbNodes += stepNodes

    nbNodesX = [val for val in dicoResBandB.keys()]
    ratiosY = []
    for liste in dicoResBandB.values():
        ## ratio of optimally solved dag
        opt = 0
        for dico in liste:
            if not dico.get('minMemPeakAllOrdering') is None:
                opt += 1
        ratio = opt / len(liste)
        ratiosY.append(ratio)
        
    plt.plot(nbNodesX, ratiosY, label="% opt. BandB only", marker='.')

    ## plot optim + BandB second
    
    stepNodes = 50
    nbNodes = 50
    maxNodes = 500
    maxDag = 20
    edgeProbability = 0.5
    timeout = 1
    dicoResAllOpt = SortedDict()
    
    while (nbNodes <= maxNodes):
        maxMem = 1 + nbNodes
        for dag, offset in generator_random_dag(nbNodes, edgeProbability,
                                                maxMem, maxMem, maxDag, SEED):
            schedRes_allOpt = rec_optims_dag_min_mem(dag, **KWargs)
            dicoResAllOpt.setdefault(nbNodes, []).append(schedRes_allOpt.dicoRes)
        nbNodes += stepNodes

    nbNodesX = [val for val in dicoResAllOpt.keys()]
    ratiosY = []
    for nbNodes, liste in dicoResAllOpt.items():
        ## ratio of optimally solved dag
        opt = 0
        for dico in liste:
            if not dico.get('minMemPeakAllOrdering') is None:
                opt += 1
        ratio = opt / len(liste)
        ratiosY.append(ratio)
        
    plt.plot(nbNodesX, ratiosY, label="% opt. red.+BandB", marker='.')
    
    ### plot

    plt.xlabel('#nodes')
    plt.ylabel('ratio in [0,1]')
    plt.legend()
    plt.grid(True)
    plt.show()


def plotRatioRed():
    """Plot ratio of reduced nodes in terminal DAG.
    """
    KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
              'local_clustering': True, # local optims serie/parallel
              'global_sequentialization': True, # global optim parallel
              'impact_sequentialization': False, # node impact optim
              'transitivity_red': True, # removes transitive edges
              'sync_separation_rec': False, # recursive dag separation
              'sync_separation_end': False, # fallback dag separation
              'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
              'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
              'fallback': heuristic_linear_1dot5steps_impact, # after all optims

              'faster': True, # option for brute_force_dag_min_mem
              'timeout': 10, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
              'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal)
              'ensureSPdag': False, # option for memdagSPschedule
              'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering)
    }

    fig, axs = plt.subplots(nrows=1, num="complexity") ## , figsize=(10,7)
    axs.xaxis.set_major_locator(MaxNLocator(integer=True))

    ## plot random dag first
    
    stepNodes = 50
    nbNodes = 50
    maxNodes = 250
    maxDag = 20
    edgeProbability = 0.5
    timeout = 1
    dicoResAllOpt = SortedDict()
    
    while (nbNodes <= maxNodes):
        maxMem = 1 + nbNodes
        for dag, offset in generator_random_dag(nbNodes, edgeProbability,
                                                maxMem, maxMem, maxDag, SEED):
            schedRes_allOpt = rec_optims_dag_min_mem(dag, **KWargs)
            dicoResAllOpt.setdefault(nbNodes, []).append(schedRes_allOpt.dicoRes)
        nbNodes += stepNodes

    nbNodesX = [val for val in dicoResAllOpt.keys()]
    ratiosY = []
    for nbNodes, liste in dicoResAllOpt.items():
        ## ratio of reduced nodes
        nbNodesTerm = 0
        for dico in liste:
            for dag in dico['listSepTermDAG']:
                nbNodesTerm += len(dag.nodes())
        ratio = nbNodesTerm / (nbNodes * len(liste))
        ratiosY.append(ratio)
        
    plt.plot(nbNodesX, ratiosY, label="% term. nodes random", marker='x')


    ## also plot minimal ratio (1 term node)

    stepNodes = 50
    nbNodes = 50
    maxNodes = 250

    nbNodesX = []
    ratiosY = []
    while (nbNodes <= maxNodes):
        nbNodesX.append(nbNodes)
        ratiosY.append(1/nbNodes)
        nbNodes += stepNodes
    
    plt.plot(nbNodesX, ratiosY, label="% term. nodes minnimal", marker='.')
    
    ## plot cxseps third
    
    stepBranchs = 1
    stepChainNodes = 0
    nbBranchs = 2
    nbChainNodes = 0
    maxNodes = 250
    maxDag = 20
    dicoResCxSeps = SortedDict()
    nbNodes = nbBranchs * ((nbBranchs-1)*2 + nbChainNodes) # + 2
    
    while (nbNodes <= maxNodes):
        maxMem = 1 + nbNodes
        for dag, offset in generator_branched_dag(nbBranchs, nbChainNodes,
                                                  generateFreeSeparableDAG,
                                                  maxMem, maxMem, maxDag, SEED):
            
            schedRes_cxsep = rec_optims_dag_min_mem(dag, **KWargs)
            dicoResCxSeps.setdefault(nbNodes, []).append(schedRes_cxsep.dicoRes)
        nbBranchs += stepBranchs
        nbChainNodes += stepChainNodes
        nbNodes = nbBranchs * ((nbBranchs-1)*2 + nbChainNodes) # + 2

    nbNodesX = [val for val in dicoResCxSeps.keys()]
    ratiosY = []
    for nbNodes, liste in dicoResCxSeps.items():
        ## ratio of reduced nodes
        nbNodesTerm = 0
        for dico in liste:
            for dag in dico['listSepTermDAG']:
                nbNodesTerm += len(dag.nodes())
        ratio = nbNodesTerm / (nbNodes * len(liste))
        ratiosY.append(ratio)
        
    plt.plot(nbNodesX, ratiosY, label="% term. nodes CxSep", marker='.')


    ### plot
    
    plt.xlabel('#nodes')
    plt.ylabel('ratio in [0,1]')
    plt.legend()
    plt.grid(True)
    plt.show()


def printLCTES23_Table1(cmpMemdag=False):
    ## modifications for camera-ready:
    ## - 'falbback' to exhaustive_BandB_lctes23fixed
    ##   instead of exhaustive_BandB_lctes23sub
    ## - basic_sequentialization = nodeParToSeqOptim
    ##   instead of nodeParToSeqOptimChain
    ## - complete_sequentialization = nodeParToSeqOptimGen3_opt
    ##   instead of nodeParToSeqOptimGen3
    ## modifications post-publication:
    ## - adds 'pegLocalPeak=False' for wrapping compatibility
    ## - adds 'nodeBestScoreFunc=getNodeBestScoreOnly' for wrapping compatibility

    KWargsOurs = {'max_global_iter': 0, 'max_local_iter': 0,
                  'local_clustering': True, # local optims serie/parallel
                  'global_sequentialization': True, # global optim parallel
                  'impact_sequentialization': False, # node impact !!wrong!! optim 
                  'transitivity_red': True, # removes transitive edges
                  'sync_separation_rec': False, # recursive dag separation
                  'sync_separation_end': False, # fallback dag separation
                  'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                  'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                  'fallback': exhaustive_BandB_lctes23fixed, # after all optims

                  'timeout': 0, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                  'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
                  # Upper bound of the best memory schedule.
                  # If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
                  'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal), step[=None] > overlap
                  'ensureSPdag': False, # option for memdagSPschedule, suboptimal result if not SPdag
                  'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
    }


    KWargsMemdag = {'max_global_iter': 0, 'max_local_iter': 0,
                    'local_clustering': False, # local optims serie/parallel
                    'global_sequentialization': False, # global optim parallel
                    'impact_sequentialization': False, # node impact !!wrong!! optim 
                    'transitivity_red': False, # removes transitive edges
                    'sync_separation_rec': False, # recursive dag separation
                    'sync_separation_end': False, # fallback dag separation
                    'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                    'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                    'fallback': memdagSPschedule, # after all optims

                    'timeout': 0, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                    'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
                    # Upper bound of the best memory schedule.
                    # If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
                    'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal), step[=None] > overlap
                    'ensureSPdag': False, # option for memdagSPschedule, suboptimal result if not SPdag
                    'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
    }
    
    optimalRepetition = False
    concur = False
    
    print("==> satrec (flat SAS)")
    if cmpMemdag:
        expeAllOptimsSDFgraph(parseSdf3('./sdf3_inputs/satellite_adfg.xml'),
                              nbFiringsObj=1, optimalRepetition=optimalRepetition,
                              pegLocalPeak=False, nodeBestScoreFunc=getNodeBestScoreOnly,
                              autoconcurrency=concur, assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(parseSdf3('./sdf3_inputs/satellite_adfg.xml'),
                          nbFiringsObj=1, optimalRepetition=optimalRepetition,
                          pegLocalPeak=False, nodeBestScoreFunc=getNodeBestScoreOnly,
                          autoconcurrency=concur, assertPeak=True, showTermDAG=False, **KWargsOurs)
    input("Press Enter to continue...")
    print("==> satrec (SRSDF)")
    if cmpMemdag:
        expeAllOptimsSDFgraph(parseSdf3('./sdf3_inputs/satellite_adfg.xml'),
                              nbFiringsObj=0, optimalRepetition=optimalRepetition,
                              pegLocalPeak=False, nodeBestScoreFunc=getNodeBestScoreOnly,
                              autoconcurrency=concur, assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(parseSdf3('./sdf3_inputs/satellite_adfg.xml'),
                          nbFiringsObj=0, optimalRepetition=optimalRepetition,
                          pegLocalPeak=False, nodeBestScoreFunc=getNodeBestScoreOnly,
                          autoconcurrency=concur, assertPeak=True, showTermDAG=False, **KWargsOurs)


def printLCTES23_Table2(cmpMemdag=False):
    ## modifications for artifact:
    ## - 'timeout' to 36 instead of 3600 (no need anymore)
    ## modifications for camera-ready:
    ## - 'falbback' to exhaustive_BandB_lctes23fixed
    ##   instead of exhaustive_BandB_lctes23sub
    ## - replace gen_filterbank by gen_filterbank_fixed
    ## - basic_sequentialization = nodeParToSeqOptim
    ##   instead of nodeParToSeqOptimChain
    ## - complete_sequentialization = nodeParToSeqOptimGen3_opt
    ##   instead of nodeParToSeqOptimGen3
    ## modifications post-publication:
    ## - adds 'pegLocalPeak=False' for wrapping compatibility
    ## - adds 'setNames=True, setSymNodes=False' for wrapping compatibility

    KWargsOurs = {'max_global_iter': 0, 'max_local_iter': 0,
                  'local_clustering': True, # local optims serie/parallel
                  'global_sequentialization': True, # global optim parallel
                  'impact_sequentialization': False, # node impact !!wrong!! optim 
                  'transitivity_red': True, # removes transitive edges
                  'sync_separation_rec': False, # recursive dag separation
                  'sync_separation_end': False, # fallback dag separation
                  'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                  'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                  'fallback': exhaustive_BandB_lctes23fixed, # after all optims

                  'timeout': 36, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                  'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
                  # Upper bound of the best memory schedule.
                  # If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
                  'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal), step[=None] > overlap
                  'ensureSPdag': False, # option for memdagSPschedule, suboptimal result if not SPdag
                  'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
    }


    KWargsMemdag = {'max_global_iter': 0, 'max_local_iter': 0,
                    'local_clustering': False, # local optims serie/parallel
                    'global_sequentialization': False, # global optim parallel
                    'impact_sequentialization': False, # node impact !!wrong!! optim 
                    'transitivity_red': False, # removes transitive edges
                    'sync_separation_rec': False, # recursive dag separation
                    'sync_separation_end': False, # fallback dag separation
                    'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                    'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                    'fallback': memdagSPschedule, # after all optims

                    'timeout': 36, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                    'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
                    # Upper bound of the best memory schedule.
                    # If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
                    'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal), step[=None] > overlap
                    'ensureSPdag': False, # option for memdagSPschedule, suboptimal result if not SPdag
                    'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
    }

    
    homogeneousQMF = False
    maxFirings = 0
    optimalRepetition = False
    concur = False
    
    print("==> qmf23_2d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(2,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(2,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf23_3d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(3,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(3,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf23_5d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf12_2d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(2,1,2,1,2, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(2,1,2,1,2, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf12_3d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(3,1,2,1,2, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(3,1,2,1,2, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf12_5d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,2,1,2, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,2,1,2, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf235_2d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(2,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(2,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf235_3d")
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(3,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    expeAllOptimsSDFgraph(gen_filterbank_fixed(3,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)

    input("Press Enter to continue...")
    print("==> qmf235_5d")
    # may timeout ...
    if cmpMemdag:
        expeAllOptimsSDFgraph(gen_filterbank_fixed(5,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                              nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                              nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                              assertPeak=True, showTermDAG=False, **KWargsMemdag)
    # may timeout ...
    expeAllOptimsSDFgraph(gen_filterbank_fixed(5,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                          nbFiringsObj=maxFirings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                          nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                          assertPeak=True, showTermDAG=False, **KWargsOurs)
    
        
def printLCTES23_Table3():
    ## modifications for artifact:
    ## - 'timeout' to 6 instead of 600
    ## modifications for camera-ready:
    ## - 'falbback' to exhaustive_BandB_lctes23fixed
    ##   instead of exhaustive_BandB_lctes23sub
    ## - replace gen_filterbank by gen_filterbank_fixed
    ## - basic_sequentialization = nodeParToSeqOptim
    ##   instead of nodeParToSeqOptimChain
    ## - complete_sequentialization = nodeParToSeqOptimGen3_opt
    ##   instead of nodeParToSeqOptimGen3
    ## modifications post-publication:
    ## - adds 'setNames=True, setSymNodes=False' for wrapping compatibility
    ## - adds 'pegLocalPeak=False' and 'nodeBestScoreFunc=getNodeBestScoreOnly'
    ##   for wrapping compatibility
    
    KWargsOurs = {'max_global_iter': 0, 'max_local_iter': 0,
                  'local_clustering': True, # local optims serie/parallel
                  'global_sequentialization': True, # global optim parallel
                  'impact_sequentialization': False, # node impact !!wrong!! optim 
                  'transitivity_red': True, # removes transitive edges
                  'sync_separation_rec': False, # recursive dag separation
                  'sync_separation_end': False, # fallback dag separation
                  'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                  'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                  'fallback': exhaustive_BandB_lctes23fixed, # after all optims

                  'timeout': 6, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                  'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
                  # Upper bound of the best memory schedule.
                  # If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
                  'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal), step[=None] > overlap
                  'ensureSPdag': False, # option for memdagSPschedule, suboptimal result if not SPdag
                  'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
    }


    homogeneousQMF = False
    maxFirings = [1,400,800,1600,3200]
    optimalRepetition = False
    concur = False
    
    minMemUpperBound = 0
    for firings in maxFirings:
        print("==> qmf23_5d [PEG: {}]".format(firings))
        KWargsOurs['minMemUpperBound'] = minMemUpperBound
        schedRes = expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=False),
                                         nbFiringsObj=firings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                                         nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                                         assertPeak=True, showTermDAG=False, **KWargsOurs)
        # update bound with previous one if found one
        if not (schedRes is None):
            minMemUpperBound = schedRes.dicoRes['memPeak']
        input("Press Enter to continue...")
        
    minMemUpperBound = 0
    for firings in maxFirings:
        print("==> qmf235_5d [PEG: {}]".format(firings))
        KWargsOurs['minMemUpperBound'] = minMemUpperBound
        schedRes = expeAllOptimsSDFgraph(gen_filterbank_fixed(5,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=False),
                                         nbFiringsObj=firings, optimalRepetition=optimalRepetition, pegLocalPeak=False,
                                         nodeBestScoreFunc=getNodeBestScoreOnly, autoconcurrency=concur,
                                         assertPeak=True, showTermDAG=False, **KWargsOurs)
        # update bound with previous one if found one
        if not (schedRes is None):
            minMemUpperBound = schedRes.dicoRes['memPeak']
        input("Press Enter to continue...")


def printLCTES23_Tables123(cmpMemdag=False):
    """Print result for benchmarked applications.

    :param cmpMemdag: If True, compares with memdag results.
    """
    ## modifications post-publication:
    ## - adds 'setNames=True, setSymNodes=False' for wrapping compatibility
    ## - adds 'pegLocalPeak=False' and 'nodeBestScoreFunc=getNodeBestScoreOnly'
    ##   for wrapping compatibility

    
    print("==> TABLE 1\n")
    printLCTES23_Table1(cmpMemdag)

    input("Press Enter to continue...")
    print("\n==> TABLE 2\n")
    printLCTES23_Table2(cmpMemdag)

    input("Press Enter to continue...")
    print("\n==> TABLE 3\n")
    printLCTES23_Table3()


def printLCTES23ext_PEGtables(saveIrreducible=False):
    """Experiment to generate table 3 of PEG results
    for journal extension of LCTES'23.

    :param saveIrreducible: Whether or not to store
    the irreducible PEG in file system.
    :returns: The names of eventual of irreducible PEG
    files stored in the system.
    """
   
    KWargsOurs = {'max_global_iter': 0, 'max_local_iter': 0,
                  'local_clustering': True, # local optims serie/parallel
                  'global_sequentialization': True, # global optim parallel
                  'impact_sequentialization': False, # node impact !!wrong!! optim 
                  'transitivity_red': True, # removes transitive edges
                  'sync_separation_rec': False, # recursive dag separation
                  'sync_separation_end': False, # fallback dag separation
                  'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
                  'STsubDAG_maxNodes': 0, # max nodes for processed STsubDAG
                  'fallback': exhaustive_BandB_V2, # after all optims

                  'timeout': 600, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
                  'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
                  # Upper bound of the best memory schedule.
                  # If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
                  'minMemHeuristic': two_heurs_solver, # option for exhaustive_BandB >= V2 and brute_force_dag_min_mem
                  'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal), step[=None] > overlap
                  'ensureSPdag': False, # option for memdagSPschedule, suboptimal result if not SPdag
                  'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
    }

    irreduciblePEGs = []
    
    homogeneousQMF = False
    optimalRepetition = True
    concur = False
    # tuple (optimalRepetition, symNode, pegLocalPeak, topoScore)
    confs = [
        (False, False, False, getNodeBestScoreOnly),
        #(True, False, False, getNodeBestScoreOnly),
        #(True, False, False, getNodeBestScoreTopoTieBreak),
        #(True, True, False, getNodeBestScoreOnly),
        (True, False, True, getNodeBestScoreOnly),
        #(True, True, True, getNodeBestScoreOnly)
        #(True, True, True, getNodeBestScoreTopoTieBreak)
    ] 
    maxFirings = [1,400,800,1600,3200]
    
    for conf in confs:
        optRep = conf[0]
        symNode = conf[1]
        cluPeak = conf[2]
        scoreFunc = conf[3]
        minMemUpperBound = 0
        for firings in maxFirings:
            print("==> qmf23_5d [PEG: {}, optRep: {}, sym: {}, cluPeak: {}, {}]".format(firings, optRep, symNode, cluPeak, scoreFunc.__name__))
            KWargsOurs['minMemUpperBound'] = minMemUpperBound
            schedRes = expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,3,2,3, homogeneousQMF, setNames=True, setSymNodes=symNode),
                                             nbFiringsObj=firings, optimalRepetition=optRep, pegLocalPeak=cluPeak,
                                             nodeBestScoreFunc=scoreFunc, autoconcurrency=concur,
                                             assertPeak=True, showTermDAG=False, **KWargsOurs)
            # update bound with previous one if found one
            if not (schedRes is None):
                minMemUpperBound = schedRes.dicoRes['memPeak']
                for idx, termDag in enumerate(schedRes.dicoRes.get('listSepTermDAG', [])):
                    if len(termDag.nodes()) > 1 and saveIrreducible:
                        fileName = "red_qmf23_5d_PEG{}_optRep{}_sym{}_cluPeak{}_topTieBrk{}_term{}".format(firings, int(optRep), int(symNode), int(cluPeak),
                                                                                                                int(scoreFunc is getNodeBestScoreTopoTieBreak), idx)
                        irreduciblePEGs.append(fileName + ".json")
                        saveNXtoJSON(termDag, "irreduciblePEGs/{}.json".format(fileName))
                        writeNXdagToDOT(termDag, "irreduciblePEGs/{}.dot".format(fileName))
            input("Press Enter to continue...")
        
    for conf in confs:
        optRep = conf[0]
        symNode = conf[1]
        cluPeak = conf[2]
        scoreFunc = conf[3]
        minMemUpperBound = 0
        for firings in maxFirings:
            print("==> qmf235_5d [PEG: {}, optRep: {}, sym: {}, cluPeak: {}, {}]".format(firings, optRep, symNode, cluPeak, scoreFunc.__name__))
            KWargsOurs['minMemUpperBound'] = minMemUpperBound
            schedRes = expeAllOptimsSDFgraph(gen_filterbank_fixed(5,2,5,3,5, homogeneousQMF, setNames=True, setSymNodes=symNode),
                                             nbFiringsObj=firings, optimalRepetition=optRep, pegLocalPeak=cluPeak,
                                             nodeBestScoreFunc=scoreFunc, autoconcurrency=concur,
                                             assertPeak=True, showTermDAG=False, **KWargsOurs)
            # update bound with previous one if found one
            if not (schedRes is None):
                minMemUpperBound = schedRes.dicoRes['memPeak']
                for idx, termDag in enumerate(schedRes.dicoRes.get('listSepTermDAG', [])):
                    if len(termDag.nodes()) > 1 and saveIrreducible:
                        fileName = "red_qmf235_5d_PEG{}_optRep{}_sym{}_cluPeak{}_topTieBrk{}_term{}".format(firings, int(optRep), int(symNode), int(cluPeak),
                                                                                                                 int(scoreFunc is getNodeBestScoreTopoTieBreak), idx)
                        irreduciblePEGs.append(fileName + ".json")
                        saveNXtoJSON(termDag, "irreduciblePEGs/{}.json".format(fileName))
                        writeNXdagToDOT(termDag, "irreduciblePEGs/{}.dot".format(fileName))
            input("Press Enter to continue...")
        
    return irreduciblePEGs


hardListIrrPEG = [
    "irreduciblePEGs/red_qmf235_5d_PEG1600_optRep0_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG1600_optRep1_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG1600_optRep1_sym0_cluPeak0_topTieBrk1_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG1600_optRep1_sym0_cluPeak1_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG3200_optRep0_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG3200_optRep1_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG3200_optRep1_sym0_cluPeak0_topTieBrk1_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG3200_optRep1_sym0_cluPeak1_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep0_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep1_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep1_sym0_cluPeak0_topTieBrk1_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep1_sym0_cluPeak1_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep1_sym1_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep1_sym1_cluPeak1_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf235_5d_PEG800_optRep1_sym1_cluPeak1_topTieBrk1_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep0_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym0_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym0_cluPeak0_topTieBrk1_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym0_cluPeak1_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym1_cluPeak0_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym1_cluPeak1_topTieBrk0_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym1_cluPeak1_topTieBrk1_term0.json",
    "irreduciblePEGs/red_qmf23_5d_PEG3200_optRep0_sym0_cluPeak0_topTieBrk0_term0.json"
]


def showIrreduciblePEG(irreduciblePEGs=hardListIrrPEG):
    """Plot and solve given list of graph file names.

    :param irreduciblePEGs: List of graph file names to plot and solve.
    (default: hard list of irreducible PEG results)
    """
    irrPEGset = set()
    for fileName in irreduciblePEGs:
        irrPEG = loadJSONtoNX(fileName)
        irrPEGw = GraphEqNodeProp(irrPEG) 
        if irrPEGw in irrPEGset:
            logger.warning("==> {} already in irreducible set", fileName)
            continue
        irrPEGset.add(irrPEGw)
        logger.info("==> Showing {}", fileName)
        schedRes = exhaustive_BandB_V3epiDual(irrPEG, **{'timeout': 6})
        logger.info("Memory peak: {}.", schedRes.dicoRes['memPeak'])
        subdag = [subdag for subdag in genSTsubDAG_generator(irrPEG)]
        logger.info("There are {} st subdag in this dag.", len(subdag))
        plotMemDAG(irrPEG, fileName, True,
                   showNow=True, subset_key=None)
        

        
            
## cannot work because of relative import inside module
## https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
if __name__ == '__main__':
    plotXXX()
