#! /usr/bin/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from .utils import *
from .dag_gen import *
from .dag_analysis import *
from .dag_transform import *

import os
import re
import time
import math
import networkx
import datetime as dt
from subprocess import Popen, PIPE
from collections import deque
from sortedcontainers import SortedSet, SortedList, SortedDict

import bracelogger
logger = bracelogger.get_logger("dagsched_log")

PATH_MEMDAG = "/home/ahonorat/Documents/logiciels/sources/memdag/src"
## 
## https://gitlab.inria.fr/lmarchal/memdag

class SchedRes:

    def __init__(self):
        self.dicoRes = SortedDict()
        self.keysToGather = set()

    def __str__(self):
        lines = ['{}: {}'.format(k,v) for (k,v) in
                 self.dicoRes.items()]
        return '\n'.join(lines)
        

def getFullOrder(clusteredDag, clusteredOrder):
    """Reconstruct full order based on clustered order.

    :param clusteredDag: Clustered dag, with edge
    attributes 'predNodes' and 'succNodes' set.
    :param clusteredOrder: Order of the clustered dag.
    :returns: Full ordering of the original dag.
    """
    fullOrder = []
    for x in clusteredOrder:
        fullOrder.extend(clusteredDag.nodes[x]['predNodes'])
        fullOrder.append(x)
        fullOrder.extend(clusteredDag.nodes[x]['succNodes'])
    return fullOrder


def check_cbp_tmpMemInc(dag):
    """Check 'tmpMemInc' node attribute in CBP model:
    it must be higher than 'nodeMemCost'.
    To be called before scheduling to avoid useless tests
    on new peak obtained by 'nodeMemCost':
    only 'tmpMemInc' need to be tested.

    :param dag: DAG with attribute 'nodeMemCost' and
    'tmpMemInc' set on nodes.
    :raise ValueError: If a node does not respect the
    condition.
    """
    for node, dicoNode in dag.nodes(data=True):
        if dicoNode['tmpMemInc'] < dicoNode['nodeMemCost']:
            raise ValueError("One node has peak lower than impact.")
        

class LinearHeurDescriptor:

    def __init__(self, precomputeFunc, nodeTupleFunc, sortTupleFunc):
        """Creates object containing all functions needed
        to execute a linear heuristic based on it.

        :param precomputeFunc: Function to call before
        executing the linear sorted scheduling, typically
        computes an extra attribute per node, takes a dag.
        :param nodeTupleFunc: Function which takes a node
        and its attributes (as a dict) and returns a tuple
        of attibutes to be sorted (node is first attribute).
        :param sortTupleFunc: Function which sorts a tuple
        of attributes and executes the max tuple node first.
        :returns: New object.
        """
        self.precomputeFunc = precomputeFunc
        self.nodeTupleFunc = nodeTupleFunc
        self.sortTupleFunc = sortTupleFunc
    

def precompute_impact1dot5(dag):
    """Computes a 'minSucc' node attribute
    being the smallest impact among all successors.

    :param dag: DAG to annotate nodes.
    """
    for node, dicoNode in dag.nodes(data=True):
        if not dag.succ[node]:
            dicoNode['minSucc'] = 0
        else:
            listSuccCost = [dag.nodes[succ]['nodeMemCost'] for succ in dag.succ[node]]
            dicoNode['minSucc'] = min(listSuccCost)
        

def getNodeTuple_impact1dot5(node, dicoNode):
    """Return the tuple needed to sort a node.
    
    :param node: Node to consider.
    :param dicoNode: Dico of node attributes.
    :returns: Tuple (node, 'nodeMemCost', 'minSucc')
    """
    return (node, dicoNode['nodeMemCost'], dicoNode['minSucc'])

def sortNodeTuple_impact1dot5(tupl):
    """Key to sort node tuple by positive impact first,
    then decreasing successor minimum impact.

    :param tupl: Sort node tuple.
    :returns: The sorting keys, by comparison order.
    """
    # (-tupl[1],-tupl[2]))
    # (math.copysign(1, -tupl[1]), -(tupl[2]+tupl[1]))
    return (math.copysign(1, -tupl[1]), -tupl[2], tupl[0])

def heuristic_linear_1dot5steps_impact(dag, **kwargs):
    """ Linear heuristic computing a schedule based on the node memory costs (current and successors).

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. Set node attribute 'minSucc'.
    :param kwargs: Unused.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the heuristic.
    """
    heuristicDescriptor = LinearHeurDescriptor(precompute_impact1dot5,
                                               getNodeTuple_impact1dot5,
                                               sortNodeTuple_impact1dot5)
    return heuristic_linear_gen(dag, heuristicDescriptor, **kwargs)


def getNodeTuple_bestParallel(node, dicoNode):
    """Return the tuple needed to sort a node.
    
    :param node: Node to consider.
    :param dicoNode: Dico of node attributes.
    :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc', 'memPeakDrop")
    """
    return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'], dicoNode['memPeakDrop'])

def sortNodeTuple_bestParallel(tupl):
    """Key to sort node tuple by positive impact nodes first,
    then highest drop if pos., or peak if neg.,
    then lowest impact if pos., or highest impact if neg.,
    and finally lowest node identifier (from left to right).

    :param tupl: Sort node tuple.
    :returns: The sorting keys, by comparison order.
    """
    nodeMemCost = tupl[1]
    if nodeMemCost > 0:
        return (-1, tupl[3], nodeMemCost, tupl[0])
    return (1, -tupl[2], -nodeMemCost, tupl[0])    

def heuristic_linear_best_parallel(dag, **kwargs):
    """ Linear heuristic computing a schedule based on the node memory costs (current and successors).

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. Set node attribute 'minSucc'.
    :param kwargs: Unused.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the heuristic.
    """
    
    def precomputeFunc(dag):
        return set_node_peakDrop(dag, opposite=True)
    
    heuristicDescriptor = LinearHeurDescriptor(precomputeFunc,
                                               getNodeTuple_bestParallel,
                                               sortNodeTuple_bestParallel)
    return heuristic_linear_gen(dag, heuristicDescriptor, **kwargs)

    
def heuristic_linear_gen(dag, heuristicDescriptor, **kwargs):
    """ Linear heuristic computing a schedule based on some node sorting function.

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. May set some node attributes.
    :param kwargs: Unused.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the heuristic.
    """
    check_cbp_tmpMemInc(dag)
    
    # first we need to store the minimum value of all successors of each node
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    if not (heuristicDescriptor.precomputeFunc is None):
        heuristicDescriptor.precomputeFunc(dag)
    # set the main ready list
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    readyNodesTSLreversed = SortedList(key = heuristicDescriptor.sortTupleFunc)
    for node in firstNodes:
        dicoNode = dag.nodes[node]
        nodeTuple = heuristicDescriptor.nodeTupleFunc(node, dicoNode)
        readyNodesTSLreversed.add(nodeTuple)
    memSum = 0
    maxMemUsed = 0
    currentMem = 0
    ordering = []
    # perform BFS like
    start_time = time.perf_counter()
    while readyNodesTSLreversed:
        tupleNode = readyNodesTSLreversed.pop()
        node = tupleNode[0]
        ordering.append(node)
        localPeak = currentMem + dag.nodes[node]['tmpMemInc']
        maxMemUsed = max(maxMemUsed, localPeak)
        currentMem += dag.nodes[node]['nodeMemCost']
        memSum += currentMem + localPeak
        # next test is useless since check_cbp_tmpMemInc
        # maxMemUsed = max(maxMemUsed, currentMem)
        addSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                          heuristicDescriptor.nodeTupleFunc)
    stop_time = time.perf_counter()
    res = SchedRes()
    res.dicoRes['memPeak'] = maxMemUsed
    res.dicoRes['memSum'] = memSum
    res.dicoRes['ordering'] = ordering
    res.dicoRes['time_search'] = stop_time - start_time
    return res


def dual_solver(dag, dag_dual, solver, **kwargs):
    """Calls a solver on the dag and
    on its dual (reversed graph).

    :param dag: DAG with node attributes 'nodeMemCost'
    and 'tmpMemInc', should not be modified by solver.
    :param dag_dual: Dual DAG with same attributes.
    :param solver: Solver to use, typically an heuristic.
    :param kwargs: If the 'minMemUpperBound' key is provided,
    it will be offset accordingly for dual call.
    :returns: Scheduling result giving best memory peak
    (obtained from either primal or dual graph).
    """
    globalImpact = 0
    for _, impact in dag.nodes(data='nodeMemCost'):
        globalImpact += impact
    schedRes_norm = solver(dag, **kwargs)
    peakPrimal = schedRes_norm.dicoRes['memPeak']
    logger.info("Heuristic primal peak: {}", peakPrimal)
    kwargsDual = dict(kwargs)
    minMemUpperBound = kwargsDual.get('minMemUpperBound', 0)
    if minMemUpperBound != 0:
       kwargsDual['minMemUpperBound'] = minMemUpperBound - globalImpact 
    schedRes_dual = solver(dag_dual, **kwargs)
    peakDual = schedRes_dual.dicoRes['memPeak'] + globalImpact
    logger.info("Heuristic dual peak: {}", peakDual)
    if peakDual < peakPrimal:
        schedRes_dual.dicoRes['memPeak'] += globalImpact
        schedRes_dual.dicoRes['ordering'].reverse()
        return schedRes_dual
    return schedRes_norm


def multi_heur_solver(dag, solverList, testDual=True, **kwargs):
    """Call multiple heuristic solvers and
    returns the scheduling result achieving best peak
    among them.

    :param dag: DAG to consider.
    :param solverList: List of heuristic solvers.
    :param testDual: Whether or not, the solver should
    also be called on the mirror graph.
    :returns: The scheduling result having smallest
    memory peak among tested heuristic solved.
    """
    if not solverList:
        raise Exception("Illegal empty solver list.")
    if testDual:
        dag_dual = reverse_sched_graph(dag)
    listSchedRes = []
    for solver in solverList:
        logger.info("Calling heuristic <{}>.", solver.__name__)
        if testDual:
            schedRes = dual_solver(dag, dag_dual, solver, **kwargs)
        else:
            schedRes = solver(dag, **kwargs)
        logger.info("result is: {}", schedRes.dicoRes['memPeak'])
        listSchedRes.append(schedRes)
    listPeak = [r.dicoRes['memPeak'] for r in listSchedRes]
    idxMin = argmin(listPeak, True)
    return listSchedRes[idxMin]


def two_heurs_solver(dag, **kwargs):
    """Call both heuristic solvers
    heuristic_linear_1dot5steps_impact and
    heuristic_linear_best_parallel, on the
    given DAG and its mirror.

    :param dag: DAG to consider.
    :returns: Best of four possible scheduling results.
    """
    solverList = [heuristic_linear_1dot5steps_impact,
                  heuristic_linear_best_parallel]
    return multi_heur_solver(dag, solverList, True, **kwargs)

        
def mpeak_cbp_schedule(dag, ordering, check=True, prefixList=None):
    """Compute memory peaks of the given schedule.

    :param dag: DAG to schedule, with node attributes
    'nodeMemCost' and 'tmpMemInc' set.
    :param ordering: List of nodes in schedule order.
    :param check: If True (default),
    checks given ordering wrt. the dag.
    :param prefixList: (default None) If not None,
    apprends the memory peaks to a shallow copy of prefixList.
    :returns: Maximum memory peak, and list of all.
    """
    if check:
        check_ordering(dag, ordering)
    maxMemUsedCurrentOrdering = 0
    currentMemUsedCurrentOrdering = 0
    if prefixList is None:
        memoryUsageList = []
    else:
        memoryUsageList = list(prefixList)
    for rank in ordering:
        dicoNode = dag.nodes[rank]
        localPeak = currentMemUsedCurrentOrdering + dicoNode['tmpMemInc']
        memoryUsageList.append(localPeak)
        currentMemUsedCurrentOrdering += dicoNode['nodeMemCost']
        memoryUsageList.append(currentMemUsedCurrentOrdering)
    return max(memoryUsageList), memoryUsageList


def getClusteredSchedule(dag, schedule):
    """Cluster a given schedule in a new graph.

    :param dag: DAG containing the schedule.
    :param schedule: Schedule to consider.
    :returns: Tuple of: a new dag of clustered
    nodes, and the related clustered schedule.
    """
    # first copy dag and insert schedule constraints
    dag_copy = networkx.DiGraph()
    for (u,v) in pairwise(schedule):
        dag_copy.add_edge(u, v)
    for node, dicoNode_copy in dag_copy.nodes(data=True):
        dicoNode = dag.nodes[node]
        dicoNode_copy['nodeMemCost'] = dicoNode['nodeMemCost']
        dicoNode_copy['tmpMemInc'] = dicoNode['tmpMemInc']
    set_default_attributes(dag_copy, resetTmpMemInc=False)
    # then cluster nodes and retrieve the (only) schedule
    nodeSerieOptim(dag_copy, recursion=True)
    clst_sched = next(iter(networkx.all_topological_sorts(dag_copy)))
    return dag_copy, clst_sched


def is_betterEq_posSide_than(dropOrdSet, dag1, posList1, dag2, posList2):
    """Compares schedules of clustered positive nodes.
    Should be equivalent to a call to is_betterEq_negSide_than()
    with reversed schedules (reverse schedules have reverse order,
    opposite impact and drop instead of peak).

    :param dropOrdSet: Set of drops to try to insert.
    :param dag1: DAG containing information of first schedule.
    :param posList1: Clustered schedule of positive nodes. 
    :param dag2: DAG containing information of second schedule.
    :param posList2: Clustered schedule of positive nodes.
    :returns: True if first schedule is better than or
    equal to the second. False otherwise.
    """
    curIdx1 = len(posList1) - 1
    curImpact1 = 0
    prevDrop1 = 0
    prevImpact1 = 0
    curIdx2 = len(posList2) - 1
    curImpact2 = 0
    prevDrop2 = 0
    prevImpact2 = 0
    for drop in dropOrdSet:
        continue1 = True
        continue2 = True
        # forward move (emulate sequentialization)
        while curIdx1 >= 0 and continue1:
            node1 = posList1[curIdx1]
            dicoNode1 = dag1.nodes[node1]
            drop1 = dicoNode1['tmpMemInc'] - dicoNode1['nodeMemCost']
            continue1 = drop1 < drop
            if continue1:
                prevImpact1 = curImpact1
                prevDrop1 = drop1
                curImpact1 -= dicoNode1['nodeMemCost']
                curIdx1 -= 1
        while curIdx2 >= 0 and continue2:
            node2 = posList2[curIdx2]
            dicoNode2 = dag2.nodes[node2]
            drop2 = dicoNode2['tmpMemInc'] - dicoNode2['nodeMemCost']
            continue2 = drop2 < drop 
            if continue2:
                prevImpact2 = curImpact2
                prevDrop2 = drop2
                curImpact2 -= dicoNode2['nodeMemCost']
                curIdx2 -= 1
        # final comparison
        max1 = max(prevImpact1 + prevDrop1, curImpact1 + drop)
        max2 = max(prevImpact2 + prevDrop2, curImpact2 + drop)
        if max1 > max2:
            return False
    # then the condition was True up to the end
    return True


def is_betterEq_negSide_than(peakOrdSet, dag1, negList1, dag2, negList2):
    """Compares schedules of clustered negative nodes.

    :param peakOrdSet: Set of peaks to try to insert.
    :param dag1: DAG containing information of first schedule.
    :param negList1: Clustered schedule of strictly negative nodes. 
    :param dag2: DAG containing information of second schedule.
    :param negList2: Clustered schedule of strictly negative nodes.
    :returns: True if first schedule is better than or
    equal to the second. False otherwise.
    """
    curIdx1 = 0
    curImpact1 = 0
    prevPeak1 = 0
    prevImpact1 = 0
    curIdx2 = 0
    curImpact2 = 0
    prevPeak2 = 0
    prevImpact2 = 0
    for peak in peakOrdSet:
        continue1 = True
        continue2 = True
        # forward move (emulate sequentialization)
        while curIdx1 < len(negList1) and continue1:
            node1 = negList1[curIdx1]
            dicoNode1 = dag1.nodes[node1]
            peak1 = dicoNode1['tmpMemInc']
            continue1 = peak1 < peak 
            if continue1:
                prevImpact1 = curImpact1
                prevPeak1 = peak1
                curImpact1 += dicoNode1['nodeMemCost']
                curIdx1 += 1
        while curIdx2 < len(negList2) and continue2:
            node2 = negList2[curIdx2]
            dicoNode2 = dag2.nodes[node2]
            peak2 = dicoNode2['tmpMemInc']
            continue2 = peak2 < peak 
            if continue2:
                prevImpact2 = curImpact2
                prevPeak2 = peak2
                curImpact2 += dicoNode2['nodeMemCost']
                curIdx2 += 1
        # final comparison
        max1 = max(prevImpact1 + prevPeak1, curImpact1 + peak)
        max2 = max(prevImpact2 + prevPeak2, curImpact2 + peak)
        if max1 > max2:
            return False
    # then the condition was True up to the end
    return True


def separateSchedule(dag, nodeList, peakSet, dropSet):
    """Separate clustered schedule in a
    list of strictly negative nodes a list of
    positive nodes. Fill the peak and drop sets.

    :param dag: DAG containing the scheduled nodes.
    :param nodeList: Schedule of clustered nodes to consider.
    :param peakSet: Set of peaks were to add peaks.
    :param dropSet: Set of drops were to add drops.
    :returns: Tuple: list of strictly negative nodes,
    list of positive ones, list of absolute peaks.
    If a nul impact node is present in the middle,
    it is added in both negative and positive ones.
    """
    maxIdxNeg = 0
    peakList = [0]
    curImpact = 0
    for node in nodeList:
        dicoNode = dag.nodes[node]
        nodeMemCost = dicoNode['nodeMemCost']
        tmpMemInc = dicoNode['tmpMemInc']
        peakList.append(curImpact+tmpMemInc)
        curImpact += nodeMemCost
        if nodeMemCost < 0:
            peakSet.add(tmpMemInc)
            maxIdxNeg += 1
        elif nodeMemCost > 0:
            drop = tmpMemInc - nodeMemCost 
            dropSet.add(drop)
    negList = nodeList[:maxIdxNeg]
    # corner case of a nul impact node in the middle
    if maxIdxNeg < len(nodeList) and \
       dag.nodes[nodeList[maxIdxNeg]]['nodeMemCost'] == 0:
        posList = nodeList[maxIdxNeg+1:]
    else:
        posList = nodeList[maxIdxNeg:]
    return negList, posList, peakList


def is_betterEq_clustered_than(dag, nodeList1, peakSet, dropSet, dag2, negList2, posList2, peakList2):
    """Compute if first schedule (left) is better on equal
    to the second one (right). Answers in augmented ternary
    logic: None is incomparable, -1 is strictly worse, +1
    is strictly better, 0 is equality.

    :param dag: Dag containing the two schedules to compare.
    :param nodeList1: First schedule.
    :param peakSet: SortedSet containing peaks of clustered nodeList1.
    :param dropSet: SortedSet containing drops of clustered nodeList1.
    :param dag2: DAG containing information of second schedule.
    :param negList2: Clustered strictly negative nodes of second schedule.
    :param posList2: Clustered positive nodes of second schedule.
    :returns: None if incomparable, -1 if strictly worse, +1
    if strictly better, 0 if equality.
    """
    # first we need to cluster the two node lists
    dag1, clstList1 = getClusteredSchedule(dag, nodeList1)
    # we get the list of common negative/positive node peaks
    negList1, posList1, peakList1 = separateSchedule(dag1, clstList1, peakSet, dropSet)
    maxPeak1 = max(peakList1)
    maxPeak2 = max(peakList2)
    betterPeak1 = maxPeak1 <= maxPeak2
    betterPeak2 = maxPeak2 <= maxPeak1
    # and add to each a lower/higher peak so that smallest impact is checked too
    if peakSet:
        peakSet.add(peakSet[-1]+1)
    if dropSet:
        dropSet.add(dropSet[-1]+1)
    # we compare both clustered chains on peak and drop
    neg12 = is_betterEq_negSide_than(peakSet, dag1, negList1, dag2, negList2)
    neg21 = is_betterEq_negSide_than(peakSet, dag2, negList2, dag1, negList1)
    pos12 = is_betterEq_posSide_than(dropSet, dag1, posList1, dag2, posList2)
    pos21 = is_betterEq_posSide_than(dropSet, dag2, posList2, dag1, posList1)
    if betterPeak1 and betterPeak2 and pos12 and neg12 and pos21 and neg21:
        return 0
    if betterPeak1 and pos12 and neg12:
        return 1
    if betterPeak2 and pos21 and neg21:
        return -1
    return None


def cmp_betterEq_clustered_than(dag, nodeList1, nodeList2):
    """Compute if first schedule (left) is better on equal
    to the second one (right). Answers in augmented ternary
    logic: None is incomparable, -1 is strictly worse, +1
    is strictly better, 0 is equality.

    :param dag: Dag containing the two schedules to compare.
    :param nodeList1: First schedule.
    :param nodeList2: Second schedule.
    :returns: None if incomparable, -1 if strictly worse, +1
    if strictly better, 0 if equality.
    """
    # first we need to cluster the two node lists
    dag2, clstList2 = getClusteredSchedule(dag, nodeList2)
    # we get the list of common negative/positive node peaks
    negPeak = SortedSet()
    posDrop = SortedSet()
    negList2, posList2, peakList2 = separateSchedule(dag2, clstList2, negPeak, posDrop)
    # call subfunction
    return is_betterEq_clustered_than(dag, nodeList1, negPeak, posDrop,
                                      dag2, negList2, posList2, peakList2)


def is_other_order_better_rec(refMemUsageList, othMemUsageList, testMax):
    """Compares two memory usage lists (must contain only positive integer).
    /!\ This function is recursive and is not meant to be called directly.

    :param refMemUsageList: Reference list of successive memory usages.
    :param othMemUsageList: Other list of successive memory usages.
    :param testMax: True if compares next max value.
    :returns: True if other memory usage is better. False if
    opposite and None if equal.
    """
    if not othMemUsageList and not refMemUsageList:
        return None
    if testMax:
        if not othMemUsageList:
            return True
        if not refMemUsageList:
            return False
        idxRef = argmax(refMemUsageList, False)
        idxOth = argmax(othMemUsageList, False)
    else:
        if not othMemUsageList:
            return False
        if not refMemUsageList:
            return True
        idxRef = argmin(refMemUsageList, False)
        idxOth = argmin(othMemUsageList, False)
    valOth = othMemUsageList[idxOth]
    valRef = refMemUsageList[idxRef]
    if valOth < valRef:
        return True
    if valOth > valRef:
        return False
    return is_other_order_better_rec(refMemUsageList[idxRef+1:],
                                     othMemUsageList[idxOth+1:],
                                     not testMax)    
    

def is_other_order_better(refMemUsageList, othMemUsageList):
    """Compares two memory usage lists (must contain only positive integer).
    /!\ Memory usages lists given by mpeak_cbp_schedule(dag, ordering),
    without the last element which normaly is 0 (if graph fully scheduled).

    :param refMemUsageList: Reference list of successive memory usages.
    :param othMemUsageList: Other list of successive memory usages.
    :returns: True if other memory usage is better. False if
    opposite and None if equal.
    """
    if not refMemUsageList or not othMemUsageList:
        return None
    # # Follows some commented attempts of wrong schedule ordering
    # return True

    refMemUsageList = removeSuccessiveDuplicate(refMemUsageList)
    othMemUsageList = removeSuccessiveDuplicate(othMemUsageList)
    
    # first!! we compare the (first) max
    idxMaxRef = argmax(refMemUsageList, True)
    idxMaxOth = argmax(othMemUsageList, True)
    valMaxRef = refMemUsageList[idxMaxRef]
    valMaxOth = othMemUsageList[idxMaxOth]
    if valMaxOth < valMaxRef:
        return True
    if valMaxOth > valMaxRef:
        return False
    # second!! we compare the (first) min
    idxMinRef = argmin(refMemUsageList, True)
    idxMinOth = argmin(othMemUsageList, True)
    valMinRef = refMemUsageList[idxMinRef]
    valMinOth = othMemUsageList[idxMinOth]
    if valMinOth < valMinRef:
        return True
    if valMinOth > valMinRef:
        return False

    # prepare sublists
    idxMinRefL = argmin(refMemUsageList, False)
    idxMinOthL = argmin(othMemUsageList, False)
    subListRefAsc = refMemUsageList[:idxMinRef]
    subListRefDsc = refMemUsageList[idxMinRefL+1:]
    subListOthAsc = othMemUsageList[:idxMinOth]
    subListOthDsc = othMemUsageList[idxMinOthL+1:]
    minSizeAsc = min(len(subListRefAsc),len(subListOthAsc))
    minSizeDsc = min(len(subListRefDsc),len(subListOthDsc))
    # if equal we compare each side separately
    # and we start by the smallest length
    # (simple heuristic to limit complexity)
    if minSizeAsc < minSizeDsc:
        subListRefAsc.reverse()
        subListOthAsc.reverse()
        resAsc = is_other_order_better_rec(subListRefAsc, subListOthAsc, True)
        if resAsc == False:
            return False
        resDsc = is_other_order_better_rec(subListRefDsc, subListOthDsc, True)
        if resDsc == False:
            return False
    else:
        resDsc = is_other_order_better_rec(subListRefDsc, subListOthDsc, True)
        if resDsc == False:
            return False
        subListRefAsc.reverse()
        subListOthAsc.reverse()
        resAsc = is_other_order_better_rec(subListRefAsc, subListOthAsc, True)
        if resAsc == False:
            return False

    return (resAsc or resDsc)


def compact_peaks_rec(memUsageList, testMax, compactedList):
    """Compact memory peaks, as for a chain (see Liu's inspired
    method compact_monotonic_chain_rec(...)), but ine one
    direction only.

    :param memUsageList: List of memory peaks to compact.
    :param testMax: Initial test (argmax or argmin).
    :param compactedList: Internal list of successive peaks
    used for recursion, should be [] for external calls.
    /!\ Python reuse the same list if default argument is [].
    :returns: List of successive peaks.
    """
    if not memUsageList:
        return compactedList
    if testMax:
        idxNext = argmax(memUsageList, False)
    else:
        idxNext = argmin(memUsageList, False)
    compactedList.append(memUsageList[idxNext])
    subList = memUsageList[idxNext+1:]
    return compact_peaks_rec(
        subList, not testMax, compactedList)


def srt_compacted_peaks(memUsageList):
    """Compact memory peaks as for a chain (see Liu's inspired
    method compact_asc_dsc(dag, chain)).

    :param memUsageList: List of memory peaks to compact.
    :returns: Compacted list of memory peaks.
    """
    if not memUsageList:
        return []
    idxMin = argmin(memUsageList, True)
    subListAsc = [m for m in memUsageList[:idxMin+1]]
    subListAsc.reverse()
    compactListAsc = compact_peaks_rec(subListAsc, False, [])
    compactListAsc.reverse()
    subListDsc = [m for m in memUsageList[idxMin+1:]]
    compactListDsc = compact_peaks_rec(subListDsc, True, [])
    return compactListAsc + compactListDsc


def avg_compacted_peaks(memUsageList):
    """Compact memory peaks as for a chain (see Liu's inspired
    method compact_asc_dsc(dag, chain)) and then compute
    the average memory.

    :param memUsageList: List of memory peaks to compact.
    :returns: Average value of the compacted memory.
    """
    if not memUsageList:
        return 0
    compactedList = srt_compacted_peaks(memUsageList)
    sumPeaks = sum(compactedList)
    nbPeaks = len(compactedList)
    return sumPeaks / nbPeaks


def check_ordering(dag, ordering):
    """Check if the given ordering respects topology
    of the given dag, all nodes must be present.

    :param dag: Dag to consider, where 'nbVisits' node attributes
    will be reset to 0.
    :param ordering: Ordering to check (containing all nodes of dag).
    :returns: True if given ordering respects the topology of dag.
    Otherwise it raises a ValueError.
    """
    if len(ordering) != len(dag.nodes()):
        raise ValueError("Given ordering does not have a correct number of nodes.")
    currentNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    for rank in ordering:
        if not rank in currentNodes:
            raise ValueError(
                "Given ordering does not respect topological order (node {}).".format(rank))
        currentNodes.remove(rank)
        ## BFS-like to respect topo order
        for succ in dag.succ[rank]:
            addSuccBFS(dag, succ, currentNodes)
    return True


def mpeak_purecbp_memdag(dag, ordering):
    """Compute memory peak of the given pure cbp schedule.

    :param dag: DAG to schedule, with node attributes
    'nodeMemCost' set. Edge attributes 'addedByPeakDup'
    will be taken into account to enforce direct precedence
    between two nodes. /!\ May not respect given order!
    :param ordering: List of nodes in schedule order.
    :returns: Maximum memory peak.
    """
    currentNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    remainingNodes = deque(ordering)
    forcedNextNode = None
    maxMemUsedCurrentOrdering = 0
    currentMemUsedCurrentOrdering = 0
    for _, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    while currentNodes and remainingNodes:
        if not (forcedNextNode == None):
            rank = forcedNextNode
            forcedNextNode = None
            remainingNodes.remove(rank)
        else:
            rank = remainingNodes.popleft()
        if not rank in currentNodes:
            raise ValueError(
                "Given ordering does not respect topological order (node {}).".format(rank))
        currentNodes.remove(rank)
        currentMemUsedCurrentOrdering += dag.nodes[rank]['nodeMemCost']
        maxMemUsedCurrentOrdering = max(maxMemUsedCurrentOrdering,
                                        currentMemUsedCurrentOrdering)
        ## BFS-like to respect topo order
        for succ in dag.succ[rank]:
            if dag[rank][succ].get('addedByPeakDup', None) == True:
                if forcedNextNode == None:
                    forcedNextNode = succ
                else:
                    raise ValueError(
                        "Unconsistent schedule with regard to nodes added by tmpMemInc_to_cbp(dag)")
            addSuccBFS(dag, succ, currentNodes)
    if remainingNodes:
       raise ValueError("Some nodes are not ordered.") 
    return maxMemUsedCurrentOrdering
    

def check_nodeMemCost(dag):
    """Check if the 'nodeMemCost' attributes
    are synced with the 'edgeMem' attributes.
    See computation in set_node_cost(dag).

    :param dag: DAG to check.
    :returns: True if attributes are synced.
    Otherwise it raises a ValueError.
    """
    for node, dicoNode in dag.nodes(data=True):
        inMemWeight = 0
        for _, dicoEdge in dag.pred[node].items():
            inMemWeight += dicoEdge['edgeMem']
        outMemWeight = 0
        for _, dicoEdge in dag.succ[node].items():
            outMemWeight += dicoEdge['edgeMem']
        if dicoNode['nodeMemCost'] != outMemWeight - inMemWeight:
            logger.error("Erreur on node {}", dicoNode['name'])
            raise ValueError(
                "Given DAG has unconsistent 'nodeMemCost' or 'edgeMem' attributes.")
    return True

        
def memdagSPschedule(dag, **kwargs):
    """Call the memdag tool on SP DAG to get the minimal
    CBP memory peak. Raises Error if invalid command,
    or if given DAG is not SP whereas is should.

    :param dag: CBP DAG with numbers as node identifiers, 
    with edge attribute 'edgeMem' set.
    :param kwargs: Dictionnary of optional default arguments:
    - 'ensureSPdag' (default: False) Whether or not to check that dag is an SP DAG.
    - 'convertTmpMemInc' (default False) Whether or not 'tmpMemInc' node attribute
    must be converted first to an extra edge between read and write operations.
    - 'offsetNewNodes' (default 0) Minimum offset for new node identifiers.
    /!\ Must be set if recursive calls to clustering functions adding nodes.
    :returns: SchedRes dictionnary with memory used 'memPeak' and
    corresponding ordering 'ordering' found by the heuristic, as well as
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak')
    if ensureSPdag option is true.
    """
    ensureSPdag = kwargs.get('ensureSPdag', False)
    convertTmpMemInc = kwargs.get('convertTmpMemInc', False)
    offsetNewNodes = kwargs.get('offsetNewNodes', 0)

    if len(dag.nodes()) <= 1:
        # for 1 node, memdag seems to fail
        # it does not matter calling any other solver
        return heuristic_linear_1dot5steps_impact(dag)
    check_cbp_tmpMemInc(dag)
    writeNXdagToDOT(dag, "tmp.dot")
    commandPath = PATH_MEMDAG + "/memdag"
    if not os.path.isfile(commandPath):
        raise RuntimeError("memdag binary couldn't be found at:\nPATH CMD: {}".
                           format(commandPath))
    # check SP dag
    if ensureSPdag:
        processCheckSP = Popen([commandPath, "-c", "tmp.dot"], stdout=PIPE)
        (output, err) = processCheckSP.communicate()
        exit_code = processCheckSP.wait()
        os.remove("tmp.dot")
        if exit_code:
            raise RuntimeError("memdag tool returned error: {}\nPATH CMD: {}".
                           format(exit_code, commandPath))
        elif output.decode('ascii').strip() != "This is a SP graph.":
            raise ValueError("Argument was not an SP dag.")
    # tmpMemInc equivalence, rewrite graph if modified
    if convertTmpMemInc:
        check_nodeMemCost(dag)
        tmpMemInc_to_cbp(dag, storeSuccInRead=True, offsetNewNodes=offsetNewNodes)
        writeNXdagToDOT(dag, "tmp.dot")
    # launch analysis
    time1 = time.perf_counter()
    processMinPeak = Popen([commandPath, "-m", "tmp.dot"], stdout=PIPE)
    (output, err) = processMinPeak.communicate()
    exit_code = processMinPeak.wait()
    time2 = time.perf_counter()
    os.remove("tmp.dot")
    if exit_code:
        raise RuntimeError("memdag tool returned error: {}\nPATH CMD: {}".
                           format(exit_code, PATH_MEMDAG))

    # parse output
    outLines = output.decode('ascii').strip().split("\n")
    if len(outLines) != 2 and len(outLines) != 4:
        raise ValueError("Unexpected output from memdag.")
    if len(outLines) == 2:
        # when graph is an stSP dag
        regexPeak = "Optimal sequantial memory: (\d+\.\d+)"
        matchPeak = re.match(regexPeak, outLines[0])
        peak = int(float(matchPeak.group(1)))
        orderingStr = outLines[1].split(',')
    elif len(outLines) == 4:
        # when graph is nor st neither SP
        regexPeak = "Peak memory: (\d+\.\d+)"
        matchPeak = re.match(regexPeak, outLines[3])
        peak = int(float(matchPeak.group(1)))
        orderingStr = outLines[2].split(',')
    orderingStr = [node.strip() for node in orderingStr]
    ## debug
    # orderingNames = []
    # for node in orderingStr:
    #     if node.startswith("SYNC_"):
    #         orderingNames.append(node)
    #     else:
    #         name = dag.nodes[int(node)]['name']
    #         orderingNames.append(name)
    # logger.debug(orderingNames)
    ##
    # remove sync nodes added if not SP dag
    if orderingStr[0].startswith("GRAPH_SOURCE"):
        orderingStr = orderingStr[1:]
    if orderingStr[-1].startswith("GRAPH_TARGET"):
        orderingStr = orderingStr[:-1]
    orderingWOsync = [node for node in orderingStr
                      if not node.startswith("SYNC_")]
    orderingNbr = [int(node) for node in orderingWOsync]
    res = SchedRes()
    res.dicoRes['memPeak'] = peak
    res.dicoRes['ordering'] = orderingNbr
    res.dicoRes['time_search'] = time2-time1
    if ensureSPdag and not convertTmpMemInc:
        res.dicoRes['minMemPeakAllOrdering'] = peak
    if convertTmpMemInc:
        # Memory peak measured by memdag maybe higher than the real one
        # because if not SP, some SYNC nodes will be added by memdag.
        # Worse, the SYNC nodes maybe inserted between a read and write
        # (sub)node if convertTmpMemInc have splitted some.
        readMemdagPeak = mpeak_purecbp_memdag(dag, orderingNbr)
        if readMemdagPeak > peak:
            raise ValueError("Memdag peak is lower than real one.")
        res.dicoRes['readMemdagPeak'] = readMemdagPeak
        res.keysToGather.add('readMemdagPeak')
    return res
    

def brute_force_dag_min_mem(dag, **kwargs):
    """Try all topological sorts to get the minimum memory needed
    with the consumed before produced (CBP) model, unicore.

    :param dag: DAG with node attributes 'nodeMemCost' and 'tmpMemInc'.
    :param kwargs: Dictionnary of optional default arguments:
    - 'minMemUpperBound' (default 0) Upper bound of the best memory schedule.
    If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
    - 'faster' (default False) If True, explores all orderings 
    but stop the evaluation of each one as soon as it equals or exceeds current minimum.
    /!\ Is not compatible with 'computeBest...' options.
    - 'timeout' (default 0) Stop search if timeout is reached (given in sec.) 
    0 means never stop (by default). Timeout is checked by specific alarm signal.
    - 'computeBestAvg' (default False) Compute the best average memory usage.
    - 'computeBestSrt' (default False) Compute the best sorted memory usage.
    /!\ Both options cannot be activated together and
    increase the complexity up to quadratic time for each schedule.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'nbOrdering' the total number of ordering,
    'nbFasterBreak' the number of ordering not completely tested,
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak'),
    'maxMemPeakAllOrdering' the maximum memory peak (whose corresponding
    ordering is not stored) only if 'faster' option is False,
    'minAvgMemAllOrdering' the average compacted peak (if option activated),
    'minSrtMemAllOrdering' the sorted compacted peaks (if option activated).
    """
    check_cbp_tmpMemInc(dag)
    halfNodes = len(dag.nodes())//2
    faster = kwargs.get('faster', False)
    timeout_sec = kwargs.get('timeout', 0)
    sumImpact = sum([impact for _, impact in dag.nodes(data='nodeMemCost')])
    if faster and sumImpact != 0:
        # to allow for this, the core part should be recoded to take into
        # account the impact offset, as done for exhaustive_BandB_V3epiDual()
        raise ValueError('faster option cannot be used on dag with non nul impact.')
    
    minMemUpperBound = kwargs.get('minMemUpperBound', 0)
    minMemHeuristic = kwargs.get('minMemHeuristic', None)
    if not (minMemHeuristic is None):
        schedRes_heur = minMemHeuristic(dag, **kwargs)
        minMemUsedAllOrdering = schedRes_heur.dicoRes['memPeak']
    else:
        minMemUsedAllOrdering = float('inf')
            
    computeBestAvg = kwargs.get('computeBestAvg', False)
    computeBestSrt = kwargs.get('computeBestSrt', False)
    if computeBestSrt + computeBestAvg + faster > 1:
        raise ValueError(
            "computeBestAvg, computeBestSrt, and faster options cannot be True at the same time.")

    nbOrdering = 0
    nbFasterBreak = 0
    bestOrdering = None
    maxMemFirstHalf = True
    maxMemUsedAllOrdering = 0
    minMemAvg = float('inf')
    minMemSrt = [float('inf')]
    minMemSum = float('inf')
    nbSamePeak = 0
    if (minMemUpperBound <= 0 and not (minMemHeuristic is None)) or \
       minMemUsedAllOrdering <= minMemUpperBound:
        minMemSum = schedRes_heur.dicoRes['memSum']
        if computeBestAvg or computeBestSrt:
            _, peakList = mpeak_cbp_schedule(
                dag, schedRes_heur.dicoRes['ordering'])
            # remove last peak which is always 0
            if computeBestAvg:
                minMemAvg = avg_compacted_peaks(peakList[2:-2])
            if computeBestSrt:
                minMemSrt = srt_compacted_peaks(peakList[2:-2])
        logger.info("Starting brute force with heur. max bound: {}", minMemUsedAllOrdering)
    elif minMemUpperBound > 0:
        schedRes_heur = None
        minMemUsedAllOrdering = minMemUpperBound
        minMemSum = 2*minMemUpperBound*len(dag.nodes())
        if computeBestAvg:
            minMemAvg = minMemUpperBound
        if computeBestSrt:
            minMemSrt = [minMemUpperBound]
        logger.info("Starting brute force with given max bound: {}", minMemUsedAllOrdering)
    else:
        logger.info("Starting brute force with no bound")
    minMemSumAllOrdering = minMemSum
    minMemAvgAllOrdering = minMemAvg
    minMemSrtAllOrdering = minMemSrt
    
    timeout_obj = Timeout()
    with timeout_obj.timeout_guard(timeout_sec):
        for ordering in networkx.all_topological_sorts(dag):
            nbOrdering += 1
            maxMemUsedCurrentOrdering = 0
            currentMemUsedCurrentOrdering = 0
            currentMemSum = 0

            try:
                # optimisation to test in regular order if maximum is outreached in first half
                if maxMemFirstHalf:
                    for rank,node in enumerate(ordering):
                        dicoNode = dag.nodes[node]
                        localPeak = currentMemUsedCurrentOrdering + dicoNode['tmpMemInc']
                        maxMemUsedCurrentOrdering = max(maxMemUsedCurrentOrdering, localPeak)
                        currentMemUsedCurrentOrdering += dicoNode['nodeMemCost']
                        currentMemSum += currentMemUsedCurrentOrdering + localPeak
                        # following test is useless since check_cbp_tmpMemInc
                        # maxMemUsedCurrentOrdering = max(maxMemUsedCurrentOrdering,
                        #                                 currentMemUsedCurrentOrdering)
                        if faster and maxMemUsedCurrentOrdering >= minMemUsedAllOrdering:
                            maxMemFirstHalf = rank < halfNodes
                            nbFasterBreak += 1
                            raise StopIteration

                # optimisation to test in reverse order if maximum is outreached in second half
                else:
                    for rank,node in enumerate(ordering[::-1]):
                        dicoNode = dag.nodes[node]
                        currentMemUsedCurrentOrdering -= dicoNode['nodeMemCost']
                        # next test is useless since check_cbp_tmpMemInc
                        # maxMemUsedCurrentOrdering = max(maxMemUsedCurrentOrdering,
                        #                                 currentMemUsedCurrentOrdering)
                        localPeak = currentMemUsedCurrentOrdering + dicoNode['tmpMemInc']
                        maxMemUsedCurrentOrdering = max(maxMemUsedCurrentOrdering, localPeak)
                        currentMemSum += currentMemUsedCurrentOrdering + localPeak
                        if faster and maxMemUsedCurrentOrdering >= minMemUsedAllOrdering:
                            maxMemFirstHalf = rank < halfNodes
                            nbFasterBreak += 1
                            raise StopIteration

            except StopIteration:
                continue

            ## completely finished the schedule, now evaluates it
            maxMemUsedAllOrdering = max(maxMemUsedAllOrdering, maxMemUsedCurrentOrdering)
            minMemSumAllOrdering = min(minMemSumAllOrdering, currentMemSum)
            if computeBestAvg or computeBestSrt:
                _, peakList = mpeak_cbp_schedule(dag, ordering)
                # remove last peak which is always 0
                if computeBestAvg:
                    currentMemAvg = avg_compacted_peaks(peakList[2:-2])
                    minMemAvgAllOrdering = min(minMemAvgAllOrdering, currentMemAvg)
                if computeBestSrt:
                    if is_other_order_better(minMemSrtAllOrdering, peakList[2:-2]):
                        minMemSrtAllOrdering = srt_compacted_peaks(peakList[2:-2]) 
            # save minimum memory over all orderings, and the corresponding ordering
            needsUpdate = False
            if maxMemUsedCurrentOrdering < minMemUsedAllOrdering:
                nbSamePeak = 1
                needsUpdate = True
                logger.info("New max bound: {} (sum: {})",
                            maxMemUsedCurrentOrdering, currentMemSum)
            elif maxMemUsedCurrentOrdering == minMemUsedAllOrdering:
                nbSamePeak += 1
                logger.info("Same max bound: {} (sum: {})",
                            maxMemUsedCurrentOrdering, currentMemSum)
                if computeBestAvg and \
                   currentMemAvg < minMemAvg:
                    needsUpdate = True
                if computeBestSrt and \
                   is_other_order_better(minMemSrt, peakList[2:-2]):
                    needsUpdate = True
                if not computeBestSrt and not computeBestAvg and \
                   currentMemSum < minMemSum:
                    needsUpdate = True
                   
            if needsUpdate:
                minMemUsedAllOrdering = maxMemUsedCurrentOrdering
                minMemSum = currentMemSum
                bestOrdering = ordering
                if computeBestAvg:
                    minMemAvg = currentMemAvg
                if computeBestSrt:
                    minMemSrt = srt_compacted_peaks(peakList[2:-2])

    ## finished search, now store results
    res = SchedRes()
    timedout = timeout_obj.timedout
    res.dicoRes['timeout'] = timedout
    res.keysToGather.add('time_search')

    if timedout:
        res.dicoRes['time_search'] = timeout_sec
        logger.info("Timeout all LE! ({:.6f} sec)", timeout_sec)
        logger.info("Not guaranteed optimal!")
    else:
        time_allLE = timeout_obj.get_elapsed_time()
        res.dicoRes['time_search'] = time_allLE
        logger.info("Time all LE: {:.6f} (sec)", time_allLE)
        logger.info("Optimal bound.")
        res.dicoRes['minMemPeakAllOrdering'] = minMemUsedAllOrdering
        if not faster:
            res.dicoRes['maxMemPeakAllOrdering'] = maxMemUsedAllOrdering
            res.dicoRes['minMemSumAllOrdering'] = minMemSumAllOrdering
            if computeBestAvg:
                res.dicoRes['minMemAvgAllOrdering'] = minMemAvgAllOrdering
            if computeBestSrt:
                res.dicoRes['minMemSrtAllOrdering'] = minMemSrtAllOrdering
    
    res.dicoRes['memPeak'] = minMemUsedAllOrdering
    res.dicoRes['memSum'] = minMemSum
    if computeBestAvg:
        res.dicoRes['memAvg'] = minMemAvg
    if computeBestSrt:
        res.dicoRes['memSrt'] = minMemSrt
    res.dicoRes['nbFasterBreak'] = nbFasterBreak
    res.dicoRes['nbOrdering'] = nbOrdering
    res.dicoRes['nbSamePeak'] = nbSamePeak
    res.keysToGather.add('nbOrdering')
    res.dicoRes['ordering'] = bestOrdering
    if not bestOrdering:
        if not schedRes_heur:
            logger.warning("No schedule found with peak lower than {}.", minMemUsedAllOrdering)
        else:
            res.dicoRes['nbSamePeak'] = 1
            res.dicoRes['ordering'] = schedRes_heur.dicoRes['ordering']
            logger.warning("No schedule found better than heuristic.")
        
    return res


def removeSortedSuccsBFS(dag, node, slist, nodeTupleFunc):
    """Subroutine for BFS-like giving sorted list of ready nodes.
    It cancels the execution of a node by removing its successors
    (but not removing self node).

    :param dag: DAG to consider, set node attribute 'nbVisits'.
    :param node: Node having successors to unvisit/remove.
    :param slist: SortedList to eventually remove successor nodes (if not completely visited).
    :param nodeTupleFunc: Function returning node tuple as in list.
    Returned tuple must start with the node itself.
    Function takes as arguments the node and its attribute dictionnary.
    """
    for succ in dag.succ[node]:
        dicoNode = dag.nodes[succ]
        nbVisits = dicoNode['nbVisits']
        # note that modulo of negative number gives a positive result
        dicoNode['nbVisits'] = (nbVisits - 1) % dag.in_degree[succ]
        if nbVisits == 0:
            slist.remove(nodeTupleFunc(succ, dicoNode))


def exhaustive_BandB_lctes23sub(dag, **kwargs):
    """Branch and Bound algorithm to explore all topological sorts.
    Unlike Knuth's algorithm, this one is quadratic in space.
    /!\ This version is the one used for results submitted to lctes'23.

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. Set node attribute 'minSucc'.
    :param kwargs: Dictionnary of optional default arguments:
    - 'minMemUpperBound' (default 0) Upper bound of the best memory schedule.
    If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
    - 'timeout' (default 0): Stop search if timeout is reached (given in sec.) 
    0 means never stop (by default). Timeout is checked by specific alarm signal.
    - 'step' (default None is number of nodes in DAG): Non-optimal heuristic
    to perform successive partial BandB of schedule length 'step'.
    - 'overlap' (default 0): Parameter of the 'step' non-optimal heuristic
    setting the overlap length between steps (strictly less than 'step' itself).
    - 'computeBestSum' (default False) Compute the best sum memory usage.
    /!\ Increase the complexity up to linear time for each found schedule.
    - 'computeBestSrt' (default False) Compute the best sorted memory usage.
    /!\ Increase the complexity up to quadratic time for each found schedule.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'nbOrdering' the number of ordering until the end,
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak').
    """
    check_cbp_tmpMemInc(dag)
    set_node_peakDrop(dag, opposite=True)
    timeout_sec = kwargs.get('timeout', 0)
    minMemUpperBound = kwargs.get('minMemUpperBound', 0)
    computeBestSum = kwargs.get('computeBestSum', False)
    computeBestSrt = kwargs.get('computeBestSrt', False)
    offsetSameMem = computeBestSrt + computeBestSum
    if offsetSameMem > 1:
        raise ValueError(
            "computeBestSum and computeBestSrt options cannot be True at the same time.")
    minMemSrt = []
    nbSamePeak = 0
    if minMemUpperBound <= 0:
        schedRes_heur = heuristic_linear_1dot5steps_impact(dag)
        maxMemAsked = schedRes_heur.dicoRes['memPeak']
        minMemSum = schedRes_heur.dicoRes['memSum']
        if computeBestSrt:
            _, peakList = mpeak_cbp_schedule(
                dag, schedRes_heur.dicoRes['ordering'])
            # remove last peak which is always 0
            minMemSrt = srt_compacted_peaks(peakList[2:-2])
        logger.info("Starting BandB with heur. max bound: {}", maxMemAsked)
    else:
        schedRes_heur = None
        maxMemAsked = minMemUpperBound
        minMemSum = 2*minMemUpperBound*len(dag.nodes())
        if computeBestSrt:
            minMemSrt = [minMemUpperBound]
        logger.info("Starting BandB with given max bound: {}", maxMemAsked)
    nbNodesTot = len(dag.nodes())
    step = kwargs.get('step', None)
    if step is None:
        step = nbNodesTot
    if step < 0 or step > nbNodesTot:
        raise ValueError("Incorrect 'step' parameter.")
    overlap = kwargs.get('overlap', 0)
    if overlap < 0 or overlap >= step:
        raise ValueError("Incorrect 'overlap' parameter.")
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    # set the main ready list
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    def getSortNodeTuple(node, dicoNode):
        """Return the tuple needed to sort a node.

        :param node: Node to consider.
        :param dicoNode: Dico of node attributes.
        :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc', 'memPeakDrop")
        """
        return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'], dicoNode['memPeakDrop'])
    def sortKeyNodeTuplePos(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then highest drop if pos., or peak if neg.,
        then lowest impact if pos., or highest impact if neg.,
        and finally lowest node identifier (from left to right).

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, tupl[3], nodeMemCost, tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])
    def sortKeyNodeTupleAlt(tupl):
        """Alternative key to sort node tuple by decreasing impact,
        then decreasing peak, then node identifier.

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        return (-tupl[1], -tupl[2], tupl[0])
    readyNodesTSLreversed = SortedList(key = sortKeyNodeTuplePos)
    for node in firstNodes:
        dicoNode = dag.nodes[node]
        readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
    currentRankBegin = 0
    lengthForward = step - overlap
    # complicated next formula to predict the rank of the last step search
    lastRankBegin = lengthForward * (((nbNodesTot  + lengthForward - 1) // lengthForward) - 1)
    currentRank = -1
    maxMemUsed = maxMemAsked
    bestCurrentMem = maxMemAsked
    currentMaxMem = 0
    currentMem = 0
    currentMaxMemOrdering = [0]
    ranksMemIncrease = [-1]
    lastUntakenMinNegIdx = []
    currentMinNegPeakChoices = [None]
    prevBestDropChoice = [None]
    bestOrdering = []
    currentOrdering = []
    backupBeginStepReadyNodes = SortedList(iterable = readyNodesTSLreversed,
                                           key = sortKeyNodeTuplePos)
    remainingChoiceList = [list(readyNodesTSLreversed)]
    timeout_obj = Timeout()
    with timeout_obj.timeout_guard(timeout_sec):
        # perform BFS like
        while remainingChoiceList:
            currentChoices = remainingChoiceList[-1]
            if not currentChoices:
                if currentRankBegin == lastRankBegin and currentRank == lastRankBegin - 1:
                    # if no choice at last rank, we are done for the whole search
                    break
                elif currentRank == currentRankBegin - 1:
                    # if no choice at step begin then we explore next step
                    if len(bestOrdering) < currentRankBegin + step:
                        # except if previous step did not find any best schedule
                        # then we can escape
                        break
                    bestCurrentMem = maxMemAsked
                    maxMemUsed = maxMemAsked
                    currentRankBegin += lengthForward
                    # logger.debug("Begin search at BnB step index: {}", currentRankBegin)
                    # we must go back to the state of actual best step ordering
                    readyNodesTSLreversed = backupBeginStepReadyNodes
                    for node in bestOrdering[currentRank+1:currentRankBegin]:
                        # replay regular test
                        dicoNode = dag.nodes[node]
                        choice = getSortNodeTuple(node, dicoNode)
                        readyNodesTSLreversed.remove(choice)
                        nodeMemCost = dicoNode['nodeMemCost']
                        tmpMemInc = dicoNode['tmpMemInc']
                        local_peak = currentMem + tmpMemInc
                        # test local_peak only since check_cbp_tmpMemInc
                        if local_peak > currentMaxMem:
                            currentMaxMem = local_peak
                            ranksMemIncrease.append(currentRank)
                        # update local variables
                        currentMem += nodeMemCost
                        currentRank += 1
                        currentOrdering.append(node)
                        currentMaxMemOrdering.append(currentMaxMem)
                        addSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                          getSortNodeTuple)
                        # fix the choice
                        remainingChoiceList.append([])
                    if not offsetSameMem:
                        ## NEG+POS OPTIM ##
                        for it in range(0, lengthForward):
                            lastUntakenMinNegIdx.append(0)
                            currentMinNegPeakChoices.append(None)
                            prevBestDropChoice.append(None)
                    # backup ready list and fix last remaining choices
                    backupBeginStepReadyNodes = SortedList(
                        iterable = readyNodesTSLreversed,
                        key = sortKeyNodeTuplePos)
                    remainingChoiceList[-1] = list(readyNodesTSLreversed)
                    # pursue search
                    continue
                else:
                    # otherwise we go back to previous choice, standard rollback
                    currentRank -= 1
                    node = currentOrdering.pop()
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                    currentMaxMemOrdering.pop()
                    prevMaxMem = currentMaxMemOrdering[-1]
                    if currentMaxMem > prevMaxMem:
                        ranksMemIncrease.pop()
                    currentMaxMem = prevMaxMem
                    remainingChoiceList.pop()
                    if not offsetSameMem:
                        ## NEG+POS OPTIM ##
                        lastUntakenMinNegIdx.pop()
                        currentMinNegPeakChoices.pop()
                        prevBestDropChoice.pop()
                    # logger.debug("One step backward (no choices)")
                    continue
            # if arriving here, there was at least one choice,
            # schedule it
            nextChoice = currentChoices.pop()
            node = nextChoice[0]
            nodeMemCost = nextChoice[1]
            tmpMemInc = nextChoice[2]
            if not offsetSameMem and nodeMemCost > 0:
                ## POS OPTIM ##
                bestDropChoice = prevBestDropChoice[-1]
                if not bestDropChoice is None:
                    prevReadyNodesTSL = list(readyNodesTSLreversed)
            readyNodesTSLreversed.remove(nextChoice)
            local_peak = currentMem + tmpMemInc
            currentMem += nodeMemCost
            currentRank += 1
            if local_peak > currentMaxMem:
                currentMaxMem = local_peak
                ranksMemIncrease.append(currentRank)
            # next test is useless since check_cbp_tmpMemInc
            # if currentMem > currentMaxMem:
            #     currentMaxMem = currentMem
            #     ranksMemIncrease.append(currentRank)
            currentOrdering.append(node)
            currentMaxMemOrdering.append(currentMaxMem)
            addSortedSuccsBFS(dag, node,
                              readyNodesTSLreversed,
                              getSortNodeTuple)
            # now clean the nextChoiceList
            nextChoiceList = list()
            if offsetSameMem:
                # clear obvious bad choices
                for nextChoice in readyNodesTSLreversed:
                    tmpMemInc = nextChoice[2]
                    local_peak = currentMem + tmpMemInc
                    # if node increase maxMem *above* a known solution
                    # we discard it
                    if local_peak > maxMemUsed:
                        # if NO 'computeBest...' option activated,
                        # then offsetSameMem == 0 and it is strict equality
                        continue
                        # IF the list was sorted by increasing peaks,
                        # then we could remove this one, and all subsequent
                        # but it does not seem so efficient
                        #nextChoiceList = nextChoiceList[:idx]
                    # next test is useless since check_cbp_tmpMemInc
                    # new_mem = currentMem + nodeMemCost
                    # if new_mem > maxMemUsed:
                    #     IF the list is sorted by increasing peaks,
                    #     then we can remove this one, and all subsequent
                    #     continue
                    nextChoiceList.append(nextChoice)
            else:
                ## BEGIN if not offsetSameMem

                ## NEG OPTIM ##
                # get current min neg if any
                minNegPeakChoice = None
                if readyNodesTSLreversed:
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    nextChoiceC = readyNodesTSLreversed[-1]
                    if nextChoiceC[1] <= 0:
                        minNegPeakChoice = nextChoiceC
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # nextChoiceC = max(readyNodesTSLreversed,
                    #                   key = sortKeyNodeTuplePos,
                    #                   default = None)
                    # if not nextChoiceC is None and nextChoiceC[1] <= 0:
                    #     minNegPeakChoice = nextChoiceC
                
                # update min neg idx
                if nextChoice == currentMinNegPeakChoices[-1]:
                    lastUntakenMinNegIdx.append(lastUntakenMinNegIdx[-1])
                else:
                    lastUntakenMinNegIdx.append(currentRank)
                minNegIdx = lastUntakenMinNegIdx[-1]
                
                # update min neg choice
                if not minNegPeakChoice is None:
                    # we copy the new neg if any
                    currentMinNegPeakChoices.append(minNegPeakChoice)
                elif nextChoice == currentMinNegPeakChoices[-1]:
                    # if currentChoice is last min neg one and no new one,
                    # then we copy the previous untaken one
                    currentMinNegPeakChoices.append(currentMinNegPeakChoices[minNegIdx])
                else:
                    # otherwise we copy the current one
                    currentMinNegPeakChoices.append(currentMinNegPeakChoices[-1])

                # in the end we get the min neg peak of most recent untaken one
                lastMinNegPeakChoice = currentMinNegPeakChoices[minNegIdx]
                if lastMinNegPeakChoice is None:
                    currentMinNegPeak = maxMemAsked
                else:
                    currentMinNegPeak = currentMem + lastMinNegPeakChoice[2]
 
                ## POS OPTIM ##
                prevHigherPosDrop = set()
                maxPosDropChoice = None

                if nodeMemCost > 0 and \
                   nextChoice != bestDropChoice and \
                   not bestDropChoice is None:
                    # in such case we will remove all higher drops from next choices
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    for prevChoice in prevReadyNodesTSL:
                        if prevChoice == nextChoice:
                            break
                        prevHigherPosDrop.add(prevChoice)
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # keyNextChoice = sortKeyNodeTuplePos(nextChoice)
                    # for prevChoice in prevReadyNodesTSL:
                    #     keyPrevChoice = sortKeyNodeTuplePos(prevChoice)
                    #     if keyPrevChoice < keyNextChoice:
                    #         prevHigherPosDrop.add(prevChoice)
                    # and we update next best drop choice
                    # get current max pos if any
                    maxPosDropChoice = None
                    if readyNodesTSLreversed:
                        # /!\ ASSUME **sortKeyNodeTuplePos**
                        nextChoiceC = readyNodesTSLreversed[0]
                        if nextChoiceC[1] > 0:
                            maxPosDropChoice = nextChoiceC
                        # /!\ version not depending on **sortKeyNodeTuplePos**
                        # nextChoiceC = min(readyNodesTSLreversed,
                        #                   key = sortKeyNodeTuplePos,
                        #                   default = None)
                        # if not nextChoiceC is None and nextChoiceC[1] > 0:
                        #     maxPosDropChoice = nextChoiceC
                
                # in any case add the next max pos choice
                prevBestDropChoice.append(maxPosDropChoice)

                ## POS+NEG OPTIM ##
                ## clear obvious bad choices
                if minNegPeakChoice is None or \
                   currentMinNegPeak > currentMaxMem:
                    # visit all choices
                    for nextChoiceC in readyNodesTSLreversed:
                        tmpMemIncC = nextChoiceC[2]
                        local_peakC = currentMem + tmpMemIncC
                        # if node increase maxMem up to a known solution
                        # or above, we discard it
                        if local_peakC >= maxMemUsed:
                            continue
                        ## NEG OPTIM ##
                        if local_peakC >= currentMinNegPeak:
                            continue
                        ## POS OPTIM ##
                        if nextChoiceC in prevHigherPosDrop:
                            continue
                        # in any other case we keep it
                        nextChoiceList.append(nextChoiceC)
                if not minNegPeakChoice is None:
                    # in any case we keep the negative choice
                    nextChoiceList.append(minNegPeakChoice)
                
                ## END if not offsetSameMem
                
            # add the cleaned next choice list for the next iteration
            remainingChoiceList.append(nextChoiceList)

            # we have finished to schedule a node, test if a step is reached
            if currentRank + 1 == min(currentRankBegin + step, nbNodesTot):
                # we reached the end of step or ordering
                # logger.debug("End schedule at BnB step index: {}", currentRankBegin)
                needsUpdate = False
                nbNodesScheduled = len(currentOrdering)

                if offsetSameMem and \
                   nbNodesScheduled == nbNodesTot and \
                   currentMaxMem <= maxMemUsed:
                    _, peakList = mpeak_cbp_schedule(dag, currentOrdering)
                    if computeBestSum:
                        memSum = sum(peakList)

                # checking if new bound
                if currentMaxMem < maxMemUsed:
                    nbSamePeak = 1
                    needsUpdate = True
                    logger.info("New max bound: {}", currentMaxMem)
                    # logger.info(dt.datetime.now())

                # or equal bound with less memory
                elif currentMaxMem == maxMemUsed:
                    if nbNodesScheduled < nbNodesTot:
                        # (seems useless if ready list sorted by low impact)
                        if currentMem < bestCurrentMem:
                            needsUpdate = True
                            # logger.info("Equivalent bound lower last mem: {}", currentMem)
                    else:
                        nbSamePeak += 1
                        if computeBestSum and memSum < minMemSum:
                            needsUpdate = True
                            # logger.info("Equivalent bound lower sum mem: {}", memSum)
                        if computeBestSrt and \
                           is_other_order_better(minMemSrt, peakList[2:-2]):
                            # remove last peak which is always 0
                            needsUpdate = True
                            # logger.info("Equivalent bound lower srt mem.")
                            
                if needsUpdate:
                    bestOrdering[currentRankBegin:] = currentOrdering[currentRankBegin:]
                    maxMemUsed = currentMaxMem
                    if nbNodesScheduled < nbNodesTot:
                        bestCurrentMem = currentMem
                    else:
                        if computeBestSum:
                            minMemSum = memSum
                        if computeBestSrt:
                            minMemSrt = srt_compacted_peaks(peakList[2:-2])
                        
                # in any case except bestCompute*,
                # we can backtrack until the last increase or step begin
                lastRankIncrease = ranksMemIncrease[-1]
                if offsetSameMem:
                    backtrackRank = currentRank
                else:
                    backtrackRank = currentRankBegin
                    # logger.debug("Go back to rankBegin: {}", backtrackRank)
                if lastRankIncrease >= backtrackRank:
                    backtrackRank = ranksMemIncrease.pop()
                    # logger.debug("Go back to last increase: {}", backtrackRank)
                currentRank = backtrackRank - 1
                # remove node in reverse order until backtrackRank included
                for node in currentOrdering[-1:backtrackRank-nbNodesScheduled-1:-1]:
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node,
                                         readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                currentOrdering = currentOrdering[:backtrackRank]
                currentMaxMemOrdering = currentMaxMemOrdering[:backtrackRank+1]
                currentMaxMem = currentMaxMemOrdering[-1]
                remainingChoiceList = remainingChoiceList[:backtrackRank+1]
                if not offsetSameMem:
                    ## NEG+POS OPTIM ##
                    lastUntakenMinNegIdx = lastUntakenMinNegIdx[:backtrackRank]
                    currentMinNegPeakChoices = currentMinNegPeakChoices[:backtrackRank+1]
                    prevBestDropChoice = prevBestDropChoice[:backtrackRank+1]
    
    res = SchedRes()
    timedout = timeout_obj.timedout
    res.dicoRes['timeout'] = timedout
    res.keysToGather.add('time_search')
    nbAllRemainingChoices = [len(choices) for choices in remainingChoiceList]
    res.dicoRes['BandB_nbAllRemainingChoices'] = nbAllRemainingChoices
    if timedout:
        res.dicoRes['time_search'] = timeout_sec
        logger.info("Timeout BnB! ({:.6f} sec)", timeout_sec)
        logger.info("Not guaranteed optimal!")
        nbRemainingChoices = 1
        for nbChoices in nbAllRemainingChoices:
            if nbChoices > 0:
                nbRemainingChoices *= nbChoices
        logger.info("Number of remaining choices (prod): {:.2E} up to rank {}\n{}",
                    nbRemainingChoices, len(remainingChoiceList), nbAllRemainingChoices)
    else:
        time_BandB = timeout_obj.get_elapsed_time()
        res.dicoRes['time_search'] = time_BandB
        logger.info("Time BandB: {:.6f} (sec)", time_BandB)
        logger.info("Optimal bound (exhausted search).")
        res.dicoRes['minMemPeakAllOrdering'] = maxMemUsed

    res.dicoRes['memPeak'] = maxMemUsed
    if computeBestSum:
        res.dicoRes['memSum'] = minMemSum
    if computeBestSrt:
        res.dicoRes['memSrt'] = minMemSrt
    res.dicoRes['nbSamePeak'] = nbSamePeak
    res.dicoRes['ordering'] = bestOrdering
    if len(bestOrdering) < nbNodesTot:
        res.dicoRes['memPeak'] = maxMemAsked
        if not schedRes_heur:
            res.dicoRes['ordering'] = None
            logger.warning("No schedule found with peak lower than {}.", maxMemAsked)
        else:
            res.dicoRes['nbSamePeak'] = 1
            res.dicoRes['ordering'] = schedRes_heur.dicoRes['ordering']
            logger.warning("No schedule found better than heuristic.")
            
    return res


def exhaustive_BandB_lctes23fixed(dag, **kwargs):
    """Branch and Bound algorithm to explore all topological sorts.
    Unlike Knuth's algorithm, this one is quadratic in space.
    /!\ This version fixes the one used for results submitted to lctes'23.
    Fixes concern positive and negative optimizations (now fully functional),
    and uses impact as a primary objective for stepped version.

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. Set node attribute 'minSucc'.
    :param kwargs: Dictionnary of optional default arguments:
    - 'minMemUpperBound' (default 0) Upper bound of the best memory schedule.
    If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
    - 'timeout' (default 0): Stop search if timeout is reached (given in sec.) 
    0 means never stop (by default). Timeout is checked by specific alarm signal.
    - 'step' (default None is number of nodes in DAG): Non-optimal heuristic
    to perform successive partial BandB of schedule length 'step'.
    - 'overlap' (default 0): Parameter of the 'step' non-optimal heuristic
    setting the overlap length between steps (strictly less than 'step' itself).
    - 'computeBestSum' (default False) Compute the best sum memory usage.
    /!\ Increase the complexity up to linear time for each found schedule.
    - 'computeBestSrt' (default False) Compute the best sorted memory usage.
    /!\ Increase the complexity up to quadratic time for each found schedule.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'nbOrdering' the number of ordering until the end,
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak').
    """
    check_cbp_tmpMemInc(dag)
    set_node_peakDrop(dag, opposite=True)
    timeout_sec = kwargs.get('timeout', 0)
    minMemUpperBound = kwargs.get('minMemUpperBound', 0)
    computeBestSum = kwargs.get('computeBestSum', False)
    computeBestSrt = kwargs.get('computeBestSrt', False)
    offsetSameMem = computeBestSrt + computeBestSum
    if offsetSameMem > 1:
        raise ValueError(
            "computeBestSum and computeBestSrt options cannot be True at the same time.")
    minMemSrt = []
    nbSamePeak = 0
    if minMemUpperBound <= 0:
        schedRes_heur = heuristic_linear_1dot5steps_impact(dag)
        maxMemAsked = schedRes_heur.dicoRes['memPeak']
        minMemSum = schedRes_heur.dicoRes['memSum']
        if computeBestSrt:
            _, peakList = mpeak_cbp_schedule(
                dag, schedRes_heur.dicoRes['ordering'])
            # remove last peak which is always 0
            minMemSrt = srt_compacted_peaks(peakList[2:-2])
        logger.info("Starting BandB with heur. max bound: {}", maxMemAsked)
    else:
        schedRes_heur = None
        maxMemAsked = minMemUpperBound
        minMemSum = 2*minMemUpperBound*len(dag.nodes())
        if computeBestSrt:
            minMemSrt = [minMemUpperBound]
        logger.info("Starting BandB with given max bound: {}", maxMemAsked)
    nbNodesTot = len(dag.nodes())
    step = kwargs.get('step', None)
    if step is None:
        step = nbNodesTot
    if step < 0 or step > nbNodesTot:
        raise ValueError("Incorrect 'step' parameter.")
    overlap = kwargs.get('overlap', 0)
    if overlap < 0 or overlap >= step:
        raise ValueError("Incorrect 'overlap' parameter.")
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    # set the main ready list
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    def getSortNodeTuple(node, dicoNode):
        """Return the tuple needed to sort a node.

        :param node: Node to consider.
        :param dicoNode: Dico of node attributes.
        :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc', 'memPeakDrop")
        """
        return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'], dicoNode['memPeakDrop'])
    def sortKeyNodeTuplePos(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then highest drop if pos., or peak if neg.,
        then lowest impact if pos., or highest impact if neg.,
        and finally lowest node identifier (from left to right).

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, tupl[3], nodeMemCost, tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])
    def sortKeyNodeTupleAlt(tupl):
        """Alternative key to sort node tuple by decreasing impact,
        then decreasing peak, then node identifier.

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        return (-tupl[1], -tupl[2], tupl[0])
    readyNodesTSLreversed = SortedList(key = sortKeyNodeTuplePos)
    for node in firstNodes:
        dicoNode = dag.nodes[node]
        readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
    currentRankBegin = 0
    lengthForward = step - overlap
    # complicated next formula to predict the rank of the last step search
    lastRankBegin = lengthForward * (((nbNodesTot  + lengthForward - 1) // lengthForward) - 1)
    currentRank = -1
    maxMemUsed = maxMemAsked
    bestCurrentMem = maxMemAsked
    currentMaxMem = 0
    currentMem = 0
    currentMaxMemOrdering = [0]
    ranksMemIncrease = [-1]
    lastUntakenMinNegPeak = [maxMemAsked]
    currentMinNegPeakChoices = [None]
    prevBestDropChoice = [None]
    bestOrdering = []
    currentOrdering = []
    backupBeginStepReadyNodes = SortedList(iterable = readyNodesTSLreversed,
                                           key = sortKeyNodeTuplePos)
    remainingChoiceList = [list(readyNodesTSLreversed)]
    timeout_obj = Timeout()
    with timeout_obj.timeout_guard(timeout_sec):
        # perform BFS like
        while remainingChoiceList:
            currentChoices = remainingChoiceList[-1]
            if not currentChoices:
                if currentRankBegin == lastRankBegin and currentRank == lastRankBegin - 1:
                    # if no choice at last rank, we are done for the whole search
                    break
                elif currentRank == currentRankBegin - 1:
                    # if no choice at step begin then we explore next step
                    if len(bestOrdering) < currentRankBegin + step:
                        # except if previous step did not find any best schedule
                        # then we can escape
                        break
                    bestCurrentMem = maxMemAsked
                    maxMemUsed = maxMemAsked
                    currentRankBegin += lengthForward
                    # logger.debug("Begin search at BnB step index: {}", currentRankBegin)
                    # we must go back to the state of actual best step ordering
                    readyNodesTSLreversed = backupBeginStepReadyNodes
                    for node in bestOrdering[currentRank+1:currentRankBegin]:
                        # replay regular test
                        dicoNode = dag.nodes[node]
                        choice = getSortNodeTuple(node, dicoNode)
                        readyNodesTSLreversed.remove(choice)
                        nodeMemCost = dicoNode['nodeMemCost']
                        tmpMemInc = dicoNode['tmpMemInc']
                        local_peak = currentMem + tmpMemInc
                        # test local_peak only since check_cbp_tmpMemInc
                        if local_peak > currentMaxMem:
                            currentMaxMem = local_peak
                            ranksMemIncrease.append(currentRank)
                        # update local variables
                        currentMem += nodeMemCost
                        currentRank += 1
                        currentOrdering.append(node)
                        currentMaxMemOrdering.append(currentMaxMem)
                        addSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                          getSortNodeTuple)
                        # fix the choice
                        remainingChoiceList.append([])
                    if not offsetSameMem:
                        ## NEG+POS OPTIM ##
                        for it in range(0, lengthForward):
                            lastUntakenMinNegPeak.append(maxMemAsked)
                            currentMinNegPeakChoices.append(None)
                            prevBestDropChoice.append(None)
                    # backup ready list and fix last remaining choices
                    backupBeginStepReadyNodes = SortedList(
                        iterable = readyNodesTSLreversed,
                        key = sortKeyNodeTuplePos)
                    remainingChoiceList[-1] = list(readyNodesTSLreversed)
                    # pursue search
                    continue
                else:
                    # otherwise we go back to previous choice, standard rollback
                    currentRank -= 1
                    node = currentOrdering.pop()
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                    currentMaxMemOrdering.pop()
                    prevMaxMem = currentMaxMemOrdering[-1]
                    if currentMaxMem > prevMaxMem:
                        ranksMemIncrease.pop()
                    currentMaxMem = prevMaxMem
                    remainingChoiceList.pop()
                    if not offsetSameMem:
                        ## NEG+POS OPTIM ##
                        lastUntakenMinNegPeak.pop()
                        currentMinNegPeakChoices.pop()
                        prevBestDropChoice.pop()
                    # logger.debug("One step backward (no choices)")
                    continue
            # if arriving here, there was at least one choice,
            # schedule it
            nextChoice = currentChoices.pop()
            node = nextChoice[0]
            nodeMemCost = nextChoice[1]
            tmpMemInc = nextChoice[2]
            if not offsetSameMem and nodeMemCost > 0:
                ## POS OPTIM ##
                bestDropChoice = prevBestDropChoice[-1]
                if not bestDropChoice is None:
                    prevReadyNodesTSL = list(readyNodesTSLreversed)
            readyNodesTSLreversed.remove(nextChoice)
            local_peak = currentMem + tmpMemInc
            currentMem += nodeMemCost
            currentRank += 1
            if local_peak > currentMaxMem:
                currentMaxMem = local_peak
                ranksMemIncrease.append(currentRank)
            # next test is useless since check_cbp_tmpMemInc
            # if currentMem > currentMaxMem:
            #     currentMaxMem = currentMem
            #     ranksMemIncrease.append(currentRank)
            currentOrdering.append(node)
            currentMaxMemOrdering.append(currentMaxMem)
            addSortedSuccsBFS(dag, node,
                              readyNodesTSLreversed,
                              getSortNodeTuple)
            # now clean the nextChoiceList
            nextChoiceList = list()
            if offsetSameMem:
                # clear obvious bad choices
                for nextChoice in readyNodesTSLreversed:
                    tmpMemInc = nextChoice[2]
                    local_peak = currentMem + tmpMemInc
                    # if node increase maxMem *above* a known solution
                    # we discard it
                    if local_peak > maxMemUsed:
                        # if NO 'computeBest...' option activated,
                        # then offsetSameMem == 0 and it is strict equality
                        continue
                        # IF the list was sorted by increasing peaks,
                        # then we could remove this one, and all subsequent
                        # but it does not seem so efficient
                        #nextChoiceList = nextChoiceList[:idx]
                    # next test is useless since check_cbp_tmpMemInc
                    # new_mem = currentMem + nodeMemCost
                    # if new_mem > maxMemUsed:
                    #     IF the list is sorted by increasing peaks,
                    #     then we can remove this one, and all subsequent
                    #     continue
                    nextChoiceList.append(nextChoice)
            else:
                ## BEGIN if not offsetSameMem

                ## NEG OPTIM ##
                # get current min neg if any
                minNegPeakChoice = None
                if readyNodesTSLreversed:
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    nextChoiceC = readyNodesTSLreversed[-1]
                    if nextChoiceC[1] <= 0:
                        minNegPeakChoice = nextChoiceC
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # nextChoiceC = max(readyNodesTSLreversed,
                    #                   key = sortKeyNodeTuplePos,
                    #                   default = None)
                    # if not nextChoiceC is None and nextChoiceC[1] <= 0:
                    #     minNegPeakChoice = nextChoiceC
                
                # update min neg peak/choices
                prevMinNegPeakChoice = currentMinNegPeakChoices[-1]
                prevMinNegPeak = lastUntakenMinNegPeak[-1]
                if nextChoice == prevMinNegPeakChoice or \
                   prevMinNegPeakChoice is None:
                    lastUntakenMinNegPeak.append(prevMinNegPeak)
                else:
                    prevUntakenNegPeak = currentMem - nextChoice[1] + prevMinNegPeakChoice[2]
                    prevMinNegPeak = min(lastUntakenMinNegPeak[-1], prevUntakenNegPeak)
                    lastUntakenMinNegPeak.append(prevMinNegPeak)
                currentMinNegPeakChoices.append(minNegPeakChoice)

                # in the end we get the min neg peak of most recent untaken one
                currentMinNegPeak = prevMinNegPeak
                if not (minNegPeakChoice is None):
                    currentMinNegPeak = min(currentMinNegPeak, currentMem + minNegPeakChoice[2])

                ## POS OPTIM ##
                prevHigherPosDrop = set()
                maxPosDropChoice = None

                if nodeMemCost > 0 and \
                   nextChoice != bestDropChoice and \
                   not bestDropChoice is None:
                    # in such case we will remove all higher drops from next choices
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    for prevChoice in prevReadyNodesTSL:
                        if prevChoice == nextChoice:
                            break
                        prevHigherPosDrop.add(prevChoice)
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # keyNextChoice = sortKeyNodeTuplePos(nextChoice)
                    # for prevChoice in prevReadyNodesTSL:
                    #     keyPrevChoice = sortKeyNodeTuplePos(prevChoice)
                    #     if keyPrevChoice < keyNextChoice:
                    #         prevHigherPosDrop.add(prevChoice)
                    # and we update next best drop choice
                    # get current max pos if any
                maxPosDropChoice = None
                if readyNodesTSLreversed:
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    nextChoiceC = readyNodesTSLreversed[0]
                    if nextChoiceC[1] > 0:
                        maxPosDropChoice = nextChoiceC
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # nextChoiceC = min(readyNodesTSLreversed,
                    #                   key = sortKeyNodeTuplePos,
                    #                   default = None)
                    # if not nextChoiceC is None and nextChoiceC[1] > 0:
                    #     maxPosDropChoice = nextChoiceC
                
                # in any case add the next max pos choice
                prevBestDropChoice.append(maxPosDropChoice)

                ## POS+NEG OPTIM ##
                ## clear obvious bad choices
                if minNegPeakChoice is None or \
                   currentMinNegPeak > currentMaxMem:
                    # visit all choices
                    for nextChoiceC in readyNodesTSLreversed:
                        tmpMemIncC = nextChoiceC[2]
                        local_peakC = currentMem + tmpMemIncC
                        # if node increase maxMem up to a known solution
                        # or above, we discard it
                        if local_peakC >= maxMemUsed:
                            continue
                        ## NEG OPTIM ##
                        if local_peakC >= currentMinNegPeak:
                            continue
                        ## POS OPTIM ##
                        if nextChoiceC in prevHigherPosDrop:
                            continue
                        # in any other case we keep it
                        nextChoiceList.append(nextChoiceC)
                if not minNegPeakChoice is None:
                    # in any case we keep the negative choice
                    nextChoiceList.append(minNegPeakChoice)
                
                ## END if not offsetSameMem
                
            # add the cleaned next choice list for the next iteration
            remainingChoiceList.append(nextChoiceList)

            # we have finished to schedule a node, test if a step is reached
            if currentRank + 1 == min(currentRankBegin + step, nbNodesTot):
                # we reached the end of step or ordering
                # logger.debug("End schedule at BnB step index: {}", currentRankBegin)
                needsUpdate = False
                nbNodesScheduled = len(currentOrdering)

                if nbNodesScheduled < nbNodesTot:
                    # (seems useless if ready list sorted by low impact)
                    if currentMem < bestCurrentMem:
                        needsUpdate = True
                        #logger.info("New last mem: {}", currentMem)
                    elif currentMem == bestCurrentMem:
                        if currentMaxMem < maxMemUsed:
                            needsUpdate = True
                            #logger.info("Equivalent last mem lower peak: {}", currentMaxMem)

                else:        
                    if offsetSameMem:
                        _, peakList = mpeak_cbp_schedule(dag, currentOrdering)
                        if computeBestSum:
                            memSum = sum(peakList)

                    # checking if new bound
                    if currentMaxMem < maxMemUsed:
                        nbSamePeak = 1
                        needsUpdate = True
                        logger.info("New max bound: {}", currentMaxMem)
                        #logger.info(dt.datetime.now())

                    # or equal bound with less memory
                    elif currentMaxMem == maxMemUsed:
                        nbSamePeak += 1
                        if computeBestSum and memSum < minMemSum:
                            needsUpdate = True
                            # logger.info("Equivalent bound lower sum mem: {}", memSum)

                        if computeBestSrt and \
                           is_other_order_better(minMemSrt, peakList[2:-2]):
                            # remove last peak which is always 0
                            needsUpdate = True
                            #logger.info("Equivalent bound lower srt mem.")

                            
                if needsUpdate:
                    bestOrdering[currentRankBegin:] = currentOrdering[currentRankBegin:]
                    maxMemUsed = currentMaxMem
                    if nbNodesScheduled < nbNodesTot:
                        bestCurrentMem = currentMem
                    else:
                        if computeBestSum:
                            minMemSum = memSum
                        if computeBestSrt:
                            minMemSrt = srt_compacted_peaks(peakList[2:-2])
                        
                # in any case except bestCompute*,
                # we can backtrack until the last increase or step begin
                lastRankIncrease = ranksMemIncrease[-1]
                if offsetSameMem:
                    backtrackRank = currentRank
                else:
                    backtrackRank = currentRankBegin
                    # logger.debug("Go back to rankBegin: {}", backtrackRank)
                if lastRankIncrease >= backtrackRank:
                    backtrackRank = ranksMemIncrease.pop()
                    # logger.debug("Go back to last increase: {}", backtrackRank)
                currentRank = backtrackRank - 1
                # remove node in reverse order until backtrackRank included
                for node in currentOrdering[-1:backtrackRank-nbNodesScheduled-1:-1]:
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node,
                                         readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                currentOrdering = currentOrdering[:backtrackRank]
                currentMaxMemOrdering = currentMaxMemOrdering[:backtrackRank+1]
                currentMaxMem = currentMaxMemOrdering[-1]
                remainingChoiceList = remainingChoiceList[:backtrackRank+1]
                if not offsetSameMem:
                    ## NEG+POS OPTIM ##
                    lastUntakenMinNegPeak = lastUntakenMinNegPeak[:backtrackRank+1]
                    currentMinNegPeakChoices = currentMinNegPeakChoices[:backtrackRank+1]
                    prevBestDropChoice = prevBestDropChoice[:backtrackRank+1]
    
    res = SchedRes()
    timedout = timeout_obj.timedout
    res.dicoRes['timeout'] = timedout
    res.keysToGather.add('time_search')
    nbAllRemainingChoices = [len(choices) for choices in remainingChoiceList]
    res.dicoRes['BandB_nbAllRemainingChoices'] = nbAllRemainingChoices
    if timedout:
        res.dicoRes['time_search'] = timeout_sec
        logger.info("Timeout BnB! ({:.6f} sec)", timeout_sec)
        logger.info("Not guaranteed optimal!")
        nbRemainingChoices = 1
        for nbChoices in nbAllRemainingChoices:
            if nbChoices > 0:
                nbRemainingChoices *= nbChoices
        logger.info("Number of remaining choices (prod): {:.2E} up to rank {}\n{}",
                    nbRemainingChoices, len(remainingChoiceList), nbAllRemainingChoices)
    else:
        time_BandB = timeout_obj.get_elapsed_time()
        res.dicoRes['time_search'] = time_BandB
        logger.info("Time BandB: {:.6f} (sec)", time_BandB)
        logger.info("Optimal bound (exhausted search).")
        res.dicoRes['minMemPeakAllOrdering'] = maxMemUsed

    res.dicoRes['memPeak'] = maxMemUsed
    if computeBestSum:
        res.dicoRes['memSum'] = minMemSum
    if computeBestSrt:
        res.dicoRes['memSrt'] = minMemSrt
    res.dicoRes['nbSamePeak'] = nbSamePeak
    res.dicoRes['ordering'] = bestOrdering
    if len(bestOrdering) < nbNodesTot:
        res.dicoRes['memPeak'] = maxMemAsked
        if not schedRes_heur:
            res.dicoRes['ordering'] = None
            logger.warning("No schedule found with peak lower than {}.", maxMemAsked)
        else:
            res.dicoRes['nbSamePeak'] = 1
            res.dicoRes['ordering'] = schedRes_heur.dicoRes['ordering']
            logger.warning("No schedule found better than heuristic.")
            
    return res


def get_forbidden_succ1(dag, dicoReadyNodes, nodeTupleFunc):
    """For each node, compute the set of its forbidden successor.

    :param dag: Dag to consider.
    :param dicoReadyNodes: Set of ready nodes.
    :returns: Dict of forbidden successors for any node.
    """
    def sortKeyNodeTupleComp(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then lowest drop if pos., or highest peak if neg.,
        then highest impact, and finally lowest node identifier 
        if pos., or highest id if neg. (from left to right).
        /!\ Reverse of sortKeyNodeTuplePos for positive nodes only!

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, -tupl[3], -nodeMemCost, -tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])

    
    dicoNodes = dict()
    for node, readyNodes in dicoReadyNodes.items():
        tuplN = nodeTupleFunc(node, dag.nodes[node])
        nodeMemCostN = tuplN[1]
        tmpMemIncN = tuplN[2]
        forbiddenNodes = set()
        for readyNode in readyNodes:
            tuplR = nodeTupleFunc(readyNode, dag.nodes[readyNode])
            nodeMemCostR = tuplR[1]
            tmpMemIncR = tuplR[2]
            peakNR = max(tmpMemIncN, nodeMemCostN + tmpMemIncR)
            peakRN = max(tmpMemIncR, nodeMemCostR + tmpMemIncN)
            if peakNR > peakRN:
               forbiddenNodes.add(readyNode)
            elif peakRN == peakNR:
                sortTuplR = sortKeyNodeTupleComp(tuplR)
                sortTuplN = sortKeyNodeTupleComp(tuplN)
                if sortTuplR > sortTuplN:
                    forbiddenNodes.add(readyNode)
                
        dicoNodes[node] = forbiddenNodes
    return dicoNodes


def estimateRemainingChoicesLens(schedLength, firstRemChoiceLens, finalRemChoiceLens):
    """Estimate the number of remaining choices and exploration ratio.

    :param schedLength: Schedule length.
    :param firstRemChoiceLens: Initial choice list lenghts.
    :param finalRemChoiceLens: Final choice list lengths.
    :returns: Tuple with estimated nb first choices and final choices, and index of first difference.
    """
    # compute avg branching factor
    sumNbChoices = sum(firstRemChoiceLens)
    avgBranchingFactor = sumNbChoices/len(firstRemChoiceLens)
    # get index of first difference
    indexDif = -1
    for firstLen,finalLen in zip(firstRemChoiceLens, finalRemChoiceLens):
        indexDif += 1
        if firstLen > finalLen:
            break
    idxComp = min([indexDif, len(firstRemChoiceLens)-1, len(finalRemChoiceLens)-1])
    # compute number choices
    nbChoicesCommon = 0
    try:
        for i in range(0, idxComp):
            nbChoicesCommon += firstRemChoiceLens[i]*(avgBranchingFactor**(schedLength-i-1))
        nbChoicesFirst = nbChoicesCommon + firstRemChoiceLens[idxComp]*(avgBranchingFactor**(schedLength-idxComp-1))
        nbChoicesFinal = nbChoicesCommon + finalRemChoiceLens[idxComp]*(avgBranchingFactor**(schedLength-idxComp-1))
    except OverflowError:
        return (float("inf"), float("inf"), indexDif)
    # return res
    return (nbChoicesFirst, nbChoicesFinal, indexDif)

    
def exhaustive_BandB_V2(dag, **kwargs):
    """Branch and Bound algorithm to explore all topological sorts.
    Unlike Knuth's algorithm, this one is quadratic in space.
    /!\ This version updates the one fixed for lctes'23.
    'computeBestSum' option is removed, as well as pos optim,
    replaced by the forbidden nodes optim.

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. Set node attribute 'minSucc'.
    :param kwargs: Dictionnary of optional default arguments:
    - 'minMemUpperBound' (default 0) Upper bound of the best memory schedule.
    If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
    - 'timeout' (default 0): Stop search if timeout is reached (given in sec.) 
    0 means never stop (by default). Timeout is checked by specific alarm signal.
    - 'step' (default None is number of nodes in DAG): Non-optimal heuristic
    to perform successive partial BandB of schedule length 'step'.
    - 'overlap' (default 0): Parameter of the 'step' non-optimal heuristic
    setting the overlap length between steps (strictly less than 'step' itself).
    - 'computeBestSrt' (default False) Compute the best sorted memory usage.
    /!\ Increase the complexity up to quadratic time for each found schedule.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'nbOrdering' the number of ordering until the end,
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak').
    """
    check_cbp_tmpMemInc(dag)
    set_node_peakDrop(dag, opposite=True)
    timeout_sec = kwargs.get('timeout', 0)
    minMemUpperBound = kwargs.get('minMemUpperBound', 0)
    minMemHeuristic = kwargs.get('minMemHeuristic', None)
    computeBestSrt = kwargs.get('computeBestSrt', False)

    if not (minMemHeuristic is None):
        schedRes_heur = minMemHeuristic(dag, **kwargs)
        maxMemAsked = schedRes_heur.dicoRes['memPeak']
    else:
        maxMemAsked = float('inf')
    
    offsetSameMem = computeBestSrt
    minMemSrt = [float('inf')]
    nbSamePeak = 0
    if (minMemUpperBound <= 0 and not (minMemHeuristic is None)) or \
       maxMemAsked <= minMemUpperBound:
        maxMemAsked = schedRes_heur.dicoRes['memPeak']
        if computeBestSrt:
            _, peakList = mpeak_cbp_schedule(
                dag, schedRes_heur.dicoRes['ordering'])
            # remove last peak which is always 0
            minMemSrt = srt_compacted_peaks(peakList[2:-2])
        logger.info("Starting BandB with heur. max bound: {}", maxMemAsked)
    elif minMemUpperBound > 0:
        schedRes_heur = None
        maxMemAsked = minMemUpperBound
        if computeBestSrt:
            minMemSrt = [minMemUpperBound]
        logger.info("Starting BandB with given max bound: {}", maxMemAsked)
    else:
        logger.info("Starting BandB with no bound")
    nbNodesTot = len(dag.nodes())
    step = kwargs.get('step', None)
    if step is None:
        step = nbNodesTot
    if step < 0 or step > nbNodesTot:
        raise ValueError("Incorrect 'step' parameter.")
    overlap = kwargs.get('overlap', 0)
    if overlap < 0 or overlap >= step:
        raise ValueError("Incorrect 'overlap' parameter.")
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0

    def getSortNodeTuple(node, dicoNode):
        """Return the tuple needed to sort a node.

        :param node: Node to consider.
        :param dicoNode: Dico of node attributes.
        :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc', 'memPeakDrop")
        """
        return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'], dicoNode['memPeakDrop'])
    def sortKeyNodeTuplePos(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then highest drop if pos., or peak if neg.,
        then lowest impact if pos., or highest impact if neg.,
        and finally lowest node identifier (from left to right).

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, tupl[3], nodeMemCost, tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])
    def sortKeyNodeTupleAlt(tupl):
        """Alternative key to sort node tuple by decreasing impact,
        then decreasing peak, then node identifier.

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        return (-tupl[1], -tupl[2], tupl[0])
    # set the main ready list
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    readyNodesTSLreversed = SortedList(key = sortKeyNodeTuplePos)
    for node in firstNodes:
        dicoNode = dag.nodes[node]
        readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
    # set forbidden nodes
    dicoReadyNodes = get_dico_readyNodes(dag, closure=None)
    dicoForbiddenSucc1 = get_forbidden_succ1(dag, dicoReadyNodes, getSortNodeTuple)
    # set all common variables
    currentRankBegin = 0
    lengthForward = step - overlap
    # complicated next formula to predict the rank of the last step search
    lastRankBegin = lengthForward * (((nbNodesTot  + lengthForward - 1) // lengthForward) - 1)
    currentRank = -1
    maxMemUsed = maxMemAsked
    bestCurrentMem = maxMemAsked
    currentMaxMem = 0
    currentMem = 0
    currentMaxMemOrdering = [0]
    ranksMemIncrease = [-1]
    lastUntakenMinNegPeak = [maxMemAsked]
    currentMinNegPeakChoices = [None]
    bestOrdering = []
    currentOrdering = []
    backupBeginStepReadyNodes = SortedList(iterable = readyNodesTSLreversed,
                                           key = sortKeyNodeTuplePos)
    remainingChoiceList = [list(readyNodesTSLreversed)]
    firstRemChoiceLens = None
    timeout_obj = Timeout()
    with timeout_obj.timeout_guard(timeout_sec):
        # perform BFS like
        while remainingChoiceList:
            currentChoices = remainingChoiceList[-1]
            if not currentChoices:
                if currentRankBegin == lastRankBegin and currentRank == lastRankBegin - 1:
                    # if no choice at last rank, we are done for the whole search
                    break
                elif currentRank == currentRankBegin - 1:
                    # if no choice at step begin then we explore next step
                    if len(bestOrdering) < currentRankBegin + step:
                        # except if previous step did not find any best schedule
                        # then we can escape
                        break
                    bestCurrentMem = maxMemAsked
                    maxMemUsed = maxMemAsked
                    currentRankBegin += lengthForward
                    # logger.debug("Begin search at BnB step index: {}", currentRankBegin)
                    # we must go back to the state of actual best step ordering
                    readyNodesTSLreversed = backupBeginStepReadyNodes
                    for node in bestOrdering[currentRank+1:currentRankBegin]:
                        # replay regular test
                        dicoNode = dag.nodes[node]
                        choice = getSortNodeTuple(node, dicoNode)
                        readyNodesTSLreversed.remove(choice)
                        nodeMemCost = dicoNode['nodeMemCost']
                        tmpMemInc = dicoNode['tmpMemInc']
                        local_peak = currentMem + tmpMemInc
                        # test local_peak only since check_cbp_tmpMemInc
                        if local_peak > currentMaxMem:
                            currentMaxMem = local_peak
                            ranksMemIncrease.append(currentRank)
                        # update local variables
                        currentMem += nodeMemCost
                        currentRank += 1
                        currentOrdering.append(node)
                        currentMaxMemOrdering.append(currentMaxMem)
                        addSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                          getSortNodeTuple)
                        # fix the choice
                        remainingChoiceList.append([])
                    if not offsetSameMem:
                        ## NEG OPTIM ##
                        for it in range(0, lengthForward):
                            lastUntakenMinNegPeak.append(maxMemAsked)
                            currentMinNegPeakChoices.append(None)
                    # backup ready list and fix last remaining choices
                    backupBeginStepReadyNodes = SortedList(
                        iterable = readyNodesTSLreversed,
                        key = sortKeyNodeTuplePos)
                    remainingChoiceList[-1] = list(readyNodesTSLreversed)
                    # pursue search
                    continue
                else:
                    # otherwise we go back to previous choice, standard rollback
                    currentRank -= 1
                    node = currentOrdering.pop()
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                    currentMaxMemOrdering.pop()
                    prevMaxMem = currentMaxMemOrdering[-1]
                    if currentMaxMem > prevMaxMem:
                        ranksMemIncrease.pop()
                    currentMaxMem = prevMaxMem
                    remainingChoiceList.pop()
                    if not offsetSameMem:
                        ## NEG OPTIM ##
                        lastUntakenMinNegPeak.pop()
                        currentMinNegPeakChoices.pop()
                    # logger.debug("One step backward (no choices)")
                    continue
            # if arriving here, there was at least one choice,
            # schedule it
            nextChoice = currentChoices.pop()
            node = nextChoice[0]
            nodeMemCost = nextChoice[1]
            tmpMemInc = nextChoice[2]
            readyNodesTSLreversed.remove(nextChoice)
            local_peak = currentMem + tmpMemInc
            currentMem += nodeMemCost
            currentRank += 1
            if local_peak > currentMaxMem:
                currentMaxMem = local_peak
                ranksMemIncrease.append(currentRank)
            # next test is useless since check_cbp_tmpMemInc
            # if currentMem > currentMaxMem:
            #     currentMaxMem = currentMem
            #     ranksMemIncrease.append(currentRank)
            currentOrdering.append(node)
            currentMaxMemOrdering.append(currentMaxMem)
            addSortedSuccsBFS(dag, node,
                              readyNodesTSLreversed,
                              getSortNodeTuple)
            # now clean the nextChoiceList
            nextChoiceList = list()
            if offsetSameMem:
                # clear obvious bad choices
                for nextChoice in readyNodesTSLreversed:
                    tmpMemInc = nextChoice[2]
                    local_peak = currentMem + tmpMemInc
                    # if node increase maxMem *above* a known solution
                    # we discard it
                    if local_peak > maxMemUsed:
                        # if NO 'computeBest...' option activated,
                        # then offsetSameMem == 0 and it is strict equality
                        continue
                        # IF the list was sorted by increasing peaks,
                        # then we could remove this one, and all subsequent
                        # but it does not seem so efficient
                        #nextChoiceList = nextChoiceList[:idx]
                    # next test is useless since check_cbp_tmpMemInc
                    # new_mem = currentMem + nodeMemCost
                    # if new_mem > maxMemUsed:
                    #     IF the list is sorted by increasing peaks,
                    #     then we can remove this one, and all subsequent
                    #     continue
                    nextChoiceList.append(nextChoice)
            else:
                ## BEGIN if not offsetSameMem

                ## NEG OPTIM ##
                # get current min neg if any
                minNegPeakChoice = None
                if readyNodesTSLreversed:
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    nextChoiceC = readyNodesTSLreversed[-1]
                    if nextChoiceC[1] <= 0:
                        minNegPeakChoice = nextChoiceC
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # nextChoiceC = max(readyNodesTSLreversed,
                    #                   key = sortKeyNodeTuplePos,
                    #                   default = None)
                    # if not nextChoiceC is None and nextChoiceC[1] <= 0:
                    #     minNegPeakChoice = nextChoiceC
                
                # update min neg peak/choices
                prevMinNegPeakChoice = currentMinNegPeakChoices[-1]
                prevMinNegPeak = lastUntakenMinNegPeak[-1]
                if nextChoice == prevMinNegPeakChoice or \
                   prevMinNegPeakChoice is None:
                    lastUntakenMinNegPeak.append(prevMinNegPeak)
                else:
                    prevUntakenNegPeak = currentMem - nextChoice[1] + prevMinNegPeakChoice[2]
                    prevMinNegPeak = min(lastUntakenMinNegPeak[-1], prevUntakenNegPeak)
                    lastUntakenMinNegPeak.append(prevMinNegPeak)
                currentMinNegPeakChoices.append(minNegPeakChoice)

                # in the end we get the min neg peak of most recent untaken one
                currentMinNegPeak = prevMinNegPeak
                if not (minNegPeakChoice is None):
                    currentMinNegPeak = min(currentMinNegPeak, currentMem + minNegPeakChoice[2])
 
                ## FORB+NEG OPTIM ##
                ## clear obvious bad choices
                if minNegPeakChoice is None or \
                   currentMinNegPeak > currentMaxMem:
                    # visit all choices
                    for nextChoiceC in readyNodesTSLreversed:
                        tmpMemIncC = nextChoiceC[2]
                        local_peakC = currentMem + tmpMemIncC
                        # if node increase maxMem up to a known solution
                        # or above, we discard it
                        if local_peakC >= maxMemUsed:
                            continue
                        ## NEG OPTIM ##
                        if local_peakC >= currentMinNegPeak:
                            continue
                        ## FORBIDDEN SUCC OPTIM ##
                        if nextChoiceC[0] in dicoForbiddenSucc1[node]:
                            continue
                        # in any other case we keep it
                        nextChoiceList.append(nextChoiceC)
                if not minNegPeakChoice is None:
                    # in any case we keep the negative choice
                    nextChoiceList.append(minNegPeakChoice)
                
                ## END if not offsetSameMem
                
            # add the cleaned next choice list for the next iteration
            remainingChoiceList.append(nextChoiceList)

            # we have finished to schedule a node, test if a step is reached
            if currentRank + 1 == min(currentRankBegin + step, nbNodesTot):
                # we reached the end of step or ordering
                # logger.debug("End schedule at BnB step index: {}", currentRankBegin)
                needsUpdate = False
                nbNodesScheduled = len(currentOrdering)

                if nbNodesScheduled < nbNodesTot:
                    # (seems useless if ready list sorted by low impact)
                    if currentMem < bestCurrentMem:
                        needsUpdate = True
                        #logger.info("New last mem: {}", currentMem)
                    elif currentMem == bestCurrentMem:
                        if currentMaxMem < maxMemUsed:
                            needsUpdate = True
                            #logger.info("Equivalent last mem lower peak: {}", currentMaxMem)
                        
                else:        
                    if computeBestSrt:
                        _, peakList = mpeak_cbp_schedule(dag, currentOrdering)

                    # checking if new bound
                    if currentMaxMem < maxMemUsed:
                        nbSamePeak = 1
                        needsUpdate = True
                        logger.info("New max bound: {}", currentMaxMem)
                        #logger.info(dt.datetime.now())

                    # or equal bound with less memory
                    elif currentMaxMem == maxMemUsed:
                        nbSamePeak += 1
                        if computeBestSrt and \
                           is_other_order_better(minMemSrt, peakList[2:-2]):
                            # remove last peak which is always 0
                            needsUpdate = True
                            #logger.info("Equivalent bound lower srt mem.")
                                
                if needsUpdate:
                    bestOrdering[currentRankBegin:] = currentOrdering[currentRankBegin:]
                    maxMemUsed = currentMaxMem
                    if nbNodesScheduled < nbNodesTot:
                        bestCurrentMem = currentMem
                    else:
                        if firstRemChoiceLens is None:
                            firstRemChoiceLens = [1+len(lst) for lst in remainingChoiceList]
                        if computeBestSrt:
                            minMemSrt = srt_compacted_peaks(peakList[2:-2])
                            
                # in any case except bestCompute*,
                # we can backtrack until the last increase or step begin
                lastRankIncrease = ranksMemIncrease[-1]
                if offsetSameMem:
                    backtrackRank = currentRank
                else:
                    backtrackRank = currentRankBegin
                    # logger.debug("Go back to rankBegin: {}", backtrackRank)
                if lastRankIncrease >= backtrackRank:
                    backtrackRank = ranksMemIncrease.pop()
                    # logger.debug("Go back to last increase: {}", backtrackRank)
                currentRank = backtrackRank - 1
                # remove node in reverse order until backtrackRank included
                for node in currentOrdering[-1:backtrackRank-nbNodesScheduled-1:-1]:
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node,
                                         readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                currentOrdering = currentOrdering[:backtrackRank]
                currentMaxMemOrdering = currentMaxMemOrdering[:backtrackRank+1]
                currentMaxMem = currentMaxMemOrdering[-1]
                remainingChoiceList = remainingChoiceList[:backtrackRank+1]
                if not offsetSameMem:
                    ## NEG OPTIM ##
                    lastUntakenMinNegPeak = lastUntakenMinNegPeak[:backtrackRank+1]
                    currentMinNegPeakChoices = currentMinNegPeakChoices[:backtrackRank+1]
    
    res = SchedRes()
    timedout = timeout_obj.timedout
    res.dicoRes['timeout'] = timedout
    res.keysToGather.add('time_search')
    if timedout:
        res.dicoRes['time_search'] = timeout_sec
        logger.info("Timeout BnB! ({:.6f} sec)", timeout_sec)
        logger.info("Not guaranteed optimal!")
        if firstRemChoiceLens is None:
            logger.info("No exploration estimate available! (did not finished first traversal)")
        else:
            finalRemChoiceLens = [1+len(lst) for lst in remainingChoiceList]
            (nbChoicesFirst, nbChoicesFinal, idx) = estimateRemainingChoicesLens(
                nbNodesTot, firstRemChoiceLens, finalRemChoiceLens)
            exploredRatio = (nbChoicesFirst - nbChoicesFinal) / nbChoicesFirst
            speed = (nbChoicesFirst - nbChoicesFinal) / timeout_sec
            logger.info("Estimated number of remaining choices: {:.2E}\n"+
                        "(speed: {:.2E} choice/sec)\n" +
                        "(explored ratio: {:.2E})\n(smallest common rank: {})",
                        nbChoicesFinal, speed, exploredRatio, idx)
            res.dicoRes['BandB_nbAllRemainingChoices'] = nbChoicesFinal
    else:
        time_BandB = timeout_obj.get_elapsed_time()
        res.dicoRes['time_search'] = time_BandB
        logger.info("Time BandB: {:.6f} (sec)", time_BandB)
        logger.info("Optimal bound (exhausted search).")
        res.dicoRes['minMemPeakAllOrdering'] = maxMemUsed

    res.dicoRes['memPeak'] = maxMemUsed
    if computeBestSrt:
        res.dicoRes['memSrt'] = minMemSrt
    res.dicoRes['nbSamePeak'] = nbSamePeak
    res.dicoRes['ordering'] = bestOrdering
    if len(bestOrdering) < nbNodesTot:
        res.dicoRes['memPeak'] = maxMemAsked
        if not schedRes_heur:
            res.dicoRes['ordering'] = None
            logger.warning("No schedule found with peak lower than {}.", maxMemAsked)
        else:
            res.dicoRes['nbSamePeak'] = 1
            res.dicoRes['ordering'] = schedRes_heur.dicoRes['ordering']
            logger.warning("No schedule found better than heuristic.")
            
    return res


def get_forbidden_succ1ext(dag, dicoReadyNodes, nodeTupleFunc, nodeTupleComp):
    """For each node, compute the set of its forbidden successor.

    :param dag: Dag to consider.
    :param dicoReadyNodes: Set of ready nodes.
    :param nodeTupleFunc: Function to make a tuple from a node.
    :param nodeTupleComp: Function to compare two node tuples.
    :returns: Dict of forbidden successors for any node.
    """
    dicoNodes = dict()
    for node, readyNodes in dicoReadyNodes.items():
        tuplN = nodeTupleFunc(node, dag.nodes[node])
        nodeMemCostN = tuplN[1]
        tmpMemIncN = tuplN[2]
        forbiddenNodes = set()
        for readyNode in readyNodes:
            tuplR = nodeTupleFunc(readyNode, dag.nodes[readyNode])
            nodeMemCostR = tuplR[1]
            tmpMemIncR = tuplR[2]
            peakNR = max(tmpMemIncN, nodeMemCostN + tmpMemIncR)
            peakRN = max(tmpMemIncR, nodeMemCostR + tmpMemIncN)
            if peakNR > peakRN:
               forbiddenNodes.add(readyNode)
            elif peakRN == peakNR:
                sortTuplR = nodeTupleComp(tuplR)
                sortTuplN = nodeTupleComp(tuplN)
                if sortTuplR > sortTuplN:
                    forbiddenNodes.add(readyNode)
                
        dicoNodes[(node,)] = frozenset(forbiddenNodes)
    return dicoNodes


def get_possible_succs(dag, closure, dicoReadyNodes,
                       sched, forbiddenSuccs):
    """Compute the set of all possible nodes directly extending
    the given schedule.

    :param dag: Dag.
    :param closure: Dag closure.
    :param dicoReadyNodes: Dict of ready nodes.
    :param sched: Current sched to extend with a ready node
    or an immediate successor of last node.
    :param forbiddenSuccs: Current sched forbidden successors. 
    :returns: Set of all possible successors.
    """
    schedSet = frozenset(sched)
    ## get list of all possible other nodes
    candidateNodes = set()
    for n in dicoReadyNodes[sched[-1]]:
        if not (n in schedSet) and \
           not (n in forbiddenSuccs):
            candidateNodes.add(n)
    candidateNodes.update(dag.succ[sched[-1]])
    allDescendants = set()
    allAncestors = set()
    for n in sched:
        allDescendants.update(closure.succ[n])                
        allAncestors.update(closure.pred[n])                
    possibleSuccs = set()
    for n in candidateNodes:
        inter = allDescendants.intersection(closure.pred[n])
        if inter <= schedSet and not (n in allAncestors):
            possibleSuccs.add(n)
    return possibleSuccs


def remove_forbidden_prefixes(dag, closure, dicoReadyNodes,
                              dicoForbiddenSuccs, longestAllowedPrefixes):
    """Remove unreachable prefixes and forbids successor nodes
    according to new longer prefixes.

    :param dag: Dag.
    :param closure: Dag closure.
    :param dicoReadyNodes: Dict of ready nodes.
    :param sched: Current sched to extend with a ready node
    or an immediate successor of last node.
    :param dicoForbiddenSuccs: Current dict of sched with 
    forbidden successors starting with source nodes.
    :param longestAllowedPrefixes: New (longer) sched
    starting with source nodes.
    """
    
    def isPrefixOf(prefixToTest, longerTupl):
        """Check if integer tuple prefixes another one.

        :param tupl1: Prefix tuple to test.
        :param tupl2: Longer tuple.
        :returns: True if a complete prefix.
        """
        idx = 0
        maxIdx = len(prefixToTest)
        while idx < maxIdx and \
              prefixToTest[idx] == longerTupl[idx]:
            idx += 1
        return idx == maxIdx

    prefixesToRemove = set()
    for schedPrefix, forbiddenSuccs in dicoForbiddenSuccs.items():
        allowedSuffixNodes = set()
        lenPrefix = len(schedPrefix)
        for longestPrefix in longestAllowedPrefixes:
            if isPrefixOf(schedPrefix, longestPrefix):
                allowedSuffixNodes.add(longestPrefix[lenPrefix])
        if not allowedSuffixNodes:
            prefixesToRemove.add(schedPrefix)
        else:
            ## in such case we have to keep only allowed nodes
            possibleSuccs = get_possible_succs(
                dag, closure, dicoReadyNodes, schedPrefix, forbiddenSuccs)
            forbiddenNodes = possibleSuccs.difference(allowedSuffixNodes)
            forbiddenSuccs.update(forbiddenNodes)
            
    # final clean
    for prefix in prefixesToRemove:
        del dicoForbiddenSuccs[prefix]
    

def get_forbidden_succN(dag, sourceNodes, nodeTupleFunc, negOptim=False,
                        maxDepth=1, maxKeys=1e+5, includePrefix=True):
    """Compute the set of forbidden successors for each reachable
    schedule tuple.

    :param dag: Dag to consider.
    :param sourceNodes: Source nodes of the dag.
    :param nodeTupleFunc: Function to get tuple from node and its dict.
    :param negOptim: (default False) It True, filters positive
    nodes according to possible positive succs and current local peak.
    /!\ This sometimes contradicts other optimizations, as the
    counter-example generated by generateBadBandB_V2extSchedGraph():
    indeed compared nodes are not always ready together.
    This option should not be used anymore.
    :param maxDepth: (default 1) Length of sched tuples.
    :param maxKeys: (default 5e+5) Maximal length of returned
    dictionnary, used only if depth > 1, without prefixes.
    :param includePrefix: (default True) If False, 
    does not include prefixes starting with source nodes.
    :returns: Reached depth and dict of
    sched tuples to forbidden successors.
    """
    def nodeTupleComp(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then lowest drop if pos., or highest peak if neg.,
        then highest impact, and finally lowest node identifier 
        if pos., or highest id if neg. (from left to right).
        /!\ Reverse of sortKeyNodeTuplePos for positive nodes only!

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, -tupl[3], -nodeMemCost, -tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])

    
    closure = networkx.transitive_closure_dag(dag)
    dicoReadyNodes = get_dico_readyNodes(dag, closure=closure)
    dicoForbiddenSuccCur = get_forbidden_succ1ext(dag, dicoReadyNodes,
                                               nodeTupleFunc, nodeTupleComp)
    dicoForbiddenSuccSources = dict()
    
    depth = 1
    while depth < min(maxDepth, len(dag.nodes())):
        depth += 1
        logger.info("Computing forbidden succs at depth: {}", depth)
        dicoBestSuccSched = dict()
        dicoBestSuccMem = dict()
        dicoForbiddenSuccNext = dict()
        dicoForbiddenSuccTemp = dict()
        for k, v in dicoForbiddenSuccCur.items():
            dicoForbiddenSuccTemp[k] = set(v)
        ## iterate over items of previous depth
        for sched, forbiddenSuccs in dicoForbiddenSuccCur.items():
            possibleSuccs = get_possible_succs(
                dag, closure, dicoReadyNodes, sched, forbiddenSuccs)
            schedPeak, schedMem = mpeak_cbp_schedule(dag, sched, check=False)
            schedSet = frozenset(sched)
            catSched = list(sched)
            catSched.append(None)
            dicoCatMem = dict()
            triggeredSuccs = list()
            for n in possibleSuccs:
                catSched[-1] = n
                catSchedSet = frozenset(catSched)
                if dicoForbiddenSuccCur.get(tuple(catSched[1:])) is None:
                    ## discard concat if subpart was not already considered
                    continue
                dicoCatMem[n] = mpeak_cbp_schedule(dag, catSched, check=False, prefixList=schedMem)
                if set(dag.pred[n]) <= schedSet:
                    nodeTuple = nodeTupleFunc(n, dag.nodes[n])
                    triggeredSuccs.append(nodeTuple)
            ## default case: filteredSuccs are keys of dicoCatMem
            filteredSuccs = list(dicoCatMem.keys())

            ## wrong neg optim !!
            if negOptim:
                ## only if negOptim, filters nodes as in BandB
                minCompChoice = max(triggeredSuccs,
                                   key = nodeTupleComp,
                                   default = None)
                if not minCompChoice is None and minCompChoice[1] <= 0:
                    filteredSuccs = [minCompChoice[0]]
                    if schedPeak >= dicoCatMem[minCompChoice[0]][0]:
                        ## keep no pos node and only neg ones with
                        ## lower peak (which cannot be anymore
                        ## in triggeredSuccs if strict inequality)
                        for n in dicoCatMem.keys():
                            tuplN = nodeTupleFunc(n, dag.nodes[n])
                            if tuplN[1] > 0:
                                continue
                            elif tuplN[2] < minCompChoice[2]:
                                filteredSuccs.append(n)
                            elif tuplN[2] == minCompChoice[2] and \
                                 not (n in triggeredSuccs):
                                sortTuplC = nodeTupleComp(minCompChoice)
                                sortTuplN = nodeTupleComp(tuplN)
                                if sortTuplN > sortTuplC:
                                    filteredSuccs.append(n)
                    else:
                        ## keep only pos and neg nodes with
                        ## lower peak (which cannot be anymore
                        ## in triggeredSuccs if strict inequality
                        ## for neg, inequality is always strict for pos)
                        for n in dicoCatMem.keys():
                            tuplN = nodeTupleFunc(n, dag.nodes[n])
                            if tuplN[2] < minCompChoice[2]:
                                filteredSuccs.append(n)
                            elif tuplN[2] == minCompChoice[2]:
                                if tuplN[1] > 0:
                                    continue
                                elif not (n in triggeredSuccs):
                                    sortTuplC = nodeTupleComp(minCompChoice)
                                    sortTuplN = nodeTupleComp(tuplN)
                                    if sortTuplN > sortTuplC:
                                        filteredSuccs.append(n)

            ## evaluate concat
            for n in filteredSuccs:
                catSched[-1] = n
                catSchedSet = frozenset(catSched)
                catSchedTupl = tuple(catSched)
                if negOptim:
                    # then we can work on the peak only
                    otherMem = dicoCatMem[n][0]
                else:
                    otherMem = dicoCatMem[n][1]
                if dicoBestSuccSched.get(catSchedSet) is None:
                    ## set best arrangement if no other
                    dicoBestSuccSched[catSchedSet] = catSchedTupl
                    dicoBestSuccMem[catSchedSet] = otherMem
                else:
                    curBestMem = dicoBestSuccMem[catSchedSet]
                    curBestSched = dicoBestSuccSched[catSchedSet]
                    lastBestNode = curBestSched[-1]
                    tuplC = nodeTupleFunc(lastBestNode, dag.nodes[lastBestNode])
                    tuplN = nodeTupleFunc(n, dag.nodes[n])
                    sortTuplC = nodeTupleComp(tuplC)
                    sortTuplN = nodeTupleComp(tuplN)
                    if negOptim:
                        isNewBest = False
                        if otherMem <= curBestMem:
                            isNewBest = True
                            if otherMem == curBestMem:
                                isNewBest = None
                    else:
                        isNewBest = is_other_order_better(curBestMem, otherMem)
                    if isNewBest == True or \
                       (isNewBest is None and sortTuplC < sortTuplN):
                        ## forbid best current arrangement
                        forbiddenNodes = dicoForbiddenSuccTemp.setdefault(
                            tuple(curBestSched[:-1]), set())
                        forbiddenNodes.add(lastBestNode)
                        ## set best arrangement as new one is better
                        dicoBestSuccSched[catSchedSet] = catSchedTupl
                        dicoBestSuccMem[catSchedSet] = otherMem
                    else:
                        ## forbid tested arrangement
                        forbiddenNodes = dicoForbiddenSuccTemp.setdefault(
                            tuple(sched), set())
                        forbiddenNodes.add(n)

            ## break (sched level) if maxSize is reached
            if len(dicoBestSuccSched) > maxKeys:
                break

        ## break (depth level) if maxSize is reached
        if len(dicoBestSuccSched) > maxKeys:
            logger.info("Reached max number of keys ({}), rollback to previous dict.",
                        int(maxKeys))
            depth -= 1
            break
        
        ## set next dico
        longestAllowedPrefixes = set()
        for nextSched in dicoBestSuccSched.values():
            schedSet = set(nextSched)
            schedTupl = tuple(nextSched)
            if nextSched[0] in sourceNodes:
                longestAllowedPrefixes.add(schedTupl)
            forbiddenNodes = dicoForbiddenSuccTemp[tuple(nextSched[1:])]
            dicoForbiddenSuccNext[schedTupl] = frozenset(
                forbiddenNodes.difference(schedSet))
        dicoForbiddenSuccCur = dicoForbiddenSuccNext
        if includePrefix:
            ## update elements of previous depth which starts at source
            for sched, forbiddenSuccs in dicoForbiddenSuccTemp.items():
                if sched[0] in sourceNodes:
                    dicoForbiddenSuccSources[sched] = forbiddenSuccs
            ## clean prefixes
            remove_forbidden_prefixes(dag, closure, dicoReadyNodes,
                                      dicoForbiddenSuccSources,
                                      longestAllowedPrefixes)
        ## safety check
        if len(dicoForbiddenSuccCur) == 0:
            if negOptim:
                # retry without the optim
                logger.warning("Dict of forbidden succs is empty, retry without the neg. optim.")
                return get_forbidden_succN(dag, sourceNodes, nodeTupleFunc,
                                           False, maxDepth, maxKeys, includePrefix)
            else:
                raise Exception("Dict of forbidden succs should not be empty.")
        logger.info("dico forbidden succs size: {}", len(dicoForbiddenSuccCur))

    ## if we end up with a single element, no need for BandB
    if len(dicoForbiddenSuccCur) == 1 and depth == len(dag.nodes()):
        for tupSched in dicoForbiddenSuccCur.keys():
            listSched = list(tupSched)
            peak, _ = mpeak_cbp_schedule(dag, listSched, check=True)
            logger.info("Asserted peak forbidden succs: {}", peak)

    if includePrefix:
        # update dico with sources sched before return
        dicoForbiddenSuccCur.update(dicoForbiddenSuccSources)
        
    return (depth, dicoForbiddenSuccCur)

    
def exhaustive_BandB_V2ext(dag, **kwargs):
    """Branch and Bound algorithm to explore all topological sorts.
    Unlike Knuth's algorithm, this one is quadratic in space.
    /!\ This version updates the V2 one with forbidden nodes
    per schedule portion (instead of per node).

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0. Set node attribute 'minSucc'.
    :param kwargs: Dictionnary of optional default arguments:
    - 'minMemUpperBound' (default 0) Upper bound of the best memory schedule.
    If None or negative, then it is computed by heuristic_linear_1dot5steps_impact(dag).
    - 'timeout' (default 0): Stop search if timeout is reached (given in sec.) 
    0 means never stop (by default). Timeout is checked by specific alarm signal.
    - 'step' (default None is number of nodes in DAG): Non-optimal heuristic
    to perform successive partial BandB of schedule length 'step'.
    - 'overlap' (default 0): Parameter of the 'step' non-optimal heuristic
    setting the overlap length between steps (strictly less than 'step' itself).
    - 'computeBestSrt' (default False) Compute the best sorted memory usage.
    /!\ Increase the complexity up to quadratic time for each found schedule.
    - 'optimDepth' (default 0: dag size) Positive depth of combinatorial optimization.
    - 'optimKeys' (default 1e5) Maximal size of forbidden succ
    dictionnary, complimentary to 'optimDepth' option.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'nbOrdering' the number of ordering until the end,
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak').
    """
    check_cbp_tmpMemInc(dag)
    set_node_peakDrop(dag, opposite=True)
    timeout_sec = kwargs.get('timeout', 0)
    minMemUpperBound = kwargs.get('minMemUpperBound', 0)
    minMemHeuristic = kwargs.get('minMemHeuristic', None)
    computeBestSrt = kwargs.get('computeBestSrt', False)

    if not (minMemHeuristic is None):
        schedRes_heur = minMemHeuristic(dag, **kwargs)
        maxMemAsked = schedRes_heur.dicoRes['memPeak']
    else:
        maxMemAsked = float('inf')
    
    offsetSameMem = computeBestSrt
    minMemSrt = [float('inf')]
    nbSamePeak = 0
    if (minMemUpperBound <= 0 and not (minMemHeuristic is None)) or \
       maxMemAsked <= minMemUpperBound:
        maxMemAsked = schedRes_heur.dicoRes['memPeak']
        if computeBestSrt:
            _, peakList = mpeak_cbp_schedule(
                dag, schedRes_heur.dicoRes['ordering'])
            # remove last peak which is always 0
            minMemSrt = srt_compacted_peaks(peakList[2:-2])
        logger.info("Starting BandB with heur. max bound: {}", maxMemAsked)
    elif minMemUpperBound > 0:
        schedRes_heur = None
        maxMemAsked = minMemUpperBound
        if computeBestSrt:
            minMemSrt = [minMemUpperBound]
        logger.info("Starting BandB with given max bound: {}", maxMemAsked)
    else:
        logger.info("Starting BandB with no bound.")
        
    nbNodesTot = len(dag.nodes())
    step = kwargs.get('step', None)
    if step is None:
        step = nbNodesTot
    if step < 0 or step > nbNodesTot:
        raise ValueError("Incorrect 'step' parameter.")
    overlap = kwargs.get('overlap', 0)
    if overlap < 0 or overlap >= step:
        raise ValueError("Incorrect 'overlap' parameter.")
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
        
    optimDepth = kwargs.get('optimDepth', 0)
    if optimDepth < 0:
        raise ValueError("Incorrect 'optimDepth' parameter.")
    if optimDepth == 0:
        optimDepth = nbNodesTot
    optimKeys = kwargs.get('optimKeys', 1e5)

    # set local functions (for node tuple sort)
    def getSortNodeTuple(node, dicoNode):
        """Return the tuple needed to sort a node.

        :param node: Node to consider.
        :param dicoNode: Dico of node attributes.
        :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc', 'memPeakDrop")
        """
        return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'], dicoNode['memPeakDrop'])
    def sortKeyNodeTuplePos(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then highest drop if pos., or peak if neg.,
        then lowest impact if pos., or highest impact if neg.,
        and finally lowest node identifier (from left to right).

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, tupl[3], nodeMemCost, tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])
    # set first nodes and forbidden ones
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    readyNodesTSLreversed = SortedList(key = sortKeyNodeTuplePos)
    for node in firstNodes:
        dicoNode = dag.nodes[node]
        readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
    time1 = time.perf_counter()
    optimDepth, dicoForbiddenSuccs = get_forbidden_succN(
        dag, firstNodes, getSortNodeTuple, negOptim=False,
        maxDepth=optimDepth, maxKeys=optimKeys)
    time2 = time.perf_counter()
    time_precompute = time2 - time1

    firstChoices = []
    for sourceNodeTupl in readyNodesTSLreversed:
        if (sourceNodeTupl[0],) in dicoForbiddenSuccs:
            firstChoices.append(sourceNodeTupl)
    # set all other common variables
    currentRankBegin = 0
    lengthForward = step - overlap
    # complicated next formula to predict the rank of the last step search
    lastRankBegin = lengthForward * (((nbNodesTot  + lengthForward - 1) // lengthForward) - 1)
    currentRank = -1
    maxMemUsed = maxMemAsked
    bestCurrentMem = maxMemAsked
    currentMaxMem = 0
    currentMem = 0
    currentMaxMemOrdering = [0]
    ranksMemIncrease = [-1]
    lastUntakenMinNegPeak = [maxMemAsked]
    currentMinNegPeakChoices = [None]
    bestOrdering = []
    currentOrdering = []
    backupBeginStepReadyNodes = SortedList(iterable = readyNodesTSLreversed,
                                           key = sortKeyNodeTuplePos)
    remainingChoiceList = [firstChoices]
    firstRemChoiceLens = None
    timeout_obj = Timeout()
    with timeout_obj.timeout_guard(timeout_sec):
        # perform BFS like
        while remainingChoiceList:
            currentChoices = remainingChoiceList[-1]
            if not currentChoices:
                if currentRankBegin == lastRankBegin and currentRank == lastRankBegin - 1:
                    # if no choice at last rank, we are done for the whole search
                    break
                elif currentRank == currentRankBegin - 1:
                    # if no choice at step begin then we explore next step
                    if len(bestOrdering) < currentRankBegin + step:
                        # except if previous step did not find any best schedule
                        # then we can escape
                        break
                    bestCurrentMem = maxMemAsked
                    maxMemUsed = maxMemAsked
                    currentRankBegin += lengthForward
                    # logger.debug("Begin search at BnB step index: {}", currentRankBegin)
                    # we must go back to the state of actual best step ordering
                    readyNodesTSLreversed = backupBeginStepReadyNodes
                    for node in bestOrdering[currentRank+1:currentRankBegin]:
                        # replay regular test
                        dicoNode = dag.nodes[node]
                        choice = getSortNodeTuple(node, dicoNode)
                        readyNodesTSLreversed.remove(choice)
                        nodeMemCost = dicoNode['nodeMemCost']
                        tmpMemInc = dicoNode['tmpMemInc']
                        local_peak = currentMem + tmpMemInc
                        # test local_peak only since check_cbp_tmpMemInc
                        if local_peak > currentMaxMem:
                            currentMaxMem = local_peak
                            ranksMemIncrease.append(currentRank)
                        # update local variables
                        currentMem += nodeMemCost
                        currentRank += 1
                        currentOrdering.append(node)
                        currentMaxMemOrdering.append(currentMaxMem)
                        addSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                          getSortNodeTuple)
                        # fix the choice
                        remainingChoiceList.append([])
                    if not offsetSameMem:
                        ## NEG OPTIM ##
                        for it in range(0, lengthForward):
                            lastUntakenMinNegPeak.append(maxMemAsked)
                            currentMinNegPeakChoices.append(None)
                    # backup ready list and fix last remaining choices
                    backupBeginStepReadyNodes = SortedList(
                        iterable = readyNodesTSLreversed,
                        key = sortKeyNodeTuplePos)
                    remainingChoiceList[-1] = list(readyNodesTSLreversed)
                    # pursue search
                    continue
                else:
                    # otherwise we go back to previous choice, standard rollback
                    currentRank -= 1
                    node = currentOrdering.pop()
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                    currentMaxMemOrdering.pop()
                    prevMaxMem = currentMaxMemOrdering[-1]
                    if currentMaxMem > prevMaxMem:
                        ranksMemIncrease.pop()
                    currentMaxMem = prevMaxMem
                    remainingChoiceList.pop()
                    if not offsetSameMem:
                        ## NEG OPTIM ##
                        lastUntakenMinNegPeak.pop()
                        currentMinNegPeakChoices.pop()
                    # logger.debug("One step backward (no choices)")
                    continue
            # if arriving here, there was at least one choice,
            # schedule it
            nextChoice = currentChoices.pop()
            node = nextChoice[0]
            nodeMemCost = nextChoice[1]
            tmpMemInc = nextChoice[2]
            readyNodesTSLreversed.remove(nextChoice)
            local_peak = currentMem + tmpMemInc
            currentMem += nodeMemCost
            currentRank += 1
            if local_peak > currentMaxMem:
                currentMaxMem = local_peak
                ranksMemIncrease.append(currentRank)
            # next test is useless since check_cbp_tmpMemInc
            # if currentMem > currentMaxMem:
            #     currentMaxMem = currentMem
            #     ranksMemIncrease.append(currentRank)
            currentOrdering.append(node)
            currentMaxMemOrdering.append(currentMaxMem)
            addSortedSuccsBFS(dag, node,
                              readyNodesTSLreversed,
                              getSortNodeTuple)
            # now clean the nextChoiceList
            nextChoiceList = list()
            if offsetSameMem:
                # clear obvious bad choices
                for nextChoice in readyNodesTSLreversed:
                    tmpMemInc = nextChoice[2]
                    local_peak = currentMem + tmpMemInc
                    # if node increase maxMem *above* a known solution
                    # we discard it
                    if local_peak > maxMemUsed:
                        # if NO 'computeBest...' option activated,
                        # then offsetSameMem == 0 and it is strict equality
                        continue
                        # IF the list was sorted by increasing peaks,
                        # then we could remove this one, and all subsequent
                        # but it does not seem so efficient
                        #nextChoiceList = nextChoiceList[:idx]
                    # next test is useless since check_cbp_tmpMemInc
                    # new_mem = currentMem + nodeMemCost
                    # if new_mem > maxMemUsed:
                    #     IF the list is sorted by increasing peaks,
                    #     then we can remove this one, and all subsequent
                    #     continue
                    nextChoiceList.append(nextChoice)
            else:
                ## BEGIN if not offsetSameMem

                ## FORB OPTIM ##
                tuplCurOr = tuple(currentOrdering[-optimDepth:])
                forbiddenSuccs = dicoForbiddenSuccs.get(tuplCurOr, None)
                prunedReadyList = list()
                if not (forbiddenSuccs is None):
                    for nextChoiceC in readyNodesTSLreversed:
                        if nextChoiceC[0] in forbiddenSuccs:
                            continue
                        else:
                            prunedReadyList.append(nextChoiceC)
                        
                ## NEG OPTIM ##
                # get current min neg if any
                minNegPeakChoice = None
                if prunedReadyList:
                    # /!\ ASSUME **sortKeyNodeTuplePos**
                    nextChoiceC = prunedReadyList[-1]
                    if nextChoiceC[1] <= 0:
                        minNegPeakChoice = nextChoiceC
                    # /!\ version not depending on **sortKeyNodeTuplePos**
                    # nextChoiceC = max(prunedReadyList,
                    #                   key = sortKeyNodeTuplePos,
                    #                   default = None)
                    # if not nextChoiceC is None and nextChoiceC[1] <= 0:
                    #     minNegPeakChoice = nextChoiceC
                
                # update min neg peak/choices
                prevMinNegPeakChoice = currentMinNegPeakChoices[-1]
                prevMinNegPeak = lastUntakenMinNegPeak[-1]
                if nextChoice == prevMinNegPeakChoice or \
                   prevMinNegPeakChoice is None:
                    lastUntakenMinNegPeak.append(prevMinNegPeak)
                else:
                    prevUntakenNegPeak = currentMem - nextChoice[1] + prevMinNegPeakChoice[2]
                    prevMinNegPeak = min(lastUntakenMinNegPeak[-1], prevUntakenNegPeak)
                    lastUntakenMinNegPeak.append(prevMinNegPeak)
                currentMinNegPeakChoices.append(minNegPeakChoice)

                # in the end we get the min neg peak of most recent untaken one
                currentMinNegPeak = prevMinNegPeak
                if not (minNegPeakChoice is None):
                    currentMinNegPeak = min(currentMinNegPeak, currentMem + minNegPeakChoice[2])
 
                ## PEAK+NEG OPTIM ##
                ## clear obvious bad choices
                ## /!\ not sure if the first test is possible
                if minNegPeakChoice is None or \
                   currentMinNegPeak > currentMaxMem:
                    # visit all choices
                    for nextChoiceC in prunedReadyList:
                        tmpMemIncC = nextChoiceC[2]
                        local_peakC = currentMem + tmpMemIncC
                        # if node increase maxMem up to a known solution
                        # or above, we discard it
                        if local_peakC >= maxMemUsed:
                            continue
                        ## NEG OPTIM ##
                        if local_peakC >= currentMinNegPeak:
                            continue
                        # in any other case we keep it
                        nextChoiceList.append(nextChoiceC)
                if not minNegPeakChoice is None:
                    # in any case we keep the negative choice
                    nextChoiceList.append(minNegPeakChoice)
                
                ## END if not offsetSameMem
                
            # add the cleaned next choice list for the next iteration
            remainingChoiceList.append(nextChoiceList)
            
            # we have finished to schedule a node, test if a step is reached
            if currentRank + 1 == min(currentRankBegin + step, nbNodesTot):
                # we reached the end of step or ordering
                # logger.debug("End schedule at BnB step index: {}", currentRankBegin)
                needsUpdate = False
                nbNodesScheduled = len(currentOrdering)

                if nbNodesScheduled < nbNodesTot:
                    # (seems useless if ready list sorted by low impact)
                    if currentMem < bestCurrentMem:
                        needsUpdate = True
                        #logger.info("New last mem: {}", currentMem)
                    elif currentMem == bestCurrentMem:
                        if currentMaxMem < maxMemUsed:
                            needsUpdate = True
                            #logger.info("Equivalent last mem lower peak: {}", currentMaxMem)
                        
                else:        
                    if computeBestSrt:
                        _, peakList = mpeak_cbp_schedule(dag, currentOrdering)

                    # checking if new bound
                    if currentMaxMem < maxMemUsed:
                        nbSamePeak = 1
                        needsUpdate = True
                        logger.info("New max bound: {}", currentMaxMem)
                        #logger.info(dt.datetime.now())

                    # or equal bound with less memory
                    elif currentMaxMem == maxMemUsed:
                        nbSamePeak += 1
                        if computeBestSrt and \
                           is_other_order_better(minMemSrt, peakList[2:-2]):
                            # remove last peak which is always 0
                            needsUpdate = True
                            #logger.info("Equivalent bound lower srt mem.")
                                
                if needsUpdate:
                    bestOrdering[currentRankBegin:] = currentOrdering[currentRankBegin:]
                    maxMemUsed = currentMaxMem
                    if nbNodesScheduled < nbNodesTot:
                        bestCurrentMem = currentMem
                    else:
                        if firstRemChoiceLens is None:
                            firstRemChoiceLens = [1+len(lst) for lst in remainingChoiceList]
                        if computeBestSrt:
                            minMemSrt = srt_compacted_peaks(peakList[2:-2])
                            
                # in any case except bestCompute*,
                # we can backtrack until the last increase or step begin
                lastRankIncrease = ranksMemIncrease[-1]
                if offsetSameMem:
                    backtrackRank = currentRank
                else:
                    backtrackRank = currentRankBegin
                    # logger.debug("Go back to rankBegin: {}", backtrackRank)
                if lastRankIncrease >= backtrackRank:
                    backtrackRank = ranksMemIncrease.pop()
                    # logger.debug("Go back to last increase: {}", backtrackRank)
                currentRank = backtrackRank - 1
                # remove node in reverse order until backtrackRank included
                for node in currentOrdering[-1:backtrackRank-nbNodesScheduled-1:-1]:
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node,
                                         readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                currentOrdering = currentOrdering[:backtrackRank]
                currentMaxMemOrdering = currentMaxMemOrdering[:backtrackRank+1]
                currentMaxMem = currentMaxMemOrdering[-1]
                remainingChoiceList = remainingChoiceList[:backtrackRank+1]
                if not offsetSameMem:
                    ## NEG OPTIM ##
                    lastUntakenMinNegPeak = lastUntakenMinNegPeak[:backtrackRank+1]
                    currentMinNegPeakChoices = currentMinNegPeakChoices[:backtrackRank+1]
    
    res = SchedRes()
    res.dicoRes['time_precompute'] = time_precompute
    timedout = timeout_obj.timedout
    res.dicoRes['timeout'] = timedout
    res.keysToGather.add('time_search')
    if timedout:
        res.dicoRes['time_search'] = timeout_sec
        logger.info("Timeout BnB! ({:.6f} sec)", timeout_sec)
        logger.info("Not guaranteed optimal!")
        if firstRemChoiceLens is None:
            logger.info("No exploration estimate available! (did not finished first traversal)")
        else:
            finalRemChoiceLens = [1+len(lst) for lst in remainingChoiceList]
            (nbChoicesFirst, nbChoicesFinal, idx) = estimateRemainingChoicesLens(
                nbNodesTot, firstRemChoiceLens, finalRemChoiceLens)
            exploredRatio = (nbChoicesFirst - nbChoicesFinal) / nbChoicesFirst
            speed = (nbChoicesFirst - nbChoicesFinal) / timeout_sec
            logger.info("Estimated number of remaining choices: {:.2E}\n"+
                        "(speed: {:.2E} choice/sec)\n" +
                        "(explored ratio: {:.2E})\n(smallest common rank: {})",
                        nbChoicesFinal, speed, exploredRatio, idx)
            res.dicoRes['BandB_nbAllRemainingChoices'] = nbChoicesFinal
    else:
        time_BandB = timeout_obj.get_elapsed_time()
        res.dicoRes['time_search'] = time_BandB
        logger.info("Time BandB: {:.6f} (sec)", time_BandB)
        logger.info("Optimal bound (exhausted search).")
        res.dicoRes['minMemPeakAllOrdering'] = maxMemUsed

    res.dicoRes['memPeak'] = maxMemUsed
    if computeBestSrt:
        res.dicoRes['memSrt'] = minMemSrt
    res.dicoRes['nbSamePeak'] = nbSamePeak
    res.dicoRes['ordering'] = bestOrdering
    if len(bestOrdering) < nbNodesTot:
        res.dicoRes['memPeak'] = maxMemAsked
        if not schedRes_heur:
            res.dicoRes['ordering'] = None
            logger.warning("No schedule found with peak lower than {}.", maxMemAsked)
        else:
            res.dicoRes['nbSamePeak'] = 1
            res.dicoRes['ordering'] = schedRes_heur.dicoRes['ordering']
            logger.warning("No schedule found better than heuristic.")
            
    return res


def get_forbidden_succNopt(dag, sourceNodes, nodeTupleFunc,
                           maxDepth=1, maxKeys=5e+5, includePrefix=True):
    """Compute the set of forbidden successors for each reachable
    schedule tuple. Same as get_forbidden_succN() without the negOptim option.

    :param dag: Dag to consider.
    :param sourceNodes: Source nodes of the dag.
    :param nodeTupleFunc: Function to get tuple from node and its dict.
    :param maxDepth: (default 1) Length of sched tuples.
    :param maxKeys: (default 5e+5) Maximal length of returned
    dictionnary, used only if depth > 1, without prefixes.
    :param includePrefix: (default True) If False, 
    does not include prefixes starting with source nodes.
    :returns: Reached depth and dict of
    sched tuples to forbidden successors.
    """
    def nodeTupleComp(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then lowest drop if pos., or highest peak if neg.,
        then highest impact, and finally lowest node identifier 
        if pos., or highest id if neg. (from left to right).
        /!\ Reverse of sortKeyNodeTuplePos for positive nodes only!

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, -tupl[3], -nodeMemCost, -tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])

    
    closure = networkx.transitive_closure_dag(dag)
    dicoReadyNodes = get_dico_readyNodes(dag, closure=closure)
    dicoForbiddenSuccCur = get_forbidden_succ1ext(dag, dicoReadyNodes,
                                               nodeTupleFunc, nodeTupleComp)
    dicoForbiddenSuccSources = dict()
    
    depth = 1
    while depth < min(maxDepth, len(dag.nodes())):
        depth += 1
        logger.info("Computing forbidden succs at depth: {}", depth)
        dicoBestSuccSched = dict()
        dicoBestSuccMem = dict()
        dicoForbiddenSuccNext = dict()
        dicoForbiddenSuccTemp = dict()
        for k, v in dicoForbiddenSuccCur.items():
            dicoForbiddenSuccTemp[k] = set(v)
        ## iterate over items of previous depth
        for sched, forbiddenSuccs in dicoForbiddenSuccCur.items():
            possibleSuccs = get_possible_succs(
                dag, closure, dicoReadyNodes, sched, forbiddenSuccs)
            schedPeak, schedMem = mpeak_cbp_schedule(dag, sched, check=False)
            schedSet = frozenset(sched)
            catSched = list(sched)
            catSched.append(None)
            dicoCatMem = dict()
            triggeredSuccs = list()
            for n in possibleSuccs:
                catSched[-1] = n
                catSchedSet = frozenset(catSched)
                if dicoForbiddenSuccCur.get(tuple(catSched[1:])) is None:
                    ## discard concat if subpart was not already considered
                    continue
                dicoCatMem[n] = mpeak_cbp_schedule(dag, catSched, check=False, prefixList=schedMem)
                if set(dag.pred[n]) <= schedSet:
                    nodeTuple = nodeTupleFunc(n, dag.nodes[n])
                    triggeredSuccs.append(nodeTuple)
            ## default case: filteredSuccs are keys of dicoCatMem
            filteredSuccs = list(dicoCatMem.keys())


            ## evaluate concat
            for n in filteredSuccs:
                catSched[-1] = n
                catSchedSet = frozenset(catSched)
                catSchedTupl = tuple(catSched)
                # comparing peaks only seems sufficient condition,
                # but does this forbids less schedules?
                otherMem = dicoCatMem[n][0]
                # otherMem = dicoCatMem[n][1]
                if dicoBestSuccSched.get(catSchedSet) is None:
                    ## set best arrangement if no other
                    dicoBestSuccSched[catSchedSet] = catSchedTupl
                    dicoBestSuccMem[catSchedSet] = otherMem
                else:
                    curBestMem = dicoBestSuccMem[catSchedSet]
                    curBestSched = dicoBestSuccSched[catSchedSet]
                    lastBestNode = curBestSched[-1]
                    tuplC = nodeTupleFunc(lastBestNode, dag.nodes[lastBestNode])
                    tuplN = nodeTupleFunc(n, dag.nodes[n])
                    sortTuplC = nodeTupleComp(tuplC)
                    sortTuplN = nodeTupleComp(tuplN)
                    # isNewBest = is_other_order_better(curBestMem, otherMem)
                    isNewBest = False
                    if otherMem <= curBestMem:
                        isNewBest = True
                        if otherMem == curBestMem:
                            isNewBest = None
                    if isNewBest == True or \
                       (isNewBest is None and sortTuplC < sortTuplN):
                        ## forbid best current arrangement
                        forbiddenNodes = dicoForbiddenSuccTemp.setdefault(
                            tuple(curBestSched[:-1]), set())
                        forbiddenNodes.add(lastBestNode)
                        ## set best arrangement as new one is better
                        dicoBestSuccSched[catSchedSet] = catSchedTupl
                        dicoBestSuccMem[catSchedSet] = otherMem
                    else:
                        ## forbid tested arrangement
                        forbiddenNodes = dicoForbiddenSuccTemp.setdefault(
                            tuple(sched), set())
                        forbiddenNodes.add(n)

            ## break (sched level) if maxSize is reached
            if len(dicoBestSuccSched) > maxKeys:
                break

        ## break (depth level) if maxSize is reached
        if len(dicoBestSuccSched) > maxKeys:
            logger.info("Reached max number of keys ({}), rollback to previous dict.",
                        int(maxKeys))
            depth -= 1
            break
        
        ## set next dico
        longestAllowedPrefixes = set()
        for nextSched in dicoBestSuccSched.values():
            schedSet = set(nextSched)
            schedTupl = tuple(nextSched)
            if nextSched[0] in sourceNodes:
                longestAllowedPrefixes.add(schedTupl)
            forbiddenNodes = dicoForbiddenSuccTemp[tuple(nextSched[1:])]
            dicoForbiddenSuccNext[schedTupl] = frozenset(
                forbiddenNodes.difference(schedSet))
        dicoForbiddenSuccCur = dicoForbiddenSuccNext
        if includePrefix:
            ## update elements of previous depth which starts at source
            for sched, forbiddenSuccs in dicoForbiddenSuccTemp.items():
                if sched[0] in sourceNodes:
                    dicoForbiddenSuccSources[sched] = forbiddenSuccs
            ## clean prefixes
            remove_forbidden_prefixes(dag, closure, dicoReadyNodes,
                                      dicoForbiddenSuccSources,
                                      longestAllowedPrefixes)
        ## safety check
        if len(dicoForbiddenSuccCur) == 0:
            raise Exception("Dict of forbidden succs should not be empty.")
        logger.info("dico forbidden succs size: {}", len(dicoForbiddenSuccCur))

    ## if we end up with a single element, no need for BandB
    if len(dicoForbiddenSuccCur) == 1 and depth == len(dag.nodes()):
        for tupSched in dicoForbiddenSuccCur.keys():
            listSched = list(tupSched)
            peak, _ = mpeak_cbp_schedule(dag, listSched, check=True)
            logger.info("Asserted peak forbidden succs: {}", peak)

    if includePrefix:
        # update dico with sources sched before return
        dicoForbiddenSuccCur.update(dicoForbiddenSuccSources)
        
    return (depth, dicoForbiddenSuccCur)


def isListOrAllContainedListsEmpty(liste):
    """Check the emptiness of a list
    and all its elements if any.

    :param liste: List to consider.
    :returns: True if list is empty or 
    if all contained lists are empty.
    """
    for l in liste:
        if l:
            return False
    return True


class BnBsharedLimits:

    def __init__(self, timeout, memoryUB):
        """Creates an object containing
        mutable limits shared across
        multiple BandB solving instances.

        :param timeout: (float) Initial timeout. 
        :param memoryUB: (int) Memory upper bound.
        :returns: New object.
        """
        self.timeout_init = timeout
        self.timeout_rem = timeout
        self.memoryUB = memoryUB
    

def exhaustive_BandB_V2extGenOpt(dag, sharedLimits, **kwargs):
    """Generator version of the 
    Branch and Bound algorithm to explore all topological sorts.
    Yields new result at each new bound.
    Unlike Knuth's algorithm, this one is quadratic in space.
    /!\ This version updates the V2 one with forbidden nodes
    per schedule portion (instead of per node).
    /!\ Suboptimal or incorrect options are removed, such as:
    - 'negOptim' for the internal call to get_forbidden_succNopt()
    - 'computeBestSrt' becaused subsumed by epi calls
    - 'step' and 'overlap' since not optimal
    To comply with a dual epi call, the bound and the timeout
    are shared in a specific object, outside kwargs. Thus,
    no heuristic is called at first which means that in case
    of timeout, result may be incomplete, that is not containing
    any valid schedule.

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    Reset node attribute 'nbVisits' to 0.
    :param sharedLimits: BnBsharedLimits object to be updated for remaining
    timeout (deactivated if negative) and current memory upper bound (strict).
    :param kwargs: Dictionnary of optional default arguments:
    - 'optimDepth' (default 0: dag size) Positive depth of combinatorial optimization.
    - 'optimKeys' (default 1e5) Maximal size of forbidden succ
    dictionnary, complimentary to 'optimDepth' option.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak').
    """
    check_cbp_tmpMemInc(dag)
    set_node_peakDrop(dag, opposite=True)
    timeout_sec = int(sharedLimits.timeout_rem)
    maxMemAsked = sharedLimits.memoryUB
    logger.info("Starting BandB with max bound: {}", maxMemAsked)


    nbNodesTot = len(dag.nodes())
    for node, dicoNode in dag.nodes(data=True):
        dicoNode['nbVisits'] = 0
    optimDepth = kwargs.get('optimDepth', 0)
    if optimDepth < 0:
        raise ValueError("Incorrect 'optimDepth' parameter.")
    if optimDepth == 0:
        optimDepth = nbNodesTot
    optimKeys = kwargs.get('optimKeys', 1e5)

    # set local functions (for node tuple sort)
    def getSortNodeTuple(node, dicoNode):
        """Return the tuple needed to sort a node.

        :param node: Node to consider.
        :param dicoNode: Dico of node attributes.
        :returns: Tuple (node, 'nodeMemCost', 'tmpMemInc', 'memPeakDrop")
        """
        return (node, dicoNode['nodeMemCost'], dicoNode['tmpMemInc'], dicoNode['memPeakDrop'])
    def sortKeyNodeTuplePos(tupl):
        """Key to sort node tuple by positive impact nodes first,
        then highest drop if pos., or peak if neg.,
        then lowest impact if pos., or highest impact if neg.,
        and finally lowest node identifier (from left to right).

        :param tupl: Sort node tuple.
        :returns: The sorting keys, by comparison order.
        """
        nodeMemCost = tupl[1]
        if nodeMemCost > 0:
            return (-1, tupl[3], nodeMemCost, tupl[0])
        return (1, -tupl[2], -nodeMemCost, tupl[0])
    # set first nodes and forbidden ones
    firstNodes = [node for node, in_degree in dag.in_degree if in_degree == 0]
    readyNodesTSLreversed = SortedList(key = sortKeyNodeTuplePos)
    for node in firstNodes:
        dicoNode = dag.nodes[node]
        readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
    time1 = time.perf_counter()
    optimDepth, dicoForbiddenSuccs = get_forbidden_succNopt(
        dag, firstNodes, getSortNodeTuple,
        maxDepth=optimDepth, maxKeys=optimKeys)
    time2 = time.perf_counter()
    time_precompute = time2 - time1

    firstChoices = []
    for sourceNodeTupl in readyNodesTSLreversed:
        if (sourceNodeTupl[0],) in dicoForbiddenSuccs:
            firstChoices.append(sourceNodeTupl)
    # set all other common variables
    currentRank = -1
    maxMemUsed = maxMemAsked
    currentMaxMem = 0
    currentMem = 0
    currentMaxMemOrdering = [0]
    ranksMemIncrease = [-1]
    lastUntakenMinNegPeak = [maxMemAsked]
    currentMinNegPeakChoices = [None]
    bestOrdering = []
    currentOrdering = []
    remainingChoiceList = [firstChoices]
    firstRemChoiceLens = None
    timeout_obj = Timeout()
    with timeout_obj.timeout_guard(timeout_sec):
        # perform BFS like
        while remainingChoiceList:
            currentChoices = remainingChoiceList[-1]
            if not currentChoices:
                if currentRank == -1:
                    # if no choice at last rank, we are done for the whole search
                    break
                else:
                    # otherwise we go back to previous choice, standard rollback
                    currentRank -= 1
                    node = currentOrdering.pop()
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node, readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                    currentMaxMemOrdering.pop()
                    prevMaxMem = currentMaxMemOrdering[-1]
                    if currentMaxMem > prevMaxMem:
                        ranksMemIncrease.pop()
                    currentMaxMem = prevMaxMem
                    remainingChoiceList.pop()
                    lastUntakenMinNegPeak.pop()
                    currentMinNegPeakChoices.pop()
                    # logger.debug("One step backward (no choices)")
                    continue
            # if arriving here, there was at least one choice,
            # schedule it
            nextChoice = currentChoices.pop()
            node = nextChoice[0]
            nodeMemCost = nextChoice[1]
            tmpMemInc = nextChoice[2]
            readyNodesTSLreversed.remove(nextChoice)
            local_peak = currentMem + tmpMemInc
            currentMem += nodeMemCost
            currentRank += 1
            if local_peak > currentMaxMem:
                currentMaxMem = local_peak
                ranksMemIncrease.append(currentRank)
            # next test is useless since check_cbp_tmpMemInc
            # if currentMem > currentMaxMem:
            #     currentMaxMem = currentMem
            #     ranksMemIncrease.append(currentRank)
            currentOrdering.append(node)
            currentMaxMemOrdering.append(currentMaxMem)
            addSortedSuccsBFS(dag, node,
                              readyNodesTSLreversed,
                              getSortNodeTuple)
            # now clean the nextChoiceList
            nextChoiceList = list()
            
            ## FORB OPTIM ##
            tuplCurOr = tuple(currentOrdering[-optimDepth:])
            forbiddenSuccs = dicoForbiddenSuccs.get(tuplCurOr, None)
            prunedReadyList = list()
            if not (forbiddenSuccs is None):
                for nextChoiceC in readyNodesTSLreversed:
                    if nextChoiceC[0] in forbiddenSuccs:
                        continue
                    else:
                        prunedReadyList.append(nextChoiceC)
                        
            ## NEG OPTIM ##
            # get current min neg if any
            minNegPeakChoice = None
            if prunedReadyList:
                # /!\ ASSUME **sortKeyNodeTuplePos**
                nextChoiceC = prunedReadyList[-1]
                if nextChoiceC[1] <= 0:
                    minNegPeakChoice = nextChoiceC
                # /!\ version not depending on **sortKeyNodeTuplePos**
                # nextChoiceC = max(prunedReadyList,
                #                   key = sortKeyNodeTuplePos,
                #                   default = None)
                # if not nextChoiceC is None and nextChoiceC[1] <= 0:
                #     minNegPeakChoice = nextChoiceC
                
            # update min neg peak/choices
            prevMinNegPeakChoice = currentMinNegPeakChoices[-1]
            prevMinNegPeak = lastUntakenMinNegPeak[-1]
            if nextChoice == prevMinNegPeakChoice or \
               prevMinNegPeakChoice is None:
                lastUntakenMinNegPeak.append(prevMinNegPeak)
            else:
                prevUntakenNegPeak = currentMem - nextChoice[1] + prevMinNegPeakChoice[2]
                prevMinNegPeak = min(lastUntakenMinNegPeak[-1], prevUntakenNegPeak)
                lastUntakenMinNegPeak.append(prevMinNegPeak)
            currentMinNegPeakChoices.append(minNegPeakChoice)

            # in the end we get the min neg peak of most recent untaken one
            currentMinNegPeak = prevMinNegPeak
            if not (minNegPeakChoice is None):
                currentMinNegPeak = min(currentMinNegPeak, currentMem + minNegPeakChoice[2])
 
            ## PEAK+NEG OPTIM ##
            ## clear obvious bad choices
            ## /!\ not sure if the first test is possible
            if minNegPeakChoice is None or \
               currentMinNegPeak > currentMaxMem:
                # visit all choices
                for nextChoiceC in prunedReadyList:
                    tmpMemIncC = nextChoiceC[2]
                    local_peakC = currentMem + tmpMemIncC
                    # if node increase maxMem up to a known solution
                    # or above, we discard it
                    if local_peakC >= maxMemUsed:
                        continue
                    ## NEG OPTIM ##
                    if local_peakC >= currentMinNegPeak:
                        continue
                    # in any other case we keep it
                    nextChoiceList.append(nextChoiceC)
            if not minNegPeakChoice is None:
                # in any case we keep the negative choice
                nextChoiceList.append(minNegPeakChoice)
                
                
            # add the cleaned next choice list for the next iteration
            remainingChoiceList.append(nextChoiceList)
            
            # we have finished to schedule a node, test if a step is reached
            if currentRank + 1 == nbNodesTot:
                # we reached the end of ordering

                # checking if new bound and YIELD!
                if currentMaxMem < maxMemUsed:
                    timeout_obj.stop_time()
                    #logger.info(dt.datetime.now())
                    logger.info("New max bound: {}", currentMaxMem)

                    bestOrdering[:] = currentOrdering[:]
                    maxMemUsed = currentMaxMem
                    if firstRemChoiceLens is None:
                        firstRemChoiceLens = [1+len(lst) for lst in remainingChoiceList]

                    sharedLimits.timeout_rem = sharedLimits.timeout_rem - timeout_obj.get_elapsed_time()
                    sharedLimits.memoryUB = maxMemUsed

                    # yield new bound if not exhausted search
                    if not isListOrAllContainedListsEmpty(remainingChoiceList):
                        res = SchedRes()
                        res.dicoRes['memPeak'] = maxMemUsed
                        res.dicoRes['ordering'] = [n for n in bestOrdering]
                        res.dicoRes['maxMemOrdering'] = [m for m in currentMaxMemOrdering]
                        yield res
                    
                        # retrieve new bound and timeout
                        if sharedLimits.memoryUB < maxMemUsed:
                            maxMemUsed = sharedLimits.memoryUB
                            bestOrdering = []
                        timeout_sec = int(sharedLimits.timeout_rem)
                        timeout_obj.start_time(timeout_sec)
                
                # we can backtrack until the last increase
                backtrackRank = 0
                if ranksMemIncrease[-1] >= backtrackRank:
                    backtrackRank = ranksMemIncrease.pop()
                # logger.debug("Go back to last increase: {}", backtrackRank)
                currentRank = backtrackRank - 1
                # remove node in reverse order until backtrackRank included
                for node in currentOrdering[-1:backtrackRank-nbNodesTot-1:-1]:
                    dicoNode = dag.nodes[node]
                    currentMem -= dicoNode['nodeMemCost']
                    removeSortedSuccsBFS(dag, node,
                                         readyNodesTSLreversed,
                                         getSortNodeTuple)
                    readyNodesTSLreversed.add(getSortNodeTuple(node, dicoNode))
                currentOrdering = currentOrdering[:backtrackRank]
                currentMaxMemOrdering = currentMaxMemOrdering[:backtrackRank+1]
                currentMaxMem = currentMaxMemOrdering[-1]
                remainingChoiceList = remainingChoiceList[:backtrackRank+1]
                ## NEG OPTIM ##
                lastUntakenMinNegPeak = lastUntakenMinNegPeak[:backtrackRank+1]
                currentMinNegPeakChoices = currentMinNegPeakChoices[:backtrackRank+1]

    res = SchedRes()
    res.dicoRes['time_precompute'] = time_precompute
    time_BandB = sharedLimits.timeout_init - sharedLimits.timeout_rem
    res.dicoRes['time_search'] = time_BandB
    timedout = timeout_obj.timedout
    res.dicoRes['timeout'] = timedout
    res.keysToGather.add('time_search')
    if timedout:
        logger.info("Timeout BnB! ({:.6f} sec)", sharedLimits.timeout_init)
        logger.info("Not guaranteed optimal!")
        if firstRemChoiceLens is None:
            logger.info("No exploration estimate available! (did not finished first traversal)")
        else:
            finalRemChoiceLens = [1+len(lst) for lst in remainingChoiceList]
            (nbChoicesFirst, nbChoicesFinal, idx) = estimateRemainingChoicesLens(
                nbNodesTot, firstRemChoiceLens, finalRemChoiceLens)
            exploredRatio = (nbChoicesFirst - nbChoicesFinal) / nbChoicesFirst
            speed = (nbChoicesFirst - nbChoicesFinal) / sharedLimits.timeout_init
            logger.info("Estimated number of remaining choices: {:.2E}\n"+
                        "(speed: {:.2E} choice/sec)\n" +
                        "(explored ratio: {:.2E})\n(smallest common rank: {})",
                        nbChoicesFinal, speed, exploredRatio, idx)
            res.dicoRes['BandB_nbAllRemainingChoices'] = nbChoicesFinal
    else:
        logger.info("Time BandB: {:.6f} (sec)", time_BandB)
        logger.info("Optimal bound (exhausted search).")
        res.dicoRes['minMemPeakAllOrdering'] = maxMemUsed

    res.dicoRes['memPeak'] = maxMemUsed
    res.dicoRes['ordering'] = bestOrdering
    if len(bestOrdering) < nbNodesTot:
        res.dicoRes['memPeak'] = maxMemAsked
        res.dicoRes['ordering'] = None
        logger.warning("No schedule found with peak lower than {}.", maxMemAsked)

    # yield last bound with augmented metrics
    yield res
    # return statement triggers StopIteration
    return res


def exhaustive_BandB_V3epiDual(dag, **kwargs):
    """
    Epi Branch and Bound algorithm to explore all topological sorts,
    calling exhaustive_BandB_V2extGenOpt() in ping-pong manner between
    primal form and dual form whether last peak is detected first left
    or right. This version is not always faster, especially if it gets
    stuck in the dual whereas the primal would prune more efficiently.

    :param dag: DAG to consider, with node attributes 'nodeMemCost and tmpMemInc'.
    :param kwargs: Dictionnary of optional default arguments:
    - 'optimDepth' (default 0: dag size) Positive depth of combinatorial optimization.
    - 'optimKeys' (default 1e5) Maximal size of forbidden succ
    dictionnary, complimentary to 'optimDepth' option.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the algorithm, plus
    'minMemPeakAllOrdering' the minimum memory peak (actually being 'memPeak').
    """
    nbNodes = len(dag.nodes())
    globalImpact = 0
    for _, impact in dag.nodes(data='nodeMemCost'):
        globalImpact += impact
    dag_dual = reverse_sched_graph(dag)

    minMemHeuristic = kwargs.get('minMemHeuristic', None)
    if not (minMemHeuristic is None):
        schedRes_heur = minMemHeuristic(dag, **kwargs)
        schedRes_prev = schedRes_heur
        memPeak, memList = mpeak_cbp_schedule(dag, schedRes_heur.dicoRes['ordering'],
                                              check=False, prefixList=None)

    else:
        schedRes_prev = None
        memPeak = float('inf')
        
    givenBound = kwargs.get('minMemUpperBound', 0)
    if givenBound == 0:
        givenBound = memPeak

    sharedLimits = BnBsharedLimits(kwargs.get('timeout', 0), min(givenBound, memPeak))

    iterNormBandB = iter(exhaustive_BandB_V2extGenOpt(dag, sharedLimits, **kwargs))
    iterDualBandB = iter(exhaustive_BandB_V2extGenOpt(dag_dual, sharedLimits, **kwargs))
    if (minMemHeuristic is None) or memList.index(memPeak) < nbNodes:
        currentIterBandB = iterNormBandB
    else:
        currentIterBandB = iterDualBandB
        sharedLimits.memoryUB -= globalImpact

    try:
        while True:
            if currentIterBandB is iterDualBandB:
                logger.info('New call of dual BandB.')                
            else:
                logger.info('New call of primal BandB.')
            schedRes = next(currentIterBandB)
            if schedRes.dicoRes.get('timeout', False) or \
               schedRes.dicoRes['ordering'] is None:
                ## if timeout or exhausted search without result,
                ## return previous one if existing
                if not (schedRes_prev is None):
                    schedRes_prev.dicoRes['time_search'] = schedRes.dicoRes['time_search']
                    return schedRes_prev
                else:
                    return schedRes
            if currentIterBandB is iterDualBandB:
                schedRes.dicoRes['memPeak'] += globalImpact
                schedRes.dicoRes['ordering'].reverse()
            if not schedRes.dicoRes.get('timeout', True):
                ## exhausted search with result
                return schedRes

            # otherwise, regular yield, so we
            # have access to maxMemOrdering
            maxMemOrdering = schedRes.dicoRes['maxMemOrdering']
            memPeak = schedRes.dicoRes['memPeak']
            if currentIterBandB is iterDualBandB:
                maxMemOrdering.reverse()
                memPeak -= globalImpact
            indexMax = maxMemOrdering.index(memPeak)
            if indexMax < (nbNodes // 2):
                if currentIterBandB is iterDualBandB:
                    sharedLimits.memoryUB += globalImpact
                currentIterBandB = iterNormBandB
            else:
                if currentIterBandB is iterNormBandB:
                    sharedLimits.memoryUB -= globalImpact
                currentIterBandB = iterDualBandB
            schedRes_prev = schedRes
            
    # should not arrive here, just for safety
    except StopIteration:
        return schedRes_prev
    return schedRes_prev


def exhaustive_BandB_V4epiDominant_neg(dag, **kwargs):
    """ Finds a dominant schedule according to [Jin+23].
    Code understood from [Jin+23] (proof Algorithm 1).
    "New Tools for Peak Memory Scheduling"
    /!\ Auxiliary function, should not be called directly.
    
    :param dag: DAG to consider with 'nodeMemCost' and
    'tmpMemInc' node attributes set.
    :param kwargs: Dictionnary of optional default arguments
    for internal call to exhaustive_BandB_V3epiDual().
    :returns: Scheduling result with at minimum
    'ordering', 'memPeak' and 'time_search' attributes.
    """
    setNodes = set(dag.nodes())
    nbNodes = len(setNodes)
    res = SchedRes()
    # trivial base cases
    if nbNodes == 0:
        res.dicoRes['ordering'] = []
        res.dicoRes['memPeak'] = float('-inf')
        res.dicoRes['time_search'] = 0
        return res
    elif nbNodes == 1:
        node = next(iter(setNodes))
        res.dicoRes['ordering'] = [node]
        res.dicoRes['memPeak'] = dag.nodes[node]['tmpMemInc']
        res.dicoRes['time_search'] = 0
        return res

    # otherwise schedule graph
    schedRes = exhaustive_BandB_V3epiDual(dag, **kwargs)

    # base case of only nul impact nodes
    # (other wise cut might be empty)
    listImpact = [impact for _, impact in dag.nodes(data='nodeMemCost') if impact == 0]
    if len(listImpact) == nbNodes:
        return schedRes
    
    # otherwise cut call
    ordering = schedRes.dicoRes['ordering']
    memPeak, memList = mpeak_cbp_schedule(dag, ordering,
                                          check=False,
                                          prefixList=None)
    indexPeakMemList = memList.index(memPeak)
    indexPeakOrdering = indexPeakMemList // 2
    nodePeak = ordering[indexPeakOrdering]

    ## /!\ original cut from [Jin+23] does not work /!\
    # maxCapacity = 1+sum([i*i for _, i in dag.nodes(data='nodeMemCost')])
    # min_cut_dag, sourceFlowId, sinkFlowId = build_node_min_cut_form(dag, maxCapacity)
    # sourceTopoId = sourceFlowId - 2
    # # enforce nodePeak *and predecessors!* to be in the cut
    # # by adding an edge with infinite capacity towards it
    # # https://stackoverflow.com/questions/66755638/finding-if-a-minimum-cut-in-a-flow-network-where-we-force-certain-nodes-to-be-in
    # for node in ordering[:indexPeakOrdering+1]:
    #     if min_cut_dag.has_edge(sourceTopoId, node):
    #         oriCap = min_cut_dag[sourceTopoId][node].get('capacity', 0)
    #         if oriCap < maxCapacity:
    #             min_cut_dag[sourceTopoId][node]['capacity'] += maxCapacity
    #     else:
    #         min_cut_dag.add_edge(sourceTopoId, node, capacity=maxCapacity)
    # try:
    #     cut_value, (leftSet, rightSet) = networkx.minimum_cut(min_cut_dag, sourceFlowId, sinkFlowId)
    # except networkx.exception.NetworkXUnbounded:
    #     logger.warning("Unbound cut, keep schedule as is.")
    #     return schedRes
    # if not leftSet.intersection(dag.nodes()):
    #     logger.warning("Empty left part, keep schedule as is.")
    #     return schedRes

    leftSet = ffst_min_cut(dag, closure=None,
                           compulsoryNodes=set(ordering[:indexPeakOrdering+1]))
    rightSet = set(dag.nodes()).difference(leftSet)
    
    if not leftSet:
        logger.warning("Empty left part, keep schedule as is.")
        return schedRes
    # if not empy, the left cut should contain nodePeak
    if not (nodePeak in leftSet):
        raise Exception("Left cut does not contain peak node.")    

    fixedOrdering = [n for n in ordering if n in leftSet]

    # recursive call
    # subgraph = networkx.subgraph(dag, setNodes.difference(leftSet))
    subgraph = getSubDAG(dag, setNodes.difference(leftSet))
    schedRes_rec = exhaustive_BandB_V4epiDominant_neg(subgraph, **kwargs)
    fixedOrdering.extend(schedRes_rec.dicoRes['ordering'])
    res.dicoRes['ordering'] = fixedOrdering
    res.dicoRes['memPeak'] = memPeak
    res.dicoRes['time_search'] = \
        schedRes.dicoRes.get('time_search', 0) + \
        schedRes_rec.dicoRes.get('time_search', 0)
    return res
    

def exhaustive_BandB_V4epiDominant(dag, **kwargs):
    """ Finds a dominant schedule according to [Jin+23].
    Code understood from [Jin+23] (proof Algorithm 2).
    "New Tools for Peak Memory Scheduling"
    
    :param dag: DAG to consider with 'nodeMemCost' and
    'tmpMemInc' node attributes set.
    :param kwargs: Dictionnary of optional default arguments
    for internal call to exhaustive_BandB_V3epiDual().
    /!\ Any timeout will be deactivated for such search.
    /!\ A starting heuristic is always called.
    :returns: Scheduling result with at minimum
    'ordering', 'memPeak' and 'time_search' attributes.
    """
    kwargsCop = dict(**kwargs)
    kwargsCop['timeout'] = 0
    # here we want a schedule to be found,
    # so we cannot have a too tight bound
    kwargsCop['minMemUpperBound'] = 0
    minMemHeuristic = kwargsCop.get('minMemHeuristic', None)
    if minMemHeuristic is None:
        kwargsCop['minMemHeuristic'] = heuristic_linear_1dot5steps_impact
    
    setNodes = set(dag.nodes())
    maxNodes = max(dag.nodes()) + 1

    ## /!\ original cut from [Jin+23] does not work /!\
    # min_cut_dag, sourceFlowId, sinkFlowId = build_node_min_cut_form(dag)
    # cut_value, (leftSet, rightSet) = networkx.minimum_cut(min_cut_dag, sourceFlowId, sinkFlowId)
    ## /!\ uses own cut algorithm instead
    leftSet = ffst_min_cut(dag, closure=None, compulsoryNodes=None)
    rightSet = set(dag.nodes()).difference(leftSet)
    
    # leftGraph = networkx.subgraph(dag, setNodes.intersection(leftSet))
    # rightGraph = networkx.subgraph(dag, setNodes.intersection(rightSet))
    leftGraph = getSubDAG(dag, leftSet)
    rightGraph = getSubDAG(dag, rightSet)
    leftGraphRev = reverse_sched_graph(leftGraph)
    schedLeft = exhaustive_BandB_V4epiDominant_neg(leftGraphRev, **kwargsCop)
    schedLeft.dicoRes['ordering'].reverse()
    schedRight = exhaustive_BandB_V4epiDominant_neg(rightGraph, **kwargsCop)
    
    res = SchedRes()
    orderingLeft = [n for n in schedLeft.dicoRes['ordering']]
    orderingLeft.extend(schedRight.dicoRes['ordering'])
    res.dicoRes['ordering'] = orderingLeft
    memPeak, _ = mpeak_cbp_schedule(dag, orderingLeft,
                                          check=False,
                                          prefixList=None)
    res.dicoRes['memPeak'] = memPeak
    res.dicoRes['time_search'] = \
        schedLeft.dicoRes['time_search'] + schedRight.dicoRes['time_search']
    return res
    

def sequentialize_STsubDAG_V1(dag, maxNodes=0, **kwargs):
    """Sequentialize STsubDAG in given DAG according
    to our own schedule sorting ('computeBestSrt'
    option of BandB versions < V3).
    /!\ Not completely proven. Optimally schedules every
    STsubDAG; this implies a worst case NP complexity.

    :param dag: DAG to sequentialize, with 'edgeMem' attributes.
    :param maxNodes: Maximal number of nodes in STsubDAG
    to allow its processing.
    :param kwargs: Dictionnary of optional default arguments.
    :returns: Number of sequentialized STsubDAG.
    """
    kwargsCop = dict(**kwargs)
    kwargsCop['timeout'] = 0
    # no forbidden succ since option is deactived in this case
    kwargsCop['optimDepth'] = 1
    # here we want a schedule to be found,
    # so we cannot have a too tight bound
    kwargsCop['minMemUpperBound'] = 0
    minMemHeuristic = kwargsCop.get('minMemHeuristic', None)
    if minMemHeuristic is None:
        kwargsCop['minMemHeuristic'] = heuristic_linear_1dot5steps_impact

    nbProcessedSTsubDAG = 0
    if maxNodes == 0:
        maxNodes = len(dag.nodes())
    topoSortASAPandALAP(dag)
    for subDAG in genSTsubDAG_generator(dag):
        (start_nodes, STsubDAG, end_nodes) = subDAG
        if len(STsubDAG.nodes()) > maxNodes:
            continue
        nbProcessedSTsubDAG += 1
        logger.info("Sequentialize isolated subgraphs with nodes: {}",
                    [dag.nodes[n]['name'] for n in STsubDAG.nodes()])
        newSourceId = max(STsubDAG.nodes()) + 1
        newSinkId = newSourceId + 1
        # update subDAG with pred memory
        totProd = 0
        for pred in dag.pred[next(iter(start_nodes))]:
            for start in start_nodes:
                edgeMem = dag[pred][start]['edgeMem']
                totProd += edgeMem
                if STsubDAG.has_edge(newSourceId, start):
                    STsubDAG[newSourceId][start]['edgeMem'] += edgeMem
                else:
                    STsubDAG.add_edge(newSourceId, start, edgeMem=edgeMem)
        STsubDAG.nodes[newSourceId]['nodeMemCost'] = totProd
        STsubDAG.nodes[newSourceId]['tmpMemInc'] = totProd
        # update subDAG with succ memory
        totCons = 0
        for succ in dag.succ[next(iter(end_nodes))]:
            for end in end_nodes:
                edgeMem = dag[end][succ]['edgeMem']
                totCons += edgeMem
                if STsubDAG.has_edge(end, newSinkId):
                    STsubDAG[end][newSinkId]['edgeMem'] += edgeMem
                else:
                    STsubDAG.add_edge(end, newSinkId, edgeMem=edgeMem)
        STsubDAG.nodes[newSinkId]['nodeMemCost'] = -totCons
        STsubDAG.nodes[newSinkId]['tmpMemInc'] = 0     
        # schedule and update dag with transitive edges
        set_default_attributes(STsubDAG, resetTmpMemInc=False)
        kwargsCop['computeBestSrt'] = True
        schedRes = exhaustive_BandB_V2(STsubDAG, **kwargsCop)
        # sample ordering since we have added a new source and sink
        suborder = (schedRes.dicoRes['ordering'])[1:-1]
        for (u,v) in pairwise(suborder):
            if not dag.has_edge(u, v):
                dag.add_edge(u, v, edgeMem=0)
    # we introduced transitive edges, remove them
    if nbProcessedSTsubDAG > 0:
        remove_transitive_edges_cbp(dag, transferOnPath=True)
    return nbProcessedSTsubDAG


def sequentialize_STsubDAG_V2(dag, maxNodes=0, **kwargs):
    """Sequentialize STsubDAG in given DAG according
    to [Jin+23] (Algorithm 2).
    /!\ Not completely proven. Optimally schedules every
    STsubDAG; this implies a worst case NP complexity.

    :param dag: DAG to sequentialize.
    :param maxNodes: Maximal number of nodes in STsubDAG
    to allow its processing.
    :param kwargs: Dictionnary of optional default arguments.
    :returns: Number of sequentialized STsubDAG.
    """
    kwargsCop = dict(**kwargs)
    nbProcessedSTsubDAG = 0
    if maxNodes == 0:
        maxNodes = len(dag.nodes())
    topoSortASAPandALAP(dag)
    for subDAG in genSTsubDAG_generator(dag):
        (start_nodes, STsubDAG, end_nodes) = subDAG
        if len(STsubDAG.nodes()) > maxNodes:
            continue
        nbProcessedSTsubDAG += 1
        logger.info("Sequentialize isolated subgraphs with nodes: {}",
                    [dag.nodes[n].get('name', str(n)) for n in STsubDAG.nodes()])
        schedRes = exhaustive_BandB_V4epiDominant(STsubDAG, **kwargsCop)
        suborder = (schedRes.dicoRes['ordering'])
        for (u,v) in pairwise(suborder):
            if not dag.has_edge(u, v):
                dag.add_edge(u, v, edgeMem=0)
    # we introduced transitive edges, remove them
    if nbProcessedSTsubDAG > 0:
        remove_transitive_edges_cbp(dag, transferOnPath=True)
    return nbProcessedSTsubDAG


class OptimMetrics:

    def __init__(self, dag):
        """Init optimization metrics storage object.

        :param dag: DAG being ptimized.
        """
        self.nbGlobalStep = 0
        self.nbsLocalStep = [] # int list
        self.nbsLocalRemovedNodeS = [] # int list list (list)
        self.nbsLocalSeqNode = [] # int list list
        self.nbsGlobalSeqNode = [] # int list
        self.nbsImpactSeqNode = [] # int list
        self.nbsGlobalRemovedEdge = [] # int list
        self.nbsSequentializedSTsubDAG = [] # int list
        self.children = []
        self.nbDagNodes = len(dag.nodes())
        self.nbDagEdges = len(dag.edges())
        
    def addGlobalStep(self):
        """Update metrics with one more global step.
        """
        self.nbsLocalStep.append(0)
        self.nbsLocalRemovedNodeS.append([])
        self.nbsLocalSeqNode.append([])
        self.nbsGlobalSeqNode.append(0)
        self.nbsImpactSeqNode.append(0)
        self.nbsGlobalRemovedEdge.append(0)
        self.nbsSequentializedSTsubDAG.append(0)
        self.nbGlobalStep += 1

    def addLocalStep(self, nbRemovedNodeS, nbSeqNode):
        """Update metrics with one more local step.

        :param nbRemovedNodeS: Number of node in serie removed during the step.
        :param nbRemovedNodeP: Number of node in parallel removed during the step.
        :param nbSeqNode: Numder of node sequentialized during the step.
        """
        idxG = self.nbGlobalStep - 1
        self.nbsLocalRemovedNodeS[idxG].append(nbRemovedNodeS)
        self.nbsLocalSeqNode[idxG].append(nbSeqNode)
        self.nbsLocalStep[idxG] += 1
        
    def __str__(self):
        """Human-readable version of the object.

        :returns: Multi-line string with all informations.
        """
        bufString = ["nbChildren: {}".format(len(self.children))]
        bufString.append("nbDagNodes: {}".format(self.nbDagNodes))
        bufString.append("nbDagEdges: {}".format(self.nbDagEdges))
        bufString.append("nbGlobalStep: {}".format(self.nbGlobalStep))
        bufString.append("nbsLocalStep: {}".format(self.nbsLocalStep))
        bufString.append("nbsLocalRemovedNodeS: {}".format(self.nbsLocalRemovedNodeS))
        bufString.append("nbsLocalSeqNode: {}".format(self.nbsLocalSeqNode))
        bufString.append("nbsGlobalSeqNode: {}".format(self.nbsGlobalSeqNode))
        bufString.append("nbsImpactSeqNode: {}".format(self.nbsImpactSeqNode))
        bufString.append("nbsGlobalRemovedEdge: {}".format(self.nbsGlobalRemovedEdge))
        bufString.append("nbsSequentializedSTsubDAG: {}".format(self.nbsSequentializedSTsubDAG))
        return '\n'.join(bufString)
        
        
def reconstruct_sep_schedules(listSchedRes, optimMetrics, nodeSkipThres=0, listSeparatedDAG=None):
    """Reconstructs a global ordering from
    a list of separated ones (in barrier order).

    :param listSchedRes: List of SchedRes objects
    of each separated DAG (in right order).
    :param optimMetrics: Object containing optimization metrics
    to be updated with children if recursive call (i.e. if no listSeparatedDAG).
    :param nodeSkipThres: If higher than 0,
    then removes all nodes higher than the threshold
    from the reconstructed ordering.
    :param listSeparatedDAG: List of DAG,
    present if terminal call.
    If None, then the separated schedules are only
    coalesced without call to getFullOrder(clusteredDag, clusteredOrder)
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering' found by the heuristic.
    """
    listMinPeak = [x.dicoRes.get('minMemPeakAllOrdering', None) for x in listSchedRes]
    listPeak = [x.dicoRes['memPeak'] for x in listSchedRes]
    listSched = [x.dicoRes['ordering'] for x in listSchedRes]

    if None in listSched:
        # one subschedule was not complete, disable the full one
        filteredOrdering = None
    else:
        fullOrdering = []
        if listSeparatedDAG:
            for sepDag, sched in zip(listSeparatedDAG, listSched):
                sepFullOrdering = getFullOrder(sepDag, sched)
                fullOrdering.extend(sepFullOrdering)
        else:
            for sched in listSched:
                fullOrdering.extend(sched)

        filteredOrdering = fullOrdering
        if nodeSkipThres:
            filteredOrdering = [n for n in fullOrdering if n <= nodeSkipThres]
                
    res = SchedRes()
    res.dicoRes['memPeak'] = max(listPeak)
    if not (None in listMinPeak):
        res.dicoRes['minMemPeakAllOrdering'] = res.dicoRes['memPeak']
    res.dicoRes['ordering'] = filteredOrdering
    res.dicoRes['optimMetrics'] = optimMetrics
    # store separated dags and optim metrics if needed
    if listSeparatedDAG:
        res.dicoRes['listSepTermDAG'] = listSeparatedDAG
        res.dicoRes['listSepTermSched'] = listSched
    else:
        res.dicoRes['listSepTermDAG'] = []
        res.dicoRes['listSepTermSched'] = []
        for schedRes in listSchedRes:
            res.dicoRes['listSepTermDAG'].extend(
                schedRes.dicoRes['listSepTermDAG'])
            res.dicoRes['listSepTermSched'].extend(
                schedRes.dicoRes['listSepTermSched'])
            optimMetrics.children.append(schedRes.dicoRes['optimMetrics'])
    # gather all other keys
    for schedRes in listSchedRes:
        res.keysToGather.update(schedRes.keysToGather)
    for key in res.keysToGather:
        res.dicoRes[key] = []
    for schedRes in listSchedRes:
       for key in res.keysToGather:
           value = schedRes.dicoRes.get(key, None)
           res.dicoRes[key].append(value)

    return res
    

def rec_optims_dag_min_mem(dag, **kwargs):
    """Apply recursively four types of optimization:
    transitive edge reduction, sync barrier DAG separation,
    serial and parallel Pascal's optims.

    :param dag: DAG to analyse, with node attributes
    'nodeMemCost' and 'tmpMemInc' already set, as well as
    all default ones. /!\ The DAG will be modified IN PLACE.
    :param kwargs: Dictionnary of optional default arguments:
    - 'max_global_iter' (default 0) Maximum number of global
    optimzation iterations, 0 means unlimited.
    - 'max_local_iter' (default 0) Maximum number of local
    optimzation iterations, 0 means unlimited. Only for useful
    for the 'local_clustering' option.
    - 'local_clustering' (default True) Apply iteratively local
    serie clustering and parallel sequentialization.
    - 'global_sequentialization' (default True) Apply global
    parallel sequentialization.
    - 'globalSeq_maxNodes' (default 0) Maximal number of nodes 
    enabling a global sequentialization. If 0, no maximum.
    - 'impact_sequentialization' (default False) /!\ Wrong
    Apply impact node sequentialization.
    Current implementation is suboptimal.
    - 'transitivity_red' (default True) Removes transitive edges.
    - 'sync_separation_rec' (default False) Wheter or not DAG sync separation
    must be attempted during optimizations. If True, enables recursion.
    - 'sync_separation_end' (default True) Wheter or not DAG sync separation
    must be attempted before fallback call. Advised if exhaustive fallback.
    - 'STsubDAG_sequentialization' (default False) /!\ Experimental
    option to sequentialize every STsubDAG if no other optimization is
    possible. Implies NP complexity and local optimal scheduling.
    - 'STsubDAG_maxNodes' (default 0) Maximum bound on the size of
    STsubDAG to sequentialize. If 0, all sizes are processed.
    - 'fallback' (default brute_force_dag_min_mem) 
    Method to call when optimizations cannot be
    applied anymore, by default brute_force_dag_min_mem(dag).
    - 'offsetNewNodes' (default 0) Minimum offset for new node identifiers.
    /!\ Should not be modified, used for self recursion.
    :returns: SchedRes dictionnary with memory used 'memPeak' and 
    corresponding ordering 'ordering', possibly containing extra 
    source and sink nodes.
    """
    max_global_iter = kwargs.get('max_global_iter', 0)
    max_local_iter = kwargs.get('max_local_iter', 0)
    local_clustering = kwargs.get('local_clustering', True)
    global_seq = kwargs.get('global_sequentialization', True)
    global_seq_maxNodes = kwargs.get('globalSeq_maxNodes', 0)
    impact_seq = kwargs.get('impact_sequentialization', False)
    transitivity_red = kwargs.get('transitivity_red', True)
    sync_separation_rec = kwargs.get('sync_separation_rec', False)
    sync_separation_end = kwargs.get('sync_separation_end', True)
    STsubDAG_maxNodes = kwargs.get('STsubDAG_maxNodes', 0)
    STsubDAG_seq = kwargs.get('STsubDAG_sequentialization', False)
    if STsubDAG_seq and not transitivity_red:
        logger.warning(
            "STsubDAG sequentialization will enforce trnasitive reduction after each call.")
    fallback = kwargs.get('fallback', exhaustive_BandB_V2)
    # /!\ memdag and STsubDAG are not correct if 'edgeMem' attributes
    # are not sync with 'nodeMemCost' (happens during transitive removal)
    # Next option enforces synchronization if needed (slightly slower)
    # STsubDAG_seq does not require anymore edgeMem
    keepEdgeMem = (fallback is memdagSPschedule)
    if fallback is rec_optims_dag_min_mem:
        raise ValueError('Cannot use self function as fallback.')    
    offsetNewNodes = kwargs.get('offsetNewNodes', 0)
    # metrics to be aggregated, also recursively
    metrics = OptimMetrics(dag)
    # offset for new nodes if sync frontier
    offset_rec = max(offsetNewNodes, max(dag.nodes()))
    # new common kwargs for recursive or fallback call
    comKWargs = dict(kwargs)
    comKWargs['offsetNewNodes'] = offset_rec


    # 0. check graph connectivity
    if not networkx.is_weakly_connected(dag):
        logger.info("Graph is not connected, automatic separation with arbitrary order.")
        ccs = networkx.weakly_connected_components(dag)
        listSubdags = []
        for cc in ccs:
            listSubdags.append(getSubDAG(dag, cc))
        listPeakSched = [rec_optims_dag_min_mem(dag_sep, **comKWargs)
                         for dag_sep in listSubdags]
        return reconstruct_sep_schedules(listPeakSched, metrics, offset_rec)

    # avoids recursion in case of chain graphs
    if len(dag.nodes()) <= 3:
        schedRes = fallback(dag)
        # get full ordering in case of base case
        fullOrdering = getFullOrder(dag, schedRes.dicoRes['ordering'])
        schedRes.dicoRes['ordering'] = fullOrdering
        schedRes.dicoRes['listSepTermDAG'] = [dag]
        schedRes.dicoRes['listSepTermSched'] = [schedRes.dicoRes['ordering']]
        schedRes.dicoRes['time_total'] = 0.0
        schedRes.dicoRes['optimMetrics'] = metrics
        return schedRes

    logger.info("Start optims part.")
    time1 = time.perf_counter()
    listSeparatedDAG = [dag]
    something_changed = True

    # main optim loop
    while something_changed and \
          (max_global_iter <= 0 or \
           metrics.nbGlobalStep < max_global_iter):
        metrics.addGlobalStep()
        something_changed = False
        logger.info("Global step optims. DAG state:\n{}", dag)
        # 1. (optional) try to apply Pascal's optims
        if local_clustering:
            nbRemovedNode = 1
            while nbRemovedNode > 0 and \
                  (max_local_iter <= 0 or \
                   metrics.nbsLocalStep[-1] < max_local_iter):
                nbRemovedNodeS = nodeSerieOptim(dag, recursion=True)
                # nbRemovedNodeSl = nodeSerieOptim(dag)
                # nbRemovedNodeS = [nbRemovedNodeSl]
                # while nbRemovedNodeSl:
                #     nbRemovedNodeSl = nodeSerieOptim(dag)
                #     nbRemovedNodeS.append(nbRemovedNodeSl)
                topoSortASAPandALAP(dag)
                nbSeqNode = nodeParToSeqOptim(dag)
                # nbSeqNode = nodeParToSeqOptimGen(dag)
                # nbSeqNode = nodeParToSeqOptimGenIter(dag)
                # nbSeqNode = nodeParToSeqOptimChain(dag, transferOnPath=keepEdgeMem)
                metrics.addLocalStep(nbRemovedNodeS, nbSeqNode)
                nbRemovedNode = nbRemovedNodeS + nbSeqNode
                # metrics.addLocalStep(nbRemovedNodeSl, nbSeqNode)
                # nbRemovedNode = sum(nbRemovedNodeS) + nbSeqNode
                logger.debug("Local step: {} nodes removed/seq", nbRemovedNode)
            nbLocalIter = metrics.nbsLocalStep[metrics.nbGlobalStep-1]
            logger.info("Finished Pascal's compaction ({} iterations).", nbLocalIter)
            something_changed |= (nbLocalIter > 1 or nbRemovedNode > 0)

        closure = None
        # 2. (optional) global parallel sequentialization
        if global_seq and \
           (global_seq_maxNodes <= 0 or \
            len(dag.nodes()) <= global_seq_maxNodes):
            topoSortASAPandALAP(dag)
            # closure = networkx.transitive_closure_dag(dag)
            # nbSeqNode = nodeParToSeqOptimGen3(dag, closure)
            nbSeqNode = nodeParToSeqOptimGen3_opt(dag)
            logger.info("Global sequentialized nodes: {}", nbSeqNode)
            metrics.nbsGlobalSeqNode[-1] = nbSeqNode
            something_changed |= (nbSeqNode > 0)
            
        # 3. (optional) remove transitive edges first (seems more efficient)
        # /!\ boolean attribute should be True if called with memdag fallback
        if transitivity_red:
            nbRemovedTransitiveEdge = remove_transitive_edges_cbp(dag, transferOnPath=keepEdgeMem)
            metrics.nbsGlobalRemovedEdge[-1] = nbRemovedTransitiveEdge
            logger.info("Transitive edges removed: {}", nbRemovedTransitiveEdge)
            something_changed |= (nbRemovedTransitiveEdge > 0)        

        # 4. (optional) node impact sequentialization
        if impact_seq:
            nbSeqNode = seqCommonImpact_deprecated(dag, closure)
            metrics.nbsImpactSeqNode[-1] = nbSeqNode
            something_changed |= (nbSeqNode > 0)

        # 5. (optional) we try to separate the DAG, and call recursively if so
        if sync_separation_rec:
            # /!\ boolean attribute should be False if called with memdag fallback
            listSeparatedDAG = separate_sync_frontiers(dag, not keepEdgeMem, offset_rec)
            if len(listSeparatedDAG) > 1:
                # call only if more than one, otherwise infinite recursion
                logger.info("Recursive call on {} separated DAGs.",
                            len(listSeparatedDAG))
                listPeakSched = [rec_optims_dag_min_mem(dag_sep, **comKWargs)
                                 for dag_sep in listSeparatedDAG]
                return reconstruct_sep_schedules(listPeakSched, metrics, offset_rec)

        # 6. (optional) at last resort we detect STsubDAG and optimally schedule them
        if not something_changed and STsubDAG_seq:
            nbSeqSTsubDAG = sequentialize_STsubDAG_V2(dag, STsubDAG_maxNodes, **comKWargs)
            metrics.nbsSequentializedSTsubDAG[-1] = nbSeqSTsubDAG
            something_changed |= (nbSeqSTsubDAG > 0)

        # prevent infinite loop if sync_separation_rec with only 1 sepDAG,
        # no need to loop if no local_clustering
        something_changed &= local_clustering

        
    # (optional) at last we try to separate the DAG
    # (if not already done recursively)
    if sync_separation_end and not sync_separation_rec:
        listSeparatedDAG = separate_sync_frontiers(dag, True, offset_rec)

    # /!\ fallback when cannot do anymore optim
    # boolean attribute should be False if called with memdag fallback
    logger.info("Fallback call on {} separated DAGs.", len(listSeparatedDAG))
    listPeakSched = [fallback(dag_sep, **comKWargs) for dag_sep in listSeparatedDAG]
    schedRes_fallback = reconstruct_sep_schedules(listPeakSched, metrics, offset_rec, listSeparatedDAG)
    time2 = time.perf_counter()
    time_total = time2 - time1
    # set time_total (will be reset if recursive call)
    schedRes_fallback.dicoRes['time_total'] = time_total
    logger.info("Time optims+fallback: {:.6f} (sec)", time_total)

    return schedRes_fallback
    
