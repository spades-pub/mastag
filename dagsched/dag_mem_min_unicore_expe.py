#! /bin/bash/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from .utils import *
from .dag_gen import *
from .dag_analysis import *
from .dag_transform import *
from .dag_mem_min_unicore_algo import *
from .sdf_transform import *

from sortedcontainers import SortedSet

import random
import networkx
import bracelogger
logger = bracelogger.get_logger("dagsched_log")

PATH_SATELLITE = "./sdf3_inputs/satellite_adfg.xml"
PATH_SATELLITE_M = "./sdf3_inputs/satellite_m.xml"


def expeRandom():
    """Generate a few random graphs and compute their best ordering."""
    nbTasks = 10
    edgeProbability = 0.5
    maxEdgeMem = 100
    seed = SEED

    printSolution = True
    nbIterMax = 10


    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasks, edgeProbability, maxEdgeMem)

    for it in range(nbIterMax):
        
        logger.info("+----------------\nGeneration of DAG:\t{}", it)
        time1 = time.perf_counter()

        (random_dag,nbIter) = generate_random_dag(nbTasks,edgeProbability, seed)
        seed += nbIter
        edgeMemSum = generate_random_edge_mem(random_dag, maxEdgeMem, seed)
        set_alphabetical_node_names(random_dag)
        set_default_attributes(random_dag)
        seed += 1
            
        time2 = time.perf_counter()
        logger.info('==> DAG generation functions took {:.0f} ms', (time2-time1)*1000)
        time1 = time.perf_counter()

        set_node_cost(random_dag)
        topoSortASAPandALAP(random_dag)
        listSeparatedDAG = separate_sync_frontiers(random_dag)
        nbIntCCs = len(listSeparatedDAG)
        logger.info("Number of DAG separated by sync barriers: {}.", nbIntCCs)
        for i in range(0, nbIntCCs):
            nodesStr = ', '.join([n for _, n in listSeparatedDAG[i].nodes(data='name')])
            logger.info("DAG {} has nodes: {}", i, nodesStr)
                
        reduced_dag = networkx.transitive_reduction(random_dag)
        # copy nodes and edge data (not done automatically ...)
        reduced_dag.add_nodes_from(random_dag.nodes(data=True))
        reduced_dag.add_edges_from((u, v, random_dag.edges[u, v]) for u, v in reduced_dag.edges)
        topoSortASAPandALAP(reduced_dag)

        time2 = time.perf_counter()
        logger.info('==> DAG metrics functions took {:.0f} ms', (time2-time1)*1000)


        timed_func = timing_logging(brute_force_dag_min_mem)
        set_cbp_tmpMemInc(random_dag)
        schedRes_brute = timed_func(random_dag, **{'minMemUpperBound': edgeMemSum + 1, 'faster': True})
            
        if printSolution:
            logger.info("-----------------\nSolution:")
            logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                        schedRes_brute.dicoRes['minMemPeakAllOrdering'],
                        schedRes_brute.dicoRes['nbOrdering'],
                        schedRes_brute.dicoRes['nbFasterBreak'])
            logger.info([random_dag.nodes[x]['name'] for x in schedRes_brute.dicoRes['ordering']])

            # comparison with separated sync
            nbSeparatedOrderings = 0
            minMemSeparatedOrderings = 0
            listBestSeparatedOrderings = []
            for disconnectedDAG in listSeparatedDAG:
                set_cbp_tmpMemInc(disconnectedDAG)
                schedRes_sep = brute_force_dag_min_mem(disconnectedDAG, **{'minMemUpperBound': edgeMemSum + 1, 'faster': True})
                nbSeparatedOrderings += schedRes_sep.dicoRes['nbOrdering']
                minMemSeparatedOrderings = max(minMemSeparatedOrderings, schedRes_sep.dicoRes['minMemPeakAllOrdering'])
                listBestSeparatedOrderings.extend(schedRes_sep.dicoRes['ordering'])
            logger.info("-----------------\nSolution (after separation):")
            logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}", 
                        minMemSeparatedOrderings, nbSeparatedOrderings)
            logger.info([random_dag.nodes[x]['name'] for x in listBestSeparatedOrderings if x in random_dag])
                
            # comparison with Pascal's serie optim
            set_cbp_tmpMemInc(random_dag)
            nbRemovedNode = nodeSerieOptim(random_dag)
            while nbRemovedNode > 0:
                logger.info('== rec iter serie optim')
                nbRemovedNode = nodeSerieOptim(random_dag)
            schedRes_serialOptim = brute_force_dag_min_mem(random_dag, **{'minMemUpperBound': edgeMemSum + 1, 'faster': True})
            logger.info("-----------------\nSolution (Pascal's serie):")
            logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}", 
                        schedRes_serialOptim.dicoRes['minMemPeakAllOrdering'],
                        schedRes_serialOptim.dicoRes['nbOrdering'])
            serieBestOrdering = getFullOrder(random_dag, schedRes_serialOptim.dicoRes['ordering'])
            (random_dag,nbIter) = generate_random_dag(nbTasks,edgeProbability, seed-1)
            set_alphabetical_node_names(random_dag)
            logger.info([random_dag.nodes[x]['name'] for x in serieBestOrdering])

            generate_random_edge_mem(random_dag, maxEdgeMem, seed-1)
            set_node_cost(random_dag)
            topoSortASAPandALAP(random_dag)    
            plotMemDAG(random_dag, 'DAG: {}'.format(it), True)


def expeRasta():
    dag = generateRastaPlpDag()
    writeNXdagToDOT(dag, 'Rasta_BeforePBC.dot', onlyMemdagInfo=True)
    topoSortASAPandALAP(dag)
    dags = separate_sync_frontiers(dag)
    logger.info("Rasta dag has {} separated parts.", len(dags))
    for sdag in dags:
        logger.info("--> {}", ','.join([n for _, n in sdag.nodes(data='name')]))
    set_node_cost(dag)
    plotMemDAG(dag, 'DAG Rasta-PLP', False)
    
        
def expeSatellite():
    """Generate the Satellite graph from Sebastian Ritz, Markus Willems, Heinrich Meyr in Figure 4 of their paper:
    "Scheduling for Optimum Data Memory Compaction in Block Diagram Oriented Software Synthesis",
    and compute its best ordering.
    """

    # dag = generateSatelliteDag()
    dagSDF = parseSdf3(PATH_SATELLITE)
    dag = sdfToDAG(dagSDF, nbFiringsObj=0, optimalRepetition=False)
    # call to surround with source/sink, separate from "w" node
    # dag = separate_sync_frontiers(dagSatSDF)[0]
    writeNXdagToDOT(dag, 'Satellite_BeforePBC.dot', onlyMemdagInfo=True)

    nbNodesOriginal = len(dag.nodes())
    nbEdgesOriginal = len(dag.edges())
    edgeMemSum = sum([edgeMem for _,_,edgeMem in dag.edges(data='edgeMem')])

    set_default_attributes(dag)
    set_node_cost(dag)
    set_pbc_tmpMemInc(dag)
    schedRes_heur = heuristic_linear_1dot5steps_impact(dag)
    logger.info("Heuristic found minimum memory of: {}.", schedRes_heur.dicoRes['memPeak'])
    checked_mpeak, _ = mpeak_cbp_schedule(dag, schedRes_heur.dicoRes['ordering'])
    logger.info("Checked memory peak: {}", checked_mpeak)
    # logger.info([dag.nodes[x]['name'] for x in schedRes.dicoRes['ordering']])
    
    # set_node_cost(pbc_dag)
    # topoSortASAPandALAP(pbc_dag)
    # plotMemDAG(pbc_dag, 'Satellite: pbc output')

    pbc_dag = convert_to_pbc(dag)
    nbNodesPBC = len(pbc_dag.nodes())
    pbc_dag_nnames = {node: name for node, name in pbc_dag.nodes(data='name')}
    set_default_attributes(pbc_dag)

    nbRemovedTransitiveEdge = remove_transitive_edges_pbc(dag, pbc_dag)
    logger.info("Satellite DAG ({} nodes).\nnbEdges before transitive compaction: {} vs after {}", 
                nbNodesOriginal, nbEdgesOriginal, nbEdgesOriginal - nbRemovedTransitiveEdge)

    compact_chains(pbc_dag, hook=compact_asc_dsc)
    # following compactions will set/used node cost and peak
    set_node_cost(pbc_dag)
    set_cbp_tmpMemInc(pbc_dag)
    
    topoSortASAPandALAP(pbc_dag)
    #plotMemDAG(pbc_dag, 'Satellite: chain compaction output', True)

    compact_chains(pbc_dag, hook=compact_hill_valley)
    logger.info("Finished hill valley compaction.")
    ## debug
    # logger.info("-----------------")
    # for node, dicoNode in pbc_dag.nodes(data=True):
    #     logger.info("Node {} has cost {} and max {}.", pbc_dag_nnames[node], dicoNode['nodeMemCost'], dicoNode['tmpMemInc'])
    #     logger.info("--> predecessors {}", ', '.join([pbc_dag_nnames[y] for y in dicoNode['predNodes']]))
    #     logger.info("----> successors {}", ', '.join([pbc_dag_nnames[y] for y in dicoNode['succNodes']]))
    # logger.info("-----------------")
    # return
    compact_pbc_edges(pbc_dag)
    logger.info("Finished PBC compaction.")
    nbNodesPBCcompacted = len(pbc_dag.nodes())
    logger.info("nbNodes (PBC) before compaction: {} vs after: {}", 
                nbNodesPBC, nbNodesPBCcompacted)

    topoSortASAPandALAP(pbc_dag)
    plotMemDAG(pbc_dag, 'Satellite: HV compaction output', True)

    logger.info("-----------------\nComputing best unicore memory peak for Satellite")
    timed_func = timing_logging(brute_force_dag_min_mem)
    KWargs = {'minMemUpperBound': 0, 'faster': True, 'timeout': 10}
    schedRes_brute = timed_func(pbc_dag, **KWargs) 
    logger.info("-----------------\nSolution:")
    logger.info("minMemUsed\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                schedRes_brute.dicoRes['memPeak'],
                schedRes_brute.dicoRes['nbOrdering'],
                schedRes_brute.dicoRes['nbFasterBreak'])
    fullBestOrdering = getFullOrder(pbc_dag, schedRes_brute.dicoRes['ordering'])
    logger.info([pbc_dag_nnames[x] for x in fullBestOrdering])


def expeTreeAntiSymChainMemdag():
    """Export a rooted intree example with antisymmetric chain,
    then schedules it optimally.
    """
    dag = generateAntisymetricChainTree()
    set_alphabetical_node_names(dag)
    set_default_attributes(dag)
    writeNXdagToDOT(dag, 'Rintree_antisymchain.dot', onlyMemdagInfo=True)
    edgeMemSum = sum([edgeMem for _,_,edgeMem in dag.edges(data='edgeMem')])
    pbc_dag = dag
    # convert_to_pbc(dag)
    # set_default_attributes(pbc_dag)
    set_node_cost(pbc_dag)
    set_cbp_tmpMemInc(pbc_dag)
    KWargs = {'minMemUpperBound': edgeMemSum + 1,
              'faster': True}
    schedRes = brute_force_dag_min_mem(pbc_dag, **KWargs) 

    logger.info("-----------------\nSolution:")
    logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                schedRes.dicoRes['minMemPeakAllOrdering'],
                schedRes.dicoRes['nbOrdering'],
                schedRes.dicoRes['nbFasterBreak'])        
    logger.info([pbc_dag.nodes[x]['name'] for x in schedRes.dicoRes['ordering']])
    topoSortASAPandALAP(pbc_dag)
    plotMemDAG(pbc_dag, 'Rintree with antisymmetric chain', True)


def expePascalSP():
    """Applies serie and parallel optim to Satellite graph.
    """
    
    dagSDF = parseSdf3(PATH_SATELLITE_M)
    dag = sdfToDAG(dagSDF, nbFiringsObj=0, optimalRepetition=False)

    nbNodesOriginal = len(dag.nodes())
    nbEdgesOriginal = len(dag.edges())
    edgeMemSum = sum([edgeMem for _,_,edgeMem in dag.edges(data='edgeMem')])
    
    set_default_attributes(dag)
    set_node_cost(dag)
    set_pbc_tmpMemInc(dag)
    schedRes_heur = heuristic_linear_1dot5steps_impact(dag)
    logger.info("Heuristic found minimum memory of: {}.", schedRes_heur.dicoRes['memPeak'])
    checked_mpeak, _ = mpeak_cbp_schedule(dag, schedRes_heur.dicoRes['ordering'])
    logger.info("Checked memory peak: {}", checked_mpeak)
    # logger.info([dag.nodes[x]['name'] for x in schedRes_heur.dicoRes['ordering']])

    # pbc_dag = convert_to_pbc(dag)
    pbc_dag = dag
    nbNodesPBC = len(pbc_dag.nodes())
    pbc_dag_nnames = {node: name for node, name in pbc_dag.nodes(data='name')}
    # set_default_attributes(pbc_dag)
    
    # nbRemovedTransitiveEdge = remove_transitive_edges_pbc(dag, pbc_dag)
    nbRemovedTransitiveEdge = remove_transitive_edges_cbp(pbc_dag)
    logger.info("Satellite DAG ({} nodes).\nnbEdges before first transitive reduction: {} vs after {}", 
                nbNodesOriginal, nbEdgesOriginal, nbEdgesOriginal - nbRemovedTransitiveEdge)
    
    # set_node_cost(pbc_dag)
    # set_pbc_tmpMemInc(pbc_dag)
    # set_cbp_tmpMemInc(pbc_dag)

    nbGlobalIter = 0
    while True:
        nbRemovedNode = nodeSerieOptim(pbc_dag)
        topoSortASAPandALAP(pbc_dag)
        nbRemovedNode += nodeParallelOptim(pbc_dag)
        nbIteration = 1
        while nbRemovedNode > 0:
            nbRemovedNode = nodeSerieOptim(pbc_dag)
            topoSortASAPandALAP(pbc_dag)
            nbRemovedNode += nodeParallelOptim(pbc_dag)
            nbIteration += 1
        nbGlobalIter += 1
        logger.info("Finished Pascal's compaction ({} iterations).", nbIteration)
        nbRemovedTransitiveEdge = remove_transitive_edges_cbp(pbc_dag)
        logger.info("Transitive edges removed after Pascal's optim: {}", nbRemovedTransitiveEdge)
        if nbRemovedTransitiveEdge == 0:
            break

    logger.info("nb global iterations (Pascal's optims + transitive reduction): {}",
                nbGlobalIter)
    nbNodesPBCcompacted = len(pbc_dag.nodes())
    logger.info("nbNodes (PBC) before compaction: {} vs after: {}", 
                nbNodesPBC, nbNodesPBCcompacted)

    schedRes_heurBis = heuristic_linear_1dot5steps_impact(pbc_dag)
    logger.info("Heuristic found minimum memory of (on compacted): {}.", schedRes_heurBis.dicoRes['memPeak'])
    fullHeurOrdering = getFullOrder(pbc_dag, schedRes_heurBis.dicoRes['ordering'])

    ## debug
    logger.info("-----------------")
    for node, dicoNode in pbc_dag.nodes(data=True):
        logger.info("Node {} has cost {} and max {}.", pbc_dag_nnames[node], dicoNode['nodeMemCost'], dicoNode['tmpMemInc'])
        # logger.info("--> predecessors {}", ', '.join([pbc_dag_nnames[y] for y in dicoNode['predNodes']]))
        # logger.info("----> successors {}", ', '.join([pbc_dag_nnames[y] for y in dicoNode['succNodes']]))
    logger.info("-----------------")
    
    dagSDF_bis = parseSdf3(PATH_SATELLITE_M)
    pbc_dag_bis = sdfToDAG(dagSDF_bis, nbFiringsObj=0, optimalRepetition=False)
    set_default_attributes(pbc_dag_bis)
    set_node_cost(pbc_dag_bis)
    # set_cbp_tmpMemInc(pbc_dag_bis)
    set_pbc_tmpMemInc(pbc_dag_bis)
    checked_mpeak, _ = mpeak_cbp_schedule(pbc_dag_bis, fullHeurOrdering)
    logger.info("Checked memory peak: {}", checked_mpeak)
    
    logger.info("-----------------\nComputing best unicore memory peak for Satellite")
    timed_func = timing_logging(brute_force_dag_min_mem)
    KWargs = {'minMemUpperBound': edgeMemSum + 1,
              'faster': True}
    schedRes_brute = timed_func(pbc_dag, **KWargs) 
    logger.info("-----------------\nSolution:")
    logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                schedRes_brute.dicoRes['minMemPeakAllOrdering'],
                schedRes_brute.dicoRes['nbOrdering'],
                schedRes_brute.dicoRes['nbFasterBreak'])        
    # logger.info([pbc_dag.nodes[x]['name'] for x in bestOrdering])
    fullBestOrdering = getFullOrder(pbc_dag, schedRes_brute.dicoRes['ordering'])
    logger.info([pbc_dag_nnames[x] for x in fullBestOrdering])

    # check schedule (if not PBC converted first)
    dagSDF = parseSdf3(PATH_SATELLITE_M)
    dag = sdfToDAG(dagSDF, nbFiringsObj=0, optimalRepetition=False)
    set_default_attributes(dag)
    set_node_cost(dag)
    # set_cbp_tmpMemInc(dag)
    set_pbc_tmpMemInc(dag)
    checked_mpeak, _ = mpeak_cbp_schedule(dag, fullBestOrdering)
    logger.info("Checked memory peak: {}", checked_mpeak)
    # plot compacted dag
    topoSortASAPandALAP(pbc_dag)
    plotMemDAG(pbc_dag, 'Satellite', True)

    
def expeCounterExamplePascalSP():
    """Generate a few random graphs and compute their best ordering,
    until if finds a counter example to Pascal serie/parallel/seq optim,
    or {nbIterMax} is reached."""
    nbBranchs = 3
    nbChainNodes = 4
    nbTasks = 28 #nbBranchs * nbChainNodes + 2
    edgeProbability = 0.5
    maxEdgeMem = 20
    seed = SEED
    
    nbIterMax = 100
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}", nbTasks, edgeProbability, maxEdgeMem)

    nbNoOptim = 0
    totNbSubgraphs = 0
    totNbNodeAtEnd = 0
    totNbGlobalIter = 0
    counterExample = False
    for it in range(nbIterMax):
        
        (random_dag,nbIter) = generate_random_dag(nbTasks, edgeProbability, seed)
        nbNodeAtBegin = len(random_dag.nodes())
        seed += nbIter
        # random_dag = generateBranchedSPdag(nbBranchs, nbChainNodes)
        edgeMemSum = generate_random_edge_mem(random_dag, maxEdgeMem, seed)
        set_alphabetical_node_names(random_dag)
        set_default_attributes(random_dag)
        set_node_cost(random_dag)
        set_pbc_tmpMemInc(random_dag)
        seed += 1

        topoSortASAPandALAP(random_dag)
        remove_transitive_edges_cbp(random_dag)
        # subgraphs = [subgraph for subgraph in strictSTsubDAG_generator_deprecated(random_dag)]
        subgraphs = [subgraph for subgraph in genSTsubDAG_generator(random_dag)]
        totNbSubgraphs += len(subgraphs)
        
        # solution Pascal + transitive followed by LE
        nbGlobalIter = 0
        while True:
            nbGlobalIter += 1
            while True:
                nbRemovedNodeSl = nodeSerieOptim(random_dag)
                nbRemovedNode = nbRemovedNodeSl
                while nbRemovedNodeSl > 0:
                    nbRemovedNodeSl = nodeSerieOptim(random_dag)
                    nbRemovedNode += nbRemovedNodeSl
                topoSortASAPandALAP(random_dag)
                nbRemovedNode += nodeParToSeqOptimGen3(random_dag)
                if nbRemovedNode == 0:
                    break
            nbRemovedTransitiveEdge = remove_transitive_edges_cbp(random_dag)
            if nbRemovedTransitiveEdge == 0:
                break

        nbNodeAtEnd = len(random_dag.nodes())
        if nbNodeAtBegin == nbNodeAtEnd:
            nbNoOptim += 1
            logger.debug("/!\ DAG no optim")
            logger.debug("Number of STsubDAG: {}", len(subgraphs))    

            offset_seed = seed - SEED - 1
            writeNXdagToDOT(random_dag, 'No_optim_Pascal_{}.dot'.format(offset_seed))
            KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
              'local_clustering': False, # optims serie/paralle
              'transitivity_red': False, # removes transitive edges
              'sync_separation_rec': False, # recursive dag separation
              'sync_separation_end': True, # fallback dag separation
              'fallback': brute_force_dag_min_mem, # after all optims

              'faster': True, # option for brute_force_dag_min_mem
              'timeout': 0, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
              'step': None, 'overlap': 0, # options for exhaustive_BandB (suboptimal)
              'ensureSPdag': False, # option for memdagSPschedule
              'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering)
              }
            schedRes_brute = rec_optims_dag_min_mem(random_dag, **KWargs)
            #schedRes_brute = brute_force_dag_min_mem(random_dag, **KWargs)
            logger.info("Nb orderings graph no optim {}: {}",
                        offset_seed, schedRes_brute.dicoRes['nbOrdering'])
            logger.info("Best order (peak {}):", schedRes_brute.dicoRes['memPeak'])
            logger.info([random_dag.nodes[x]['name'] for x in schedRes_brute.dicoRes['ordering']])
            topoSortASAPandALAP(random_dag)
            plotMemDAG(random_dag, 'No optim DAG seed+{}'.format(offset_seed), False)
            continue
        else:
            totNbNodeAtEnd += nbNodeAtEnd
            totNbGlobalIter += nbGlobalIter
        
        KWargs = {'minMemUpperBound': edgeMemSum + 1,
                  'faster': True}
        schedRes_brute = exhaustive_BandB_V2(random_dag, **KWargs) 
        fullBestOrdering = getFullOrder(random_dag, schedRes_brute.dicoRes['ordering'])
        minMemUsedAllOrdering = schedRes_brute.dicoRes['memPeak']

        # ground truth solution, regenerate dag because modified by Pascal's optim
        schedRes_gt = SchedRes()
        (random_dag_bis, nbIter) = generate_random_dag(nbTasks,edgeProbability, seed-1)
        #random_dag_bis = generateBranchedSPdag(nbBranchs, nbChainNodes)
        edgeMemSum = generate_random_edge_mem(random_dag_bis, maxEdgeMem, seed-1)
        set_alphabetical_node_names(random_dag_bis)
        set_default_attributes(random_dag_bis)
        set_node_cost(random_dag_bis)
        set_pbc_tmpMemInc(random_dag_bis)
        # check schedule of Pascal solution
        checked_mpeak, _ = mpeak_cbp_schedule(random_dag_bis, fullBestOrdering)
        if checked_mpeak != minMemUsedAllOrdering:
            counterExample = True
            schedRes_gt.dicoRes['memPeak'] = checked_mpeak
            break

        pbc_dag_bis = random_dag_bis #convert_to_pbc(random_dag_bis)
        # reset params if conversion
        # set_default_attributes(pbc_dag_bis)
        # set_node_cost(pbc_dag_bis)
        # set_cbp_tmpMemInc(pbc_dag_bis) 
        # schedRes_gt = memdagSPschedule(pbc_dag_bis, **{'ensureSPdag': False})
        # schedRes_gt.dicoRes['nbOrdering'] = nbLEbranchesSPdag(nbBranchs, nbChainNodes) 
        schedRes_gt = exhaustive_BandB_V2(pbc_dag_bis, **KWargs)
        checked_mpeak, _ = mpeak_cbp_schedule(pbc_dag_bis, schedRes_gt.dicoRes['ordering'])
        minMemUsedAllOrdering_bis = schedRes_gt.dicoRes['memPeak']
        if checked_mpeak != minMemUsedAllOrdering_bis:
            raise ValueError("Ground truth result is wrong.")

        # check if Pascal solution is optimal (compared with all LE) 
        if schedRes_brute.dicoRes['memPeak'] != schedRes_gt.dicoRes['memPeak']:
            counterExample = True
            break

    averageSubgraphs = totNbSubgraphs / (it + 1)
    logger.info("Avg subgraphs before optims: {}", averageSubgraphs)
    if it + 1 - nbNoOptim > 0:
        averageNodeAtEnd = totNbNodeAtEnd / (it + 1 - nbNoOptim)
        logger.info("Avg nodes at end of optims: {}", averageNodeAtEnd)
        averageGlobIter = totNbGlobalIter / (it + 1 - nbNoOptim)
        logger.info("Avg nb iterations of optims: {}", averageGlobIter)
    else:
        logger.info("Other Avg metrics not available.")
    logger.info("Nb no optim: {}", nbNoOptim)
    logger.info("Nb iter: {}", it + 1)
    logger.info("Last seed: {}", seed-1)
    if counterExample:
        logger.info("-----------------\nSolution Pascal's serie optim:")
        logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}", 
                    schedRes_brute.dicoRes['minMemPeakAllOrdering'],
                    schedRes_brute.dicoRes['nbOrdering'],
                    schedRes_brute.dicoRes['nbFasterBreak'])
        logger.info([random_dag_bis.nodes[x]['name'] for x in schedRes_brute.dicoRes['ordering']])

        logger.info("-----------------\nSolution LE/memdag:")
        logger.info("minMemUsedAllOrdering\t{}", 
                    schedRes_gt.dicoRes['minMemPeakAllOrdering'],
                    schedRes_gt.dicoRes['nbOrdering'])        
        if schedRes_gt.dicoRes['ordering']:
            logger.info([pbc_dag_bis.nodes[x]['name'] for x in schedRes_gt.dicoRes['ordering']])

        writeNXdagToDOT(random_dag_bis, 'Counter_example_Pascal_bis.dot')
        topoSortASAPandALAP(random_dag_bis)
        plotMemDAG(random_dag_bis, 'Counter example', False)

    else:
        logger.info("No counter example until iter {}.", nbIterMax)
            
    
def expeSeparable():
    """Generate a separable DAG, cuts it in two trees and schedule them all.
    However, the cut forming trees is not optimal. This is a wrong intuition.
    """
    maxEdgeMem = 100
    seed = SEED

    nbBranchs = 2
    nbChainNodes = 1
    dag = generateSeparableDAG(nbBranchs, nbChainNodes)
    edgeMemSum = generate_random_edge_mem(dag, maxEdgeMem, seed)

    topoSortASAPandALAP(dag)
    set_node_cost(dag)
    plotMemDAG(dag, 'Separable DAG', True)
    # no need to compute the transitive reduction, it is the same graph

    set_default_attributes(dag)
    set_alphabetical_node_names(dag)
    writeNXdagToDOT(dag, 'SeparableDAG_{}_{}.dot'.format(nbBranchs, nbChainNodes), True)
    (rtree1, rtree2, minCutSize) = generateTreesOfSeperableDAG(dag, nbBranchs, nbChainNodes)
    writeNXdagToDOT(rtree1, 'SeparableDAG_{}_{}_rtree1.dot'.format(nbBranchs, nbChainNodes), True)
    writeNXdagToDOT(rtree2, 'SeparableDAG_{}_{}_rtree2.dot'.format(nbBranchs, nbChainNodes), True)
    logger.info("Separable DAG cut size: {}.", minCutSize)

    timed_func = timing_logging(brute_force_dag_min_mem)
    KWargs = {'minMemUpperBound': edgeMemSum + 1,
              'faster': True}

    set_cbp_tmpMemInc(dag)
    schedRes_dag = timed_func(dag, **KWargs)
    logger.info("-----------------\nSolution (full DAG):")
    logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                schedRes_dag.dicoRes['minMemPeakAllOrdering'],
                schedRes_dag.dicoRes['nbOrdering'],
                schedRes_dag.dicoRes['nbFasterBreak'])        
    logger.info([dag.nodes[x]['name'] for x in schedRes_dag.dicoRes['ordering']])
    
    set_node_cost(rtree1)
    set_cbp_tmpMemInc(rtree1)
    schedRes_rtree1 = timed_func(rtree1, **KWargs)
    logger.info("-----------------\nSolution (rtree1):")
    logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                schedRes_rtree1.dicoRes['minMemPeakAllOrdering'],
                schedRes_rtree1.dicoRes['nbOrdering'],
                schedRes_rtree1.dicoRes['nbFasterBreak'])        
    logger.info([rtree1.nodes[x]['name'] for x in schedRes_rtree1.dicoRes['ordering']])

    set_node_cost(rtree2)
    set_cbp_tmpMemInc(rtree2)
    schedRes_rtree2 = timed_func(rtree2, **KWargs)
    logger.info("-----------------\nSolution (rtree2):")
    logger.info("minMemUsedAllOrdering\t{}\nnbOrdering\t{}\nnbFasterBreak\t{}", 
                schedRes_rtree2.dicoRes['minMemPeakAllOrdering'],
                schedRes_rtree2.dicoRes['nbOrdering'],
                schedRes_rtree2.dicoRes['nbFasterBreak'])        
    logger.info([rtree2.nodes[x]['name'] for x in schedRes_rtree2.dicoRes['ordering']])


def expeSPdagLN():
    n, m = (7, 22)
    logger.info("Checking nbLE for two chains graph ({}, {}).", n, m)
    maxEdgeMem = 100
    seed = SEED
    FnbLEtwoChains = nbLEtwoChainsFast(n, m)
    logger.info("Fast: {}", FnbLEtwoChains)
    SnbLEtwoChains = nbLEtwoChainsSlow(n, m)
    logger.info("Slow: {}", SnbLEtwoChains)
    RnbLEtwoChains = 0
    twoChains = genTwoChains(n, m)
    for t in networkx.all_topological_sorts(twoChains):
        RnbLEtwoChains += 1
    logger.info("Enum: {}", RnbLEtwoChains)
    set_alphabetical_node_names(twoChains)
    topoSortASAPandALAP(twoChains)
    generate_random_edge_mem(twoChains, maxEdgeMem, seed)
    set_node_cost(twoChains)
    plotMemDAG(twoChains, 'Two chains ({},{}) DAG'.format(n,m), False)
    

def expeTransfoSDF():
    """Check SDF transformation"""
    # graph = networkx.DiGraph()
    # graph.add_edge(0, 1, rateSrc=3, rateDst=2, delay=0)
    # # graph.add_edge(1, 0, rateSrc=2, rateDst=3, delay=4)
    # graph.add_edge(1, 2, rateSrc=1, rateDst=1, delay=0)
    # graph.add_edge(2, 3, rateSrc=2, rateDst=3, delay=0)
    # graph.add_edge(0, 3, rateSrc=2, rateDst=2, delay=0)
    # set_alphabetical_node_names(graph)
    # set_default_attributes(graph)
    # exp_dag = convert_sdf_to_srsdf(graph, autoconcurrency=True)
    dagSDF = parseSdf3("./sdf3_inputs/modem_adfg.xml")
    exp_dag = sdfToDAG(dagSDF, nbFiringsObj=0, optimalRepetition=False)
    logger.info("Generated dag has {} nodes and {} edges.", len(exp_dag.nodes()), len(exp_dag.edges()))
    # for src, dst, edgeMem in exp_dag.edges(data='edgeMem'):
    #     logger.info("{} -- t{} --> {}",
    #           exp_dag.nodes[src]['name'], edgeMem, exp_dag.nodes[dst]['name'])
    for chain in chains_generator(exp_dag):
        logger.debug("Finished chain (length = {}).",len(chain.nodes))
        logger.debug(chain.strWNames(exp_dag))

    topoSortASAPandALAP(exp_dag)
    set_node_cost(exp_dag)
    plotMemDAG(exp_dag, 'Expanded SDF', False)
    

def expeChainCompaction():
    """Check chain transformations"""
    logger.info("=== Compact asc-dsc with Double Liu method")
    dag_asc_dsc = generateChainAscDsc()
    set_default_attributes(dag_asc_dsc)
    set_alphabetical_node_names(dag_asc_dsc)
    nbNodeRemoved = compact_chains(dag_asc_dsc, hook=compact_asc_dsc)
    logger.info("Nb node removed: {}", nbNodeRemoved)
    set_node_cost(dag_asc_dsc)
    set_cbp_tmpMemInc(dag_asc_dsc)
    nbNodeRemoved = compact_chains(dag_asc_dsc, hook=compact_hill_valley)
    logger.info("Nb node removed Hill-Valley: {}", nbNodeRemoved)
    schedRes = heuristic_linear_1dot5steps_impact(dag_asc_dsc)
    logger.info("Heuristic found minimum memory of: {}.", schedRes.dicoRes['memPeak'])
    heurFullOrder = getFullOrder(dag_asc_dsc, schedRes.dicoRes['ordering'])
    dag_asc_dsc = generateChainAscDsc()
    set_default_attributes(dag_asc_dsc)
    set_node_cost(dag_asc_dsc)
    set_cbp_tmpMemInc(dag_asc_dsc)
    checked_mpeak, _ = mpeak_cbp_schedule(dag_asc_dsc, heurFullOrder)
    logger.info("Checked memory peak: {}", checked_mpeak)

    
    logger.info("=== Compact asc-dsc with Pascal Serie method")
    dag_asc_dsc = generateChainAscDsc()
    set_default_attributes(dag_asc_dsc)
    set_alphabetical_node_names(dag_asc_dsc)
    set_node_cost(dag_asc_dsc)
    set_cbp_tmpMemInc(dag_asc_dsc)
    nbNodeRemoved = nodeSerieOptim(dag_asc_dsc)
    nbNodeRemovedTot = nbNodeRemoved
    nbIteration = 1
    while nbNodeRemoved > 0:
        logger.debug("New iter of serie optim")
        nbNodeRemoved = nodeSerieOptim(dag_asc_dsc)
        nbNodeRemovedTot += nbNodeRemoved
        nbIteration += 1
    logger.info("Number of iterations: {}", nbIteration)
    logger.info("Nb node removed: {}", nbNodeRemovedTot)
    schedRes = heuristic_linear_1dot5steps_impact(dag_asc_dsc)
    logger.info("Heuristic found minimum memory of: {}.", schedRes.dicoRes['memPeak'])
    heurFullOrder = getFullOrder(dag_asc_dsc, schedRes.dicoRes['ordering'])
    dag_asc_dsc = generateChainAscDsc()
    set_default_attributes(dag_asc_dsc)
    set_node_cost(dag_asc_dsc)
    set_cbp_tmpMemInc(dag_asc_dsc)
    checked_mpeak, _ = mpeak_cbp_schedule(dag_asc_dsc, heurFullOrder)
    logger.info("Checked memory peak: {}", checked_mpeak)


    logger.info("=== Compact dsc-asc with Double Liu method")
    dag_dsc_asc = generateChainDscAsc()
    set_default_attributes(dag_dsc_asc)
    set_alphabetical_node_names(dag_dsc_asc)
    nbNodeRemoved = compact_chains(dag_dsc_asc, hook=compact_asc_dsc)
    logger.info("Nb node removed Liu: {}", nbNodeRemoved)
    set_node_cost(dag_dsc_asc)
    set_cbp_tmpMemInc(dag_dsc_asc)
    nbNodeRemoved = compact_chains(dag_dsc_asc, hook=compact_hill_valley)
    logger.info("Nb node removed Hill-Valley: {}", nbNodeRemoved)
    schedRes = heuristic_linear_1dot5steps_impact(dag_dsc_asc)
    logger.info("Heuristic found minimum memory of: {}.", schedRes.dicoRes['memPeak'])
    heurFullOrder = getFullOrder(dag_dsc_asc, schedRes.dicoRes['ordering'])
    dag_dsc_asc = generateChainDscAsc()
    set_default_attributes(dag_dsc_asc)
    set_node_cost(dag_dsc_asc)
    set_cbp_tmpMemInc(dag_dsc_asc)
    checked_mpeak, _ = mpeak_cbp_schedule(dag_dsc_asc, heurFullOrder)
    logger.info("Checked memory peak: {}", checked_mpeak)


    logger.info("=== Compact dsc-asc with Pascal Serie method")
    dag_dsc_asc = generateChainDscAsc()
    set_default_attributes(dag_dsc_asc)
    set_alphabetical_node_names(dag_dsc_asc)
    set_node_cost(dag_dsc_asc)
    set_cbp_tmpMemInc(dag_dsc_asc)
    nbNodeRemoved = nodeSerieOptim(dag_dsc_asc)
    nbNodeRemovedTot = nbNodeRemoved
    nbIteration = 1
    while nbNodeRemoved > 0:
        logger.debug("New iter of serie optim")
        nbNodeRemoved = nodeSerieOptim(dag_dsc_asc)
        nbNodeRemovedTot += nbNodeRemoved
        nbIteration += 1
    logger.info("Number of iterations: {}", nbIteration)
    logger.info("Nb node removed: {}", nbNodeRemovedTot)
    schedRes = heuristic_linear_1dot5steps_impact(dag_dsc_asc)
    logger.info("Heuristic found minimum memory of: {}.", schedRes.dicoRes['memPeak'])
    heurFullOrder = getFullOrder(dag_dsc_asc, schedRes.dicoRes['ordering'])
    dag_dsc_asc = generateChainDscAsc()
    set_default_attributes(dag_dsc_asc)
    set_node_cost(dag_dsc_asc)
    set_cbp_tmpMemInc(dag_dsc_asc)
    checked_mpeak, _ = mpeak_cbp_schedule(dag_dsc_asc, heurFullOrder)
    logger.info("Checked memory peak: {}", checked_mpeak)

    
def expeAllOptimsSDFgraph(graph, nbFiringsObj=0, optimalRepetition=False,
                          pegLocalPeak=True, nodeBestScoreFunc=getNodeBestScoreOnly,
                          autoconcurrency=False, assertPeak=True, showTermDAG=False, **kwargs):
    """Apply all optimizations recursively and schedule the transformed graph.

    :param graph: Path to the SDF graphe having edge attributes
    'rateSrc' and 'rateDst' set.
    :param nbFiringsObj: Objective for the number of firings in the
    extension. If 0 a new DAG is created being
    single-rate. If between 0 and number of nodes in dagSDF,
    'edgeMem' attributes are set in place (equivalent flat SAS).
    If higher, then a heuristic is used to compute partial expansion.
    :param pegLocalPeak: Wheter or not use the PEG local peak, otherwise use raw PBC.
    :param nodeBestScoreFunc: Function used to select node to reduce based on score.
    :param optimalRepetition: If the extended graph repetition must be optimized.
    :param autoconcurrency: If True, adds depdencies
    limiting autoconcurrency. /!\ Not tested with self-loops.
    :param assertPeak: If the peak must be asserted on original graph.
    :param showTermDAG: If the terminal clustered DAG must be shown.
    :param kwargs: Dictionnary of optional arguments to override for inner solving methods.
    :returns: Scheduling result.
    """
    if pegLocalPeak:
        set_sdf_tmpMemInc(graph)

    # get graph to analyze
    dag = sdfToDAG(graph, nbFiringsObj, optimalRepetition, autoconcurrency)

    if pegLocalPeak:
        def pegLocalPeakFun(dag):
            return set_peg_tmpMemInc(dag, graph)            
        localPeakFun = pegLocalPeakFun 
    else:
        localPeakFun = set_pbc_tmpMemInc
    return expeAllOptimsDAG(dag, localPeakFun, assertPeak, showTermDAG, **kwargs)


def expeAllOptimsDAG(dag, localPeakFun=None,
                     assertPeak=True, showTermDAG=False, **kwargs):
    """Apply all optimizations recursively and schedule the transformed graph.

    ;param dag: DAG to analyse with 'edgeMem' attribute set (will be modified in place).
    :param localPeakFun: (defulat None) Function to set local peaks of node.
    If None, uses set_pbc_tmpMemInc(dag).
    :param assertPeak: If the peak must be asserted on original graph.
    :param showTermDAG: If the terminal clustered DAG must be shown.
    :param kwargs: Dictionnary of optional arguments to override for inner solving methods.
    :returns: Scheduling result.
    """
    set_node_cost(dag)
    if localPeakFun:
        localPeakFun(dag)
    else:
        set_pbc_tmpMemInc(dag)

    dag_copy = copy_graph_solver(dag)
    set_default_attributes(dag_copy, resetTmpMemInc=False)

    # topoSortASAPandALAP(dag)
    # plotMemDAG(dag, 'DAG before optims', False, subset_key=None)
        
    # transform and schedule
    minMemHeuristic = kwargs.get('minMemHeuristic', None)
    if not (minMemHeuristic is None):
        schedRes_heur = minMemHeuristic(dag, **kwargs)
        peakHeur = schedRes_heur.dicoRes['memPeak']
        logger.info("Memory peak heuristic: {}.", peakHeur)
    else:
        peakHeur = float('inf')
        
    peakAsked = kwargs.get('minMemUpperBound', 0) 
    KWargs = dict(kwargs)
    if (peakAsked == 0 and not (minMemHeuristic is None)) \
       or peakAsked > peakHeur: 
        KWargs['minMemUpperBound'] = peakHeur

    schedRes = rec_optims_dag_min_mem(dag_copy, **KWargs)
    logger.info("Optim metrics:\n{}", schedRes.dicoRes['optimMetrics'])
    logger.info("Memory peak all optims (fallback={}): {}.",
                kwargs.get('fallback', exhaustive_BandB_V2).__name__, schedRes.dicoRes['memPeak'])

    
    # print terminal dag
    termDAGs = schedRes.dicoRes['listSepTermDAG']
    nbNodesTerm = 0
    for termDAG in termDAGs:
        nbNodesTerm += len(termDAG.nodes())
    logger.info("There were {} terminal DAGs totalizing {} nodes.", len(termDAGs), nbNodesTerm)


    if showTermDAG:
        for it,termDAG in enumerate(termDAGs):
            termSched = schedRes.dicoRes['listSepTermSched'][it]
            subOrdering = [None]
            if not (termSched is None):
                subOrdering = [termDAG.nodes[n]['name'] for n in termSched]
            logger.info("Showing terminal dag {} with ordering:\n{}",
                        it, subOrdering)
            # topoSortASAPandALAP(termDAG)
            writeNXdagToDOT(termDAG, 'TerminalDAG_{}.dot'.format(it))
            plotMemDAG(termDAG, 'Terminal DAG: {}'.format(it),
                       True, showNow=False, subset_key=None)
            _, termPeaks = mpeak_cbp_schedule(termDAG, termSched)
            plotMemSched(termDAG, termSched, termPeaks)
            
    # check result
    fullOrdering = schedRes.dicoRes['ordering']
    if assertPeak and fullOrdering:
        checked_mpeak, peaks = mpeak_cbp_schedule(dag, fullOrdering)
        if checked_mpeak < schedRes.dicoRes['memPeak'] and \
           'readMemdagPeak' in schedRes.keysToGather:
            logger.warning("Real peak is lower than memdag one, take minimum of:\n{}",
                           schedRes.dicoRes['readMemdagPeak'])
        elif checked_mpeak != schedRes.dicoRes['memPeak']:
            logger.error("Real peak is: {}", checked_mpeak)
            raise ValueError("Wrong peak.")
        # plotMemSched(dag, fullOrdering, peaks)

    if fullOrdering is None:
        logger.warning("/!\ No schedule found (timeout or tight bound)!")
        if not (minMemHeuristic is None):
            logger.info("Uses heuristic instead ({}).", minMemHeuristic.__name__)
            fullOrdering = schedRes_heur.dicoRes['ordering']
            schedRes.dicoRes['ordering'] = fullOrdering

    # log schedule
    # logger.info("Schedule:\n{}", [dag.nodes[x]['name'] for x in fullOrdering])
    # logger.info("Schedule:\n{}", [dag.nodes[x]['sdfNode'] for x in fullOrdering])
    # rawSdfSched = [dag.nodes[x]['sdfNode'] for x in fullOrdering]
    # writeListToFile(rawSdfSched, 'sched.bin')
    # strSdfSched = ','.join([str(x) for x in rawSdfSched])
    # with open('sched.txt', 'wb') as f:
    #     f.write(str(strSdfSched).encode('ascii'))
        
    return schedRes


    
def expeNoReduceAllOptim():
    """Generate random graphs and apply all optims until
    resulting DAG has more than one node in parallel.
    This function quickly finds such example except for SP DAG."""
    
    # nbTasks = 10
    nbBranchs = 2
    nbChainNodes = 0
    nbTasks = nbBranchs * nbChainNodes + 2
    edgeProbability = 0.5
    maxEdgeMem = 25
    seed = SEED

    nbIterMax = 10000

    counterExample = False

    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasks, edgeProbability, maxEdgeMem)

    for it in range(nbIterMax):
        
        # (random_dag,nbIter) = generate_random_dag(nbTasks, edgeProbability, seed)
        # seed += nbIter
        # random_dag = generateBranchedSPdag(nbBranchs, nbChainNodes)
        random_dag = generateSeparableDAG(nbBranchs=nbBranchs, nbChainNodes=nbChainNodes,
                                          offsetNewNodes=0, noSTdag=False, cplxVersion=True)

        generate_random_edge_mem(random_dag, maxEdgeMem, seed+1)
        set_alphabetical_node_names(random_dag)
        set_default_attributes(random_dag)
        # set_node_cost(random_dag)
        generate_random_node_metric(random_dag, -maxEdgeMem, maxEdgeMem, seed+1, 'nodeMemCost')
        # set_pbc_tmpMemInc(random_dag)
        generate_random_tmpMemInc(random_dag, maxEdgeMem, seed+2)
        set_cbp_tmpMemInc(random_dag)
        seed += 1

        dag_copy = copy_graph_solver(random_dag)
            
        # transform and schedule
        schedRes = rec_optims_dag_min_mem(random_dag, **{'sync_separation_end': False})
        fullOrdering = schedRes.dicoRes['ordering']
        # check result
        checked_mpeak, _ = mpeak_cbp_schedule(dag_copy, fullOrdering)
        if checked_mpeak != schedRes.dicoRes['memPeak']:
            logger.error("Real peak is: {}", checked_mpeak)
            raise ValueError("Wrong peak.")

        # check gt with memdag (comment this part if you do not have memdag)
        # set_cbp_tmpMemInc(dag_copy) 
        # schedRes_gt = memdagSPschedule(dag_copy, **{'ensureSPdag': True, 'convertTmpMemInc': True})
        # checked_mpeak, _ = mpeak_cbp_schedule(dag_copy, schedRes_gt.dicoRes['ordering'])
        # if schedRes.dicoRes['memPeak'] != checked_mpeak:
        #     raise ValueError("Ground truth result is wrong.")
        # if schedRes.dicoRes['memPeak'] != schedRes_gt.dicoRes['memPeak']:
        #     raise ValueError("Ground truth result is different than transformation.")
        
        # check if chain
        isChain = False
        if len(random_dag.edges()) == len(random_dag.nodes()) - 1:
            isChain = True

        # # if not a chain, we have what we look for
        # if not isChain:
        # alternatively check if clustered:
        if len(random_dag.nodes()) == len(dag_copy.nodes()):
            offset_seed = seed - SEED - 1
            plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True, subset_key=None)
            # counterExample = True
            # break

        
    if counterExample:
        offset_seed = seed - SEED - 1
        logger.info("Found a counter-example at seed offset: {}", offset_seed)
        logger.info("Optim metrics:\n{}", schedRes.dicoRes['optimMetrics'])
        logger.info("Memory peak: {}.", schedRes.dicoRes['memPeak'])
        logger.info([dag_copy.nodes[x]['name'] for x in fullOrdering])
        writeNXdagToDOT(random_dag, 'Counter_example_Pascal_ter.dot')
        topoSortASAPandALAP(random_dag)
        plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)
    else:
        logger.info("No counter example until iter {}", it)
                

def expeSTsubDAG():
    """Generate graphs until found one with ST subgraph
    and visualize it, then continue.
    """

    # nbBranchs = 4
    # nbChainNodes = 5
    # nbTasks = nbBranchs * nbChainNodes + 2
    nbTasks = 10
    edgeProbability = 0.5
    maxEdgeMem = 100
    seed = SEED

    nbIterMax = 10
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasks, edgeProbability, maxEdgeMem)

    nbNoST = 0
    for it in range(nbIterMax):

        (random_dag,nbIter) = generate_random_dag(nbTasks, edgeProbability, seed)
        seed += nbIter
        # random_dag = generateBranchedSPdag(nbBranchs, nbChainNodes)
        edgeMemSum = generate_random_edge_mem(random_dag, maxEdgeMem, seed+1)
        set_alphabetical_node_names(random_dag)
        set_default_attributes(random_dag)
        set_node_cost(random_dag)
        #set_pbc_tmpMemInc(random_dag)
        generate_random_tmpMemInc(random_dag, maxEdgeMem, seed+2)
        set_cbp_tmpMemInc(random_dag)
        topoSortASAPandALAP(random_dag)
        seed += 1

        nbRemovedTransitiveEdge = remove_transitive_edges_cbp(random_dag)
        # subgraphs = [subgraph for subgraph in strictSTsubDAG_generator_deprecated(random_dag, onlyBiggest=True)]
        subgraphs = [subgraph for subgraph in genSTsubDAG_generator(random_dag)]
        nbSubgraphs = len(subgraphs)

        if nbSubgraphs == 0:
            nbNoST += 1
        else:
            offset_seed = seed - SEED - 1
            logger.info("offset_seed {} has: {} ST subgraph (>1 nodes)",
                        offset_seed, nbSubgraphs)
            for num, tupleSTsubDAG in enumerate(subgraphs):
                subgraph = tupleSTsubDAG[1]
                logger.info("nodes in subgraph {}: {}", num,
                            [name for node, name in subgraph.nodes(data='name')])
            plotMemDAG(random_dag, 'DAG seed+{} with #STsub={}'.
                       format(offset_seed, nbSubgraphs), False)
        
    logger.info("Nb no ST subgraph: {}", nbNoST)


def expeSTsubDAGafterComp():
    """Generate graphs and compress them until 
    found a resulting one with ST subgraph.
    """

    # nbBranchs = 4
    # nbChainNodes = 5
    # nbTasks = nbBranchs * nbChainNodes + 2
    nbTasks = 15
    edgeProbability = 0.5
    maxEdgeMem = 1000
    seed = SEED + 9923

    nbIterMax = 1 # 10000
    example = False

    nbTested = 0
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasks, edgeProbability, maxEdgeMem)

    nbNoST = 0
    for it in range(nbIterMax):

        ## random generation
        
        (random_dag,nbIter) = generate_random_dag(nbTasks, edgeProbability, seed)
        seed += nbIter
        # random_dag = generateBranchedSPdag(nbBranchs, nbChainNodes)
        edgeMemSum = generate_random_edge_mem(random_dag, maxEdgeMem, seed+1)
        set_alphabetical_node_names(random_dag)
        set_default_attributes(random_dag)
        set_node_cost(random_dag)
        #set_pbc_tmpMemInc(random_dag)
        generate_random_tmpMemInc(random_dag, maxEdgeMem, seed+2)
        set_cbp_tmpMemInc(random_dag)
        seed += 1

        ## compression

        schedRes = rec_optims_dag_min_mem(random_dag, **{'fallback': heuristic_linear_1dot5steps_impact,
                                                         'sync_separation_end': True,
                                                         'STsubDAG_sequentialization': False,
                                                         'STsubDAG_maxNodes': 0})
        termDAGs = schedRes.dicoRes['listSepTermDAG']
        if len(termDAGs) < 1:
            continue
        unCtermDAGs = []
        for termDAG in termDAGs:
            nbNodesTerm = len(termDAG.nodes())
            if nbNodesTerm == 1:
                continue
            else:
                unCtermDAGs.append(termDAG)
            
        if not unCtermDAGs:
            continue
        nbTested += len(unCtermDAGs)

        ## check STsubDAG

        for termDAG in unCtermDAGs:
            topoSortASAPandALAP(termDAG)
            # subgraphs = [subgraph for subgraph in strictSTsubDAG_generator_deprecated(termDAG, onlyBiggest=True)]
            subgraphs = [subgraph for subgraph in genSTsubDAG_generator(termDAG)]
            nbSubgraphs = len(subgraphs)

            if nbSubgraphs == 0:
                continue
            else:
                example = True
                break
        if example:
            break

    if example:
        offset_seed = seed - SEED - 1
        logger.info("offset_seed {} has: {} ST subgraph (>1 nodes)",
                    offset_seed, nbSubgraphs)
        for num, tupleSTsubDAG in enumerate(subgraphs):
            subgraph = tupleSTsubDAG[1]
            logger.info("nodes in subgraph {}: {}", num,
                        [name for node, name in subgraph.nodes(data='name')])
        writeNXdagToDOT(termDAG, 'Example_STsubAfterComp.dot')
        plotMemDAG(termDAG, 'DAG seed+{} with #STsub={}'.format(offset_seed, nbSubgraphs),
                   binaryColors=False, showNow=True, subset_key=None)
    else:
        logger.info("No ST subgraph after compression until iter: {}", it)
    logger.info("Nb graphs tested for presence of ST subgraphs: {}", nbTested)

    
def expeMemSumCounterExample(orCompactedMemAvg = False, orCompactedMemSrt = False):
    """Generate a few random graphs and try to find a memeroy smaller
    than the one of the peak. Quickly finds one for the sum and average. 
    For the compacted sort (no single metric): no counter-example at all.

    :param orCompactedMemAvg: Instead of memory peak sum,
    test average compacted memory peaks.
    :param orCompactedMemSrt: Instead of memory peak sum,
    test sorted compacted memory peaks.
    """

    if orCompactedMemAvg and orCompactedMemSrt:
        raise ValueError("Both options cannot be True at the same time.")
    
    nbBranchs = 3
    nbChainNodes = 3
    nbTasks = nbBranchs * nbChainNodes + 2
    # nbTasks = 10
    edgeProbability = 0.5
    maxEdgeMem = 10
    seed = SEED

    nbIterMax = 100

    counterExample = False

    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasks, edgeProbability, maxEdgeMem)

    for it in range(nbIterMax):
        
        logger.info("+----------------\nGeneration of DAG:\t{}", it)

        random_dag = generateBranchedSPdag(nbBranchs, nbChainNodes)
        # (random_dag,nbIter) = generate_random_dag(nbTasks,edgeProbability, seed)
        # seed += nbIter
        edgeMemSum = generate_random_edge_mem(random_dag, maxEdgeMem, seed)
        set_alphabetical_node_names(random_dag)
        set_default_attributes(random_dag)
        seed += 1
            
        set_node_cost(random_dag)
        set_cbp_tmpMemInc(random_dag)
        schedRes = brute_force_dag_min_mem(
            random_dag, **{'minMemUpperBound': edgeMemSum + 1, 'faster': False,
                           'computeBestAvg': orCompactedMemAvg,
                           'computeBestSrt': orCompactedMemSrt})
        
        if orCompactedMemAvg and \
           schedRes.dicoRes['memAvg'] != schedRes.dicoRes['minMemAvgAllOrdering']:
            counterExample = True
            break
        if orCompactedMemSrt and \
           schedRes.dicoRes['memSrt'] != schedRes.dicoRes['minMemSrtAllOrdering']:
            counterExample = True
            break        
        if not orCompactedMemSrt and not orCompactedMemAvg and \
           schedRes.dicoRes['memSum'] != schedRes.dicoRes['minMemSumAllOrdering']:
            counterExample = True
            break

        
    if counterExample:
        offset_seed = seed - SEED - 1
        logger.info("Found a counter-example at seed offset: {}", offset_seed)
        if orCompactedMemAvg:
            logger.info("minMemAvgAllOrdering: {}", schedRes.dicoRes['minMemAvgAllOrdering'])
            logger.info("Memory avg: {}", schedRes.dicoRes['memAvg'])
        elif orCompactedMemSrt:
            logger.info("minMemSrtAllOrdering: {}", schedRes.dicoRes['minMemSrtAllOrdering'])
            logger.info("Memory srt: {}", schedRes.dicoRes['memSrt'])
        else:
            logger.info("minMemSumAllOrdering: {}", schedRes.dicoRes['minMemSumAllOrdering'])
            logger.info("Memory sum: {}", schedRes.dicoRes['memSum'])
        logger.info("Memory peak: {}.", schedRes.dicoRes['memPeak'])
        logger.info([random_dag.nodes[x]['name'] for x in schedRes.dicoRes['ordering']])
        writeNXdagToDOT(random_dag, 'Counter_example_Mem_Sum.dot')
        topoSortASAPandALAP(random_dag)
        plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)
    else:
        logger.info("No counter example until iter {}", it)

            
def expeMemComposCounterExample():
    """Generate two random graphs and merge them, then try to minimize
    the memory compositionnaly (with computeBestSrt option) and to
    find a counter-example (should not find one).
    """

    # nbBranchsOut = 3
    # nbChainNodesOut = 3
    # nbTasksOut = nbBranchsOut * nbChainNodesOut + 2
    nbTasksOut = 12
    # nbBranchsIn = 3
    # nbChainNodesIn = 4
    # nbTasksIn = nbBranchsIn * nbChainNodesIn + 2
    nbTasksIn = 8
    edgeProbability = 0.5
    maxEdgeMem = 10
    seed = SEED #+20458

    nbIterMax = 20000

    counterExample = False

    nbSkipped = 0
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasksIn+nbTasksOut, edgeProbability, maxEdgeMem)

    for it in range(nbIterMax):
        
        logger.info("+----------------\nGeneration of DAG:\t{}", it)

        # generate graph Out
        # random_dag_out = generateBranchedSPdag(nbBranchsOut, nbChainNodesOut)
        (random_dag_out,nbIter) = generate_random_dag(nbTasksOut,edgeProbability, seed)
        seed += nbIter
        offset_seed = seed - SEED
        edgeMemSumOut = generate_random_edge_mem(random_dag_out, maxEdgeMem, seed)
        random_dag_out_bis = networkx.DiGraph()
        random_dag_out_bis.add_edges_from(random_dag_out.edges(data=True))
        seed += 1        
        # generate graph In
        # random_dag_in = generateBranchedSPdag(nbBranchsIn, nbChainNodesIn,
        #                                    offsetNewNodes=nbTasksOut)
        (random_dag_in,nbIter) = generate_random_dag(nbTasksIn,edgeProbability, seed,
                                                     offsetNewNodes=nbTasksOut)
        seed += nbIter
        edgeMemSumIn = generate_random_edge_mem(random_dag_in, maxEdgeMem, seed)
        seed += 1
        fixedRandom = random.Random(seed)
        # generate random source and sink to connect graph in and out
        firstNodesOut = [n for n, in_degree in random_dag_out.in_degree if in_degree == 0]
        lastNodesOut = [n for n, out_degree in random_dag_out.out_degree if out_degree == 0]
        sourceId = firstNodesOut[math.floor(fixedRandom.uniform(0,len(firstNodesOut)))]
        sinkId = lastNodesOut[math.floor(fixedRandom.uniform(0,len(lastNodesOut)))]
        # generate random edge mem to connect in and out
        edgeMemSource = round(fixedRandom.uniform(1,maxEdgeMem))
        edgeMemSink = round(fixedRandom.uniform(1,maxEdgeMem))
        firstNodesIn = [n for n, in_degree in random_dag_in.in_degree if in_degree == 0]
        lastNodesIn = [n for n, out_degree in random_dag_in.out_degree if out_degree == 0]
        # connect graph in and out
        newSourceIn = nbTasksOut + nbTasksIn
        newSinkIn = newSourceIn + 1
        random_dag_out.add_edges_from(random_dag_in.edges(data=True))
        random_dag_out_bis.add_edges_from(random_dag_in.edges(data=True))
        for n in firstNodesIn:
            random_dag_out.add_edge(sourceId, n, edgeMem=edgeMemSource)
            random_dag_out_bis.add_edge(sourceId, n, edgeMem=edgeMemSource)
            random_dag_in.add_edge(newSourceIn, n, edgeMem=edgeMemSource)
        for n in lastNodesIn:
            random_dag_out.add_edge(n, sinkId, edgeMem=edgeMemSink)
            random_dag_out_bis.add_edge(n, sinkId, edgeMem=edgeMemSink)        
            random_dag_in.add_edge(n, newSinkIn, edgeMem=edgeMemSink)

        # prepare graph in/out bis for compositional solving
        set_alphabetical_node_names(random_dag_in)
        set_default_attributes(random_dag_in)
        set_node_cost(random_dag_in)
        # set_cbp_tmpMemInc(random_dag_in)
        edgeMemSumIn += edgeMemSource*len(firstNodesIn) + edgeMemSink*len(lastNodesIn)
        edgeMemSumOut += edgeMemSumIn
        generate_random_tmpMemInc(random_dag_in, edgeMemSumIn, seed)
        for node, tmpMemInc in random_dag_in.nodes(data='tmpMemInc'):
            if node == newSourceIn or node == newSinkIn:
                continue
            random_dag_out.nodes[node]['tmpMemInc'] = tmpMemInc
            random_dag_out_bis.nodes[node]['tmpMemInc'] = tmpMemInc
        set_pbc_tmpMemInc(random_dag_in)
        # schedRes_In = brute_force_dag_min_mem(
        # schedRes_In = exhaustive_BandB_V2(
        random_dag_in.remove_node(newSourceIn)
        random_dag_in.remove_node(newSinkIn)
        schedRes_In = exhaustive_BandB_V4epiDominant(
            random_dag_in, **{'minMemUpperBound': 0, #edgeMemSumIn + 1,
                              ## will fail if computeBestAvg
                              ## conjecture: no counter-example if computeBestSrt
                              ## conjecture: counter-example if best peak sum (default)
                              'computeBestSrt': False})
        if schedRes_In.dicoRes.get('nbSamePeak', 2) < 2:
            # there was only one best peak (if counted),
            # so it is (not so obviously) true
            nbSkipped += 1
            continue
        # sample ordering since we have added a new source and sink
        suborder_In = (schedRes_In.dicoRes['ordering'])[1:-1]
        # firstOrder = next(networkx.all_topological_sorts(random_dag_in))
        # suborder_In = firstOrder[1:-1]
        for (u,v) in pairwise(suborder_In):
            if not random_dag_out_bis.has_edge(u, v):
                random_dag_out_bis.add_edge(u, v, edgeMem=0) 
        # now prepare out graphs and solve them completely
        set_alphabetical_node_names(random_dag_out)
        set_default_attributes(random_dag_out, resetTmpMemInc=False)
        set_node_cost(random_dag_out)
        set_pbc_tmpMemInc(random_dag_out)
        schedRes_allInOne = exhaustive_BandB_V2(
            random_dag_out, **{'minMemUpperBound': edgeMemSumOut + 1})
        set_alphabetical_node_names(random_dag_out_bis)
        set_default_attributes(random_dag_out_bis, resetTmpMemInc=False)
        set_node_cost(random_dag_out_bis)
        set_pbc_tmpMemInc(random_dag_out_bis)
        schedRes_compos = exhaustive_BandB_V2(
            random_dag_out_bis, **{'minMemUpperBound': edgeMemSumOut + 1})

        # final check
        if schedRes_allInOne.dicoRes['memPeak'] != schedRes_compos.dicoRes['memPeak']:
            counterExample = True
            break

        
    if counterExample:
        logger.info("Found a counter-example at seed offset: {}", offset_seed)
        logger.info("Selected source: {}", random_dag_out.nodes[sourceId]['name'])
        logger.info("Selected sink: {}", random_dag_out.nodes[sinkId]['name'])
        logger.info("Peak In: {}", schedRes_In.dicoRes['memPeak'])
        _, peakList = mpeak_cbp_schedule(random_dag_in, schedRes_In.dicoRes['ordering'])
        logger.info(peakList)
        logger.info("Peak allInOne: {}", schedRes_allInOne.dicoRes['memPeak'])
        _, peakList = mpeak_cbp_schedule(random_dag_out, schedRes_allInOne.dicoRes['ordering'])
        logger.info(peakList)
        logger.info("Peak compos: {}", schedRes_compos.dicoRes['memPeak'])
        _, peakList = mpeak_cbp_schedule(random_dag_out_bis, schedRes_compos.dicoRes['ordering'])
        logger.info(peakList)
        logger.info([random_dag_in.nodes[x]['name'] for x in schedRes_In.dicoRes['ordering']])
        logger.info([random_dag_out.nodes[x]['name'] for x in schedRes_allInOne.dicoRes['ordering']])
        logger.info([random_dag_out_bis.nodes[x]['name'] for x in schedRes_compos.dicoRes['ordering']])
        remove_transitive_edges_cbp(random_dag_out, transferOnPath=True)
        writeNXdagToDOT(random_dag_in, 'Counter_example_Mem_Compo_in.dot')
        writeNXdagToDOT(random_dag_out, 'Counter_example_Mem_Compo_out.dot')
        writeNXdagToDOT(random_dag_out_bis, 'Counter_example_Mem_Compo_out_bis.dot')
        topoSortASAPandALAP(random_dag_out)
        plotMemDAG(random_dag_out, 'DAG seed+{}'.format(offset_seed), True, subset_key=None)
    else:
        logger.info("No counter example until iter {} (nbSkipped: {})", it, nbSkipped)



def expeCheckBandB():
    """This experience generates random graphs to be solved
    by brute force and by Branch and Bound. If both answers
    different result, the generation stop and the counter-example
    is logged.
    """
    
    nbTasks = 15
    edgeProbability = 0.5
    maxEdgeMem = 10
    seed = SEED # + 757

    nbIterMax = 100

    counterExample = False
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxEdgeMem\t{}",
                nbTasks, edgeProbability, maxEdgeMem)

    totTimeBandB = 0
    totTimeBrute = 0
    
    for it in range(nbIterMax):
        
        logger.info("+----------------\nGeneration of DAG:\t{}", it)

        # generate graph Out
        (random_dag,nbIter) = generate_random_dag(nbTasks, edgeProbability, seed)
        seed += nbIter
        offset_seed = seed - SEED
        edgeMemSum = generate_random_edge_mem(random_dag, maxEdgeMem, seed)
        generate_random_tmpMemInc(random_dag, maxEdgeMem, seed)
        seed += 1        
        # random_dag = generatePosOptimDAG(setNames=False)
        # offset_seed = -1
        # edgeMemSum = sum([edgeMem for _,_,edgeMem in random_dag.edges(data='edgeMem')])
        # edgeMemSum as a peak upper bound may be too low since it does not take into account the peaks
        
        set_default_attributes(random_dag)
        set_alphabetical_node_names(random_dag)
        set_node_cost(random_dag)
        set_cbp_tmpMemInc(random_dag)

        schedRes_BandB = exhaustive_BandB_V2ext(
            random_dag, **{'minMemUpperBound': maxEdgeMem + edgeMemSum + 1,
                           'computeBestSrt': False, 'optimDepth': nbTasks})
        # writeNXdagToDOT(random_dag, 'Counter_example_BandB.dot')
        # topoSortASAPandALAP(random_dag)
        # plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)
        # return

        totTimeBandB += schedRes_BandB.dicoRes['time_search']
        # if schedRes_BandB.dicoRes['ordering'] is None:
        #     # remove_transitive_edges_cbp(random_dag, transferOnPath=True)
        #     writeNXdagToDOT(random_dag, 'Counter_example_BandB.dot')
        #     topoSortASAPandALAP(random_dag)
        #     plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)
        #     return
        # checked_mpeak, _ = mpeak_cbp_schedule(random_dag, schedRes_BandB.dicoRes['ordering'])
        # if checked_mpeak != schedRes_BandB.dicoRes['memPeak']:
        #     raise ValueError("BandB result is wrong.")
        

        schedRes_brute = brute_force_dag_min_mem(
            random_dag, **{'minMemUpperBound': maxEdgeMem + edgeMemSum + 1,
                           ## /!\ mutually exclusive options
                           'computeBestSrt': False, 'faster': True})
        totTimeBrute += schedRes_brute.dicoRes['time_search']

        
        # if schedRes_BandB.dicoRes['memPeak'] != schedRes_brute.dicoRes['memPeak'] or \
        #    schedRes_BandB.dicoRes['nbSamePeak'] != schedRes_brute.dicoRes['nbSamePeak']:
        if schedRes_BandB.dicoRes['memPeak'] != schedRes_brute.dicoRes['memPeak']:
            _, peakList_brute = mpeak_cbp_schedule(random_dag, schedRes_brute.dicoRes['ordering'])
            _, peakList_BandB = mpeak_cbp_schedule(random_dag, schedRes_BandB.dicoRes['ordering'])
            isBandBbetter = is_other_order_better(peakList_brute[:-1], peakList_BandB[:-1])
            # if not isBandBbetter is None:
            counterExample = True
            break

        
    if counterExample:
        logger.info("Found a counter-example at seed offset: {}", offset_seed)
        logger.info("nbSamePeak BandB: {}", schedRes_BandB.dicoRes['nbSamePeak'])
        logger.info("nbSamePeak brute: {}", schedRes_brute.dicoRes['nbSamePeak'])
        logger.info("Peak BandB: {}", schedRes_BandB.dicoRes['memPeak'])
        logger.info(srt_compacted_peaks(peakList_BandB[:-1]))
        logger.info(peakList_BandB)
        logger.info([random_dag.nodes[x]['name'] for x in schedRes_BandB.dicoRes['ordering']])
        logger.info("Peak brute: {}", schedRes_brute.dicoRes['memPeak'])
        logger.info(srt_compacted_peaks(peakList_brute[:-1]))
        logger.info(peakList_brute)
        logger.info([random_dag.nodes[x]['name'] for x in schedRes_brute.dicoRes['ordering']])
        logger.info("Is BandB result better? {}", isBandBbetter)
        remove_transitive_edges_cbp(random_dag, transferOnPath=True)
        writeNXdagToDOT(random_dag, 'Counter_example_BandB.dot')
        topoSortASAPandALAP(random_dag)
        plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)
    else:
        logger.info("No counter example until iter {}", it)
        logger.info("Avg time BandB: {:.6f} (sec)", totTimeBandB / nbIterMax)
        logger.info("Avg time brute: {:.6f} (sec)", totTimeBrute / nbIterMax)


def expeCheckBandB_Vplus(algBandBplus=exhaustive_BandB_V2ext):
    """This experience generates random graphs to be solved
    by Branch and Bound V2 and newer version. If both answers
    different result, the generation stop and the counter-example
    is logged.
    """

    nbTasks = 50
    timeout = 1 # sec
    maxDag = 1000
    edgeProbability = 0.5
    maxMem = 1 + nbTasks
    seed = SEED
    
    counterExample = False
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxMem\t{}",
                nbTasks, edgeProbability, maxMem)

    totTimeBandB_V2 = 0
    totTimeBandB_Vplus = 0

    nbIter = 0
    for random_dag, offset_seed in generator_random_dag(nbTasks, edgeProbability,
                                                        maxMem, maxMem, maxDag, seed):
        nbIter += 1
        edgeMemSum = sum([edgeMem for _,_,edgeMem in random_dag.edges(data='edgeMem')])

        schedRes_BandB_V2 = exhaustive_BandB_V2(
            random_dag, **{'minMemUpperBound': maxMem + edgeMemSum + 1,
                    'computeBestSrt': False, 'timeout': timeout})
        totTimeBandB_V2 += schedRes_BandB_V2.dicoRes['time_search']


        try:
            schedRes_BandB_Vplus = algBandBplus(
                random_dag, **{'minMemUpperBound': maxMem + edgeMemSum + 1, 'optimDepth': 0,
                               'computeBestSrt': False, 'timeout': timeout}) # 'optimDepth': 2

            totTimeBandB_Vplus += schedRes_BandB_Vplus.dicoRes['time_search']
            time_pre = schedRes_BandB_Vplus.dicoRes.get('time_precompute', 0)
            totTimeBandB_Vplus += time_pre
        except Exception as e:
            logger.warning("/!\ Exception during scheduling:\n{}", e)
            schedRes_BandB_Vplus = None
            counterExample = True
        
        if not (schedRes_BandB_Vplus is None) and not (schedRes_BandB_Vplus.dicoRes['ordering'] is None):
            try:
                check_ordering(random_dag, schedRes_BandB_Vplus.dicoRes['ordering'])
            except ValueError:
                logger.warning("/!\ Bad ordering.")
                counterExample = True
        else:
            schedRes_BandB_Vplus = None
        
        if counterExample or \
           schedRes_BandB_Vplus.dicoRes['memPeak'] != schedRes_BandB_V2.dicoRes['memPeak']:
            _, peakList_BandB_V2 = mpeak_cbp_schedule(random_dag, schedRes_BandB_V2.dicoRes['ordering'])
            if not (schedRes_BandB_Vplus is None):
                _, peakList_BandB_Vplus = mpeak_cbp_schedule(random_dag, schedRes_BandB_Vplus.dicoRes['ordering'], check=False)
                isBandB_Vplus_better = is_other_order_better(peakList_BandB_V2[:-1], peakList_BandB_Vplus[:-1])
            # if not isBandB_Vplus_better is None:
            counterExample = True
            break
        
    if counterExample:
        logger.info("Found a counter-example at seed offset: {}", offset_seed)
        logger.info("Peak BandB_V2: {}", schedRes_BandB_V2.dicoRes['memPeak'])
        logger.info(srt_compacted_peaks(peakList_BandB_V2[:-1]))
        logger.info(peakList_BandB_V2)
        logger.info([random_dag.nodes[x]['name'] for x in schedRes_BandB_V2.dicoRes['ordering']])
        if not (schedRes_BandB_Vplus is None):
            logger.info("Peak BandB_Vplus: {}", schedRes_BandB_Vplus.dicoRes['memPeak'])
            logger.info(srt_compacted_peaks(peakList_BandB_Vplus[:-1]))
            logger.info(peakList_BandB_Vplus)
            logger.info([random_dag.nodes[x]['name'] for x in schedRes_BandB_Vplus.dicoRes['ordering']])
            logger.info("Is BandB_Vplus result better? {}", isBandB_Vplus_better)
        else:
            logger.info("No scheduling information on tested BandB.")
        # remove_transitive_edges_cbp(random_dag, transferOnPath=True)
        writeNXdagToDOT(random_dag, 'Counter_example_BandB_Vplus.dot')
        topoSortASAPandALAP(random_dag)
        plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True, subset_key=None)
    else:
        logger.info("No counter example until iter {}", maxDag)
        logger.info("Avg time BandB_Vplus: {:.6f} (sec)", totTimeBandB_Vplus / nbIter)
        logger.info("Avg time BandB_V2: {:.6f} (sec)", totTimeBandB_V2 / nbIter)
        

def expeScheduleSortingBottom():
    """Experiment looking for a counter-example to
    the old schedule sorting relation not being
    a bottom element of the new sorting relation.
    """
    nbTasks = 10
    edgeProbability = 0.5
    maxMetricMem = 10
    seed = SEED

    nbIterMax = 200

    counterExample = False
    
    logger.info("-----------------\nParameters:")
    logger.info("nbTasks\t{}\nedgeProbability\t{}\nmaxMetricMem\t{}",
                nbTasks, edgeProbability, maxMetricMem)

    for it in range(nbIterMax):
        
        logger.info("+----------------\nGeneration of DAG:\t{}", it)

        # generate graph
        (random_dag,nbIter) = generate_random_dag(nbTasks, edgeProbability, seed)
        seed += nbIter
        offset_seed = seed - SEED
        generate_random_node_metric(random_dag, -maxMetricMem, maxMetricMem, seed, 'nodeMemCost')
        generate_random_tmpMemInc(random_dag, maxMetricMem, seed)
        set_cbp_tmpMemInc(random_dag)
        seed += 1

        # # adds single source and single sink
        # # following code is not necessary if V4epiDominant
        # # if commented, no need for [1:-1] ordering filter
        # firstNodes = [n for n, in_degree in random_dag.in_degree if in_degree == 0]
        # lastNodes = [n for n, out_degree in random_dag.out_degree if out_degree == 0]
        # sourceId = nbTasks
        # sinkId = sourceId + 1
        # for n in firstNodes:
        #     random_dag.add_edge(sourceId, n)
        # for n in lastNodes:
        #     random_dag.add_edge(n, sinkId)
        # random_dag.nodes[sourceId]['nodeMemCost'] = maxMetricMem
        # random_dag.nodes[sinkId]['nodeMemCost'] = -maxMetricMem
        # random_dag.nodes[sourceId]['tmpMemInc'] = maxMetricMem
        # random_dag.nodes[sinkId]['tmpMemInc'] = 0
        
        # find best schedule according to previous relation

        #schedRes = brute_force_dag_min_mem(
        #schedRes = exhaustive_BandB_V2ext(
        schedRes = exhaustive_BandB_V4epiDominant(
            random_dag, **{'minMemUpperBound': 0, 'computeBestSrt': True})
        bestSched = schedRes.dicoRes['ordering']

        dagBest, clstList = getClusteredSchedule(random_dag, bestSched) #[1:-1])
        peakSet = SortedSet()
        dropSet = SortedSet()
        negList, posList, peakList = separateSchedule(dagBest, clstList, peakSet, dropSet)
        # check that it is a bottom for the new relation
        for ordering in networkx.all_topological_sorts(random_dag):
            res = is_betterEq_clustered_than(random_dag, ordering, #[1:-1],
                                             peakSet, dropSet,
                                             dagBest, negList, posList, peakList)
            if res is None or res > 0:
                counterExample = True
                break
        # set_alphabetical_node_names(random_dag)
        # topoSortASAPandALAP(random_dag)
        # plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)

        if counterExample:
            break
            
    if counterExample:
        logger.info("Found a counter-example at seed offset: {}", offset_seed)
        set_alphabetical_node_names(random_dag)
        logger.info("Comparison result (None if not comparable, +1 if better): {}", res)
        logger.info("Better or incomparable schedule:")
        logger.info([random_dag.nodes[x]['name'] for x in ordering])
        logger.info("Best schedule according to old sort:")
        logger.info([random_dag.nodes[x]['name'] for x in bestSched])
        for _, _, dicoEdge in random_dag.edges(data=True):
            dicoEdge['edgeMem'] = 0
        writeNXdagToDOT(random_dag, 'Counter_example_Best_Schedule_Bottom.dot')
        topoSortASAPandALAP(random_dag)
        plotMemDAG(random_dag, 'DAG seed+{}'.format(offset_seed), True)
    else:
        logger.info("No counter example until iter {}", nbIterMax)


def expeWorthComposition():
    """Experiment solving a synthetic dag (made of
    multiple irreducible ones put in parallel)
    thanks to composition. Composition should
    show a benefit in solving time: first is shorter and optimal.
    """

    # irreducible and 123 nodes, timesout ?
    # dag = loadJSONtoNX('irreduciblePEGs/red_qmf23_5d_PEG3200_optRep0_sym0_cluPeak0_topTieBrk0_term0.json')
    # irreducible and 53 nodes, heuristic is best, can be solved by forbSuccN
    # dag = loadJSONtoNX('irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym1_cluPeak0_topTieBrk0_term0.json')
    # modified copy of 53 nodes, already having 2 ST subcomponents
    # dag2 = loadJSONtoNX('irreduciblePEGs/test53.json')
    # irreducible and 32 nodes, heuristic is best, can be solved by forbSuccN, but V2 needs 1sec
    dag = loadJSONtoNX('irreduciblePEGs/red_qmf23_5d_PEG1600_optRep1_sym0_cluPeak0_topTieBrk0_term0.json')
    # modified copy of the 32 nodes version, seems to be best combo if twiced (direct BandB is too long)
    dag2 = loadJSONtoNX('irreduciblePEGs/test32.json')
    # irreducible and 15 nodes 
    # dag = loadJSONtoNX('irreduciblePEGs/red_qmf235_5d_PEG1600_optRep0_sym0_cluPeak0_topTieBrk0_term0.json')
    # irreducible and 7 nodes
    # dag = loadJSONtoNX('irreduciblePEGs/red_qmf235_5d_PEG800_optRep0_sym0_cluPeak0_topTieBrk0_term0.json')
    # set_default_attributes(dag, resetTmpMemInc=False)
    # pdag = gen_parallel_dag(dag, 5, cplxVersion=False)
    pdag = gen_parallel_dagL([dag, dag2, dag, dag2], cplxVersion=True)
    set_default_attributes(pdag, resetTmpMemInc=False)
    plotDAG(pdag, name="DAG", binaryColors=True, showNow=True, subset_key=None, metric_key='nodeMemCost')
    schedRes = rec_optims_dag_min_mem(pdag, **{'STsubDAG_sequentialization': True, 'optimDepth':2,
                                               'minMemHeuristic': two_heurs_solver})
    logger.info(schedRes)
    pdag = gen_parallel_dagL([dag, dag2, dag, dag2], cplxVersion=True)
    set_default_attributes(pdag, resetTmpMemInc=False)
    # dag_dual = reverse_sched_graph(pdag)
    schedRes = exhaustive_BandB_V3epiDual(pdag, **{'timeout': 600, 'optimDepth':2,
                                                   'minMemHeuristic': two_heurs_solver})
    logger.info(schedRes)
    sched = schedRes.dicoRes['ordering']
    _, peaks = mpeak_cbp_schedule(pdag, sched)
    plotMemSched(pdag, sched, peaks, showNow=True)

        
        
## cannot work because of relative import inside module
## https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
if __name__ == '__main__':
    expeXXX()


