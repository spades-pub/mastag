#! /usr/bin/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import time
import math
import itertools
import networkx
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

import bitarray # 2.9.2
import huffman # 0.1.2
import collections

import signal
from contextlib import contextmanager

import bracelogger
logger = bracelogger.get_logger("dagsched_log")


# math functions
################

def argmax(elList, strictEq):
    """Return index of maximum element in list.

    :param elList: List of numbers.
    :param strictEq: True if returning the first occurence, False otherwise.
    :returns: Positive argmax index, 0 if empty list.
    """
    offset = 0 + strictEq
    maxValue = offset + sum((el for el in elList if el < 0)) - 1
    maxIdx = 0
    idx = 0
    for x in elList:
        if x >= maxValue:
            maxValue = x + offset
            maxIdx = idx
        idx += 1
    return maxIdx

        
def argmin(elList, strictEq):
    """Return index of minimum element in list.

    :param elList: List of numbers.
    :param strictEq: True if returning the first occurence, False otherwise.
    :returns: Positive argmin index, 0 if empty list.
    """
    offset = 0 - strictEq
    minValue = offset + sum((el for el in elList if el > 0)) + 1
    minIdx = 0
    idx = 0
    for x in elList:
        if x <= minValue:
            minValue = x + offset
            minIdx = idx
        idx += 1
    return minIdx


def removeSuccessiveDuplicate(elList):
    """Return new list without successive
    duplicates. Example:
    [0, 0, 0, 1, 1, 0] => [0, 1, 0]

    :param elList: List of comparable elements.
    :returns: New list of elements.
    """
    newList = []
    if not elList:
        return newList
    newList.append(elList[0])
    for el in elList[1:]:
        if el != newList[-1]:
            newList.append(el)
    return newList
    

## not available in python 3.8, later provided directly in math
def lcm(a, b):
    gcd = math.gcd(a, b)
    return (a // gcd) * b


## not available in python 3.8, later provided directly in itertools
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


# formatting functions
######################

def strNodesWNames(dag, nodeList):
    """Node list formatter, with node names.

    :param dag: DAG containing the nodes.
    :param nodeList: List of nodes to convert (in order).
    :returns: String representing the nodes (by their names).
    """
    bufString = []
    for x in nodeList:
        bufString.append(dag.nodes[x]['name'])
    return ', '.join(bufString)
        

def strNodesWPI(dag, nodeList):
    """Node list formatter, with node attributes.

    :param dag: DAG containing the nodes.
    :param nodeList: List of nodes to convert (in order).
    :returns: String representing the nodes id
    with their local peak and impact: 'id (peak/impact), ...'
    """
    bufString = []
    for x in nodeList:
        dicoNode = dag.nodes[x]
        strNode = "{} ({}/{})".format(x, dicoNode['tmpMemInc'],
                                      dicoNode['nodeMemCost'])
        bufString.append(strNode)
    return ', '.join(bufString)
        


# code from https://stackoverflow.com/questions/5478351/python-time-measure-function
def timing_logging(f):
    """Create a function wrapper to measure its execution time.
    /!\ Should not be called as an annotation, otherwise it breaks the function documentation.

    :param f: The function to wrap.
    :returns: A wrapper that will log (info) execution time each time it is called.
    """
    def wrap(*args, **kwargs):
        time1 = time.perf_counter()
        ret = f(*args, **kwargs)
        time2 = time.perf_counter()
        logger.info('==> {:s} function took {:.0f} ms',
                    f.__name__, (time2-time1)*1000)
        return ret
    return wrap



# timeout functions
###################

class Timeout:
# Timeout class adapted from:
# https://www.jujens.eu/posts/en/2018/Jun/02/python-timeout-function/
# https://stackoverflow.com/questions/492519/timeout-on-a-function-call/494273#494273
# Note that two timeout guards cannot exist concurrently because
# it will only reset the timedout signal and not create a new one.
    
    def __init__(self):
        self.timedout = False
        self.start = None
        self.stop = None
        self.tottime = 0

    def get_elapsed_time(self):
        """Return elapsed time from last start
        to last stop if any, or now otherwise.

        :returns: (float) Time in second.
        """
        if not self.stop is None:
            return (self.stop - self.start)
        else:
            return (time.perf_counter() - self.start)

    def start_time(self, timeout):
        """Set the signal timeout.
        Must be called in the context of a timeout_guard() call.

        :param timeout: (int) In second. Do nothing if 0.
        """
        # Register a function to raise a TimeoutError on the signal.
        signal.signal(signal.SIGALRM, __raise_timeout__)
        # Schedule the signal to be sent after ``timeout``.
        signal.alarm(timeout)
        # start time measure
        self.start = time.perf_counter()
        # update total time since previous stop if any
        if not self.stop is None:
            self.tottime += self.start - self.stop
        
    def stop_time(self):
        """Stops time and unregister any alarm signal.
        Must be called in the context of a timeout_guard() call.
        """
        # Unregister the signal so it won't be triggered
        # if the timeout is not reached.
        signal.signal(signal.SIGALRM, signal.SIG_IGN)
        # stop time measure
        self.stop = time.perf_counter()
        # update total time
        self.tottime += self.stop - self.start
        
    @contextmanager
    def timeout_guard(self, timeout):
        """To be used in a 'with' statement to timeout the inner statements.
        /!\ The signal seems to fail when triggered in the middle of a logging call.
        
        :param timeout: (int) In second. Do nothing if 0 or less.
        """
        self.start_time(timeout)        
        try:
            yield
        except TimeoutError:
            self.timedout = True
            pass
        finally:
            self.stop_time()

def __raise_timeout__(signum, frame):
    raise TimeoutError


# plot functions
################

def plotMemSched(dag, sched, peaks, showNow=True):
    """Plot graph of memory usage.

    :param dag: DAG of scheduled nodes, with 'name' attributes.
    :param sched: Schedule of nodes.
    :param peaks: Peaks of memory (two per node).
    :param showNow: Whether or not to display the figure now or later.
    """
    nbPeaks = len(peaks)
    if 2*len(sched) != nbPeaks:
        raise Exception("Wrong input, there must be two peaks per node.")
    nodeNames = [dag.nodes[node].get('name',str(node)) for node in sched]
    ## plot
    plt.plot(range(nbPeaks), peaks, label="Memory usage", marker='.')
    plt.xticks(range(0,nbPeaks,2), nodeNames)
    plt.xlabel('node')
    plt.ylabel('memory')
    plt.grid(True)
    plt.legend()
    if showNow:
        plt.show()
        #plt.clf()
    else:
        plt.figure()


def getNodeColorIntensities(dag, binaryColors=False, metric_key='nodeMemCost'):
    """Create a list of node color intensities based on
    their attribute 'nodeMemCost'.

    :param dag: DAG to consider.
    :param binaryColors: If True, will ''binarize'' the colors, as positive, 0 or negative.
    :returns: List of color intensities (integer values) in order of nodes in the DAG.
    """
    if metric_key is None:
        return [0 for node in dag.nodes()]
    node_color_intensities = []
    if (binaryColors):
        for _, metric in dag.nodes(data=metric_key):
            if metric > 0:
                node_color_intensities.append(1)
            elif metric < 0:
                node_color_intensities.append(-1)
            else:
                node_color_intensities.append(0)
    else:
        node_color_intensities = [metric for _, metric in dag.nodes(data=metric_key)]
    return node_color_intensities


def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    '''
    https://stackoverflow.com/a/18926541
    '''
    if isinstance(cmap, str):
        cmap = plt.get_cmap(cmap)
    new_cmap = mpl.colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap


def plotDAG(dag, name="DAG", binaryColors=False, showNow=True, subset_key=None, metric_key=None):
    """Plot graph (with color map if metric_key is provided).

    :param dag: DAG with edge attribute 'asapTopoRank' and node attribute 'nodeMemCost'.
    :param name: Name of the window displaying the two graphs.
    :param showNow: Whether or not to display the figure now or later.
    :param subset_key: (default None) Integer node key used to order them.
    For example, 'asapTopoRank' if the DAG has previously been topologically sorted.
    If None, then uses the graphical representation of Graphviz/dot.
    :param metric_key: (default None) Integer node key used to color them.
    For example, 'nodeMemCost' for the impact. If None, all nodes have the same color.
    """
    # https://stackoverflow.com/questions/70110954/plotting-multiple-plots-and-text-with-networkx-and-matplotlib
    if subset_key:
        node_layout_full = networkx.multipartite_layout(dag, subset_key=subset_key)
    else:
        node_layout_raw = networkx.nx_agraph.graphviz_layout(dag, prog='dot', root=None)
        node_layout_full = {node: (-y,x) for (node, (x,y)) in node_layout_raw.items()}
    fig, axs = plt.subplots(nrows=1, figsize=(10,7), num=name)
    node_color_intensities = getNodeColorIntensities(dag, binaryColors, metric_key)
    if binaryColors:
        color_map = truncate_colormap('nipy_spectral', minval=0.5, maxval=0.85, n=3) # 'turbo'
    else:
        color_map = plt.cm.viridis ## color maps: PiYG, PRGn, viridis, reversed with _r
    networkx.draw(dag, pos=node_layout_full, node_color=node_color_intensities, cmap=color_map, ax=axs)
    nodeNames = {x:dico.get('name',str(x)) for x,dico in dag.nodes(data=True)}
    networkx.draw_networkx_labels(dag, pos=node_layout_full, labels=nodeNames, ax=axs)
    if metric_key:
        norm = plt.Normalize(vmax=max(node_color_intensities), vmin=min(node_color_intensities))
        sm = plt.cm.ScalarMappable(cmap=color_map, norm=norm)
        color_bar = fig.colorbar(sm, orientation = 'horizontal', label=metric_key, shrink=0.6, ticks=MaxNLocator(integer=True))
        # https://stackoverflow.com/questions/16826754/change-the-labels-of-a-colorbar-from-increasing-to-decreasing-values
        color_bar.ax.invert_xaxis()
    if showNow:
        plt.show()
        #plt.clf()
    else:
        plt.figure()
    

def plotMemDAG(dag, name="DAG", binaryColors=False, showNow=True, subset_key='asapTopoRank'):
    """Plot graph (with color map for node memory cost).
    /!\ DAG must have been topologically sorted first.

    :param dag: DAG with edge attribute 'asapTopoRank' and node attribute 'nodeMemCost'.
    :param name: Name of the window displaying the two graphs.
    :param showNow: Whether or not to display the figure now or later.
    :param subset_key: (default 'asapTopoRank') Integer node key used to order them.
    """
    plotDAG(dag, name, binaryColors, showNow, subset_key, 'nodeMemCost')
    

# MISC functions
################

## experimental storing of schedule
def writeListToFile(hList, outFile):
    """Function using Huffman bit coding
    to write collection of hashable elements
    into a binary file. Encoding is not
    included in the tile (instead use:
    https://github.com/yannickperrenet/huffman-coding)
    

    :param hList: List of anything,
    in practice SDF node schedules.
    :param outFile: File to write the 
    compressed data representing the list.

    """
    # get huffman encoding first
    counts = collections.Counter(hList)
    hcodec = huffman.codebook(counts.items())
    strCode = ''.join([hcodec[h] for h in hList])
    # then write to file
    a = bitarray.bitarray(strCode)
    with open(outFile, 'wb') as f:
        a.tofile(f)


