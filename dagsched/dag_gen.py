#! /usr/bin/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from .alphanumericBaseConversion import *
from .dag_transform import *

from lxml import etree
import os
import re
import yaml
import math
import random
import networkx
import simplejson as json
from subprocess import Popen, PIPE
from collections import deque, OrderedDict
# from random import randint, uniform, gauss, normalvariate, seed


import bracelogger
logger = bracelogger.get_logger("dagsched_log")

PATH_TAGADA = "/home/ahonorat/Documents/logiciels/sources/tagada_mial"
## https://www-verimag.imag.fr/~raymond/home/tools/tagada/
## https://gricad-gitlab.univ-grenoble-alpes.fr/prdistrib/tagada

SEED = 17831


def generate_random_edge_mem(graph, maxEdgeMem, seed):
    """Generate uniform random weights on edges, representing their memory cost.

    :param graph: Graph where to set edge attribute 'edgeMem'.
    :param maxEdgeMem: Maximum memory value (uniform distribution between 1 and this value).
    :param seed: Seed of randomness.
    :returns: The sum of edge attribute 'edgeMem'.
    """
    fixedRandom = random.Random(seed)
    edgeMemSum = 0
    for e in graph.edges():
        edgeMem = round(fixedRandom.uniform(1,maxEdgeMem))
        graph[e[0]][e[1]]['edgeMem'] = edgeMem
        edgeMemSum += edgeMem
    return edgeMemSum


def generate_random_node_metric(graph, minValue, maxValue, seed, metricName):
    """Generate uniform random metric on nodes.

    :param graph: Graph where to set node attribute 'metricName'
    (uniform distribution between minValue and maxValue).
    :param minValue: Minimum value.
    :param maxValue: Maximum value.
    :param seed: Seed of randomness.
    """
    fixedRandom = random.Random(seed)
    for node, dicoNode in graph.nodes(data=True):
        randMetric = round(fixedRandom.uniform(minValue,maxValue))
        dicoNode[metricName] = randMetric

        
def generate_random_tmpMemInc(graph, maxMemInc, seed):
    """Generate uniform random peaks on nodes, representing their memory cost.

    :param graph: Graph where to set node attribute 'tmpMemInc'.
    :param maxMemInc: Maximum memory peak value (uniform distribution between 1 and this value).
    :param seed: Seed of randomness.
    """
    generate_random_node_metric(graph, 1, maxMemInc, seed, 'tmpMemInc')
        

def generate_random_dag_aux(n, p, seed, nbIter, nbIterMax, offsetNewNodes):
    """Generate a random DAG iteratively until it is connected.
    /!\ This method should not be called directly.

    :param n: Number of nodes.
    :param p: Edge probabilty.
    :param seed: Seed for randomness.
    :param nbIter: Number of iterations so far (until connectivity).
    :param nbIterMax: Maximum number of attemps to get a connected graph.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :returns: A connected DAG and the number of iterations to find it.
    """
    if nbIter >= nbIterMax:
        logger.warning("Could not find a connected graph after {} attempts.", nbIterMax)
        return (networkx.DiGraph(), nbIterMax)
    # code from https://gist.github.com/flekschas/0ea70dec4d92bc706e61
    # generate an undirected graph with Erdos method
    # then retain only edges in topological order, so that it is a DAG
    # then retry if the graph is not connected
    random_graph = networkx.fast_gnp_random_graph(n, p, seed=seed+nbIter, directed=False)
    ## commented code is slower
    # random_dag = networkx.DiGraph(
    #     [
    #         (u, v) for (u, v) in random_graph.edges() if u < v
    #     ]
    # ) 
    random_dag = networkx.DiGraph()
    for (u, v) in random_graph.edges():
        if u < v:
            random_dag.add_edge(u+offsetNewNodes,v+offsetNewNodes)
    ## this test takes a lot of time
    if len(random_dag.nodes()) == n and \
       len(random_dag.edges()) >= n - 1 and \
       networkx.is_weakly_connected(random_dag):
    ## maybe research of longest path is faster? seems not
    # if networkx.dag_longest_path_length(random_dag) < n:    
        logger.debug("Succeed to generate graph after {} attempts.",nbIter+1)
        return (random_dag, nbIter)
    else:
        return generate_random_dag_aux(n, p, seed, nbIter+1, nbIterMax, offsetNewNodes)


def generate_random_dag(n, p, seed, nbIterMax=100, offsetNewNodes=0):
    """Generate a random DAG iteratively until it is connected.

    :param n: Number of nodes.
    :param p: Edge probabilty.
    :param seed: Seed for randomness.
    :param nbIterMax: Maximum number of attemps to get a connected graph.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :returns: A connected DAG and the number of iterations to find it.
    """
    return generate_random_dag_aux(n, p, seed, 0, nbIterMax, offsetNewNodes)


def generator_random_dag(nbNodes, probaEdges, maxEdgeMem, maxNodeMem, nbDAG, seed, offsetNewNodes=0):
    """Generate multiple random DAG in the cbp model.
    /!\ Genrated DAG may be empty if original graph was not connected.

    :param nbNodes: Size of the generated graphs.
    :param probaEdges: Erdos probability for edges.
    :param maxEdgeMem: Maximum memory set per edge.
    :param maxNodeMem: Maximum memory set per node.
    :param nbDAG: Maximum number of DAG to generate (may be lower).
    :param seed: Starting seed for randomness.
    :param offsetNewNodes: Offset for new nodes index.
    :returns: Tuple of (dad, seedOffset).
    """
    currentSeed = seed
    for i in range(0, nbDAG):
        (dag, seedOffset) = generate_random_dag(
            nbNodes, probaEdges, currentSeed, offsetNewNodes=offsetNewNodes)
        if len(dag.nodes()) == 0:
            continue
        currentSeed += seedOffset
        generate_random_edge_mem(dag, maxEdgeMem, currentSeed)
        generate_random_tmpMemInc(dag, maxNodeMem, currentSeed)
        set_cbp_common_attr(dag)
        seedOffset = currentSeed - seed
        yield (dag, seedOffset)
        currentSeed += 1


################################################################
# Generation of graphs with particular topology
################################################################
        

def generate_random_tagada(n, pratio, seed, offsetNewNodes, fj=False):
    """Generate a random dag using the tagada tool.

    :param n: Number of nodes.
    :param pratio: Parallel to sequence ratio (from 0 to 100).
    :param seed: Seed for randomness.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :param fj: Enforce fork/join style (SP-DAG?).
    :returns: A connected dag.
    """
    # launch tagada
    commandPath = PATH_TAGADA + "/_build/default/main-tagada/tagada.exe"
    if not os.path.isfile(commandPath):
        raise RuntimeError("tagada binary couldn't be found at:\nPATH CMD: {}".
                           format(commandPath))
    
    ## uses light mial custom output
    outType = "mial"
    cmdTokens = [commandPath,
                 "-seed", str(seed),
                 "-t", outType,
                 "-s", str(n),
                 "-pratio", str(pratio)]
    if fj:
        cmdTokens.append("-fj")
    cmdTokens.append("tmp")
    processGenDag = Popen(cmdTokens, stdout=PIPE)
    (output, err) = processGenDag.communicate()
    exit_code = processGenDag.wait()
    if exit_code:
        raise RuntimeError("tagada tool returned error: {}\nPATH CMD: {}".
                           format(exit_code, commandPath))
    # parses tagada output
    tagada_dag = networkx.DiGraph()
    regexTaskName = "t(\d+)"

    with open("tmp.yaml", "r") as stream:
        dicoMia = yaml.safe_load(stream)
        for taskName, dicoNodes in dicoMia['nodes'].items():
            matchTaskIdx = re.match(regexTaskName, taskName)
            taskIdx = offsetNewNodes + int(matchTaskIdx.group(1))
            for succL in dicoNodes['accessTo']:
                succName = succL[0]
                matchSuccIdx = re.match(regexTaskName, succName)
                succIdx = offsetNewNodes + int(matchSuccIdx.group(1))
                tagada_dag.add_edge(taskIdx, succIdx)

    # clean and return
    if not exit_code:
        os.remove("tmp.yaml")
    return tagada_dag


def generator_tagada_dag(n, pratio, fj, maxEdgeMem, maxNodeMem, nbDAG, seed, offsetNewNodes=0):
    """Generate multiple branched DAG in the cbp model.

    :param n: Number of nodes.
    :param pratio: Parallel to sequence ratio (from 0 to 100).
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :param fj: Enforce fork/join style (SP-DAG?).
    :param maxEdgeMem: Maximum memory set per edge.
    :param maxNodeMem: Maximum memory set per node.
    :param nbDAG: Maximum number of DAG to generate (may be lower).
    :param seed: Starting seed for randomness.
    :param offsetNewNodes: Offset for new nodes index.
    :returns: Tuple of (dad, seedOffset).
    """
    currentSeed = seed
    for i in range(0, nbDAG):
        dag = generate_random_tagada(n, pratio, currentSeed, offsetNewNodes, fj)
        generate_random_edge_mem(dag, maxEdgeMem, currentSeed)
        generate_random_tmpMemInc(dag, maxNodeMem, currentSeed)
        set_cbp_common_attr(dag)
        yield (dag, i)
        currentSeed += 1


def generate_random_tree(n, seed, offsetNewNodes=0):
    """Generate a random DAG being a tree.

    :param n: Number of nodes.
    :param seed: Seed for randomness.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :returns: A DAG being a tree.
    """
    undir_tree = networkx.random_tree(n, seed=seed)
    dir_tree = networkx.DiGraph()
    visited = set()
    queue = [0]
    while queue:
        node = queue.pop()
        visited.add(node)
        for child in undir_tree.neighbors(node):
            if not (child in visited):
                dir_tree.add_edge(node+offsetNewNodes, child+offsetNewNodes)
                queue.append(child)
    return dir_tree


def generateBranchedTree(nbBranchs=3, nbChainNodes=2, offsetNewNodes=0):
    """Generate a tree of multiple parallel branches.

    :param nbBranchs: Number of branchs in the generated graphs
    (all coming from a single source and going to a single sink).
    :param nbChainNodes: Number of nodes forming a mere chain in the middle of each branch.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :returns: A DAG without any attribute set. Source node has the
    highest node identifier.
    """
    dag = networkx.DiGraph()
    if nbBranchs < 2 or nbChainNodes < 0:
        raise ValueError("Number of branches must be at least two.")
    nbNodesAllBranches = nbChainNodes * nbBranchs
    # first we create the chains
    for rank in range(0, nbChainNodes-1):
        idxNodeOffset = nbBranchs * rank
        for idxBr in range(0, nbBranchs):
            dag.add_edge(offsetNewNodes + idxNodeOffset + idxBr,
                         offsetNewNodes + idxNodeOffset + nbBranchs + idxBr)
    # and connect them to source
    for idxBr in range(0, nbBranchs):
        dag.add_edge(offsetNewNodes + nbNodesAllBranches,
                     offsetNewNodes + idxBr)
    return dag

    

def generateBranchedSPdag(nbBranchs=3, nbChainNodes=2, offsetNewNodes=0):
    """Generate an SP DAG of multiple parallel branches.

    :param nbBranchs: Number of branchs in the generated graphs
    (all coming from a single source and going to a single sink).
    :param nbChainNodes: Number of nodes forming a mere chain in the middle of each branch.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :returns: A DAG without any attribute set. Source and sink nodes
    have the highest node identifier (+1 for the sink).
    """
    dag = generateBranchedTree(nbBranchs, nbChainNodes, offsetNewNodes)
    nbNodesAllBranches = nbChainNodes * nbBranchs
    # connect branches to sink
    for idxBr in range(0, nbBranchs):
        dag.add_edge(offsetNewNodes + nbNodesAllBranches - idxBr - 1,
                     offsetNewNodes + nbNodesAllBranches + 1)
    return dag


def nbLEbranchesSPdag(nbBranchs=3, nbChainNodes=2):
    """Compute #LE of multiple parallel branches.

    :param nbBranchs: Number of branches in parallel.
    :param nbChainNodes: Number of nodes in every branch.
    :returns: #LE of the graph with multiple parallel branches.
    """
    numerator = math.factorial(nbBranchs*nbChainNodes)
    denominator = math.factorial(nbChainNodes)**nbBranchs
    return numerator//denominator


def generateChainedSPdag(nbChainedDAG, nbBranchsInDAG=2, nbNodesInBranch=2):
    """Generate a SP DAG being the chain of the same repeated
    simple SP DAG, as generated per the function generateBranchedSPdag.

    :param nbChainedDAG: Number of repetition of the SP DAG.
    :param nbBranchsInDAG: Number of branch per SP DAG.
    :param nbNodesInBranch: Number of nodes per SP DAG branch.
    :returns: A chain of SP DAG, itself being a SP DAG.
    """
    dag = generateBranchedSPdag(nbBranchsInDAG, nbNodesInBranch, offsetNewNodes=0)
    nbNodesDAG = len(dag.nodes())
    nbNodesTot = nbNodesDAG
    idxPrevSrc = nbNodesTot - 2
    for i in range(0, nbChainedDAG - 1):
        subdag = generateBranchedSPdag(nbBranchsInDAG, nbNodesInBranch, nbNodesTot)
        dag.add_edges_from(subdag.edges(data=False))
        nbNodesTot += nbNodesDAG
        dag.add_edge(nbNodesTot-1, idxPrevSrc)
        idxPrevSrc += nbNodesDAG
    return dag


def generateSeparableDAG(nbBranchs=3, nbChainNodes=2, offsetNewNodes=0,
                         noSTdag=False, cplxVersion=False):
    """Generate graph that if split in two, forms two rooted trees.

    :param nbBranchs: Number of branchs in the generated graphs
    (all coming from a single source and going to a single sink).
    :param nbChainNodes: Number of nodes forming a mere chain in the middle of each branch.
    :param offsetNewNodes: Integer offset for nodes identifiers.
    :param noSTdag: If True, removes source and sink nodes.
    :param cplxVersion: Makes the graph way more difficult
    to compress by removing a few edges (not clear how/why).
    :returns: A DAG (not SP!) without any attribute set.
    """
    nbNodesInBranch = (nbBranchs-1)*2 + nbChainNodes
    dag = generateBranchedSPdag(nbBranchs, nbNodesInBranch, offsetNewNodes)
    if noSTdag:
        offsetNodes = nbBranchs*nbNodesInBranch
        dag.remove_node(offsetNewNodes+offsetNodes)
        dag.remove_node(offsetNewNodes+offsetNodes+1)

    # makes it complicated
    if cplxVersion:
        # dag.remove_node(offsetNewNodes)
        # dag.remove_node(offsetNewNodes+offsetNodes-1)
        for idxBr in range(0, nbBranchs-1):
            idxNodeOffset = (nbBranchs + 1) * idxBr
            dag.remove_node(offsetNewNodes+idxBr)
        
    # now we inter-connect the chains to create branchs
    for rankSrc in range(0, nbBranchs-1):
        idxNodeOffsetSrc = nbBranchs * rankSrc
        for idxBrSrc in range(0, nbBranchs):
            idxBrDst = (idxBrSrc + (nbBranchs - 1 - rankSrc)) % nbBranchs
            rankDst = nbNodesInBranch - 1 - rankSrc
            idxNodeOffsetDst = nbBranchs * rankDst
            dag.add_edge(offsetNewNodes + idxNodeOffsetSrc + idxBrSrc,
                         offsetNewNodes + idxNodeOffsetDst + idxBrDst)
    return dag


def generateFreeSeparableDAG(nbBranchs, nbChainNodes, offsetNewNodes):
    """Proxy to generateSeparableDAG(nbBranchs, nbChainNodes, offsetNewNodes, noSTdag=True, cplxVersion=True)
    """
    return generateSeparableDAG(nbBranchs, nbChainNodes, offsetNewNodes, noSTdag=True, cplxVersion=True)


def generator_branched_dag(nbBranchs, nbChainNodes, branchedDagFunction, maxEdgeMem, maxNodeMem, nbDAG, seed, offsetNewNodes=0):
    """Generate multiple branched DAG in the cbp model.

    :param nbBranchs: Number of branchs in the generated graphs
    (all coming from a single source and going to a single sink).
    :param nbChainNodes: Number of nodes forming a mere chain in the middle of each branch.
    :param branchedDagFunction: Function to generate single branched dag.
    :param maxEdgeMem: Maximum memory set per edge.
    :param maxNodeMem: Maximum memory set per node.
    :param nbDAG: Maximum number of DAG to generate (may be lower).
    :param seed: Starting seed for randomness.
    :param offsetNewNodes: Offset for new nodes index.
    :returns: Tuple of (dad, seedOffset).
    """
    currentSeed = seed
    for i in range(0, nbDAG):
        dag = branchedDagFunction(nbBranchs, nbChainNodes, offsetNewNodes)
        generate_random_edge_mem(dag, maxEdgeMem, currentSeed)
        generate_random_tmpMemInc(dag, maxNodeMem, currentSeed)
        set_cbp_common_attr(dag)
        yield (dag, i)
        currentSeed += 1


def generateTreesOfSeperableDAG(separableDAG, nbBranchs=3, nbChainNodes=2):
    """Splits a SeparableDAG into two trees where the cut is of minimal memory size.

    :param separableDAG: SeparableDAG to split in two trees (NOT in-place),
    with edge attribute 'edgeMem' set.
    :param nbBranchs: Number of branchs in the SeparableDAG
    :param nbChainNodes: Number of chain nodes in the SeperableDAG.
    :returns: Tuple with two trees and the minimum cut size between them.
    """
    nbNodesInBranch = (nbBranchs-1)*2 + nbChainNodes
    nbNodesAllBranches = nbNodesInBranch * nbBranchs
    nbNodesTot = len(separableDAG.nodes())
    if nbNodesTot != (nbNodesAllBranches + 2):
        raise ValueError("SeparableDAG has not been generated with the corresponding function.")
    dag = networkx.DiGraph()
    dag.add_nodes_from(separableDAG.nodes(data=True))
    dag.add_edges_from((u, v, separableDAG.edges[u, v]) for u, v in separableDAG.edges)
    # inter-connect are direct edges and not chains, so we cut them directly
    memoryCutSize = 0
    for rankSrc in range(0, nbBranchs-1):
        idxNodeOffsetSrc = nbBranchs * rankSrc
        for idxBrSrc in range(0, nbBranchs):
            idxBrDst = (idxBrSrc + (nbBranchs - 1 - rankSrc)) % nbBranchs
            rankDst = nbNodesInBranch - 1 - rankSrc
            idxNodeOffsetDst = nbBranchs * rankDst
            idxSrc = idxNodeOffsetSrc + idxBrSrc
            idxDst = idxNodeOffsetDst + idxBrDst
            memoryCutSize += dag[idxSrc][idxDst]['edgeMem']
            dag.remove_edge(idxSrc,idxDst)
    # on each branch, we can cut only between the chain nodes
    for idxBr in range(0, nbBranchs):
        minMem = 0
        minEdge = None
        for rank in range(nbBranchs-2, nbBranchs + nbChainNodes - 1):
            idxPred = nbBranchs * rank + idxBr
            idxSucc = nbBranchs * (rank+1) + idxBr
            edgeMem = dag[idxPred][idxSucc]['edgeMem']
            if not minEdge or edgeMem < minMem:
                minMem = edgeMem
                minEdge = (idxPred, idxSucc)
        dag.remove_edge(minEdge[0], minEdge[1])
        memoryCutSize += minMem
    # now we should have two connected components being
    # a rooted intree and a rooted outtree
    connectedComponents = [cc for cc in networkx.weakly_connected_components(dag)]
    nbCCs = len(connectedComponents) 
    if nbCCs != 2:
        raise ValueError("There should be 2 weakly cconnected components here instead of {}.".
                         format(nbCCs))
    # if we cared about which component is the out/in tree,
    # we could check the presence of source/sink node respectively
    rtree0 = networkx.DiGraph()
    rtree0.add_nodes_from(dag.nodes(data=True))
    rtree0.add_edges_from((u, v, dag.edges[u, v]) for u, v in dag.edges)
    rtree0.remove_nodes_from(connectedComponents[0])
    rtree1 = networkx.DiGraph()
    rtree1.add_nodes_from(dag.nodes(data=True))
    rtree1.add_edges_from((u, v, dag.edges[u, v]) for u, v in dag.edges)
    rtree1.remove_nodes_from(connectedComponents[1])
    return (rtree0, rtree1, memoryCutSize)


def sum1toN(n):
    """Computes sum of integer serie starting from 1.

    :param n: End index of the serie.
    :returns: Sum of integer (from 1 to n, included).
    """
    return (n*(n+1))/2


def nextCoefs(listCoefs):
    """Subfunction for nbLEtwoChainsSlow(n, m).
    /!\ Do not use directly.

    :param listCoefs: Previous values of coefficients.
    :returns: New values of coefficients (i.e. for one more node in the chain).
    """
    nbCoefs = len(listCoefs)
    newCoefs = [0 for i in range(0, nbCoefs)]
    for i in range(0, nbCoefs):
        for j in range(0, i+1):
            newCoefs[j] += listCoefs[i]
    return newCoefs


def nbLEtwoChainsSlow(n, m):
    """Compute #LE of two chains graph.
    Use complex numbering of Alexandre Honorat.
    Slow but may avoid number overflow (no division).

    :param n: Number of nodes in first chain.
    :param m: Number of nodes in second chain.
    :returns: #LE of the two chains graph.
    """
    minC = min(n, m)
    maxC = max(n, m)
    if minC <= 1:
        return maxC+1
    listSums = [sum1toN(n) for n in range(1, maxC+2)]
    coefsSum = [0 for i in range(0, maxC+1)]
    coefsSum[-1] = 1
    for i in range(2, minC):
        coefsSum = nextCoefs(coefsSum)
    nbLEtwoChains = 0
    for c, s in zip(coefsSum, listSums):
        nbLEtwoChains += c*s
    return nbLEtwoChains


def nbLEtwoChainsFast(n, m):
    """Compute #LE of two chains graph.
    See Eq. (2.19) page 127 of 'Algorithms and Order' (Ian Rival).

    :param n: Number of nodes in first chain.
    :param m: Number of nodes in second chain.
    :returns: #LE of the two chains graph.
    """
    minC = min(n, m)
    maxC = max(n, m)
    if minC <= 1:
        return maxC+1
    else:
        numerator = math.factorial(n+m)
        denominator = math.factorial(n)*math.factorial(m)
        return numerator//denominator


def genTwoChains(n, m):
    """Generate DAG with two chains in parallel,
    connected by one common sink and one common source.

    :param n: Number of nodes in first chain.
    :param m: Number of nodes in second chain.
    :returns: A DAG without any attribute set.
    """
    minC = min(n, m)
    if minC < 1:
        raise ValueError("Inputs must be positive integers.")
    dag = networkx.DiGraph()
    for i in range(0, n):
        dag.add_edge(i, i+1)
    for i in range(0, m):
        dag.add_edge(n+1+i, n+1+i+1)
    dag.add_edge(0, n+1)
    dag.add_edge(n, n+m+1)
    return dag


def genSingleChainNNtrain(n):
    """Generate chain with extra nested edges as
    in a NN training.

    :param n: Number of nodes in the chain.
    :returns: A DAG with node attribute 'name'
    and unitary edge attribute 'edgeMem'.
    """
    if n < 2:
        raise ValueError("Input must be a positive integer >= 2.")
    dag = networkx.DiGraph()
    for i in range(0, n-1):
        dag.add_edge(i, i+1, edgeMem=1)
    for i in range(0, (n-1)//2):
        dag.add_edge(i, n-i-1, edgeMem=1)
        # node names
        dag.nodes[i]['name'] = "A_{}".format(i+1)
        dag.nodes[n-i-1]['name'] = "B_{}".format(i+1)
    dag.nodes[n//2]['name'] = "B_{}".format(n//2)
    dag.nodes[n//2-1]['name'] = "A_{}".format(n//2)
    if n % 2 == 1:
        dag.nodes[n//2]['name'] = "L"
    return dag




################################################################
# Generation of example graphs
# espcially used to demonstrate some properties
################################################################


def parallelDag0():
    """A task graph without any topological contraints to study optimal
    scheduling in these cases

    :returns: A DAG with edge attribute 'edgeMem' set, and node attribute 'name' set.
    """
    dag = networkx.DiGraph()
    dag.add_edge(0, 1,  edgeMem=1) 
    dag.add_edge(0, 2,  edgeMem=2) 
    dag.add_edge(0, 3,  edgeMem=3) 
    dag.add_edge(0, 4,  edgeMem=4) 
    dag.add_edge(0, 5,  edgeMem=5) 
    dag.add_edge(0, 6,  edgeMem=6) 
 
    dag.add_edge(1, 10, edgeMem=2) 
    dag.add_edge(2, 10, edgeMem=3) 
    dag.add_edge(3, 10, edgeMem=4)  
    dag.add_edge(4, 10, edgeMem=5) 
    dag.add_edge(5, 10, edgeMem=6) 
    dag.add_edge(6, 10, edgeMem=7) 
  

    dag.nodes[0]['name'] = 'A0'
    dag.nodes[1]['name'] = 'A1'
    dag.nodes[2]['name'] = 'A2'
    dag.nodes[3]['name'] = 'A3'
    dag.nodes[4]['name'] = 'A4'
    dag.nodes[5]['name'] = 'A5'
    dag.nodes[6]['name'] = 'A6'
    dag.nodes[10]['name'] = 'A10'

    return dag
    
def parallelDag1(seed):
    """A task graph without any topological contraints to study optimal
    scheduling in these cases

    :param seed: Seed for random edgeMem.
    :returns: A DAG with edge attribute 'edgeMem' set, and node attribute 'name' set.
    """
    fixedRandom = random.Random(seed)
    dag = networkx.DiGraph()
    dag.add_edge(0, 1,  edgeMem=round(fixedRandom.uniform(1,10))) 
    dag.add_edge(0, 2,  edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(0, 3,  edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(0, 4,  edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(0, 5,  edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(0, 6,  edgeMem=round(fixedRandom.uniform(1,10)))
 
    dag.add_edge(1, 10, edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(2, 10, edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(3, 10, edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(4, 10, edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(5, 10, edgeMem=round(fixedRandom.uniform(1,10)))
    dag.add_edge(6, 10, edgeMem=round(fixedRandom.uniform(1,10)))
  

    dag.nodes[0]['name'] = 'A0'
    dag.nodes[1]['name'] = 'A1'
    dag.nodes[2]['name'] = 'A2'
    dag.nodes[3]['name'] = 'A3'
    dag.nodes[4]['name'] = 'A4'
    dag.nodes[5]['name'] = 'A5'
    dag.nodes[6]['name'] = 'A6'
    dag.nodes[10]['name'] = 'A10'

    return dag

def generateRastaPlpDag():
    """Generate the RASTA-PLP graph from Kathrin Rosvall and Ingo Sander in Figure 6 of their paper:
    "A constraint-based design space exploration frame- work for real-time applications on MPSoCs".

    :returns: A DAG with edge attribute 'edgeMem' set, and node attribute 'name' set.
    """
    dag = networkx.DiGraph()
    dag.add_edge(0, 1, edgeMem=2) # frontEnd to rasta
    dag.add_edge(1, 2, edgeMem=2) # rasta to powspec
    dag.add_edge(2, 3, edgeMem=2) # powspec to audspec
    dag.add_edge(3, 4, edgeMem=2) # audspec to complah
    dag.add_edge(0, 4, edgeMem=1) # frontEnd to complah
    dag.add_edge(4, 5, edgeMem=3) # complah to rastaFilter
    dag.add_edge(3, 5, edgeMem=1) # audspec to rastaFilter
    dag.add_edge(5, 6, edgeMem=1) # rastaFilter to backEnd
    dag.add_edge(0, 6, edgeMem=1) # frontEnd to backEnd

    dag.nodes[0]['name'] = 'frontEnd'
    dag.nodes[1]['name'] = 'rasta'
    dag.nodes[2]['name'] = 'powspec'
    dag.nodes[3]['name'] = 'audspec'
    dag.nodes[4]['name'] = 'complah'
    dag.nodes[5]['name'] = 'rastaFilter'
    dag.nodes[6]['name'] = 'backEnd'
    return dag


def generateSatelliteDag():
    """Generate the Satellite graph from Sebastian Ritz, Markus Willems, Heinrich Meyr in Figure 4 of their paper:
    "Scheduling for Optimum Data Memory Compaction in Block Diagram Oriented Software Synthesis".

    :returns: A DAG with edge attribute 'edgeMem' set, and node attribute 'name' set.
    """
    dag = networkx.DiGraph()
    dag.add_edge(0,1,edgeMem=1056) # A to B
    dag.add_edge(1,2,edgeMem=264) # B to C
    dag.add_edge(3,4,edgeMem=1056) # D to E
    dag.add_edge(4,5,edgeMem=264) # E to F
    dag.add_edge(2,6,edgeMem=24) # C to G
    dag.add_edge(6,7,edgeMem=24) # G to H
    dag.add_edge(7,8,edgeMem=264) # H to I
    dag.add_edge(8,9,edgeMem=240) # I to J
    dag.add_edge(5,10,edgeMem=24) # F to K
    dag.add_edge(10,11,edgeMem=24) # K to L
    dag.add_edge(11,12,edgeMem=264) # L to M
    dag.add_edge(12,13,edgeMem=240) # M to N
    dag.add_edge(2,14,edgeMem=240) # C to P
    dag.add_edge(5,14,edgeMem=240) # F to P
    dag.add_edge(9,14,edgeMem=240) # J to P
    dag.add_edge(13,14,edgeMem=240) # N to P
    dag.add_edge(14,15,edgeMem=240) # P to Q
    dag.add_edge(14,16,edgeMem=240) # P to R
    dag.add_edge(13,17,edgeMem=240) # N to S
    dag.add_edge(9,18,edgeMem=240) # J to T
    dag.add_edge(17,19,edgeMem=240) # S to U
    dag.add_edge(18,19,edgeMem=240) # T to U
    dag.add_edge(19,20,edgeMem=240) # U to V
    dag.add_edge(15,21,edgeMem=240) # Q to W
    dag.add_edge(16,21,edgeMem=240) # R to W
    dag.add_edge(20,21,edgeMem=240) # V to W

    for node, dicoNode in dag.nodes(data=True):
        # we skip letter 'O', (14th letter in the alphabet)
        if node < 14:
            dicoNode['name'] = base_10_to_alphabet(node+1)
        else:
            dicoNode['name'] = base_10_to_alphabet(node+2)            

    return dag


def generateChainAscDsc():
    """Generate a chain which cannot be compacted.

    :returns: A DAG with edge attribute 'edgeMem' set.
    """
    dag = networkx.DiGraph()
    # makes it SP
    dag.add_edge(0,12,edgeMem=1)
    dag.add_edge(11,12,edgeMem=20)
    # ascending part
    dag.add_edge(0,1,edgeMem=20)
    dag.add_edge(1,2,edgeMem=25)
    dag.add_edge(2,3,edgeMem=18)
    dag.add_edge(3,4,edgeMem=26)
    dag.add_edge(4,5,edgeMem=16)
    dag.add_edge(5,6,edgeMem=30)
    # descending part
    dag.add_edge(6,7,edgeMem=16)
    dag.add_edge(7,8,edgeMem=26)
    dag.add_edge(8,9,edgeMem=18)
    dag.add_edge(9,10,edgeMem=25)
    dag.add_edge(10,11,edgeMem=20)
    return dag


def generateChainDscAsc():
    """Generate a chain which can be compacted entirely.

    :returns: A DAG with edge attribute 'edgeMem' set.
    """
    dag = networkx.DiGraph()
    # makes it SP
    dag.add_edge(0,12,edgeMem=1)
    dag.add_edge(11,12,edgeMem=20)
    # descending part
    dag.add_edge(0,1,edgeMem=30)
    dag.add_edge(1,2,edgeMem=16)
    dag.add_edge(2,3,edgeMem=26)
    dag.add_edge(3,4,edgeMem=18)
    dag.add_edge(4,5,edgeMem=25)
    dag.add_edge(5,6,edgeMem=20)
    # ascending part
    dag.add_edge(6,7,edgeMem=25)
    dag.add_edge(7,8,edgeMem=18)
    dag.add_edge(8,9,edgeMem=26)
    dag.add_edge(9,10,edgeMem=16)
    dag.add_edge(10,11,edgeMem=30)
    return dag


def generateAntisymetricChainTree():
    """Generate a rooted in-tree with two chains (one and its antisymmetric).

    :returns: A DAG with edge attribute 'edgeMem' set.
    """
    dag = networkx.DiGraph()
    # first chain ascending
    dag.add_edge(0,1,edgeMem=20)
    dag.add_edge(1,2,edgeMem=25)
    dag.add_edge(2,3,edgeMem=18)
    dag.add_edge(3,4,edgeMem=26)
    dag.add_edge(4,5,edgeMem=16)
    dag.add_edge(5,6,edgeMem=30)
    dag.add_edge(6,7,edgeMem=20)
    # first chain descending
    dag.add_edge(7,8,edgeMem=16)
    dag.add_edge(8,9,edgeMem=26)
    dag.add_edge(9,10,edgeMem=18)
    dag.add_edge(10,11,edgeMem=25)
    dag.add_edge(11,12,edgeMem=20)
    # antisymmetric chain asc
    dag.add_edge(0,13,edgeMem=20)
    dag.add_edge(13,14,edgeMem=15)
    dag.add_edge(14,15,edgeMem=22)
    dag.add_edge(15,16,edgeMem=14)
    dag.add_edge(16,17,edgeMem=24)
    dag.add_edge(17,18,edgeMem=10)
    dag.add_edge(18,19,edgeMem=20)
    # antisymmetric chain dsc
    dag.add_edge(19,20,edgeMem=24)
    dag.add_edge(20,21,edgeMem=14)
    dag.add_edge(21,22,edgeMem=22)
    dag.add_edge(22,23,edgeMem=15)
    dag.add_edge(23,24,edgeMem=20)
    
    return dag


def generateAntisymetricChainSPdag():
    """Generate an SP-DAG with two chains (one and its antisymmetric).

    :returns: A DAG with edge attribute 'edgeMem' set.
    """
    dag = generateAntisymetricChainTree()
    dag.add_edge(12,25,edgeMem=20)
    dag.add_edge(24,25,edgeMem=20)
    return dag


def generateSmallSDFrinTree(setNames=True):
    """Generate a small SDF tree which gives weird
    but optimal schedules if no transitive reduction
    before serial and parallel optimizations on the
    SRDAG extended version.

    :returns: An SDF graph with edge attributes 
    'rateSrc', 'rateDst' and 'delay' (always 0)
    and node attribute 'name' set.
    """
    
    dag = networkx.DiGraph()

    dag.add_edge(0,1,rateSrc=1,rateDst=2,delay=0)
    dag.add_edge(2,3,rateSrc=1,rateDst=2,delay=0)
    dag.add_edge(1,4,rateSrc=1,rateDst=1,delay=0)
    dag.add_edge(3,4,rateSrc=1,rateDst=1,delay=0)

    if setNames:
        set_alphabetical_node_names(dag)
        
    return dag

        
def badSDFfiringReduction(setNames=True):
    """Generates problematic SDF DAG where increasing
    number of firings increases the memory peak if only
    suboptimal moves.

    :returns: SDF DAG (adapted subpart of Satellite).
    """
    dag = networkx.DiGraph()

    dag.add_edge(23,18,rateSrc=3,rateDst=1) # X to S
    dag.add_edge(23,19,rateSrc=3,rateDst=3) # X to T
    
    dag.add_edge(18,20,rateSrc=1,rateDst=3) # S to U
    dag.add_edge(19,20,rateSrc=3,rateDst=3) # T to U
    
    dag.add_edge(20,22,rateSrc=3,rateDst=6) # U to W (add)

    if setNames:
        set_alphabetical_node_names(dag)
    return dag


def badSDFfiringReduction2(setNames=True):
    """Generates problematic SDF DAG where increasing
    number of firings increases the memory peak if only
    wrongly optimal moves.
    /!\ Seems not to be the case anymore.

    :returns: SDF DAG.
    """
    dag = networkx.DiGraph()

    dag.add_edge(0,1,rateSrc=6,rateDst=1) # A to B
    dag.add_edge(1,2,rateSrc=1,rateDst=6) # B to C

    if setNames:
        set_alphabetical_node_names(dag)
    return dag


def generateSampleSDF(setNames=True):
    """Generates sample SDF DAG for LCTES'23.

    :returns: SDF DAG (adapted subpart of Satellite).
    """
    dag = networkx.DiGraph()
    dag.add_edge(0,1,rateSrc=1,rateDst=1) # A to B
    dag.add_edge(1,2,rateSrc=2,rateDst=2) # B to C
    dag.add_edge(2,3,rateSrc=3,rateDst=3) # C to D
    dag.add_edge(0,3,rateSrc=4,rateDst=4) # B to D
    dag.add_edge(0,23,rateSrc=4,rateDst=4) # A to X
    dag.add_edge(23,3,rateSrc=1,rateDst=1) # X to D
    
    if setNames:
        set_alphabetical_node_names(dag)
    return dag


def generateNegOptimDAG(setNames=True):
    """Generates sample DAG to highlight neg. optim.

    :returns: DAG.
    """
    dag = networkx.DiGraph()
    dag.add_edge(0,1,edgeMem=10) # A to B
    dag.add_edge(0,2,edgeMem=10) # A to C
    dag.add_edge(2,5,edgeMem=5) # C to F
    dag.add_edge(1,3,edgeMem=15) # B to D
    dag.add_edge(3,4,edgeMem=15) # D to E
    dag.add_edge(4,5,edgeMem=15) # E to F

    dag.nodes[0]['tmpMemInc'] = 20
    dag.nodes[1]['tmpMemInc'] = 5
    dag.nodes[2]['tmpMemInc'] = 10
    dag.nodes[3]['tmpMemInc'] = 2
    dag.nodes[4]['tmpMemInc'] = 5
    dag.nodes[5]['tmpMemInc'] = 10
    
    if setNames:
        set_alphabetical_node_names(dag)
    return dag


def generatePosOptimDAG(setNames=True):
    """Generates sample DAG to highlight pos. optim.

    :returns: DAG.
    """
    dag = networkx.DiGraph()
    dag.add_edge(0,1,edgeMem=1) # A to B
    dag.add_edge(0,2,edgeMem=1) # A to C
    dag.add_edge(0,3,edgeMem=1) # A to D
    dag.add_edge(1,4,edgeMem=2) # B to E
    dag.add_edge(2,4,edgeMem=6) # C to E
    dag.add_edge(3,4,edgeMem=8) # D to E

    dag.nodes[0]['tmpMemInc'] = 3
    dag.nodes[1]['tmpMemInc'] = 10
    dag.nodes[2]['tmpMemInc'] = 10
    dag.nodes[3]['tmpMemInc'] = 10
    dag.nodes[4]['tmpMemInc'] = 0
    
    if setNames:
        set_alphabetical_node_names(dag)
    return dag


def generateBadBandB_V2extSchedGraph():
    """Generates a graph demonstrating a
    problem in the BandB V2ext pre-computation
    of forbidden successors when using the
    internal negative nodes optimization in
    get_forbidden_succN(...).

    :returns: A schedule graph (with name,
    tmpMemInc and nodeMemCost node attribtues).
    """
    dag = networkx.DiGraph()
    dag.add_edge(0,2,edgeMem=0)
    dag.add_edge(0,36,edgeMem=0)
    dag.add_edge(2,4,edgeMem=0)
    dag.add_edge(2,40,edgeMem=0)
    dag.add_edge(36,40,edgeMem=0)
    dag.add_edge(36,38,edgeMem=0)
    dag.add_edge(4,5,edgeMem=0)
    dag.add_edge(40,5,edgeMem=0)
    dag.add_edge(38,5,edgeMem=0)

    dag.nodes[0]['nodeMemCost'] = 4
    dag.nodes[0]['tmpMemInc'] = 12
    dag.nodes[2]['nodeMemCost'] = 2
    dag.nodes[2]['tmpMemInc'] = 2
    dag.nodes[4]['nodeMemCost'] = -1
    dag.nodes[4]['tmpMemInc'] = 4
    dag.nodes[40]['nodeMemCost'] = -4
    dag.nodes[40]['tmpMemInc'] = 0
    dag.nodes[5]['nodeMemCost'] = -2
    dag.nodes[5]['tmpMemInc'] = 1
    dag.nodes[36]['nodeMemCost'] = 2
    dag.nodes[36]['tmpMemInc'] = 2
    dag.nodes[38]['nodeMemCost'] = -1
    dag.nodes[38]['tmpMemInc'] = 4
    
    set_alphabetical_node_names(dag)
    set_default_attributes(dag, resetTmpMemInc=False)
    
    return dag


def generateIrreducibleGraph_wSTsubDAG():
    """Generates a graph that cannot be compressed
    by clustering, sequentialization or transitive rules.
    However, this graph contains one STsubDAG (nodes F, G,
    H and I) and can be solve with the compositionality result.

    :returns: A schedule graph (with name,
    tmpMemInc and nodeMemCost node attribtues,
    and edgeMem edge attribute).
    """
    dag = networkx.DiGraph()
    dag.add_edge(0,5,edgeMem=7421)
    dag.add_edge(0,6,edgeMem=1398)
    dag.add_edge(0,11,edgeMem=2757)
    dag.add_edge(5,8,edgeMem=7616)
    dag.add_edge(6,7,edgeMem=1111)
    dag.add_edge(6,8,edgeMem=479)
    dag.add_edge(7,12,edgeMem=815)
    dag.add_edge(8,12,edgeMem=7134)
    dag.add_edge(11,13,edgeMem=1086)
    dag.add_edge(12,13,edgeMem=2807)

    dag.nodes[0]['nodeMemCost'] = 11576
    dag.nodes[0]['tmpMemInc'] = 11859
    dag.nodes[5]['nodeMemCost'] = 195
    dag.nodes[5]['tmpMemInc'] = 256
    dag.nodes[6]['nodeMemCost'] = 192
    dag.nodes[6]['tmpMemInc'] = 192
    dag.nodes[7]['nodeMemCost'] = -296
    dag.nodes[7]['tmpMemInc'] = 126
    dag.nodes[8]['nodeMemCost'] = -961
    dag.nodes[8]['tmpMemInc'] = 10
    dag.nodes[11]['nodeMemCost'] = -1671
    dag.nodes[11]['tmpMemInc'] = 769
    dag.nodes[12]['nodeMemCost'] = -5142
    dag.nodes[12]['tmpMemInc'] = 499
    dag.nodes[13]['nodeMemCost'] = -3893
    dag.nodes[13]['tmpMemInc'] = 860
    
    set_alphabetical_node_names(dag)
    set_default_attributes(dag, resetTmpMemInc=False)
    
    return dag




################################################################
# Generation of filterbank graphs
# For qmf235-3D call gen_filterbank(3,2,5,3,5)
################################################################


def gen_filterbank (depth, src1, dst1, src2, dst2,
                    homogeneous=False, setNames=True,
                    setSymNodes=True):
    dag = networkx.DiGraph()
    gen_filterbankdn (dag,depth,src1, dst1, src2, dst2, 0, setSymNodes)
    if setNames:
        set_alphabetical_node_names(dag)
    if homogeneous:
        for u, v, dicoEdge in dag.edges(data=True):
            rateSrc = dicoEdge['rateSrc']
            rateDst = dicoEdge['rateDst']
            if rateSrc == rateDst:
                dicoEdge['rateSrc'] = 1
                dicoEdge['rateDst'] = 1
    return dag
    
def gen_filterbankdn (dag,depth, src1, dst1, src2, dst2, i, setSymNodes=True):
    #global dag
    if depth < 1:
       raise ValueError("Depth must be positive.")
    elif depth == 1:
       gen_filterbankd1 (dag,src1, dst1, src2, dst2, i, setSymNodes)
       return (i+8)
    else:
       i1 = gen_filterbankdn (dag,depth-1, src1, dst1, src2, dst2, i+2, setSymNodes)
       i2 = gen_filterbankdn (dag,depth-1, src1, dst1, src2, dst2, i1, setSymNodes)
       dag.add_edge(i,i+1,rateSrc=1,rateDst=1)
       dag.add_edge(i+1,i+2,rateSrc=src1,rateDst=dst1)
       dag.add_edge(i+1,i1,rateSrc=src2,rateDst=dst2)          
       dag.add_edge(i1-1, i2, rateSrc=dst1,rateDst=src1)
       dag.add_edge(i2-1, i2,rateSrc=dst2,rateDst=src2)
       dag.add_edge(i2, i2+1,rateSrc=1,rateDst=1)
       # annote mirror vertices
       if setSymNodes:
           dag.nodes[i]['sdfSymNode'] = i2+1
           dag.nodes[i+1]['sdfSymNode'] = i2
           dag.nodes[i2+1]['sdfSymNode'] = i
           dag.nodes[i2]['sdfSymNode'] = i+1
       return i2+2
                     
def gen_filterbankd1 (dag,src1, dst1, src2, dst2, i, setSymNodes=True):
    #global dag
    dag.add_edge(i,i+1,rateSrc=1,rateDst=1)
    dag.add_edge(i+1,i+2,rateSrc=src1,rateDst=dst1)
    dag.add_edge(i+1,i+3,rateSrc=src2,rateDst=dst2)
    dag.add_edge(i+2,i+4,rateSrc=1,rateDst=1)
    dag.add_edge(i+3,i+5,rateSrc=1,rateDst=1)
    dag.add_edge(i+4,i+6,rateSrc=dst1,rateDst=src1)
    dag.add_edge(i+5,i+6,rateSrc=dst2,rateDst=src2)
    dag.add_edge(i+6,i+7,rateSrc=1,rateDst=1)
    # annote mirror vertices
    if setSymNodes:
        dag.nodes[i]['sdfSymNode'] = i+7
        dag.nodes[i+1]['sdfSymNode'] = i+6
        dag.nodes[i+2]['sdfSymNode'] = i+4
        dag.nodes[i+3]['sdfSymNode'] = i+5
        dag.nodes[i+4]['sdfSymNode'] = i+2
        dag.nodes[i+5]['sdfSymNode'] = i+3
        dag.nodes[i+6]['sdfSymNode'] = i+1
        dag.nodes[i+7]['sdfSymNode'] = i


def gen_filterbank_fixed (depth, src1, dst1, src2, dst2,
                          homogeneous=False, setNames=True,
                          setSymNodes=True):
    dag = networkx.DiGraph()
    i1 = gen_filterbankdn_fixed (dag, depth, src1, dst1, src2, dst2, 1, setSymNodes)
    dag.add_edge(0, 1,rateSrc=1,rateDst=1)  
    dag.add_edge(i1-1, i1,rateSrc=1,rateDst=1)  

    if setNames:
        set_alphabetical_node_names(dag)
    if homogeneous:
        for u, v, dicoEdge in dag.edges(data=True):
            rateSrc = dicoEdge['rateSrc']
            rateDst = dicoEdge['rateDst']
            if rateSrc == rateDst:
                dicoEdge['rateSrc'] = 1
                dicoEdge['rateDst'] = 1
    return dag


def gen_filterbankdn_fixed (dag,depth, src1, dst1, src2, dst2, i, setSymNodes=True):
    #global dag
    if depth < 1:
       raise ValueError("Depth must be positive.")
    elif depth == 1:
       gen_filterbankd1_fixed (dag,src1, dst1, src2, dst2, i, setSymNodes)
       return (i+6)
    else:
       i1 = gen_filterbankdn_fixed (dag,depth-1, src1, dst1, src2, dst2, i+3, setSymNodes)
       i2 = gen_filterbankdn_fixed (dag,depth-1, src1, dst1, src2, dst2, i1, setSymNodes)

       dag.add_edge(i,i+1,rateSrc=1,rateDst=dst1)
       dag.add_edge(i,i+2,rateSrc=1,rateDst=dst2)
       dag.add_edge(i+1,i+3,rateSrc=src1,rateDst=1)
       dag.add_edge(i+2,i1,rateSrc=src2,rateDst=1)
       
       dag.add_edge(i1-1, i2, rateSrc=1,rateDst=src1)
       dag.add_edge(i2-1, i2+1, rateSrc=1,rateDst=src2)
       dag.add_edge(i2, i2+2, rateSrc=dst1,rateDst=1)  
       dag.add_edge(i2+1, i2+2, rateSrc=dst2,rateDst=1)  

       # annote mirror vertices
       if setSymNodes:
           dag.nodes[i]['sdfSymNode'] = i2+2
           dag.nodes[i+1]['sdfSymNode'] = i2
           dag.nodes[i+2]['sdfSymNode'] = i2+1
           dag.nodes[i2+1]['sdfSymNode'] = i+2
           dag.nodes[i2]['sdfSymNode'] = i+1
           dag.nodes[i2+2]['sdfSymNode'] = i

       return i2+3
                     
def gen_filterbankd1_fixed (dag,src1, dst1, src2, dst2, i, setSymNodes=True):
    #global dag
    dag.add_edge(i+0,i+1,rateSrc=1,rateDst=dst1)
    dag.add_edge(i+0,i+2,rateSrc=1,rateDst=dst2)
    dag.add_edge(i+1,i+3,rateSrc=src1,rateDst=src1)
    dag.add_edge(i+2,i+4,rateSrc=src2,rateDst=src2)
    dag.add_edge(i+3,i+5,rateSrc=dst1,rateDst=1)
    dag.add_edge(i+4,i+5,rateSrc=dst2,rateDst=1)
    # annote mirror vertices
    if setSymNodes:
        dag.nodes[i]['sdfSymNode'] = i+5
        dag.nodes[i+1]['sdfSymNode'] = i+3
        dag.nodes[i+2]['sdfSymNode'] = i+4
        dag.nodes[i+3]['sdfSymNode'] = i+1
        dag.nodes[i+4]['sdfSymNode'] = i+2
        dag.nodes[i+5]['sdfSymNode'] = i

    
def generator_splitjoin_dag(depth, maxEdgeMem, maxNodeMem, nbDAG, seed):
    """Generate multiple branched DAG in the cbp model.

    :param depth: Number of split/join stages.
    :param maxEdgeMem: Maximum memory set per edge.
    :param maxNodeMem: Maximum memory set per node.
    :param nbDAG: Maximum number of DAG to generate (may be lower).
    :param seed: Starting seed for randomness.
    :returns: Tuple of (dad, seedOffset).
    """
    currentSeed = seed
    for i in range(0, nbDAG):
        dag = gen_filterbank (depth, 1, 1, 1, 1, setNames=False)
        generate_random_edge_mem(dag, maxEdgeMem, currentSeed)
        generate_random_tmpMemInc(dag, maxNodeMem, currentSeed)
        set_cbp_common_attr(dag)
        yield (dag, i)
        currentSeed += 1


def gen_parallel_dagL(dagList, cplxVersion=False):
    """Generate a new DAG made of multiple parallel
    instances of a given one.

    :param dagList: List of DAG to copy in parallel instances.
    :param cplxVersion: Adds some nodes to forbid
    sequentialization of source/sink nodes from
    different instances.
    :returns: New DAG containing multiple instances
    in parallel of the given one, all connected by 
    the same source and sink nodes. Source and sink nodes
    have the highest node identifier (+1 for the sink).
    """
    if not dagList:
        raise ValueError("Number of parallel instances must be strictly positive.")

    prevDag = None
    offsetList = []
    firstNodesList = []
    lastNodesList = []
    # adds all given dags in a single one
    for dag in dagList:
        if prevDag is None:
            offsetNodes = 0
        else:
            maxOldNodes = max(prevDag.nodes())
            offsetNodes = maxOldNodes - min(dag.nodes()) + 1
        offsetList.append(offsetNodes)
        prevDag = getSubDAG(dag, nodes=None, offset=offsetNodes, subdag=prevDag)
        # original first and last nodes
        firstNodes = [n for n, in_degree in dag.in_degree if in_degree == 0]
        firstNodesList.append(firstNodes)
        lastNodes = [n for n, out_degree in dag.out_degree if out_degree == 0]
        lastNodesList.append(lastNodes)
    newInstance = prevDag
        
    # computes offsets
    offsetInstances = max(dagList[-1].nodes()) + offsetList[-1]
    offsetCplx = 0
    if cplxVersion:
        for i in range(0, len(dagList)):
            offsetCplx += len(firstNodesList[i])
            offsetCplx += len(lastNodesList[i])
    nodeS = offsetInstances + offsetCplx + 1
    nodeT = nodeS + 1

    # connect the main dag
    cummulativeCplxOffset = 0
    for i,dag in enumerate(dagList):
        # makes is an ST/SP dag, by adding single source and single sink
        for n in firstNodesList[i]:
            newInstance.add_edge(nodeS, n+offsetList[i], edgeMem=0)
            if cplxVersion:
                idxSrc = offsetInstances+cummulativeCplxOffset+1
                impact = math.ceil(math.fabs(dag.nodes[n].get('nodeMemCost', 0)))
                newInstance.add_edge(idxSrc, n+offsetList[i], edgeMem=0)
                newInstance.nodes[idxSrc]['nodeMemCost'] = impact
                newInstance.nodes[idxSrc]['tmpMemInc'] = 1 + impact + dag.nodes[n].get('tmpMemInc', 0)
                cummulativeCplxOffset += 1
        for n in lastNodesList[i]:
            newInstance.add_edge(n+offsetList[i], nodeT, edgeMem=0)
            if cplxVersion:
                idxDst = offsetInstances + cummulativeCplxOffset+1
                impact = - math.ceil(math.fabs(dag.nodes[n].get('nodeMemCost', 0)))
                newInstance.add_edge(n+offsetList[i], idxDst, edgeMem=0)
                newInstance.nodes[idxDst]['nodeMemCost'] = impact
                newInstance.nodes[idxDst]['tmpMemInc'] = 1 - impact + dag.nodes[n].get('tmpMemInc', 0)
                cummulativeCplxOffset += 1

    # finally fill ST node attributes with artificially positive impact
    newInstance.nodes[nodeS]['nodeMemCost'] = 0
    newInstance.nodes[nodeS]['tmpMemInc'] = 0
    newInstance.nodes[nodeT]['nodeMemCost'] = 0
    newInstance.nodes[nodeT]['tmpMemInc'] = 0
    if cplxVersion:
        newInstance.add_edge(nodeS, nodeT, edgeMem=1)
        newInstance.nodes[nodeS]['nodeMemCost'] = 1
        newInstance.nodes[nodeS]['tmpMemInc'] = 1
        newInstance.nodes[nodeT]['nodeMemCost'] = -1
        sumImpact = sum((ip for _,ip in newInstance.nodes(data='nodeMemCost')))
        newInstance.nodes[nodeT]['nodeMemCost'] -= sumImpact 
        newInstance.nodes[nodeT]['tmpMemInc'] = max(0, 1-sumImpact)
       
    return newInstance
    
        
def gen_parallel_dag(dag, nbParallelInstances, cplxVersion=False):
    """Generate a new DAG made of multiple parallel
    instances of a given one.

    :param dag: DAG to copy in parallel instances.
    :param nbParallelInstances: Number of parallel
    instances to have in the final dag.
    :param cplxVersion: Adds some nodes to forbid
    sequentialization of source/sink nodes from
    different instances.
    :returns: New DAG containing multiple instances
    in parallel of the given one, all connected by 
    the same source and sink nodes. Source and sink nodes
    have the highest node identifier (+1 for the sink).
    """
    if nbParallelInstances < 1:
        raise ValueError("Number of parallel instances must be strictly positive.")
    maxNodes = max(dag.nodes())
    offsetNodes = maxNodes - min(dag.nodes()) + 1
    # original first and last nodes
    firstNodes = [n for n, in_degree in dag.in_degree if in_degree == 0]
    lastNodes = [n for n, out_degree in dag.out_degree if out_degree == 0]
    offsetInstances = maxNodes + (nbParallelInstances-1) * offsetNodes
    offsetCplx = 0
    if cplxVersion:
        offsetCplx += len(firstNodes)*nbParallelInstances
        offsetCplx += len(lastNodes)*nbParallelInstances
        
    nodeS = offsetInstances + offsetCplx + 1
    nodeT = nodeS + 1

    prevDag = None
    for i in range(0, nbParallelInstances):
        newInstance = getSubDAG(dag, nodes=None, offset=i*offsetNodes, subdag=prevDag)
        prevDag = newInstance
        # makes is an ST/SP dag, by adding single source and single sink
        for idx,n in enumerate(firstNodes):
            newInstance.add_edge(nodeS, n+i*offsetNodes, edgeMem=0)
            if cplxVersion:
                idxSrc = offsetInstances+i*len(firstNodes)+idx+1
                impact = math.ceil(math.fabs(dag.nodes[n].get('nodeMemCost', 0)))
                newInstance.add_edge(idxSrc, n+i*offsetNodes, edgeMem=0)
                newInstance.nodes[idxSrc]['nodeMemCost'] = impact
                newInstance.nodes[idxSrc]['tmpMemInc'] = 1 + impact + dag.nodes[n].get('tmpMemInc', 0)
        for idx,n in enumerate(lastNodes):
            newInstance.add_edge(n+i*offsetNodes, nodeT, edgeMem=0)
            if cplxVersion:
                idxDst = offsetInstances + offsetCplx - (i*len(lastNodes) + idx)
                impact = - math.ceil(math.fabs(dag.nodes[n].get('nodeMemCost', 0)))
                newInstance.add_edge(n+i*offsetNodes, idxDst, edgeMem=0)
                newInstance.nodes[idxDst]['nodeMemCost'] = impact
                newInstance.nodes[idxDst]['tmpMemInc'] = 1 - impact + dag.nodes[n].get('tmpMemInc', 0)
    # finally fill ST node attributes with artificially positive impact
    newInstance.nodes[nodeS]['nodeMemCost'] = 0
    newInstance.nodes[nodeS]['tmpMemInc'] = 0
    newInstance.nodes[nodeT]['nodeMemCost'] = 0
    newInstance.nodes[nodeT]['tmpMemInc'] = 0
    if cplxVersion:
        newInstance.add_edge(nodeS, nodeT, edgeMem=1)
        newInstance.nodes[nodeS]['nodeMemCost'] = 1
        newInstance.nodes[nodeS]['tmpMemInc'] = 1
        newInstance.nodes[nodeT]['nodeMemCost'] = -1
        newInstance.nodes[nodeT]['tmpMemInc'] = 0

    return newInstance
        

################################################################
# I/O Serialization of graphs
# Read from SDF3 file, Write to DOT file
################################################################

        

def parseSdf3(fileSdf3, keepDelays=True, keepSelfLoops=False, keepCtrlChannels=False):
    """Parses an SDF3 file and return the corresponding networkx graph.

    :param fileSdf3: Path (as a string) to a valid SDF3 file.
    :param keepDelays: Wether or not to keep delay information (otherwise set to 0).
    :param keepSelfLoops: Wether or not to keep self-loops.
    :param keepCtrlChannels: Weter or not to keep channels and ports 
    whose names start by "_" (see names in some SDF3 files).
    :returns: A simple graph with node attributes 'name' and 'wcet' set,
    and edge attributes 'rateSrc' and 'rateDst' set.
    """
    # adapted from an old script for PREESM
    ## parse actors to get prod and cons rates
    parser = etree.XMLParser(remove_blank_text=True)
    treeSdf = etree.parse(fileSdf3, parser)
    rootSdf = treeSdf.getroot()

    sdfName = ""
    # applicationGraph
    for child in rootSdf.getchildren():
        name = child.get("name")
        if name:
            sdfName = name

    logger.debug("==> Parsing application: {}",sdfName)
    
    # rootAppNSmap = rootSdf.nsmap
    # logger.debug(rootAppNSmap)
    
    # from here: https://stackoverflow.com/questions/6920073/how-to-use-xpath-from-lxml-on-null-namespaced-nodes
    # prefix = 'gmd:' #'{' + rootAppNSmap[None] + '}'
    # rootAppNSmap['gmd'] = rootAppNSmap[None]
    # rootAppNSmap.pop(None)

    # dico of dict: {'actorName': {portName': (in/out, rate)} }
    dicoActors = {}
    dicoActorsIdx = OrderedDict()
    
    for actor in treeSdf.xpath("/sdf3/applicationGraph/sdf/actor"):
        kind = actor.get("type")
        # if kind == "Node":
        name = actor.get("name")
        dicoPorts = {}
        for port in actor.getchildren():
            port_kind = port.get("type")
            if port_kind == "in" or port_kind == "out":
                port_name = port.get("name")
                if port_name.startswith("_") and not keepCtrlChannels:
                    continue
                port_rate = int(port.get("rate"))
                dicoPorts[port_name] = (port_kind, port_rate)                    
        dicoActors[name] = dicoPorts
        dicoActorsIdx[name] = len(dicoActorsIdx)
        # else:
        # continue


    # dico of dict: {'channelName': {'srcActor': , 'srcPort': , 'dstActor': , 'dstPort':, 'delay': } }
    dicoChannels = {}
    
    for channel in treeSdf.xpath("/sdf3/applicationGraph/sdf/channel"):
        name = channel.get("name")
        if name.startswith("_") and not keepCtrlChannels:
            continue
        dicoChannel = {}
        dicoChannel['srcActor'] = channel.get('srcActor')
        dicoChannel['srcPort'] = channel.get('srcPort')
        dicoChannel['dstActor'] = channel.get('dstActor')
        dicoChannel['dstPort'] = channel.get('dstPort')
        if dicoChannel['srcActor'] == dicoChannel['dstActor'] and \
           not keepSelfLoops:
            continue
        dicoChannel['delay'] = 0
        if channel.get('initialTokens') and keepDelays:
            dicoChannel['delay'] = int(channel.get('initialTokens'))
        dicoChannels[name] = dicoChannel

    logger.info("Parsed {} actors and {} channels.",len(dicoActors), len(dicoChannels))

    dicoTimings = {}
    setProcs = set()
    for prop in treeSdf.xpath("/sdf3/applicationGraph/sdfProperties/actorProperties"):
        actor = prop.get('actor')
        for proc in prop.xpath(".//processor"):
            default = proc.get('default')
            if default == "true":
                proc_name = proc.get('type')
                setProcs.add(proc_name)
                for time in proc.xpath(".//executionTime"):
                    dicoTimings[actor] = time.get('time')

    if len(setProcs) > 1:
        logger.warning("WARNING: More than one default processor for timings.")    
    logger.info("Parsed {} timings.",len(dicoTimings))
        
    # adaptation to generate networkx graph
    graph = networkx.DiGraph()
    graph.add_nodes_from(range(0,len(dicoActorsIdx)))
    for actor, wcet in dicoTimings.items():
        index = dicoActorsIdx[actor]
        graph.nodes[index]['wcet'] = wcet
        graph.nodes[index]['name'] = actor

    for dicoChannel in dicoChannels.values():
        delay = dicoChannel['delay']
        srcActor = dicoChannel['srcActor']
        dstActor = dicoChannel['dstActor']
        srcIndex = dicoActorsIdx[srcActor]
        dstIndex = dicoActorsIdx[dstActor]
        srcRate = dicoActors[srcActor][dicoChannel['srcPort']][1]
        dstRate = dicoActors[dstActor][dicoChannel['dstPort']][1]
        if srcRate <= 0 or dstRate <= 0:
            continue
        if graph.has_edge(srcIndex, dstIndex):
            dicoEdge = graph[srcIndex][dstIndex]
            # check if rates are consistent with former ones and add it
            srcRateOri = dicoEdge['rateSrc']
            dstRateOri = dicoEdge['rateDst']
            delayOri = dicoEdge['delay']
            if srcRate * dstRateOri != dstRate * srcRateOri:
                raise ValueError ("Graph is not rate consistent on edge from {} to {}."
                                  .format(srcActor, dstActor))
            if srcRate * delayOri != srcRateOri * delay:
                raise ValueError("Graph is not delay consisten on edge from {} to {}."
                                 .format(srcActor, dstActor))
            dicoEdge['rateSrc'] += srcRate
            dicoEdge['rateDst'] += dstRate
            dicoEdge['delay'] += delay
        else:
            graph.add_edge(srcIndex, dstIndex)
            dicoEdge = graph[srcIndex][dstIndex]
            dicoEdge['delay'] = delay
            dicoEdge['rateSrc'] = srcRate
            dicoEdge['rateDst'] = dstRate
            
    return graph

            
def writeNXdagToDOT(dag, fileName, onlyMemdagInfo=False):
    """Write Networkx DAG into a dot file.

    :param dag: DAG to write.
    :param fileName: Filename (including relative path) where to write.
    :param onlyMemdagInfo: (default: False) Whether or not 
    only node names and edge weights should be printed.
    If not, 'tmpMemInc' and 'nodeMemCost' are added to node attributes
    with 'peak' and 'impact' labels respectively, holding integers.
    """
    preamble = "strict digraph \"{}\" {{\n".format(fileName)
    strNodeList = []
    for node, dicoNode in dag.nodes(data=True):
        attributes = ["label=\"{}\"".format(dicoNode['name'])]
        if not onlyMemdagInfo:
            attributes.append("peak=\"{}\"".format(dicoNode['tmpMemInc']))
            attributes.append("impact=\"{}\"".format(dicoNode['nodeMemCost']))
        strNode = "  {} [{}];".format(node, ' '.join(attributes))
        strNodeList.append(strNode)
    strEdgeList = []
    for src, dst, dicoEdge in dag.edges(data=True):
        attributes = ["weight=\"{}\"".format(dicoEdge['edgeMem'])]
        strEdge = "  {} -> {} [{}];".format(src, dst, ' '.join(attributes))
        strEdgeList.append(strEdge)
    epilog = "\n}\n"
    with open(fileName, 'w') as f:
        f.write(preamble)
        f.write('\n'.join(strNodeList))
        f.write('\n\n')
        f.write('\n'.join(strEdgeList))
        f.write(epilog)

        
def writeNXsdfToDOT(sdf, fileName, forPrint=True):
    """Write Networkx SDF into a dot file.

    :param sdf: DAG to write.
    :param fileName: Filename (including relative path) where to write.
    :param forPrint: (default True) Pretty print for dot generation.
    """
    preamble = "strict digraph \"{}\" {{\n  graph[fontsize=10];\n\n".format(fileName)
    strNodeList = []
    for node, dicoNode in sdf.nodes(data=True):
        attributes = ["label=\"{}\"".format(dicoNode['name'])]
        strNode = "  {} [{}];".format(node, ' '.join(attributes))
        strNodeList.append(strNode)
    strEdgeList = []
    for src, dst, dicoEdge in sdf.edges(data=True):
        if forPrint:
            attributes = []
            rateSrc = dicoEdge['rateSrc']
            if rateSrc > 1:
                attributes.append("taillabel=\"{}\"".format(rateSrc))
            rateDst = dicoEdge['rateDst']
            if rateDst > 1:
                attributes.append("headlabel=\"{}\"".format(rateDst))
        else:
            attributes = ["rateSrc=\"{}\"".format(dicoEdge['rateSrc']),
                          "rateDst=\"{}\"".format(dicoEdge['rateDst'])]
        strEdge = "  {} -> {} [{}];".format(src, dst, ' '.join(attributes))
        strEdgeList.append(strEdge)
    epilog = "\n}\n"
    with open(fileName, 'w') as f:
        f.write(preamble)
        f.write('\n'.join(strNodeList))
        f.write('\n\n')
        f.write('\n'.join(strEdgeList))
        f.write(epilog)


## JSON serialization from https://stackoverflow.com/a/76309676

def saveNXtoJSON(graph, fileName):
    """Dump networkx graph into json file,
    with all node and edge attributes.

    :param graph: Graph to dump.
    :param fileName: File name (if path,
    folders must already exist).
    """
    nodeArr = []
    for node in list(graph.nodes(data=True)):
        nodeArr.append(node)

    edgeArr = []
    for edge in list(graph.edges(data=True)):
        edgeArr.append(edge)

    graphDict = { 'nodes': nodeArr,
                  'edges': edgeArr}
    json.dump(graphDict, open(fileName, 'w'), indent=2)

    
def loadJSONtoNX(fileName):
    """Load networkx graph from file,
    with all node and edge attributes.

    :param fileName: Name of a file
    written thanks to saveNXtoJSON().
    :returns: A networkx graph.
    """
    graph = networkx.DiGraph()
    d = json.load(open(fileName))
    graph.add_nodes_from(d['nodes'])
    graph.add_edges_from(d['edges'])
    return graph
        
