#! /usr/bin/python3

# Copyright or © or Copr.
# Pascal Fradet, INRIA
# Alain Girault, INRIA 
# Alexandre Honorat, INRIA
# (created in 2022)

# Contact: alexandre.honorat@inria.fr

# This software is a computer program whose purpose is to
# compute sequential schedules of a task graph or an SDF graph
# in order to minimize its memory peak.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


# Uncomment to output in out.txt file
#import sys
#fd = open('out.txt', 'w')
#sys.stderr = fd

from dagsched.dag_gen import *
from dagsched.sdf_transform import sdfToDAG
from dagsched.dag_mem_min_unicore_algo import *
from dagsched.dag_mem_min_unicore_expe import *
from dagsched.dag_mem_min_unicore_plot import *

# import sys
import bracelogger
logger = bracelogger.get_logger("dagsched_log")
import logging
# logging.raiseExceptions = False
logger.setLevel(logging.INFO)

# https://stackoverflow.com/questions/13733552/logger-configuration-to-log-to-file-and-print-to-stdout
# logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
consoleHandler = logging.StreamHandler()
# consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)


# availbale fallbacks:
# - two_heurs_solver
# - heuristic_linear_best_parallel
# - heuristic_linear_1dot5steps_impact
# - memdagSPschedule
# - brute_force_dag_min_mem
# - exhaustive_BandB_[lctes23sub|lctes23fixed|
#                     V2|V2ext|V3epiDual|
#                     V4epiDominant]
KWargs = {'max_global_iter': 0, 'max_local_iter': 0,
          'local_clustering': True, # local optims serie/parallel
          'global_sequentialization': True, # global optim parallel
          'globalSeq_maxNodes': 40000, # max nodes supported by global sequentialization
          'impact_sequentialization': False, # node impact !!wrong!! optim 
          'transitivity_red': True, # removes transitive edges
          'sync_separation_rec': False, # recursive dag separation
          'sync_separation_end': False, # fallback dag separation
          'STsubDAG_sequentialization': False, # experimental STsubDAG scheduling
          'STsubDAG_maxNodes': 50, # max nodes for processed STsubDAG
          'fallback': exhaustive_BandB_V2, # after all optims

          'faster': True, # option for brute_force_dag_min_mem
          'timeout': 60, # (sec) option for exhaustive_BandB and brute_force_dag_min_mem
          'minMemUpperBound': 0, # option for exhaustive_BandB and brute_force_dag_min_mem
          # Upper bound of the best memory schedule. If 0 or negative, then it is computed by minMemHeuristic(dag).
          'minMemHeuristic': heuristic_linear_1dot5steps_impact, # option for exhaustive_BandB >= V2 and brute_force_dag_min_mem
          'step': None, 'overlap': 0, # options for exhaustive_BandB <= V2ext (suboptimal), step[=None] > overlap
          'optimDepth': 0, # option for exhaustive_BandB >= V2ext, risk of space explosion if 0
          'optimKeys': 1e5, # option for exhaustive_BandB >= V2ext, complimentary to optimDepth
          'computeBestSrt':False, # option for exhaustive_BandB <= V2ext,
          # old slow version of exhaustive_BandB_V4epiDominant for ST dag only
          'ensureSPdag': True, # option for memdagSPschedule, suboptimal result if not SPdag
          'convertTmpMemInc': True # option for memdagSPschedule (required if local_clustering or PBC model)
}


if __name__ == '__main__':


    ################ TESTS UTILS ###############

    # qmf = gen_filterbank_fixed(2,1,3,2,3, homogeneous=False)
    # qmf_exp = sdfToDAG(qmf, nbFiringsObj=0,
    #                    optimalRepetition=True,
    #                    autoconcurrency=True)
    # writeNXdagToDOT(qmf_exp, 'qmf23_depth5_fxd_htg_exp400.dot', onlyMemdagInfo=True)
    # plotDAG(qmf_exp, name="DAG", binaryColors=True, showNow=True, subset_key=None, metric_key=None)
    # ## dot -Grankdir=LR -Tsvg -o qmf23_depth3_fxd_htg_exp.svg qmf23_depth3_fxd_htg_exp.dot
    # dag = generateSeparableDAG(nbBranchs=3, nbChainNodes=0, offsetNewNodes=0, noSTdag=True)
    # set_alphabetical_node_names(dag)
    # plotDAG(dag, name="DAG", binaryColors=True, showNow=True, subset_key=None, metric_key=None)

    ################ DAG_MEM_MIN_UNICPRE ########

    
    #expeRandom()
    #expeRasta()
    #expeSatellite()
    #expeTreeAntiSymChainMemdag()
    #expePascalSP()
    #expeCounterExamplePascalSP()
    #expeSeparable()
    #expeSPdagLN()
    #expeTransfoSDF()
    #expeChainCompaction()

    # expeAllOptimsSDF3file(filePath='./sdf3_inputs/satellite_adfg.xml', nbFiringsObj=0,
    #                       pegLocalPeak=True, optimalRepetition=False, autoconcurrency=False,
    #                       assertPeak=True, showTermDAG=False, **KWargs)
    # expeAllOptimsSDFgraph(gen_filterbank_fixed(5,1,3,2,3, homogeneous=False,
    #                                            setNames=True, setSymNodes=False),
    #                       nbFiringsObj=3200, pegLocalPeak=False,
    #                       nodeBestScoreFunc=getNodeBestScoreOnly,
    #                       optimalRepetition=False, autoconcurrency=False,
    #                       assertPeak=True, showTermDAG=True, **KWargs)
    # expeAllOptimsSDFgraph(gen_filterbank(2,2,5,3,5), nbFiringsObj=0,
    #                       pegLocalPeak=True, optimalRepetition=False, autoconcurrency=False,
    #                       assertPeak=True, showTermDAG=True, **KWargs)
    # expeAllOptimsSDFgraph(badSDFfiringReduction(), nbFiringsObj=0,
    #                       pegLocalPeak=True, optimalRepetition=False, autoconcurrency=False,
    #                       assertPeak=True, showTermDAG=False, **KWargs)
    # for (dag,offset) in generator_tagada_dag(n=100, pratio=70, fj=False, maxEdgeMem=100, maxNodeMem=100, nbDAG=10, seed=SEED):
    #     expeAllOptimsDAG(dag, localPeakFun=None, assertPeak=True, showTermDAG=False, **KWargs)


    # expeAllOptimsDAG(parallelDag1(14), localPeakFun=None, assertPeak=True, showTermDAG=False, **KWargs)
    
    #expeNoReduceAllOptim()
    #expeSTsubDAG()
    #expeSTsubDAGafterComp()
    #expeMemSumCounterExample(orCompactedMemSrt=True)
    #expeMemComposCounterExample()
    #expeCheckBandB()
    #expeCheckBandB_Vplus(algBandBplus=exhaustive_BandB_V3epiDual)
    #rec_optims_dag_min_mem(generateBadBandB_V2extSchedGraph(), **KWargs)
    # schedRes = rec_optims_dag_min_mem(generateIrreducibleGraph_wSTsubDAG(), **KWargs)
    # print(schedRes)
    # schedRes = exhaustive_BandB_V3epiDual(generateIrreducibleGraph_wSTsubDAG())
    # print(schedRes)
    # schedRes = exhaustive_BandB_V4epiDominant(generateIrreducibleGraph_wSTsubDAG())
    # print(schedRes)    
    #expeScheduleSortingBottom()
    #expeWorthComposition()

    #plotSDFparetoPEG(maxFirings=3200, minRatioFiringsMore=0.01)
    #plotPartialBandB(dStep=1,dOverlap=1)
    #plotComplexityDAGsolving()
    #plotRatioOpt()
    #plotRatioRed()
    #printLCTES23_Tables123(cmpMemdag=False)
    #printLCTES23_Table3()
    printLCTES23ext_PEGtables()
    #showIrreduciblePEG()
    

#fd.close()
